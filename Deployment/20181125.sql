ALTER TABLE `food_portions` ADD `value` FLOAT(10,2) NULL DEFAULT NULL AFTER `portion`;
update `food_portions` set value = portion;