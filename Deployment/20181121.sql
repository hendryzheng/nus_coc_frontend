ALTER TABLE `daily_log` ADD `plant_randomize` VARCHAR(500) NULL AFTER `completed`;
UPDATE `daily_log` set plant_randomize = 'tomato' where plant_randomize is null and day_status = 4;