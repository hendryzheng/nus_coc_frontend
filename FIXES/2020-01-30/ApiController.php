<?php
// define('IV_SIZE', mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
header("Access-Control-Allow-Origin: *");
class ApiController extends Controller {

	public $layout = '//layouts/column2';
	public $limit_per_page = 20;
	public $array_png = array(
		'Shower/Washup' => 'shower-washup.png',
		'Travelling' => 'travelling.png',
		'Eat and Drink' => 'eat-and-drink.png',
		'Nap/Sleep' => 'nap-bed.png',
		'Sitting Activities' => 'sitting.png',
		'Active Activities' => 'active.png'
	);
	public $array_activity_location = array(
			'Shower/Washup' => 'Indoor',
			'Travelling' => '',
			'Eat and Drink' => 'Indoor',
			'Nap/Sleep' => 'Indoor',
			'Sitting Activities' => 'Indoor',
			'Active Activities' => 'Outdoor'
	);

	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules() {
		return array (
				array (
						'allow', // allow all users to perform 'index' and 'view' actions
						'actions' => array (
								'login',
								'updateKid',
								'getKid',
								'tokenRegistration',
								'updateActivity',
								'updateSpecificActivity',
								'createActivity',
								'getActivity',
								'getSummary',
								'completeDailyLog',
								'getFoodMain',
								'getDrinks',
								'getFruits',
								'getDeserts',
								'searchFood',
								'export',
								'updatePlant',
								'getReport',
								'patchDailyLogSleeping',
								'uploadReport',
								'deleteActivity',
								'checkDateExist',
								'deleteLog'
								
						),
						'users' => array (
								'*' 
						) 
				),
				array (
						'deny', // deny all users
						'users' => array (
								'*' 
						) 
				) 
		);
	}
	
	public function actionDeleteLog(){
		$id = $_POST['daily_log_id'];
		DailyLog::model()->deleteByPk($id);
		JsonHelper::json_ok();
	}
	
	public function actionCheckDateExist(){
		$yesterday = $_POST['yesterday'];
		$today = $_POST['today'];
		$user_kid_id = $_POST['kid_id'];
		$ytd_check = Yii::app()->db->createCommand()
					->select('*')
					->from('daily_log')
					->where('user_kid_id = '.$user_kid_id)
					->andWhere("date_log = '".$yesterday."'")
					->queryRow();
		$today_check = Yii::app()->db->createCommand()
					->select('*')
					->from('daily_log')
					->where('user_kid_id = '.$user_kid_id)
					->andWhere("date_log = '".$today."'")
					->queryRow();
		
		$arr = array(
			'yesterday' => empty($ytd_check) ? false : true,
			'today' => empty($today_check) ? false: true
		);
		
		JsonHelper::json_return_ok($arr);
		
	}
	
	public function actionPatchDailyLogSleeping(){
		$daily_log = Yii::app()->db->createCommand()
					->select('*')
					->from('daily_log')
					->where('sleeping_done = 1')
					->queryAll();
		foreach($daily_log as $row){
			$date = DateTime::createFromFormat('Y-m-d', $row['date_log']);
			$date->modify('-1 day');
			$sleeping_time = $date->format('Y-m-d')." ".$row['time_sleep_hr1'].$row['time_sleep_hr2'].":".$row['time_sleep_minute1'].$row['time_sleep_minute2']." ".$row['time_sleep_am'];
			$time_sleep = date("Y-m-d H:i:s",strtotime($sleeping_time));
			
			$wakeup_time = $row['date_log']." ".$row['time_wakeup_hr1'].$row['time_wakeup_hr2'].":".$row['time_wakeup_minute1'].$row['time_wakeup_minute2']." ".$row['time_wakeup_am'];
			$time_wakeup = date("Y-m-d H:i:s",strtotime($wakeup_time));
			
			if(strtotime($time_sleep) > strtotime('2000-01-01') && strtotime($time_wakeup) > strtotime('2000-01-01')){
				DailyLog::model()->updateByPk($row['id'], array('sleeping_time'=>$time_sleep,'wakeup_time'=>$time_wakeup));
				
			}
			
		}
	}
	
	public function actionDeleteActivity(){
		$id = $_POST['id'];
		Activities::model()->deleteByPk($id);
		JsonHelper::json_ok();
	}
	
	public function actionUploadReport(){
		header("Access-Control-Allow-Origin: *");
		
		
		if(!empty($_POST['file'])) {
			$img = $_POST['file']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
			$img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$fileName = "report-" . time() . ".png";
			file_put_contents(URLHelper::getRootAdminPath() . DIRECTORY_SEPARATOR . 'uploads/'.$fileName, $data);
			JsonHelper::json_return_ok(array(
					'imageUrl'=>URLHelper::getBackendBaseUrl().'/uploads/'.$fileName,
					'message'=>'Report uploaded successfully'
			));
			exit;
			
		} else {
			throw new Exception("no data");
		}
	}
	
	public function actionGetReport(){
		$id = $_POST['kid_id'];
		$pa = 0;
		$pa_msg = '';
		$outdoor_time = 0;
		$outdoor_msg = '';
		$screen_viewing = 0;
		$screen_viewing_msg = '';
		$sleeping_time = 0;
		$sleeping_msg = '';
		
		//FOOD REPORT
		$fruits_consume = 0;
		$fruits_msg = '';
		
		$vegetables_consume = 0;
		$vegetables_msg = '';
		
		$dairy_consume = 0;
		$dairy_msg = '';
		
		$wholegrains_consume = 0;
		$wholegrains_msg = '';
		
		$ssb_consume = 0;
		$ssb_type = '';
		$ssb_msg = '';
		
		$screen_activity = array(
			'phone_tablet' => 0,
			'watch_tv' => 0,
			'video_games' => 0,
			'computer' => 0
		);
		$this->actionPatchDailyLogSleeping();
		
		$daily_logs = Yii::app()->db->createCommand()
					->select('TIMESTAMPDIFF(MINUTE,sleeping_time,wakeup_time) as sleep_duration')
					->from('daily_log')
					->where('user_kid_id='.$id)
					->andWhere('completed = 1')
					->queryAll();
		
		foreach($daily_logs as $row){
			if(!empty($row['sleep_duration'])){
				$sleeping_time += $row['sleep_duration'];
			}
		}
		
		$exports = Yii::app()->db->createCommand()
		->select("TIMESTAMPDIFF(MINUTE,t3.start_date_time,t3.complete_date_time) as duration, IF(t3.dual_activity_id > 0, 'Yes', 'No') as is_dual, t1.day_status, t1.last_login_date, t1.username, t1.character_type, t3.start_date_time, t3.complete_date_time, t3.activity_type, t3.selected_activity_display, t4.activity_type as concurrent_activity_type, t4.selected_activity_display as subconcurrent_activity, '-' as portion_size, '-' as intensity_activity, t2.plant_randomize as plant, t3.location, t3.json as json")
		->from('user_kid t1')
		->join('daily_log t2', 't1.id = t2.user_kid_id')
		->join('activities t3', 't3.daily_log_id = t2.id')
		->leftJoin('activities t4', 't4.id = t3.dual_activity_id')
		->where('t3.complete = 1')
		->andWhere('t1.id = '.$id)
		->having('duration > 0')
		->order('t1.id, t2.date_log, t3.start_date_time')
		->queryAll();
// 		echo '<pre>';
// 		print_r($exports);exit;
		foreach($exports as $key => $val){
			if($val['activity_type'] == 'Active Activities'){
				$updatePa = false;
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
					if(!empty($json_decode['intensity'])){
						$exports[$key]['intensity_activity'] = $json_decode['intensity'];
						if($exports[$key]['intensity_activity'] == 'Quite Tiring' || $exports[$key]['intensity_activity'] == 'Very Tiring'){
							$updatePa = true;
						}
					}
				}
				if($updatePa){
					$pa += $val['duration'];
				}
			}
// 			if($val['selected_activity_display'] == 'Walk' || $val['selected_activity_display'] == 'Cycle'){
// 				$pa += $val['duration'];
// 			}
			if($val['location'] == 'Outdoor'){
				$outdoor_time += $val['duration'];
			}
			if($val['selected_activity_display'] == 'Using your phone/tablet' || $val['selected_activity_display'] == 'Watching TV' || $val['selected_activity_display'] == 'Playing Video Games' || $val['selected_activity_display'] == 'Using the Computer/Laptop'){
				if($val['selected_activity_display'] == 'Using your phone/tablet'){
					$screen_activity['phone_tablet'] += $val['duration'];
				}
				if($val['selected_activity_display'] == 'Watching TV'){
					$screen_activity['watch_tv'] += $val['duration'];
				}
				if($val['selected_activity_display'] == 'Playing Video Games'){
					$screen_activity['video_games'] += $val['duration'];
				}
				if($val['selected_activity_display'] == 'Using the Computer/Laptop'){
					$screen_activity['computer'] += $val['duration'];
				}
				
				
				$screen_viewing += $val['duration'];
			}
			if($val['activity_type'] == 'Nap/Sleep'){
				$sleeping_time += $val['duration'];
			}
			if($val['activity_type'] == 'Eat and Drink'){
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
					$portion = array();
					if(!empty($json_decode['selected_option'])){
						$selected_option = $json_decode['selected_option'];
						if(is_array($selected_option)){
							foreach($selected_option as $r){
								
								// FRUITS $r['food_categories_id'] == '8'
								$arr = array(7,47,48,49,50,51,52,114);
								
								if(in_array($r['id'], $arr)){
									if(!empty($r['portions'])){
										foreach($r['portions'] as $p){
											if($p['selected'] == '1'){
												$foodPortion = FoodPortions::model()->findByPk($p['id']);
												$fruits_consume += $foodPortion->value;
											}
										}
									}
								}
								
								// VEGETABLES $r['food_categories_id'] == '15'
								$arr = array(90,94,112,113,99,111,100);
								if(in_array($r['id'], $arr)){
									if(!empty($r['portions'])){
										foreach($r['portions'] as $p){
											if($p['selected'] == '1'){
												$foodPortion = FoodPortions::model()->findByPk($p['id']);
												$vegetables_consume += $foodPortion->value;
											}
										}
									}
								}
								
								// WHOLEGRAINS: Wholemeal bread,Chapati,Oats,Brown rice,Mixed white and brown rice
								$arr = array(23,17,25,78,82);
								if(in_array($r['id'], $arr)){
									if(!empty($r['portions'])){
										foreach($r['portions'] as $p){
											if($p['selected'] == '1'){
												$foodPortion = FoodPortions::model()->findByPk($p['id']);
												$wholegrains_consume += $foodPortion->value;
											}
										}
									}
								}
								
								// DAIRY $r['food_categories_id'] == '16'
								// 12,104,105,27,41,106
								$arr = array(12,27,41,104,105);
								if(in_array($r['id'], $arr)){
									if(!empty($r['portions'])){
										foreach($r['portions'] as $p){
											if($p['selected'] == '1'){
												$foodPortion = FoodPortions::model()->findByPk($p['id']);
												$dairy_consume += $foodPortion->value;
											}
										}
									}
								}
								
								// SSB
								$arr = array(105,117,106,13,14,115,116,5,4);
								if(in_array($r['id'], $arr)){
									if(!empty($r['portions'])){
										foreach($r['portions'] as $p){
											if($p['selected'] == '1'){
												$foodPortion = FoodPortions::model()->findByPk($p['id']);
												$ssb_consume += $foodPortion->value;
											}
										}
									}
								}
								
								
							}
						}
						$exports[$key]['portion_size'] = json_encode($portion);
					}
				}
			}
		}
		$pa = round($pa/4);
		if($pa >= 60){
			$pa_msg = 'FANTASTIC! You’re amazing! Continue to play sports and exercise regularly for at least 60 minutes daily!';
		}else if($pa >= 40 && $pa < 60){
			$pa_msg = 'You’re almost there! Aim for at least 60 minutes daily of physical activity such as playing sports and exercising. I know you can do it!';
		}else{
			$pa_msg = 'Keep on trying! You should aim for at least 60 minutes daily of physical activity such as playing sports and exercising. I know you can do it! Don’t give up!';
		}
		
		$outdoor_time = round($outdoor_time / 4);
		if($outdoor_time >= 180){	
			$outdoor_msg = 'Great job! I’m so proud of you! You should continue to spend at least 3 hours daily outdoor.';
		}else if($outdoor_time > 120 && $outdoor_time < 180){
			$outdoor_msg = 'Just a bit more! Aim to spend at least 3 hours daily outdoor. Keep going!';
		}else{
			$outdoor_msg = 'You should aim to have at least 3 hours daily doing outdoor activities. Keep going! You can achieve!';
		}
		
		$screen_viewing = round($screen_viewing / 4 / 60,1, PHP_ROUND_HALF_UP);
		arsort($screen_activity);
		$key_of_max = key($screen_activity);
		
		if($screen_viewing < 2){
			$screen_viewing_msg = 'TERRIFIC! Super-duper proud of you! You should continue to spend less time on the screen.';
		}else if($screen_viewing > 2 && $screen_viewing < 4){
			$screen_viewing_msg = 'Almost there! Aim to spend less time on the screen. Keep going!';
		}else{
			$screen_viewing_msg = 'Your should spend less time on the screen. Aim for less than 2 hours of screen time daily. Persevere! I believe in you!!';
		}
		$sleeping_time = round($sleeping_time/60,2)/4;
		
		if($sleeping_time >= 9){
			$sleeping_msg = 'That’s the way! AMAZING! You should sleep at least 9 hours daily.';
		}else{
			$sleeping_msg = 'Strive to have at least 9 hours of sleep daily. Come on! You can do it!';
		}
		
		$fruits_consume = $fruits_consume/4;
		if($fruits_consume >= 2){
			$fruits_msg = 'Great work! Let’s continue eating fruits for a strong and healthy body!';
		}else if($fruits_consume >=1 && $fruits_consume < 2){
			$fruits_msg = 'Almost there! Let’s try having an extra serving of fruit to get more vitamins! Keep going!';
		}else{
			$fruits_msg = 'Fruits give us vitamins to stay strong and healthy. Let’s try having an apple or banana after lunch and dinner to get our vitamins! Remember, small changes make a difference!';
		}
		
		$vegetables_consume = $vegetables_consume/4;
		if($vegetables_consume >= 2){
			$vegetables_msg = 'Great work! Let’s continue eating our veggies for a strong and healthy body!';
		}else if($vegetables_consume >=1 && $vegetables_consume< 2){
			$vegetables_msg = 'Almost there! Let’s try having more veggies! Ask for more vegetables during recess or dinner. Keep going!';
		}else{
			$vegetables_msg = 'Veggies, especially dark green leafy ones give us lots of vitamins and minerals to stay healthy! Let’s ask for veggies during recess and dinner, or swap French fries for corn or salads! You can do it!*';
		}
		
		$dairy_consume = $dairy_consume/4;
		if($dairy_consume >= 1){
			$dairy_msg = 'Well done! Keep it up and continue consuming your dairy products such as milk, cheese, and yogurt to have strong, healthy bones and teeth!';
		}else if($dairy_consume >= 0.5 && $dairy_consume< 1){
			$dairy_msg = 'Almost there! Let’s try adding a little more dairy, such as having a cheese sandwich or yogurt for a snack! You can do it!';
		}else{
			$dairy_msg = 'We need calcium for strong bones and teeth! Let’s try having 1 cup of milk for breakfast and before bed, or a cheese sandwich or yogurt for a snack! Remember, small changes make a difference!';
		}
		
		$wholegrains_consume = $wholegrains_consume/4;
		if($wholegrains_consume > 0){
			$wholegrains_msg = 'Continue choosing wholegrains daily, such as wholemeal bread, wholegrain cereals, biscuits, and pasta and brown rice daily!';
		}else{
			$wholegrains_msg = 'Let’s try having wholemeal bread or cereal for breakfast, and brown rice for lunch and dinner for a start!';
		}
		
		$ssb_consume = $ssb_consume/4;
		if($ssb_consume > 2){
			$ssb_type = 'High';
			$ssb_msg = 'Did you know that flavoured milk, cultured drinks, and even sports drinks often contain hidden sugars? Choose plain milk and hydrate with plain water instead of sweet drinks!';
		}else if($ssb_consume > 1 && $ssb_consume <= 2){
			$ssb_type = 'Medium';
			$ssb_msg = 'Did you know that flavoured milk, cultured drinks, and even sports drinks often contain hidden sugars? Choose plain milk and hydrate with plain water instead of sweet drinks!';
		}else{
			$ssb_type = 'Low';
			$ssb_msg = 'Excellent! Keep it up, and continue choosing plain water and unsweetened drinks!';
		}
		
		$return = array(
			'pa' => $pa,
			'pa_msg' => $pa_msg,
			'outdoor' => $outdoor_time,
			'outdoor_msg' => $outdoor_msg,
			'screen_viewing' => $screen_viewing,
			'screen_viewing_msg' => $screen_viewing_msg,
			'sleeping' => $sleeping_time,
			'sleeping_msg' => $sleeping_msg,
			'fruits' => $fruits_consume,
			'fruits_msg' => $fruits_msg,
			'vegetables' => $vegetables_consume,
			'vegetables_msg' => $vegetables_msg,
			'wholegrains' => $wholegrains_consume,
			'wholegrains_msg' => $wholegrains_msg,
			'dairy' => $dairy_consume,
			'dairy_msg' => $dairy_msg,
			'ssb' => $ssb_consume,
			'ssb_type' => $ssb_type,
			'ssb_msg' => $ssb_msg
		);
		JsonHelper::json_return_ok($return);
		
	}
	
	public function actionUpdatePlant(){
		DailyLog::model()->updateAll(array('plant_randomize'=>$_POST['plant']),'day_status=4 and user_kid_id='.$_POST['kid_id']);
		JsonHelper::json_ok();
	}
	
	public function actionSearchFood(){
		$path= Yii::app()->getBaseUrl(true);
		$food_type = $_POST['food_type'];
		$cat_id = $_POST['cat_id'];
		$keyword = $_POST['keyword'];
		if(!empty($cat_id)){
			$result = Yii::app()->db->createCommand()
			->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
			->from('food')
			->where('food_categories_id = :cat_id', array('cat_id'=>$cat_id))
			->andWhere('id in (select food_id from food_portions)')
			->andWhere("food_name like '%".$keyword."%'")
			->order('sequence asc')
			->queryAll();
		}else{
			$result = Yii::app()->db->createCommand()
			->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
			->from('food')
			->where('food_categories_id in (select id from food_categories where food_type = '.$food_type.')')
			->andWhere('id in (select food_id from food_portions)')
			->andWhere("food_name like '%".$keyword."%'")
			->order('sequence asc')
			->queryAll();
		}
		
		$result = Yii::app()->db->createCommand()
		->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
		->from('food')
		->where("food_name like '%".$keyword."%'")
		->order('sequence asc')
		->queryAll();
		
		foreach($result as $k => $v){
			$portions = Yii::app()->db->createCommand()
			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
			->from('food_portions')
			->where('food_id = :id',array(':id'=>$v['id']))
			->queryAll();
			$result[$k]['portions'] = $portions;
		}
		JsonHelper::json_return_ok($result);
	}
	
	public function actionGetFoodMain(){
		$path= Yii::app()->getBaseUrl(true);
		$foodCategories = Yii::app()->db->createCommand()
						  ->select('id, food_type, name')
						  ->from('food_categories')
						  ->order('food_type asc, name asc')
						  ->queryAll();
		$foodMain = Yii::app()->db->createCommand()
					->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
					->from('food')
					->where('food_categories_id in (select id from food_categories where food_type = 1)')
					->andWhere('id in (select food_id from food_portions)')
					->order('sequence asc')
					->queryAll();
		
		$foodMainAll = Yii::app()->db->createCommand()
					->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
					->from('food')
					->where('id in (select food_id from food_portions)')
					->order('sequence asc')
					->queryAll();
		foreach($foodCategories as $key => $val){
			$temp_food = Yii::app()->db->createCommand()
						->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
						->from('food')
						->where('food_categories_id = :id',array(':id'=>$val['id']))
						->order('sequence asc')
						->queryAll();
			foreach($temp_food as $k => $v){
				$portions = Yii::app()->db->createCommand()
							->select('id, portion, measurement, portion_image, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
							->from('food_portions')
							->where('food_id = :id',array(':id'=>$v['id']))
							->queryAll();
				$temp_food[$k]['portions'] = $portions;
			}
			$foodCategories[$key]['food_list'] = $temp_food;
		}
		foreach($foodMain as $k => $v){
			$portions = Yii::app()->db->createCommand()
			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
			->from('food_portions')
			->where('food_id = :id',array(':id'=>$v['id']))
			->queryAll();
			$foodMain[$k]['portions'] = $portions;
		}
		
// 		foreach($foodMainAll as $k => $v){
// 			$portions = Yii::app()->db->createCommand()
// 			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
// 			->from('food_portions')
// 			->where('food_id = :id',array(':id'=>$v['id']))
// 			->queryAll();
// 			$foodMainAll[$k]['portions'] = $portions;
// 		}
// 		$temp = array(
// 				'id' => 0,
// 				'food_type' =>0,
// 				'name' => 'All',
// 				'food_list'=>$foodMainAll
// 		);
// 		array_unshift($foodCategories, $temp);
		
		
		$res = array(
			'categories' => $foodCategories,
			'main' => $foodMain
		);
		JsonHelper::json_return_ok($res);
	}
	
	public function actionGetDrinks(){
		$path= Yii::app()->getBaseUrl(true);
		$foodMain = Yii::app()->db->createCommand()
		->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
		->from('food')
		->where('food_categories_id in (select id from food_categories where food_type = 2)')
		->andWhere('id in (select food_id from food_portions)')
		->order('sequence asc')
		->queryAll();
		foreach($foodMain as $k => $v){
			$portions = Yii::app()->db->createCommand()
			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
			->from('food_portions')
			->where('food_id = :id',array(':id'=>$v['id']))
			->queryAll();
			$foodMain[$k]['portions'] = $portions;
		}
		JsonHelper::json_return_ok($foodMain);
	}
	
	public function actionGetFruits(){
		$path= Yii::app()->getBaseUrl(true);
		$foodMain = Yii::app()->db->createCommand()
		->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
		->from('food')
		->where('food_categories_id in (select id from food_categories where food_type = 3)')
		->andWhere('id in (select food_id from food_portions)')
		->order('sequence asc')
		->queryAll();
		foreach($foodMain as $k => $v){
			$portions = Yii::app()->db->createCommand()
			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
			->from('food_portions')
			->where('food_id = :id',array(':id'=>$v['id']))
			->queryAll();
			$foodMain[$k]['portions'] = $portions;
		}
		JsonHelper::json_return_ok($foodMain);
	}
	
	public function actionGetDeserts(){
		$path= Yii::app()->getBaseUrl(true);
		$foodMain = Yii::app()->db->createCommand()
		->select('id, food_name, food_categories_id, concat(\''.$path.'\',image) as image_path')
		->from('food')
		->where('food_categories_id in (select id from food_categories where food_type = 4)')
		->andWhere('id in (select food_id from food_portions)')
		->order('sequence asc')
		->queryAll();
		foreach($foodMain as $k => $v){
			$portions = Yii::app()->db->createCommand()
			->select('id, portion, measurement, concat(\''.$path.'\',portion_image) as portion_image_path, 0 as selected')
			->from('food_portions')
			->where('food_id = :id',array(':id'=>$v['id']))
			->queryAll();
			$foodMain[$k]['portions'] = $portions;
		}
		JsonHelper::json_return_ok($foodMain);
	}
	
	public function actionGetSummary(){
		$daily_log_id = $_POST['daily_log_id'];
		$activities = Yii::app()->db->createCommand()
					  ->select("*, DATE_FORMAT(start_date_time,'%h:%i %p') as start_time, DATE_FORMAT(complete_date_time,'%h:%i %p') as end_time")
					  ->from('activities')
					  ->where('daily_log_id=:daily_log_id',array('daily_log_id'=>$daily_log_id))
					  ->andWhere('dual_activity_id is null')
					  ->andWhere('complete = 1')
					  ->order('sequence asc')
					  ->queryAll();
		foreach($activities as $key => $val){
			$dual = Yii::app()->db->createCommand()
					->select("*, DATE_FORMAT(start_date_time,'%h:%i %p') as start_time, DATE_FORMAT(complete_date_time,'%h:%i %p') as end_time")
					->from('activities')
					->where('dual_activity_id =:id',array(':id'=>$val['id']))
					->andWhere('complete = 1')
					->queryRow();
			
			if(!empty($dual)){
				if($dual['activity_type'] == 'Travelling' || $val['activity_type'] == 'Travelling'){
					if(!empty($dual['location'])){
						$dual['location'] = '';
					}
					$activities[$key]['location'] = '';
				}
				$activities[$key]['dual'] = true;
				$activities[$key]['dual_activity'] = $dual;
			}else{
				$activities[$key]['dual'] = false;
			}
		}
		JsonHelper::json_return_ok($activities);
		
	}
	
	public function actionExport(){
		require_once('third_party/PHPExcel.php');
		
		$exports = Yii::app()->db->createCommand()
		->select("IF(t3.dual_activity_id > 0, 'Yes', 'No') as is_dual, t1.day_status, t1.last_login_date, t1.username, t1.character_type, t3.start_date_time, t3.complete_date_time, t3.activity_type, t3.selected_activity_display, t4.activity_type as concurrent_activity_type, t4.selected_activity_display as subconcurrent_activity, '-' as portion_size, '-' as intensity_activity, t2.plant_randomize as plant, t3.location, t3.json as json")
		->from('user_kid t1')
		->join('daily_log t2', 't1.id = t2.user_kid_id')
		->join('activities t3', 't3.daily_log_id = t2.id')
		->leftJoin('activities t4', 't4.id = t3.dual_activity_id')
		->where('t3.complete = 1')
		->order('t1.id, t2.date_log, t3.start_date_time')
		->queryAll();
// 		echo '<pre>';
// 		print_r($exports);exit;

		foreach($exports as $key => $val){
			if($val['activity_type'] == 'Active Activities'){
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
					if(!empty($json_decode['intensity'])){
						$exports[$key]['intensity_activity'] = $json_decode['intensity'];
					}
				}
				
			}
			if($val['activity_type'] == 'Eat and Drink'){
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
// 					echo '<pre>';
// 					print_r($json_decode);exit;
					$portion = array();
					if(!empty($json_decode['selected_option'])){
						$selected_option = $json_decode['selected_option'];
						if(is_array($selected_option)){
							foreach($selected_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1'){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$exports[$key]['portion_size'] = json_encode($portion);
					}
				}
				
			}
			unset($exports[$key]['json']);
		}
		
		
		array_unshift($exports,array(
				'Is Dual',
				'Day Log',
				'Last Login',
				'Username',
				'Character',
				'Start Time',
				'End Time',
				'Activities',
				'Suboption',
				'Concurrent Activity Recorded',
				'Sub-Concurrent Option Activity Recorded',
				'Portion Size',
				'Intensity Activity',
				'Plant',
				'Location'
		));
		
		// create php excel object
		$doc = new PHPExcel();
		
		// set active sheet
		$doc->setActiveSheetIndex(0);
		
		// read data to active sheet
		$doc->getActiveSheet()->fromArray($exports);
		
		//save our workbook as this file name
		$filename = 'exports.xls';
		//mime type
		header('Content-Type: application/vnd.ms-excel');
		//tell browser what's the file name
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		
		$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
		
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		
	}
	
	public function actionCreateActivity(){
		$kid_id = $_POST['kid_id'];
		$activity = new Activities();
		$activity->attributes = $_POST['Activities'];
		$activity->created_date = date("Y-m-d h:i:s");
		if($activity->save()){
			$user_kid = UserKid::model()->findByPk($kid_id);
			$data = JsonHelper::json_arrayObj($user_kid);
			$data['daily_log']= Yii::app()->db->createCommand()
			->select('*, DAYNAME(date_log) as dayname')
			->from('daily_log')
			->where('user_kid_id=:id',array(':id'=>$kid_id))
			->limit('1')
			->order('date_log desc')
			->queryRow();
			$data['last_activity'] = JsonHelper::json_arrayObj($activity);
			JsonHelper::json_return_ok($data);
		}
	}
	
	private function getPng($activity_type){
		foreach($this->array_png as $key => $val){
			if($activity_type == $key){
				return $val;
			}
		}
	}
	
	private function getLocation($activity_type){
		foreach($this->array_activity_location as $key => $val){
			if($activity_type == $key){
				return $val;
			}
		}
	}
	
	public function actionGetActivity($id){
		$activity = Activities::model()->findByPk($id);
		$activity_arr = JsonHelper::json_arrayObj($activity);
		if(!empty($activity_arr['dual_activity_id'])){
			$dual = Activities::model()->findByPk($activity_arr['dual_activity_id']);
			$activity_arr['dual'] = JsonHelper::json_arrayObj($dual);
		}
		JsonHelper::json_return_ok($activity_arr);
	}
	
	public function actionUpdateSpecificActivity($id){
		$activity = Activities::model()->findByPk($id);
		$activity->attributes = $_POST['Activities'];
		$activity->update();
		JsonHelper::json_ok();
	}
	
	public function actionUpdateActivity(){
		$kid_id = $_POST['kid_id'];
		if(!empty($_POST['new'])){
			$command = Yii::app()->db->createCommand();
			$command->delete('activities', 'daily_log_id=:id and complete=0', array(':id'=>$_POST['Activities']['daily_log_id']));
		}
		$checkPrevious = Yii::app()->db->createCommand()
						->select('*')
						->from('activities')
						->where('user_kid_id = :kid_id', array(':kid_id'=>$kid_id))
						->andWhere('complete = 1')
						->order('sequence desc')
						->queryRow();
		
		$checkExist = Yii::app()->db->createCommand()
						->select('*')
						->from('activities')
						->where('user_kid_id = :kid_id', array(':kid_id'=>$kid_id))
						->andWhere('complete = 0')
						->order('sequence desc')
						->queryRow();
		
		if(!empty($_POST['dual'])){
			$activity = new Activities();
			$activity->start_date_time = $checkExist['start_date_time'];
			$activity->dual_activity_id = $_POST['activity_id'];
			$activity->daily_log_id = $_POST['daily_log_id'];
			$activity->activity_type = $_POST['dual_activity'];
			$activity->user_kid_id = $_POST['user_kid_id'];
			$activity->sequence = intval($checkExist['sequence']) + 1;
			$activity->complete = 0;
			$activity->icon = $this->getPng($activity->activity_type);
			$activity->location = $this->getLocation($activity->activity_type);
			$activity->created_date = date("Y-m-d h:i:s");
		}else{
			if(empty($checkExist)){
				$activity = new Activities();
				$activity->complete = 0;
				$activity->created_date = date("Y-m-d h:i:s");
				$activity->sequence = intval($checkPrevious['sequence']) + 1;
				
				
			}else{
				$activity = Activities::model()->findByPk($checkExist['id']);
			}
			$activity->attributes = $_POST['Activities'];
			if($activity->isNewRecord){
				$activity->location = $this->getLocation($activity->activity_type);
				$activity->icon = $this->getPng($activity->activity_type);
			}
			if($activity->complete == '1'){
				if(!empty($checkExist['dual_activity_id'])){
					Activities::model()->updateByPk($checkExist['dual_activity_id'], array('complete'=>'1','complete_date_time'=>$activity->complete_date_time));
				}
			}
		}
		
		if(!empty($checkPrevious)){
			if($checkPrevious['activity_type']  === 'Shower/Washup' && $activity->activity_type === 'Shower/Washup'){
				JsonHelper::json_error('You have chosen Shower/Washup before this');
				exit;
			}
		}
		if($activity->save()){
			$user_kid = UserKid::model()->findByPk($kid_id);
			$data = JsonHelper::json_arrayObj($user_kid);
			$data['daily_log']= Yii::app()->db->createCommand()
			->select('*, DAYNAME(date_log) as dayname')
			->from('daily_log')
			->where('id=:id', array(':id'=>$activity->daily_log_id))
			->queryRow();
			$data['last_activity'] = JsonHelper::json_arrayObj($activity);
			JsonHelper::json_return_ok($data);
		}else{
			print_r($activity->getErrors());
		}
		
	}
	
	public function actionLogin() {
// 		TokenHelper::checkToken ();
		$username = $_POST ['username'];
		$password = $_POST ['password'];
		$account = UserKid::model()->findByAttributes(array('username'=>$username));
		
		if (! empty ( $account)) {
			if ($account->password == $password) {
				$customer_data = JsonHelper::json_arrayObj ( $account);
				
				$kid_id = $account->id;
				$user_kid = $customer_data;
				$data = JsonHelper::json_arrayObj($user_kid);
				$data['daily_log']= Yii::app()->db->createCommand()
				->select('*, DAYNAME(date_log) as dayname')
				->from('daily_log')
				->where('user_kid_id=:id',array(':id'=>$kid_id))
				->limit('1')
				->order('date_log desc')
				->queryRow();
				$data['last_activity'] = Yii::app()->db->createCommand()
				->select('*')
				->from('activities')
				->where('user_kid_id = :kid_id', array(':kid_id'=>$kid_id))
				->andWhere('complete = 0')
				->queryRow();
				$account->last_login_date = date("Y-m-d H:i:s");
				$account->update();
				unset($data['password']);
				$data['token'] = md5($account->id.'-'.$account->password);
				JsonHelper::json_return_ok ( $data);
			} else {
				JsonHelper::json_error ( 'Your credentials are incorrect' );
			}
		}else{
			JsonHelper::json_error ( 'Email does not exist' );
		}
	}
	
	private function actionGetRootActivity($id){
		$row = Yii::app()->db->createCommand()
				->select('*')
				->from('activities')
				->where('id = :id', array(':id'=>$id))
				->queryRow();
		if(!empty($row['dual_activity_id'])){
			$this->actionGetRootActivity($row['dual_activity_id']);
		}else{
			return $row;
		}
	}
	
	private function actionCompleteRootActivity($id){
		$row = Yii::app()->db->createCommand()
		->select('*')
		->from('activities')
		->where('id = :id', array(':id'=>$id))
		->queryRow();
		if(!empty($row['dual_activity_id'])){
			return $this->actionGetRootActivity($row['dual_activity_id']);
		}else{
			return $row;
		}
	}
	
	public function actionCompleteDailyLog($id){
		$daily_log_id = $_POST['id'];
		$daily_log_detail = DailyLog::model()->findByPk($daily_log_id);
		$user_kid = UserKid::model()->findByPk($daily_log_detail->user_kid_id);
		DailyLog::model()->updateByPk($daily_log_id, array('completed'=>1));
		$user_kid->day_status = $user_kid->day_status+1;
		$user_kid->update();
		JsonHelper::json_ok();
	}
	
	public function actionGetKid(){
		$kid_id = @$_POST['kid_id'];
		$token = @$_POST['token'];
		if(empty($kid_id) || empty($token)){
			echo JsonHelper::json_error('UNATHORIZED ACCESS');
			exit;
		}
		
		$user_kid = UserKid::model()->findByPk($kid_id);
		
		$compare_token = md5($user_kid->id.'-'.$user_kid->password);
		if($token !== $compare_token){
			echo JsonHelper::json_error('UNATHORIZED ACCESS');
			exit;
		}
		
		if($user_kid->day_status == 0){
			$user_kid->day_status = 1;
		}
		$data = JsonHelper::json_arrayObj($user_kid);
		unset($data['password']);
		$daily_log = Yii::app()->db->createCommand()
					->select('*, DAYNAME(date_log) as dayname')
					->from('daily_log')
					->where('user_kid_id=:id',array(':id'=>$kid_id))
					->andWhere('completed = 0')
					->limit('1')
					->order('date_log desc')
					->queryRow();
		$daily_log_last = Yii::app()->db->createCommand()
					->select('*')
					->from('daily_log')
					->where('user_kid_id=:id',array(':id'=>$kid_id))
					->andWhere('completed = 1 and day_status = 4')
					->limit('1')
					->order('date_log desc')
					->queryRow();
		if(!empty($daily_log_last)){
			$data['plant'] = $daily_log_last['plant_randomize'];
		}
		
		$data['daily_log'] = $daily_log;
		if(!empty($daily_log)){
			$incomplete_activity = Yii::app()->db->createCommand()
			->select('*')
			->from('activities')
			->where('user_kid_id = :kid_id', array(':kid_id'=>$kid_id))
			->andWhere('daily_log_id = '.$daily_log['id'])
			->andWhere('complete = 0')
			->order('sequence desc')
			->queryRow();
			$data['last_completed_activity'] = Yii::app()->db->createCommand()
			->select('*')
			->from('activities')
			->where('user_kid_id = :kid_id', array(':kid_id'=>$kid_id))
			->andWhere('daily_log_id = '.$daily_log['id'])
			->andWhere('complete = 1')
			->order('id desc')
			->queryRow();
			$data['last_incomplete_activity'] = $incomplete_activity;
		}else{
			$data['last_completed_activity'] = false;
			$data['last_incomplete_activity'] = false;
		}
		
		
		
		if(!empty($data['daily_log'])){
			$daily_log_id = $data['daily_log']['id'];
			$check_activities = Yii::app()->db->createCommand()
			->select('*')
			->from('activities')
			->where('daily_log_id = :daily_log_id',array(':daily_log_id'=>$daily_log_id))
			->andWhere('complete_date_time is not null')
			->andWhere("param_display <> ''")
			->andWhere('complete = 1')
			->order('sequence desc')
			->queryRow();
			if(!empty($check_activities)){
				$data['start_date_time'] = $check_activities['complete_date_time'];
				$data['param_display'] = $check_activities['param_display'];
				$data['time'] = $check_activities['param_display'];
				$data['last_end_time'] = date('h:i a', strtotime($data['start_date_time']));
			}else{
				$time = $data['daily_log']['time_wakeup_hr1'].$data['daily_log']['time_wakeup_hr2'].':'.$data['daily_log']['time_wakeup_minute1'].$data['daily_log']['time_wakeup_minute2'].' '.$data['daily_log']['time_wakeup_am'];
				$data['start_date_time'] = date("Y-m-d H:i:s", strtotime($data['daily_log']['date_log'].' '.$time));
				$data['param_display'] = 'Alright, what did you do after waking up at '.$time;
				$data['time'] = $time;
				$data['last_end_time'] = date('h:i a', strtotime($data['start_date_time']));
			}
		}
		
		
		
		if(!empty($incomplete_activity['dual_activity_id'])){
			$root_activity = Yii::app()->db->createCommand()
			->select('*')
			->from('activities')
			->where('id = :id', array(':id'=>$incomplete_activity['dual_activity_id']))
			->queryRow();
			$data['root_activity'] = $root_activity;
		}
		
		JsonHelper::json_return_ok($data);
	}
	
	public function actionUpdateKid(){
// 		TokenHelper::checkToken();
		$kid_id = $_POST['kid_id'];
		$user_kid = UserKid::model()->findByPk($kid_id);
		$data = JsonHelper::json_arrayObj($user_kid);
		if(!empty($_POST['UserKid'])){
			
			$user_kid->attributes = $_POST['UserKid'];
			
			if($user_kid->update()){
				$data = JsonHelper::json_arrayObj($user_kid);
				// 			JsonHelper::json_return_ok($data);
			}
		}
		
		if(!empty($_POST['DailyLog'])){
			$daily_log_attr = $_POST['DailyLog'];
			$check = DailyLog::model()->findByAttributes(array('user_kid_id'=>$daily_log_attr['user_kid_id'],'day_status'=>$user_kid->day_status));
			if(!empty($check)){
				$daily_log = $check;
			}else{
				$daily_log = new DailyLog();
				$daily_log->created_date = date("Y-m-d h:i:s");
				
			}
			$daily_log->attributes = $_POST['DailyLog'];
			if($daily_log->isNewRecord){
				if($user_kid->day_status == 0){
					$user_kid->day_status = 1;
				}
				$user_kid->day_status = $user_kid->day_status;
				$user_kid->update();
				$daily_log->day_status = $user_kid->day_status;
			}
			
			$daily_log->save();
			$data['daily_log'] = JsonHelper::json_arrayObj($daily_log);
		}
		JsonHelper::json_return_ok($data);
	}
	
	
	public function actionTokenRegistration(){
		TokenHelper::checkToken();
		$token_str = $_POST['token'];
		$user_type = $_POST['user_type'];
		$id = $_POST['id'];
		
		$checkToken = Token::model()->findByAttributes(array('token'=>$token_str));
		if(!empty($checkToken)){
			if($user_type === 'agent'){
				$checkToken->agent_id = @$id;
			}else if($user_type === 'pca_member'){
				$checkToken->pca_member_id = @$id;
			}else{
				$checkToken->member_id = @$id;
			}
			$checkToken->update();
		}else{
			$token = new Token();
			$token->token = $token_str;
			if($user_type === 'agent'){
				$token->agent_id = @$id;
			}else if($user_type === 'pca_member'){
				$checkToken->pca_member_id = @$id;
			}else{
				$token->member_id = @$id;
			}
			
			$token->save();
		}
		JsonHelper::json_ok();
		
	}
}
