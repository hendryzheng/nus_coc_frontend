-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2018 at 10:18 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.34-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nus_coc`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `mobile` varchar(500) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1. active, 2.inactive, 3.pending verification',
  `created_date` datetime DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `email`, `password`, `firstname`, `lastname`, `mobile`, `status`, `created_date`, `updated_date`) VALUES
(1, 'hndry92@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Hendry', 'Zheng', '0172932993', 1, '2018-05-27 10:20:06', '2018-05-27 02:31:33'),
(2, 'hendry.zheng', '21232f297a57a5a743894a0e4a801fc3', 'hendry', 'zheng', '0172131663', 1, '2018-05-31 12:26:47', '2018-06-05 15:21:10');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `dual_activity_id` int(11) DEFAULT NULL,
  `daily_log_id` int(11) NOT NULL,
  `user_kid_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `activity_type` varchar(500) NOT NULL,
  `location` varchar(500) DEFAULT NULL,
  `icon` varchar(500) DEFAULT NULL,
  `selected_activity_display` varchar(500) DEFAULT NULL,
  `complete` tinyint(1) NOT NULL DEFAULT '0',
  `param_display` varchar(500) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `complete_date_time` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `json` longtext,
  `created_date` datetime DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `dual_activity_id`, `daily_log_id`, `user_kid_id`, `sequence`, `activity_type`, `location`, `icon`, `selected_activity_display`, `complete`, `param_display`, `start_date_time`, `complete_date_time`, `duration`, `json`, `created_date`, `updated_date`) VALUES
(17, NULL, 3, 8, 1, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nThosai or Idli\n', 1, NULL, '2018-11-03 06:00:00', '2018-11-03 06:05:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":1},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":0},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":1},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-04 02:11:14', '2018-11-04 06:11:33'),
(18, 17, 3, 8, 2, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 06:05 AM', '2018-11-03 06:00:00', '2018-11-03 06:05:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","time":{"time_hr1":"0","time_hr2":"6","time_minute1":"0","time_minute2":"5","time_am":"AM"}}', NULL, '2018-11-04 06:11:24'),
(19, NULL, 3, 8, 3, 'Sitting Activities', 'Indoor', 'sitting.png', 'Reading/Studying', 1, NULL, '2018-11-03 06:05:00', '2018-11-03 06:10:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Reading/Studying","others":""}', '2018-11-04 02:11:47', '2018-11-04 06:12:04'),
(20, 19, 3, 8, 4, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 06:10 AM', '2018-11-03 06:05:00', '2018-11-03 06:10:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":1},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":0},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:10 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:11:51'),
(21, NULL, 3, 8, 5, 'Sitting Activities', 'Indoor', 'sitting.png', 'Tests', 1, NULL, '2018-11-03 06:10:00', '2018-11-03 06:15:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Tests","others":"Tests"}', '2018-11-04 02:12:16', '2018-11-04 06:12:34'),
(22, 21, 3, 8, 6, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'Thosai or Idli\nRoti prata\nBreakfast cereals (e.g. Koko KrunchÂ®, Cornflakes)\n', 1, 'Cool! What did you do after doing those activities at 06:15 AM', '2018-11-03 06:10:00', '2018-11-03 06:15:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":1},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"24","food_name":"Breakfast cereals (e.g. Koko KrunchÂ®, Cornflakes)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8182cda8cc6.jpeg","portions":[{"id":"417","portion":"0.50","measurement":"rice bowl","portion_image_path":null,"selected":0},{"id":"418","portion":"0.75","measurement":"rice bowl","portion_image_path":null,"selected":0},{"id":"419","portion":"1.00","measurement":"rice bowl","portion_image_path":null,"selected":1},{"id":"421","portion":"1.50","measurement":"rice bowls","portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"06:15 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:12:21'),
(23, NULL, 3, 8, 7, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-03 06:15:00', '2018-11-03 06:25:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk"}', '2018-11-04 02:12:44', '2018-11-04 06:13:01'),
(24, 23, 3, 8, 8, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nChapati\nWhite bread\n', 1, 'Cool! What did you do after doing those activities at 06:25 AM', '2018-11-03 06:15:00', '2018-11-03 06:25:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"17","food_name":"Chapati","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817aefc4297.jpeg","portions":[{"id":"51","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b21eaeca.jpeg","selected":0},{"id":"52","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b34d1662.jpeg","selected":1},{"id":"53","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b58a7270.jpeg","selected":0},{"id":"54","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b6e03329.jpeg","selected":0}]},{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":0},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":0},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":1},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"06:25 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:12:49'),
(31, NULL, 3, 8, 9, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 06:30 AM', '2018-11-03 06:25:00', '2018-11-03 06:30:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:30 AM","moment_from":null,"moment_to":null}}', '2018-11-04 02:20:28', '2018-11-04 06:20:28'),
(32, NULL, 3, 8, 10, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, NULL, '2018-11-03 06:30:00', '2018-11-03 06:40:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":""}', '2018-11-04 02:20:39', '2018-11-04 06:20:56'),
(33, 32, 3, 8, 11, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'White bread\nDeep fried fish\nStir-fry or steamed meat\n', 1, 'Cool! What did you do after doing those activities at 06:40 AM', '2018-11-03 06:30:00', '2018-11-03 06:40:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":0},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":0},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":1},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]},{"id":"56","food_name":"Deep fried fish","food_categories_id":"9","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83a6b83a909.jpeg","portions":[{"id":"183","portion":"0.25","measurement":"piece","portion_image":"/uploads/food_portions/5b83a74096de0.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a74096de0.jpeg","selected":0},{"id":"184","portion":"0.50","measurement":"piece","portion_image":"/uploads/food_portions/5b83a756aa2c9.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a756aa2c9.jpeg","selected":0},{"id":"185","portion":"0.75","measurement":"piece","portion_image":"/uploads/food_portions/5b83a78196976.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a78196976.jpeg","selected":1},{"id":"186","portion":"1.00","measurement":"piece","portion_image":"/uploads/food_portions/5b83a7a606099.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a7a606099.jpeg","selected":0}]},{"id":"60","food_name":"Stir-fry or steamed meat","food_categories_id":"9","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83a9a857d3b.jpeg","portions":[{"id":"199","portion":"1.00","measurement":"dessert spoon","portion_image":"/uploads/food_portions/5b83a9d1ef632.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9d1ef632.jpeg","selected":0},{"id":"200","portion":"3.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83a9e8c2080.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9e8c2080.jpeg","selected":0},{"id":"201","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83a9ffd31ce.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9ffd31ce.jpeg","selected":1},{"id":"202","portion":"7.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83aa1746008.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa1746008.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Meat and Fish","location":"Student Care","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"06:40 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:20:43'),
(34, NULL, 3, 8, 12, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Video Games', 1, NULL, '2018-11-03 06:40:00', '2018-11-03 06:45:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Video Games","others":""}', '2018-11-04 02:20:58', '2018-11-04 06:21:10'),
(35, 34, 3, 8, 13, 'Travelling', '', 'travelling.png', 'Cycle', 1, 'Cool! What did you do after reaching your destination at 06:45 AM', '2018-11-03 06:40:00', '2018-11-03 06:45:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle","time":{"time_hr1":"0","time_hr2":"6","time_minute1":"4","time_minute2":"5","time_am":"AM"}}', NULL, '2018-11-04 06:21:02'),
(36, NULL, 3, 8, 14, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 06:50 AM', '2018-11-03 06:45:00', '2018-11-03 06:50:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":0},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":1},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":1},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":0},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"Student Care","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"06:50 AM","moment_from":null,"moment_to":null}}', '2018-11-04 02:21:15', '2018-11-04 06:21:15'),
(37, NULL, 3, 8, 15, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bun or pow\nThosai or Idli\nWholemeal bread\nWhite bread\n', 1, NULL, '2018-11-03 06:50:00', '2018-11-03 06:55:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"16","food_name":"Bun or pow","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bd28430e7916.jpg","portions":[{"id":"443","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"444","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":0},{"id":"445","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"446","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":1}]},{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":0},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":1}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":0},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":1},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]},{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":0},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":1},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":0},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-04 02:21:34', '2018-11-04 06:21:53'),
(38, 37, 3, 8, 16, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, 'Cool! What did you do after doing those activities at 06:55 AM', '2018-11-03 06:50:00', '2018-11-03 06:55:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":"","location":"Student Care","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:55 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:21:46'),
(39, NULL, 3, 8, 17, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Dry noodles (e.g. wanton noodles, mee goreng)\nNoodles with gravy\nFrench fries and hash brown\n', 1, NULL, '2018-11-03 06:55:00', '2018-11-03 07:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"70","food_name":"Dry noodles (e.g. wanton noodles, mee goreng)","food_categories_id":"10","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83af0c5adb5.jpeg","portions":[{"id":"226","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83af4b0d8fe.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83af4b0d8fe.jpeg","selected":0},{"id":"227","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83af59e98c5.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83af59e98c5.jpeg","selected":0},{"id":"228","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83af69cf763.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83af69cf763.jpeg","selected":1},{"id":"229","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b83af858e4a4.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83af858e4a4.jpeg","selected":0}]},{"id":"73","food_name":"Noodles with gravy","food_categories_id":"10","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83b1be157af.jpeg","portions":[{"id":"238","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b1e695c7b.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b1e695c7b.jpeg","selected":0},{"id":"239","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b1f8274bf.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b1f8274bf.jpeg","selected":1},{"id":"240","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b21031422.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b21031422.jpeg","selected":0},{"id":"241","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b83b22562b4d.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b22562b4d.jpeg","selected":0}]},{"id":"45","food_name":"French fries and hash brown","food_categories_id":"7","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82c6f4b7421.jpeg","portions":[{"id":"471","portion":"0.50","measurement":"small packet","portion_image":null,"portion_image_path":null,"selected":0},{"id":"472","portion":"1.00","measurement":"small packet","portion_image":null,"portion_image_path":null,"selected":1},{"id":"473","portion":"1.50","measurement":"small packets","portion_image":null,"portion_image_path":null,"selected":0},{"id":"474","portion":"2.00","measurement":"small packets","portion_image":null,"portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Fast food"}', '2018-11-04 02:21:56', '2018-11-04 06:22:18'),
(40, 39, 3, 8, 18, 'Travelling', '', 'travelling.png', 'Cycle', 1, 'Cool! What did you do after reaching your destination at 07:00 AM', '2018-11-03 06:55:00', '2018-11-03 07:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"0","time_minute2":"0","time_am":"AM"}}', NULL, '2018-11-04 06:22:09'),
(41, NULL, 3, 8, 19, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nChapati\nWhite bread\n', 1, NULL, '2018-11-03 07:00:00', '2018-11-03 07:05:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":0},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":1},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":1},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":0},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"17","food_name":"Chapati","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817aefc4297.jpeg","portions":[{"id":"51","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b21eaeca.jpeg","selected":0},{"id":"52","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b34d1662.jpeg","selected":0},{"id":"53","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b58a7270.jpeg","selected":1},{"id":"54","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b6e03329.jpeg","selected":0}]},{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":0},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":0},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":1},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-04 02:22:27', '2018-11-04 06:22:46'),
(42, 41, 3, 8, 20, 'Travelling', '', 'travelling.png', 'Car/Taxi', 1, 'Cool! What did you do after reaching your destination at 07:05 AM', '2018-11-03 07:00:00', '2018-11-03 07:05:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Car/Taxi","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"0","time_minute2":"5","time_am":"AM"}}', NULL, '2018-11-04 06:22:40'),
(43, NULL, 3, 8, 21, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 07:10 AM', '2018-11-03 07:05:00', '2018-11-03 07:10:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"1","time_minute2":"0","time_am":"AM"}}', '2018-11-04 02:22:49', '2018-11-04 06:22:49'),
(46, NULL, 3, 8, 22, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 07:15 AM', '2018-11-03 07:10:00', '2018-11-03 07:15:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"1","time_minute2":"5","time_am":"AM"}}', '2018-11-04 02:24:17', '2018-11-04 06:24:17'),
(47, NULL, 3, 8, 23, 'Travelling', '', 'travelling.png', 'Cycle', 1, NULL, '2018-11-03 07:15:00', '2018-11-03 07:20:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle"}', '2018-11-04 02:24:29', '2018-11-04 06:24:39'),
(48, 47, 3, 8, 24, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:20 AM?', '2018-11-03 07:15:00', '2018-11-03 07:20:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"2","time_sleep_minute2":"0","time_sleep_am":"AM"}}', NULL, '2018-11-04 06:24:35'),
(49, NULL, 3, 8, 25, 'Travelling', '', 'travelling.png', 'Car/Taxi', 1, NULL, '2018-11-03 07:20:00', '2018-11-03 07:25:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Car/Taxi"}', '2018-11-04 02:24:42', '2018-11-04 06:25:02'),
(50, 49, 3, 8, 26, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 07:25 AM', '2018-11-03 07:20:00', '2018-11-03 07:25:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","form":{"minute1":"","minute2":"","from":"","to":"07:25 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 06:24:57'),
(65, NULL, 3, 8, 27, 'Travelling', '', 'travelling.png', 'Cycle', 1, NULL, '2018-11-03 07:25:00', '2018-11-03 07:40:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle"}', '2018-11-04 03:02:56', '2018-11-04 07:03:04'),
(66, 65, 3, 8, 28, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:40 AM?', '2018-11-03 07:25:00', '2018-11-03 07:40:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"4","time_sleep_minute2":"0","time_sleep_am":"AM"}}', NULL, '2018-11-04 07:02:59'),
(67, NULL, 3, 8, 29, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 07:45 AM', '2018-11-03 07:40:00', '2018-11-03 07:45:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"4","time_minute2":"5","time_am":"AM"}}', '2018-11-04 03:03:05', '2018-11-04 07:03:05'),
(68, NULL, 3, 8, 30, 'Travelling', '', 'travelling.png', 'Car/Taxi', 1, NULL, '2018-11-03 07:45:00', '2018-11-03 07:50:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Car/Taxi"}', '2018-11-04 03:03:15', '2018-11-04 07:03:24'),
(69, 68, 3, 8, 31, 'Sitting Activities', 'Indoor', 'sitting.png', 'Sit and Chat', 1, 'Cool! What did you do after doing those activities at 07:50 AM', '2018-11-03 07:45:00', '2018-11-03 07:50:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Sit and Chat","others":"","form":{"minute1":"","minute2":"","from":"","to":"07:50 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 07:03:18'),
(72, NULL, 3, 8, 32, 'Active Activities', 'Indoor', 'active.png', 'Ball Games', 1, 'Cool! What did you after doing these active activities at 07:55 AM', '2018-11-03 07:50:00', '2018-11-03 07:55:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Ball Games","other_activities":"","intensity":"Quite Tiring","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:55 AM","moment_from":null,"moment_to":null}}', '2018-11-04 03:03:43', '2018-11-04 07:03:43'),
(73, NULL, 3, 8, 33, 'Active Activities', 'Outdoor', 'active.png', 'Cycle/Skate/Scoot', 1, 'Cool! What did you after doing these active activities at 08:00 AM', '2018-11-03 07:55:00', '2018-11-03 08:00:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Cycle/Skate/Scoot","other_activities":"","intensity":"Quite Tiring","location":"Student Care","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"08:00 AM","moment_from":null,"moment_to":null}}', '2018-11-04 03:03:57', '2018-11-04 07:03:57'),
(75, NULL, 3, 8, 34, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 08:05 AM?', '2018-11-03 08:00:00', '2018-11-03 08:05:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"8","time_sleep_minute1":"0","time_sleep_minute2":"5","time_sleep_am":"AM"}}', '2018-11-04 03:05:36', '2018-11-04 07:05:36'),
(76, NULL, 3, 8, 35, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 08:50 AM?', '2018-11-03 08:05:00', '2018-11-03 08:50:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"8","time_shower_minute1":"5","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-04 03:05:44', '2018-11-04 07:05:44'),
(77, NULL, 3, 8, 36, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 09:15 PM?', '2018-11-03 08:50:00', '2018-11-03 21:15:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"9","time_sleep_minute1":"1","time_sleep_minute2":"5","time_sleep_am":"PM"}}', '2018-11-04 03:05:58', '2018-11-04 07:05:58'),
(78, NULL, 3, 8, 37, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 06:00 AM?', '2018-11-03 21:15:00', '2018-11-03 06:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"6","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-04 03:06:08', '2018-11-04 07:06:08'),
(79, NULL, 4, 9, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 06:25 AM?', '2018-11-03 06:00:00', '2018-11-03 06:25:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"6","time_shower_minute1":"2","time_shower_minute2":"5","shower_am":"AM"}}', '2018-11-04 03:18:34', '2018-11-04 07:18:34'),
(80, NULL, 4, 9, 2, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:00 AM?', '2018-11-03 06:25:00', '2018-11-03 07:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-04 03:18:42', '2018-11-04 07:18:42'),
(81, NULL, 5, 10, 1, 'Travelling', '', 'travelling.png', 'Cycle', 1, NULL, '2018-11-03 07:00:00', '2018-11-03 07:10:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle"}', '2018-11-04 07:33:29', '2018-11-04 11:37:01'),
(82, 81, 5, 10, 2, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\n', 1, 'Cool! What did you do after doing those activities at 07:10 AM', '2018-11-03 07:00:00', '2018-11-03 07:10:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","form":{"minute1":"","minute2":"","from":"","to":"07:10 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-04 11:33:48'),
(86, NULL, 6, 9, 3, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:25 AM?', '1970-01-01 07:30:00', '2018-11-03 07:25:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"2","time_sleep_minute2":"5","time_sleep_am":"AM"}}', '2018-11-04 07:46:19', '2018-11-04 11:46:19'),
(87, NULL, 5, 10, 3, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:10 AM?', '2018-11-03 07:10:00', '2018-11-03 07:10:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"1","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-04 07:47:08', '2018-11-04 11:47:08'),
(88, NULL, 1, 1, 1, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:30 AM?', '2018-11-02 08:10:00', '2018-11-02 07:30:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"3","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-04 07:49:48', '2018-11-04 11:49:48'),
(89, NULL, 1, 1, 2, 'Travelling', '', 'travelling.png', 'Cycle', 1, 'Cool! What did you do after reaching your destination at 10:10 AM', '2018-11-02 07:30:00', '2018-11-02 10:10:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"1","time_minute2":"0","time_am":"AM"}}', '2018-11-04 07:56:22', '2018-11-04 11:56:22'),
(90, NULL, 1, 1, 3, 'Travelling', '', 'travelling.png', 'merangkak', 1, 'Cool! What did you do after reaching your destination at 10:15 AM', '2018-11-02 10:10:00', '2018-11-02 10:15:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"merangkak","others":"merangkak","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"1","time_minute2":"5","time_am":"AM"}}', '2018-11-04 07:58:34', '2018-11-04 11:58:34'),
(91, NULL, 7, 8, 38, 'Active Activities', 'Indoor', 'active.png', 'Cycle/Skate/Scoot', 1, 'Cool! What did you after doing these active activities at 06:05 AM', '2018-11-04 06:00:00', '2018-11-04 06:05:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Cycle/Skate/Scoot","other_activities":"","intensity":"Just a Little","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:05 AM","moment_from":null,"moment_to":null}}', '2018-11-05 09:23:45', '2018-11-05 01:23:45'),
(92, NULL, 7, 8, 39, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-04 06:05:00', '2018-11-04 06:10:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-05 09:24:09', '2018-11-05 01:24:20'),
(93, 92, 7, 8, 40, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 06:10 AM?', '2018-11-04 06:05:00', '2018-11-04 06:10:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"6","time_sleep_minute1":"1","time_sleep_minute2":"0","time_sleep_am":"AM"}}', NULL, '2018-11-05 01:24:16');
INSERT INTO `activities` (`id`, `dual_activity_id`, `daily_log_id`, `user_kid_id`, `sequence`, `activity_type`, `location`, `icon`, `selected_activity_display`, `complete`, `param_display`, `start_date_time`, `complete_date_time`, `duration`, `json`, `created_date`, `updated_date`) VALUES
(94, NULL, 7, 8, 41, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Yellow, orange, red vegetables (e.g. corn, carrot, tomato)\nPumpkin \nLentils and other beans (e.g. chickpeas, broad beans)\n', 1, 'Cool! What did you do after doing those activities at 06:15 AM', '2018-11-04 06:10:00', '2018-11-04 06:15:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"113","food_name":"Yellow, orange, red vegetables (e.g. corn, carrot, tomato)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97cfbc964d.jpg","portions":[{"id":"386","portion":"3.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97d1b7e95d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97d1b7e95d.jpg","selected":0},{"id":"387","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97dbf1b258.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97dbf1b258.jpg","selected":1},{"id":"388","portion":"7.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97ddd4bc91.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97ddd4bc91.jpg","selected":0},{"id":"389","portion":"10.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97df72479d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97df72479d.jpg","selected":0}]},{"id":"100","food_name":"Pumpkin ","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97cd7582ea.jpg","portions":[{"id":"341","portion":"3.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b841e0e897b1.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b841e0e897b1.jpeg","selected":0},{"id":"342","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b841e56081a4.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b841e56081a4.jpeg","selected":1},{"id":"343","portion":"7.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b841e7cc66c5.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b841e7cc66c5.jpeg","selected":0},{"id":"344","portion":"10.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b841ea671503.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b841ea671503.jpeg","selected":0}]},{"id":"111","food_name":"Lentils and other beans (e.g. chickpeas, broad beans)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97b72d4e91.jpg","portions":[{"id":"391","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc98177bca98.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc98177bca98.jpg","selected":0},{"id":"393","portion":"8.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc98193d92cb.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc98193d92cb.jpg","selected":0},{"id":"395","portion":"10.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc981aa06dbc.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc981aa06dbc.jpg","selected":1},{"id":"397","portion":"15.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc981bbe37ab.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc981bbe37ab.jpg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Vegetables","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:15 AM","moment_from":null,"moment_to":null}}', '2018-11-05 09:26:20', '2018-11-05 01:26:20'),
(95, NULL, 7, 8, 42, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, 'Cool! What did you do after doing those activities at 06:20 AM', '2018-11-04 06:15:00', '2018-11-04 06:20:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:20 AM","moment_from":null,"moment_to":null}}', '2018-11-05 09:27:19', '2018-11-05 01:27:19'),
(96, NULL, 7, 8, 43, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-04 06:20:00', '2018-11-04 06:25:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-05 09:27:52', '2018-11-05 01:28:22'),
(97, 96, 7, 8, 44, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nChapati\nWhite bread\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 06:25 AM', '2018-11-04 06:20:00', '2018-11-04 06:25:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":0},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":1},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"17","food_name":"Chapati","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817aefc4297.jpeg","portions":[{"id":"51","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b21eaeca.jpeg","selected":0},{"id":"52","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b34d1662.jpeg","selected":1},{"id":"53","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b58a7270.jpeg","selected":0},{"id":"54","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817b6e03329.jpeg","selected":0}]},{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":0},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":0},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":1},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":0},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":1},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","form":{"minute1":"","minute2":"","from":"","to":"06:25 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 01:27:56'),
(98, NULL, 7, 8, 45, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Roti prata\nThosai or Idli\nBreakfast cereals (e.g. Koko KrunchÂ®, Cornflakes)\n', 1, NULL, '2018-11-04 06:25:00', '2018-11-04 06:30:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":1},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":0},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":1},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]},{"id":"24","food_name":"Breakfast cereals (e.g. Koko KrunchÂ®, Cornflakes)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8182cda8cc6.jpeg","portions":[{"id":"417","portion":"0.50","measurement":"rice bowl","portion_image_path":null,"selected":0},{"id":"418","portion":"0.75","measurement":"rice bowl","portion_image_path":null,"selected":0},{"id":"419","portion":"1.00","measurement":"rice bowl","portion_image_path":null,"selected":1},{"id":"421","portion":"1.50","measurement":"rice bowls","portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-05 09:28:26', '2018-11-05 01:28:45'),
(99, 98, 7, 8, 46, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 06:30 AM', '2018-11-04 06:25:00', '2018-11-04 06:30:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","time":{"time_hr1":"0","time_hr2":"6","time_minute1":"3","time_minute2":"0","time_am":"AM"}}', NULL, '2018-11-05 01:28:40'),
(100, NULL, 7, 8, 47, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-04 06:30:00', '2018-11-04 06:35:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-05 09:28:50', '2018-11-05 01:29:08'),
(101, 100, 7, 8, 48, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nRoti prata\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 06:35 AM', '2018-11-04 06:30:00', '2018-11-04 06:35:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"18","food_name":"Roti prata","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b817bcebd866.jpeg","portions":[{"id":"55","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817c0ee4973.jpeg","selected":0},{"id":"56","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817ca488d40.jpeg","selected":0},{"id":"57","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cbc780e2.jpeg","selected":1},{"id":"58","portion":"3.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b817cd04d956.jpeg","selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":1},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":0},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","form":{"minute1":"","minute2":"","from":"","to":"06:35 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 01:28:55'),
(102, NULL, 7, 8, 49, 'Travelling', '', 'travelling.png', 'Scooter', 1, 'Cool! What did you do after reaching your destination at 06:40 AM', '2018-11-04 06:35:00', '2018-11-04 06:40:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Scooter","others":"Scooter","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"6","time_minute1":"4","time_minute2":"0","time_am":"AM"}}', '2018-11-05 09:29:25', '2018-11-05 01:29:25'),
(104, NULL, 7, 8, 50, 'Travelling', '', 'travelling.png', 'Cycle', 1, 'Cool! What did you do after reaching your destination at 10:00 AM', '2018-11-04 06:40:00', '2018-11-04 10:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"0","time_minute2":"0","time_am":"AM"}}', '2018-11-05 09:34:01', '2018-11-05 01:34:01'),
(105, NULL, 7, 8, 51, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 12:40 PM?', '2018-11-04 10:00:00', '2018-11-04 12:40:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"1","time_sleep_hr2":"2","time_sleep_minute1":"4","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 09:34:20', '2018-11-05 01:34:20'),
(106, NULL, 7, 8, 52, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 05:00 PM?', '2018-11-04 12:40:00', '2018-11-04 17:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"5","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 09:34:33', '2018-11-05 01:34:33'),
(107, NULL, 7, 8, 53, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 07:00 PM', '2018-11-04 17:00:00', '2018-11-04 19:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 09:34:48', '2018-11-05 01:34:48'),
(108, NULL, 7, 8, 54, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 08:05 PM?', '2018-11-04 19:00:00', '2018-11-04 20:05:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"8","time_shower_minute1":"0","time_shower_minute2":"5","shower_am":"PM"}}', '2018-11-05 09:35:10', '2018-11-05 01:35:10'),
(109, NULL, 7, 8, 55, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 09:00 PM?', '2018-11-04 20:05:00', '2018-11-04 21:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"9","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 09:35:19', '2018-11-05 01:35:19'),
(110, NULL, 8, 9, 4, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 06:10 AM?', '2018-11-04 06:05:00', '2018-11-04 06:10:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"6","time_shower_minute1":"1","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-05 09:37:26', '2018-11-05 01:37:26'),
(112, NULL, 10, 8, 56, 'Active Activities', 'Indoor', 'active.png', 'Cycle/Skate/Scoot', 1, 'Cool! What did you after doing these active activities at 06:55 AM', '2018-11-04 06:00:00', '2018-11-04 18:55:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Cycle/Skate/Scoot","other_activities":"","intensity":"Just a Little","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:55 AM","moment_from":null,"moment_to":null}}', '2018-11-05 10:08:49', '2018-11-05 02:08:49'),
(113, NULL, 10, 8, 57, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 10:00 PM?', '2018-11-04 18:55:00', '2018-11-04 22:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"1","time_sleep_hr2":"0","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 10:09:12', '2018-11-05 02:09:12'),
(115, NULL, 9, 5, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 07:20 AM?', '2018-11-04 07:00:00', '2018-11-04 07:20:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"7","time_shower_minute1":"2","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-05 10:24:49', '2018-11-05 02:24:49'),
(116, NULL, 9, 5, 2, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 08:00 AM', '2018-11-04 07:20:00', '2018-11-04 08:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"8","time_minute1":"0","time_minute2":"0","time_am":"AM"}}', '2018-11-05 10:25:02', '2018-11-05 02:25:02'),
(117, NULL, 9, 5, 3, 'Sitting Activities', 'Indoor', 'sitting.png', 'Reading/Studying', 1, 'Cool! What did you do after doing those activities at 09:00 AM', '2018-11-04 08:00:00', '2018-11-04 09:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Reading/Studying","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 10:25:27', '2018-11-05 02:25:27'),
(118, NULL, 9, 5, 4, 'Active Activities', 'Outdoor', 'active.png', 'Water Activities', 1, 'Cool! What did you after doing these active activities at 11:00 AM', '2018-11-04 09:00:00', '2018-11-04 11:00:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Water Activities","other_activities":"","intensity":"Quite Tiring","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"11:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 10:25:51', '2018-11-05 02:25:51'),
(120, NULL, 9, 5, 5, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, NULL, '2018-11-04 11:00:00', '2018-11-04 12:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":""}', '2018-11-05 10:26:52', '2018-11-05 02:27:40'),
(121, 120, 9, 5, 6, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Chips (e.g. potato chips, keropok)\n', 1, 'Cool! What did you do after doing those activities at 12:00 AM', '2018-11-04 11:00:00', '2018-11-04 12:00:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[{"id":"28","food_name":"Chips (e.g. potato chips, keropok)","food_categories_id":"5","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82415fb0c75.jpeg","portions":[{"id":"89","portion":"0.50","measurement":"small packet","portion_image":"/uploads/food_portions/5b824191146b2.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b824191146b2.jpeg","selected":0},{"id":"90","portion":"0.75","measurement":"small packet","portion_image":"/uploads/food_portions/5b8241a75bddf.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8241a75bddf.jpeg","selected":1},{"id":"91","portion":"1.00","measurement":"small packet","portion_image":"/uploads/food_portions/5b8241e4d5a34.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8241e4d5a34.jpeg","selected":0},{"id":"92","portion":"1.50","measurement":"small packets","portion_image":"/uploads/food_portions/5b82424e0b3d5.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82424e0b3d5.jpeg","selected":0}]}],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Snacks and Desserts","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"12:00 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 02:27:05'),
(124, NULL, 9, 5, 7, 'Travelling', '', 'travelling.png', 'Bus/Train', 1, 'Cool! What did you do after reaching your destination at 01:00 PM', '2018-11-04 12:00:00', '2018-11-04 13:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Bus/Train","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"1","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', '2018-11-05 10:35:48', '2018-11-05 02:35:48'),
(125, NULL, 9, 5, 8, 'Sitting Activities', 'Indoor', 'sitting.png', 'Tuition-Music-Lesson', 1, 'Cool! What did you do after doing those activities at 03:00 PM', '2018-11-04 13:00:00', '2018-11-04 15:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Tuition-Music-Lesson","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"03:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 10:44:49', '2018-11-05 02:44:49'),
(126, NULL, 9, 5, 9, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Noodles in soup (e.g. beehoon soup, mee soto)\nApple or pear\n', 1, 'Cool! What did you do after doing those activities at 04:00 PM', '2018-11-04 15:00:00', '2018-11-04 16:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"72","food_name":"Noodles in soup (e.g. beehoon soup, mee soto)","food_categories_id":"10","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83b054ae6e7.jpeg","portions":[{"id":"234","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b07c6806d.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b07c6806d.jpeg","selected":1},{"id":"235","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b08fb47c9.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b08fb47c9.jpeg","selected":0},{"id":"236","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b0a406a64.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b0a406a64.jpeg","selected":0},{"id":"237","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b83b0d87605b.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b0d87605b.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[{"id":"7","food_name":"Apple or pear","food_categories_id":"8","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80fbf8a48ce.jpg","portions":[{"id":"10","portion":"0.25","measurement":"apple / pear","portion_image":"/uploads/food_portions/5b80fc2f12c07.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fc2f12c07.jpg","selected":0},{"id":"11","portion":"0.50","measurement":"apple / pear","portion_image":"/uploads/food_portions/5b80fc3bb954d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fc3bb954d.jpg","selected":0},{"id":"12","portion":"0.75","measurement":"apple / pear","portion_image":"/uploads/food_portions/5b80fc4ca56d6.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fc4ca56d6.jpg","selected":0},{"id":"13","portion":"1.00","measurement":"apple / pear","portion_image":"/uploads/food_portions/5b80fc59a09d4.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fc59a09d4.jpg","selected":1}]}],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Fruits","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"04:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 10:45:05', '2018-11-05 02:45:05'),
(127, NULL, 9, 5, 10, 'Sitting Activities', 'Indoor', 'sitting.png', 'Reading/Studying', 1, 'Cool! What did you do after doing those activities at 06:00 PM', '2018-11-04 16:00:00', '2018-11-04 18:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Reading/Studying","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 10:45:46', '2018-11-05 02:45:46'),
(128, NULL, 9, 5, 11, 'Sitting Activities', 'Indoor', 'sitting.png', 'Watching TV', 1, 'Cool! What did you do after doing those activities at 07:00 PM', '2018-11-04 18:00:00', '2018-11-04 19:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Watching TV","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 10:46:02', '2018-11-05 02:46:02'),
(129, NULL, 9, 5, 12, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Chicken or duck rice\nCultured drinks (e.g. YakultÂ®, VitagenÂ®)\n', 1, NULL, '2018-11-04 19:00:00', '2018-11-04 20:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"81","food_name":"Chicken or duck rice","food_categories_id":"12","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b840337133f6.jpeg","portions":[{"id":"265","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b840363e1b31.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b840363e1b31.jpeg","selected":1},{"id":"266","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b84038596530.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b84038596530.jpeg","selected":0},{"id":"267","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b84042dcaabf.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b84042dcaabf.jpeg","selected":0},{"id":"268","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b840454dadbe.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b840454dadbe.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[{"id":"5","food_name":"Cultured drinks (e.g. YakultÂ®, VitagenÂ®)","food_categories_id":"1","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b7f8d7badd4c.jpg","portions":[{"id":"422","portion":"0.50","measurement":"bottle","portion_image":null,"portion_image_path":null,"selected":0},{"id":"423","portion":"1.00","measurement":"bottle","portion_image":null,"portion_image_path":null,"selected":1},{"id":"424","portion":"1.50","measurement":"bottle","portion_image":null,"portion_image_path":null,"selected":0},{"id":"425","portion":"2.00","measurement":"bottle","portion_image":null,"portion_image_path":null,"selected":0}]}],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Drinks"}', '2018-11-05 10:46:19', '2018-11-05 02:46:53'),
(130, 129, 9, 5, 13, 'Sitting Activities', 'Indoor', 'sitting.png', 'Watching TV', 1, 'Cool! What did you do after doing those activities at 08:00 PM', '2018-11-04 19:00:00', '2018-11-04 20:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Watching TV","others":"","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"08:00 PM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 02:46:44'),
(131, NULL, 9, 5, 14, 'Sitting Activities', 'Indoor', 'sitting.png', 'Sit and Chat', 1, NULL, '2018-11-04 20:00:00', '2018-11-04 21:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Sit and Chat","others":""}', '2018-11-05 10:46:56', '2018-11-05 02:47:25'),
(132, 131, 9, 5, 15, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Chocolates\n', 1, 'Cool! What did you do after doing those activities at 09:00 PM', '2018-11-04 20:00:00', '2018-11-04 21:00:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[{"id":"29","food_name":"Chocolates","food_categories_id":"5","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82428d3db5a.jpeg","portions":[{"id":"93","portion":"1.00","measurement":"piece / mini bar/ sncak size","portion_image":"/uploads/food_portions/5b8243c619329.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8243c619329.jpeg","selected":0},{"id":"94","portion":"2.00","measurement":"pieces / mini bars/ sncak sizes","portion_image":"/uploads/food_portions/5b8243ee3f0ef.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8243ee3f0ef.jpeg","selected":1},{"id":"95","portion":"3.00","measurement":"pieces / mini bars/ sncak sizes","portion_image":"/uploads/food_portions/5b82444175573.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82444175573.jpeg","selected":0},{"id":"96","portion":"4.00","measurement":"pieces / mini bars/ sncak sizes","portion_image":"/uploads/food_portions/5b82446c63345.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82446c63345.jpeg","selected":0}]}],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Snacks and Desserts","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 PM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 02:47:03'),
(133, NULL, 9, 5, 16, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 09:30 PM?', '2018-11-04 21:00:00', '2018-11-04 21:30:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"9","time_shower_minute1":"3","time_shower_minute2":"0","shower_am":"PM"}}', '2018-11-05 10:47:28', '2018-11-05 02:47:28'),
(134, NULL, 9, 5, 17, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:00 PM?', '2018-11-04 21:30:00', '2018-11-04 19:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 10:47:56', '2018-11-05 02:47:56'),
(135, NULL, 11, 5, 18, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 07:30 AM?', '2018-11-04 07:00:00', '2018-11-04 07:30:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"7","time_shower_minute1":"3","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-05 11:28:15', '2018-11-05 03:28:15'),
(137, NULL, 11, 5, 19, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nWhite bread\n', 1, NULL, '2018-11-04 07:30:00', '2018-11-04 08:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":1},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":0},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"22","food_name":"White bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81810094826.jpeg","portions":[{"id":"63","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81812e8eda5.jpeg","selected":1},{"id":"64","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81814f602b4.jpeg","selected":0},{"id":"65","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81816c34838.jpeg","selected":0},{"id":"66","portion":"3.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81818039d12.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-05 11:53:20', '2018-11-05 03:53:45'),
(138, 137, 11, 5, 20, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 08:00 AM', '2018-11-04 07:30:00', '2018-11-04 08:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"08:00 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 03:53:35'),
(139, NULL, 11, 5, 21, 'Travelling', '', 'travelling.png', 'Car/Taxi', 1, 'Cool! What did you do after reaching your destination at 08:30 AM', '2018-11-04 08:00:00', '2018-11-04 08:30:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Car/Taxi","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"8","time_minute1":"3","time_minute2":"0","time_am":"AM"}}', '2018-11-05 11:53:49', '2018-11-05 03:53:49'),
(140, NULL, 11, 5, 22, 'Active Activities', 'Outdoor', 'active.png', 'Run/Jog', 1, 'Cool! What did you after doing these active activities at 09:00 AM', '2018-11-04 08:30:00', '2018-11-04 09:00:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Run/Jog","other_activities":"","intensity":"Just a Little","location":"Home","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 11:54:04', '2018-11-05 03:54:04'),
(141, NULL, 11, 5, 23, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, NULL, '2018-11-04 09:00:00', '2018-11-04 14:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":""}', '2018-11-05 11:54:24', '2018-11-05 03:54:41'),
(142, 141, 11, 5, 24, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 02:00 AM', '2018-11-04 09:00:00', '2018-11-04 14:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","time":{"time_hr1":"0","time_hr2":"2","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', NULL, '2018-11-05 03:54:31'),
(143, NULL, 11, 5, 25, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 05:00 PM?', '2018-11-04 14:00:00', '2018-11-04 17:00:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"5","time_shower_minute1":"0","time_shower_minute2":"0","shower_am":"PM"}}', '2018-11-05 11:54:56', '2018-11-05 03:54:56'),
(144, NULL, 11, 5, 26, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'goreng pisang\n', 1, 'Cool! What did you do after doing those activities at 06:00 PM', '2018-11-04 17:00:00', '2018-11-04 18:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[],"selected_option_others":[{"name":"goreng pisang","portion":2}],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 11:55:06', '2018-11-05 03:55:06'),
(145, NULL, 11, 5, 27, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Video Games', 1, 'Cool! What did you do after doing those activities at 09:00 PM', '2018-11-04 18:00:00', '2018-11-04 21:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Video Games","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 11:55:37', '2018-11-05 03:55:37'),
(146, NULL, 11, 5, 28, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:00 AM?', '2018-11-04 21:00:00', '2018-11-04 07:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-05 11:55:57', '2018-11-05 03:55:57'),
(147, NULL, 12, 5, 29, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-05 07:00:00', '2018-11-05 08:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-05 12:28:50', '2018-11-05 04:32:14'),
(148, 147, 12, 5, 30, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 08:00 AM?', '2018-11-05 07:00:00', '2018-11-05 08:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"8","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', NULL, '2018-11-05 04:29:58'),
(149, NULL, 12, 5, 31, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nBun or pow\n', 1, 'Cool! What did you do after doing those activities at 09:00 AM', '2018-11-05 08:00:00', '2018-11-05 09:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":1},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":0},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"16","food_name":"Bun or pow","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bd28430e7916.jpg","portions":[{"id":"443","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":1},{"id":"444","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":0},{"id":"445","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"446","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 12:32:17', '2018-11-05 04:32:17'),
(150, NULL, 12, 5, 32, 'Active Activities', 'Outdoor', 'active.png', 'Cycle/Skate/Scoot', 1, 'Cool! What did you after doing these active activities at 10:00 AM', '2018-11-05 09:00:00', '2018-11-05 10:00:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Cycle/Skate/Scoot","other_activities":"","intensity":"Very Tiring","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"10:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 12:32:43', '2018-11-05 04:32:43'),
(151, NULL, 12, 5, 33, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Pastries (e.g. curry puff, apple pie)\n', 1, 'Cool! What did you do after doing those activities at 12:00 AM', '2018-11-05 10:00:00', '2018-11-05 12:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"38","food_name":"Pastries (e.g. curry puff, apple pie)","food_categories_id":"5","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82627f87039.jpeg","portions":[{"id":"130","portion":"0.50","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8262a5c67ac.jpeg","selected":1},{"id":"131","portion":"1.00","measurement":"piece","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8262bd5978b.jpeg","selected":0},{"id":"132","portion":"1.50","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8262d68d73f.jpeg","selected":0},{"id":"133","portion":"2.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8262efd5f0a.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"12:00 AM","moment_from":null,"moment_to":null}}', '2018-11-05 12:34:26', '2018-11-05 04:34:26');
INSERT INTO `activities` (`id`, `dual_activity_id`, `daily_log_id`, `user_kid_id`, `sequence`, `activity_type`, `location`, `icon`, `selected_activity_display`, `complete`, `param_display`, `start_date_time`, `complete_date_time`, `duration`, `json`, `created_date`, `updated_date`) VALUES
(153, NULL, 8, 9, 5, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 06:15 AM', '2018-11-04 06:10:00', '2018-11-04 06:15:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"6","time_minute1":"1","time_minute2":"5","time_am":"AM"}}', '2018-11-05 03:11:03', '2018-11-05 07:11:03'),
(154, NULL, 8, 9, 6, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, 'Cool! What did you do after doing those activities at 01:45 AM', '2018-11-04 06:15:00', '2018-11-04 13:45:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"01:45 AM","moment_from":null,"moment_to":null}}', '2018-11-05 03:11:32', '2018-11-05 07:11:32'),
(155, NULL, 8, 9, 7, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 02:00 PM', '2018-11-04 13:45:00', '2018-11-04 14:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"2","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', '2018-11-05 03:11:49', '2018-11-05 07:11:49'),
(156, NULL, 8, 9, 8, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 02:05 PM', '2018-11-04 14:00:00', '2018-11-04 14:05:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"2","time_minute1":"0","time_minute2":"5","time_am":"PM"}}', '2018-11-05 03:12:12', '2018-11-05 07:12:12'),
(157, NULL, 8, 9, 9, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 0, NULL, '2018-11-04 14:05:00', NULL, NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor"}', '2018-11-05 03:12:25', '2018-11-05 07:12:25'),
(158, NULL, 13, 11, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 07:05 AM?', '2018-11-04 06:05:00', '2018-11-04 07:05:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"7","time_shower_minute1":"0","time_shower_minute2":"5","shower_am":"AM"}}', '2018-11-05 03:33:20', '2018-11-05 07:33:20'),
(159, NULL, 13, 11, 2, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 08:05 AM?', '2018-11-04 07:05:00', '2018-11-04 08:05:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"8","time_sleep_minute1":"0","time_sleep_minute2":"5","time_sleep_am":"AM"}}', '2018-11-05 03:33:27', '2018-11-05 07:33:27'),
(160, NULL, 13, 11, 3, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 10:05 AM', '2018-11-04 08:05:00', '2018-11-04 10:05:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"0","time_minute2":"5","time_am":"AM"}}', '2018-11-05 03:33:34', '2018-11-05 07:33:34'),
(161, NULL, 13, 11, 4, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 02:10 PM?', '2018-11-04 10:05:00', '2018-11-04 14:10:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"2","time_shower_minute1":"1","time_shower_minute2":"0","shower_am":"PM"}}', '2018-11-05 03:33:45', '2018-11-05 07:33:45'),
(165, NULL, 13, 11, 5, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 02:15 PM', '2018-11-04 14:10:00', '2018-11-04 14:15:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"2","time_minute1":"1","time_minute2":"5","time_am":"PM"}}', '2018-11-05 03:53:19', '2018-11-05 07:53:19'),
(166, NULL, 13, 11, 6, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 02:20 PM?', '2018-11-04 14:15:00', '2018-11-04 14:20:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"2","time_sleep_minute1":"2","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-05 03:54:25', '2018-11-05 07:54:25'),
(167, NULL, 12, 5, 34, 'Sitting Activities', 'Indoor', 'sitting.png', 'Sit and Chat', 1, 'Cool! What did you do after doing those activities at 03:00 PM', '2018-11-05 12:00:00', '2018-11-05 15:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Sit and Chat","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"03:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 03:54:39', '2018-11-05 07:54:39'),
(170, NULL, 12, 5, 35, 'Active Activities', 'Outdoor', 'active.png', 'Cycle/Skate/Scoot', 1, 'Cool! What did you after doing these active activities at 06:00 PM', '2018-11-05 15:00:00', '2018-11-05 18:00:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Cycle/Skate/Scoot","other_activities":"","intensity":"Very Tiring","location":"Home","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"06:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 03:54:57', '2018-11-05 07:54:57'),
(171, NULL, 12, 5, 36, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Chicken or fish porridge\n', 1, 'Cool! What did you do after doing those activities at 08:00 PM', '2018-11-05 18:00:00', '2018-11-05 20:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"84","food_name":"Chicken or fish porridge","food_categories_id":"12","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8409c34d66c.jpeg","portions":[{"id":"277","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b8409f25fe25.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8409f25fe25.jpeg","selected":1},{"id":"278","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b840a3043711.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b840a3043711.jpeg","selected":0},{"id":"279","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b840a6c6d8f3.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b840a6c6d8f3.jpeg","selected":0},{"id":"280","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b840ab02c3ce.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b840ab02c3ce.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Rice and Porridge","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"08:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 03:55:12', '2018-11-05 07:55:12'),
(172, NULL, 12, 5, 37, 'Sitting Activities', 'Indoor', 'sitting.png', 'Watching TV', 1, 'Cool! What did you do after doing those activities at 09:00 PM', '2018-11-05 20:00:00', '2018-11-05 21:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Watching TV","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 PM","moment_from":null,"moment_to":null}}', '2018-11-05 03:55:35', '2018-11-05 07:55:35'),
(174, NULL, 12, 5, 38, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:00 AM?', '2018-11-05 21:00:00', '2018-11-05 07:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-05 03:57:32', '2018-11-05 07:57:32'),
(175, NULL, 13, 11, 7, 'Travelling', '', 'travelling.png', 'Walk', 0, NULL, '2018-11-04 14:20:00', NULL, NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing"}', '2018-11-05 03:58:49', '2018-11-05 07:58:49'),
(176, NULL, 14, 102, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 11:00 AM?', '2018-11-04 07:00:00', '2018-11-04 11:00:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"1","time_shower_hr2":"1","time_shower_minute1":"0","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-05 04:02:52', '2018-11-05 08:02:52'),
(177, NULL, 14, 102, 2, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 12:00 AM', '2018-11-04 11:00:00', '2018-11-04 12:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"2","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', '2018-11-05 04:03:46', '2018-11-05 08:03:46'),
(179, NULL, 14, 102, 3, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 0, NULL, '2018-11-04 12:00:00', NULL, NULL, '{"current_page":"/page-activity-diary","next_page":"/page-activity-diary-sleep"}', '2018-11-05 04:09:57', '2018-11-05 08:09:57'),
(180, NULL, 16, 8, 58, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 06:05 AM?', '2018-11-04 06:00:00', '2018-11-04 06:05:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"6","time_shower_minute1":"0","time_shower_minute2":"5","shower_am":"AM"}}', '2018-11-05 05:47:21', '2018-11-05 09:47:21'),
(181, NULL, 16, 8, 59, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 06:10 AM?', '2018-11-04 06:05:00', '2018-11-04 06:10:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"6","time_sleep_minute1":"1","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-05 05:47:26', '2018-11-05 09:47:26'),
(183, NULL, 16, 8, 60, 'Travelling', '', 'travelling.png', 'Scooter', 1, 'Cool! What did you do after reaching your destination at 07:00 AM', '2018-11-04 06:10:00', '2018-11-04 07:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Scooter","others":"Scooter","selected_activity":"Nothing","time":{"time_hr1":"0","time_hr2":"7","time_minute1":"0","time_minute2":"0","time_am":"AM"}}', '2018-11-05 05:47:51', '2018-11-05 09:47:51'),
(184, NULL, 16, 8, 61, 'Travelling', '', 'travelling.png', 'Walk', 1, NULL, '2018-11-04 07:00:00', '2018-11-04 07:05:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-05 05:48:12', '2018-11-05 09:48:30'),
(185, 184, 16, 8, 62, 'Sitting Activities', 'Indoor', 'sitting.png', 'Drawing', 1, 'Cool! What did you do after doing those activities at 07:05 AM', '2018-11-04 07:00:00', '2018-11-04 07:05:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Drawing","others":"Drawing","form":{"minute1":"","minute2":"","from":"","to":"07:05 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-05 09:48:16'),
(186, NULL, 16, 8, 63, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\nThosai or Idli\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 07:10 AM', '2018-11-04 07:05:00', '2018-11-04 07:10:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]},{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":0},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":1},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":0},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":1},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:10 AM","moment_from":null,"moment_to":null}}', '2018-11-05 05:48:38', '2018-11-05 09:48:38'),
(187, NULL, 16, 8, 64, 'Active Activities', 'Indoor', 'active.png', 'Playground', 1, 'Cool! What did you after doing these active activities at 07:10 AM', '2018-11-04 07:10:00', '2018-11-04 07:10:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Playground","other_activities":"Playground","intensity":"Just a Little","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:10 AM","moment_from":null,"moment_to":null}}', '2018-11-05 05:49:32', '2018-11-05 09:49:32'),
(188, NULL, 16, 8, 65, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:15 AM?', '2018-11-04 07:10:00', '2018-11-04 07:15:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"1","time_sleep_minute2":"5","time_sleep_am":"AM"}}', '2018-11-05 05:50:14', '2018-11-05 09:50:14'),
(189, NULL, 16, 8, 66, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:20 AM?', '2018-11-04 07:15:00', '2018-11-04 07:20:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"2","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-05 05:50:22', '2018-11-05 09:50:22'),
(190, NULL, 16, 8, 67, 'Sitting Activities', 'Indoor', 'sitting.png', 'Drawing', 1, 'Cool! What did you do after doing those activities at 07:25 AM', '2018-11-04 07:20:00', '2018-11-04 07:25:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Drawing","others":"Drawing","dual_selected_activity":"Nothing","location":"Student Care","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"07:25 AM","moment_from":null,"moment_to":null}}', '2018-11-05 05:50:30', '2018-11-05 09:50:30'),
(191, NULL, 17, 4, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 08:15 AM?', '2018-11-05 08:00:00', '2018-11-05 08:15:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"8","time_shower_minute1":"1","time_shower_minute2":"5","shower_am":"AM"}}', '2018-11-06 11:29:59', '2018-11-06 03:29:59'),
(192, NULL, 17, 4, 2, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Oats\nSoya milk\nBanana\nNuts\n', 1, NULL, '2018-11-05 08:15:00', '2018-11-05 08:45:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"25","food_name":"Oats","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81845677907.jpeg","portions":[{"id":"74","portion":"0.50","measurement":"rice bowl","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81847409b68.jpeg","selected":0},{"id":"76","portion":"0.75","measurement":"rice bowl","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81849ba79c3.jpeg","selected":0},{"id":"77","portion":"1.00","measurement":"rice bowl","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8184bbd4cd1.jpeg","selected":0},{"id":"78","portion":"1.50","measurement":"rice bowl","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8184d82ecc8.jpeg","selected":1}]},{"id":"13","food_name":"Soya milk","food_categories_id":"1","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8176fbb182e.jpeg","portions":[{"id":"39","portion":"0.50","measurement":"cup / packet","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81773c6641a.jpeg","selected":0},{"id":"40","portion":"1.00","measurement":"cup / packet","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81775d2e2f5.jpeg","selected":1},{"id":"41","portion":"1.50","measurement":"cups / packets","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81779533ffc.jpeg","selected":0},{"id":"42","portion":"2.00","measurement":"cups / packets","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8177abc9618.jpeg","selected":0}]},{"id":"48","food_name":"Banana","food_categories_id":"8","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82c88d1a706.jpeg","portions":[{"id":"152","portion":"0.50","measurement":"banana","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c8bc33480.jpeg","selected":0},{"id":"153","portion":"0.75","measurement":"banana","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c8d2a903d.jpeg","selected":0},{"id":"154","portion":"1.00","measurement":"banana","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c8e61eda4.jpeg","selected":1},{"id":"155","portion":"2.00","measurement":"bananas","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c8fe8bcf7.jpeg","selected":0}]},{"id":"37","food_name":"Nuts","food_categories_id":"5","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82617990225.jpeg","portions":[{"id":"126","portion":"10.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8261993159d.jpeg","selected":0},{"id":"127","portion":"15.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8261b2c57f5.jpeg","selected":0},{"id":"128","portion":"20.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8261f925079.jpeg","selected":1},{"id":"129","portion":"30.00","measurement":"pieces","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82620b706a0.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals"}', '2018-11-06 11:30:20', '2018-11-06 03:37:35'),
(193, 192, 17, 4, 3, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using the Computer/Laptop', 1, 'Cool! What did you do after doing those activities at 08:45 AM', '2018-11-05 08:15:00', '2018-11-05 08:45:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using the Computer/Laptop","others":"","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"08:45 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 03:35:09'),
(194, NULL, 17, 4, 4, 'Travelling', '', 'travelling.png', 'Bus/Train', 1, NULL, '2018-11-05 08:45:00', '2018-11-05 09:15:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Bus/Train","others":""}', '2018-11-06 11:37:38', '2018-11-06 03:38:12'),
(195, 194, 17, 4, 5, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 09:15 AM', '2018-11-05 08:45:00', '2018-11-05 09:15:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","form":{"minute1":"","minute2":"","from":"","to":"09:15 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 03:37:56'),
(196, NULL, 17, 4, 6, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using the Computer/Laptop', 1, 'Cool! What did you do after doing those activities at 12:00 PM', '2018-11-05 09:15:00', '2018-11-05 12:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using the Computer/Laptop","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"12:00 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:38:15', '2018-11-06 03:38:15'),
(197, NULL, 17, 4, 7, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Stir-fry or steamed meat\nLentils and other beans (e.g. chickpeas, broad beans)\nFrench fries and hash brown\nLight green vegetables (e.g. cabbage, beansprouts)\n', 1, 'Cool! What did you do after doing those activities at 01:00 PM', '2018-11-05 12:00:00', '2018-11-05 13:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"60","food_name":"Stir-fry or steamed meat","food_categories_id":"9","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83a9a857d3b.jpeg","portions":[{"id":"199","portion":"1.00","measurement":"dessert spoon","portion_image":"/uploads/food_portions/5b83a9d1ef632.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9d1ef632.jpeg","selected":0},{"id":"200","portion":"3.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83a9e8c2080.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9e8c2080.jpeg","selected":0},{"id":"201","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83a9ffd31ce.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83a9ffd31ce.jpeg","selected":0},{"id":"202","portion":"7.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b83aa1746008.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa1746008.jpeg","selected":1}]},{"id":"111","food_name":"Lentils and other beans (e.g. chickpeas, broad beans)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97b72d4e91.jpg","portions":[{"id":"391","portion":"5.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc98177bca98.jpg","selected":1},{"id":"393","portion":"8.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc98193d92cb.jpg","selected":0},{"id":"395","portion":"10.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc981aa06dbc.jpg","selected":0},{"id":"397","portion":"15.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc981bbe37ab.jpg","selected":0}]},{"id":"45","food_name":"French fries and hash brown","food_categories_id":"7","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82c6f4b7421.jpeg","portions":[{"id":"471","portion":"0.50","measurement":"small packet","portion_image_path":null,"selected":1},{"id":"472","portion":"1.00","measurement":"small packet","portion_image_path":null,"selected":0},{"id":"473","portion":"1.50","measurement":"small packets","portion_image_path":null,"selected":0},{"id":"474","portion":"2.00","measurement":"small packets","portion_image_path":null,"selected":0}]},{"id":"112","food_name":"Light green vegetables (e.g. cabbage, beansprouts)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97c11e952a.jpg","portions":[{"id":"382","portion":"2.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97c3cb5ce5.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97c3cb5ce5.jpg","selected":1},{"id":"383","portion":"4.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97c4ad4fe2.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97c4ad4fe2.jpg","selected":0},{"id":"384","portion":"8.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97c5d7bb0d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97c5d7bb0d.jpg","selected":0},{"id":"385","portion":"12.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97c6b71c52.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97c6b71c52.jpg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Vegetables","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"01:00 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:38:36', '2018-11-06 03:38:36'),
(198, NULL, 17, 4, 8, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using the Computer/Laptop', 1, 'Cool! What did you do after doing those activities at 05:00 PM', '2018-11-05 13:00:00', '2018-11-05 17:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using the Computer/Laptop","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"05:00 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:40:52', '2018-11-06 03:40:52'),
(199, NULL, 17, 4, 9, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Tropical fruits (e.g. mango, guava)\n', 1, 'Cool! What did you do after doing those activities at 05:10 PM', '2018-11-05 17:00:00', '2018-11-05 17:10:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[],"selected_option_others":[],"selected_fruits_option":[{"id":"52","food_name":"Tropical fruits (e.g. mango, guava)","food_categories_id":"8","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82caf48af1f.jpeg","portions":[{"id":"168","portion":"0.50","measurement":"wedge","portion_image":"/uploads/food_portions/5b82cb1d4cc58.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82cb1d4cc58.jpeg","selected":1},{"id":"169","portion":"0.75","measurement":"wedge","portion_image":"/uploads/food_portions/5b82cb3053d39.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82cb3053d39.jpeg","selected":0},{"id":"170","portion":"1.00","measurement":"wedge","portion_image":"/uploads/food_portions/5b82cb413dfbf.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82cb413dfbf.jpeg","selected":0},{"id":"171","portion":"2.00","measurement":"wedges","portion_image":"/uploads/food_portions/5b82cb557b07e.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82cb557b07e.jpeg","selected":0}]}],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Fruits","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"05:10 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:41:17', '2018-11-06 03:41:17'),
(200, NULL, 17, 4, 10, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using the Computer/Laptop', 1, 'Cool! What did you do after doing those activities at 06:15 PM', '2018-11-05 17:10:00', '2018-11-05 18:15:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using the Computer/Laptop","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"06:15 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:41:55', '2018-11-06 03:41:55'),
(201, NULL, 17, 4, 11, 'Active Activities', 'Outdoor', 'active.png', 'Run/Jog', 1, 'Cool! What did you after doing these active activities at 07:30 PM', '2018-11-05 18:15:00', '2018-11-05 19:30:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Run/Jog","other_activities":"","intensity":"Quite Tiring","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"07:30 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:42:16', '2018-11-06 03:42:16'),
(202, NULL, 17, 4, 12, 'Travelling', '', 'travelling.png', 'Bus/Train', 1, NULL, '2018-11-05 19:30:00', '2018-11-05 20:30:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Bus/Train","others":""}', '2018-11-06 11:42:44', '2018-11-06 03:42:58'),
(203, 202, 17, 4, 13, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, 'Cool! What did you do after doing those activities at 08:30 PM', '2018-11-05 19:30:00', '2018-11-05 20:30:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":"","form":{"minute1":"","minute2":"","from":"","to":"08:30 PM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 03:42:49'),
(204, NULL, 17, 4, 14, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Noodles in soup (e.g. beehoon soup, mee soto)\nDark green leafy vegetables (e.g. spinach, kailan)\nYellow, orange, red vegetables (e.g. corn, carrot, tomato)\nTofu\nBoiled or steamed eggs\nPrawn or other shellfish\n', 1, NULL, '2018-11-05 20:30:00', '2018-11-05 21:30:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"72","food_name":"Noodles in soup (e.g. beehoon soup, mee soto)","food_categories_id":"10","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83b054ae6e7.jpeg","portions":[{"id":"234","portion":"0.50","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b07c6806d.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b07c6806d.jpeg","selected":1},{"id":"235","portion":"0.75","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b08fb47c9.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b08fb47c9.jpeg","selected":0},{"id":"236","portion":"1.00","measurement":"rice bowl","portion_image":"/uploads/food_portions/5b83b0a406a64.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b0a406a64.jpeg","selected":0},{"id":"237","portion":"1.50","measurement":"rice bowls","portion_image":"/uploads/food_portions/5b83b0d87605b.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83b0d87605b.jpeg","selected":0}]},{"id":"94","food_name":"Dark green leafy vegetables (e.g. spinach, kailan)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b84189c667ff.jpeg","portions":[{"id":"317","portion":"2.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b8418bec13c6.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8418bec13c6.jpeg","selected":0},{"id":"318","portion":"4.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b84191c17175.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b84191c17175.jpeg","selected":0},{"id":"319","portion":"8.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b8419444a961.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8419444a961.jpeg","selected":1},{"id":"320","portion":"12.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5b841971ad324.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b841971ad324.jpeg","selected":0}]},{"id":"113","food_name":"Yellow, orange, red vegetables (e.g. corn, carrot, tomato)","food_categories_id":"15","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc97cfbc964d.jpg","portions":[{"id":"386","portion":"3.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97d1b7e95d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97d1b7e95d.jpg","selected":0},{"id":"387","portion":"5.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97dbf1b258.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97dbf1b258.jpg","selected":0},{"id":"388","portion":"7.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97ddd4bc91.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97ddd4bc91.jpg","selected":0},{"id":"389","portion":"10.00","measurement":"dessert spoons","portion_image":"/uploads/food_portions/5bc97df72479d.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc97df72479d.jpg","selected":1}]},{"id":"109","food_name":"Tofu","food_categories_id":"6","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bc9775b5345a.jpg","portions":[{"id":"378","portion":"0.50","measurement":"piece","portion_image":"/uploads/food_portions/5bc977943b800.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc977943b800.jpg","selected":0},{"id":"379","portion":"1.00","measurement":"piece","portion_image":"/uploads/food_portions/5bc977a314b53.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc977a314b53.jpg","selected":0},{"id":"380","portion":"1.50","measurement":"pieces","portion_image":"/uploads/food_portions/5bc977c6cf315.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc977c6cf315.jpg","selected":1},{"id":"381","portion":"2.00","measurement":"pieces","portion_image":"/uploads/food_portions/5bc977d4981dd.jpg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5bc977d4981dd.jpg","selected":0}]},{"id":"42","food_name":"Boiled or steamed eggs","food_categories_id":"6","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b82c530c0a36.jpeg","portions":[{"id":"138","portion":"0.50","measurement":"piece","portion_image":"/uploads/food_portions/5b82c58092e17.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c58092e17.jpeg","selected":0},{"id":"139","portion":"1.00","measurement":"piece","portion_image":"/uploads/food_portions/5b82c5a2b8f99.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c5a2b8f99.jpeg","selected":1},{"id":"140","portion":"1.50","measurement":"pieces","portion_image":"/uploads/food_portions/5b82c5b72764e.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c5b72764e.jpeg","selected":0},{"id":"141","portion":"2.00","measurement":"pieces","portion_image":"/uploads/food_portions/5b82c5cc065a3.jpeg","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b82c5cc065a3.jpeg","selected":0}]},{"id":"67","food_name":"Prawn or other shellfish","food_categories_id":"9","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83addd1695d.jpeg","portions":[{"id":"463","portion":"1.00","measurement":"piece","portion_image":null,"portion_image_path":null,"selected":0},{"id":"464","portion":"2.00","measurement":"pieces","portion_image":null,"portion_image_path":null,"selected":0},{"id":"465","portion":"3.00","measurement":"pieces","portion_image":null,"portion_image_path":null,"selected":1},{"id":"466","portion":"4.00","measurement":"pieces","portion_image":null,"portion_image_path":null,"selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Snacks and Desserts"}', '2018-11-06 11:43:03', '2018-11-06 03:47:04'),
(205, 204, 17, 4, 15, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using the Computer/Laptop', 1, 'Cool! What did you do after doing those activities at 09:30 PM', '2018-11-05 20:30:00', '2018-11-05 21:30:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using the Computer/Laptop","others":"","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"09:30 PM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 03:46:52'),
(206, NULL, 17, 4, 16, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 07:00 AM?', '2018-11-05 21:30:00', '2018-11-05 07:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"7","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-06 11:47:10', '2018-11-06 03:47:10'),
(208, NULL, 18, 4, 17, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', '', 1, 'Cool! What did you do after doing those activities at 12:45 PM', '2018-11-06 08:00:00', '2018-11-06 12:45:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Supplements","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"12:45 PM","moment_from":null,"moment_to":null}}', '2018-11-06 11:55:25', '2018-11-06 03:55:25'),
(210, NULL, 18, 4, 18, 'Travelling', '', 'travelling.png', 'Bus/Train', 1, NULL, '2018-11-06 08:30:00', '2018-11-06 08:45:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-06 03:26:07', '2018-11-06 07:27:28'),
(211, 210, 18, 4, 19, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 08:45 AM?', '2018-11-06 08:30:00', '2018-11-06 08:45:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"8","time_sleep_minute1":"4","time_sleep_minute2":"5","time_sleep_am":"AM"}}', NULL, '2018-11-06 07:26:16'),
(212, NULL, 18, 4, 20, 'Sitting Activities', 'Outdoor', 'sitting.png', 'Playing Video Games', 1, 'Cool! What did you do after doing those activities at 09:00 AM', '2018-11-06 08:45:00', '2018-11-06 09:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Video Games","others":"","dual_selected_activity":"Nothing","location":"Student Care","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"09:00 AM","moment_from":null,"moment_to":null}}', '2018-11-06 03:27:36', '2018-11-06 07:27:36'),
(215, NULL, 18, 4, 21, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 09:15 AM?', '2018-11-06 09:00:00', '2018-11-06 09:15:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"0","time_shower_hr2":"9","time_shower_minute1":"1","time_shower_minute2":"5","shower_am":"AM"}}', '2018-11-06 03:31:48', '2018-11-06 07:31:48'),
(216, NULL, 18, 4, 22, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 09:30 AM?', '2018-11-06 09:15:00', '2018-11-06 09:30:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"9","time_sleep_minute1":"3","time_sleep_minute2":"0","time_sleep_am":"AM"}}', '2018-11-06 03:32:12', '2018-11-06 07:32:12'),
(217, NULL, 18, 4, 23, 'Active Activities', 'Outdoor', 'active.png', 'Dance', 1, 'Cool! What did you after doing these active activities at 09:45 AM', '2018-11-06 09:30:00', '2018-11-06 09:45:00', NULL, '{"current_page":"/page-activity-diary-active-options","next_page":"/","selected_option":"Dance","other_activities":"","intensity":"Quite Tiring","location":"School","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"09:45 AM","moment_from":null,"moment_to":null}}', '2018-11-06 03:32:40', '2018-11-06 07:32:40'),
(218, NULL, 18, 4, 24, 'Sitting Activities', 'Outdoor', 'sitting.png', 'Sit and Chat', 1, 'Cool! What did you do after doing those activities at 10:00 AM', '2018-11-06 09:45:00', '2018-11-06 10:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Sit and Chat","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"10:00 AM","moment_from":null,"moment_to":null}}', '2018-11-06 03:33:47', '2018-11-06 07:33:47'),
(219, NULL, 18, 4, 25, 'Sitting Activities', 'Indoor', 'sitting.png', 'Playing Board/Card Games', 1, NULL, '2018-11-06 10:00:00', '2018-11-06 10:15:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Playing Board/Card Games","others":"","location":"School","location_indoor_outdoor":"Indoor"}', '2018-11-06 03:34:08', '2018-11-06 07:34:30'),
(220, 219, 18, 4, 26, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', 'Bun or pow\nThosai or Idli\nWholemeal bread\n', 1, 'Cool! What did you do after doing those activities at 10:15 AM', '2018-11-06 10:00:00', '2018-11-06 10:15:00', NULL, '{"current_page":"/page-food-record-main","selected_option":[{"id":"16","food_name":"Bun or pow","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5bd28430e7916.jpg","portions":[{"id":"443","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"444","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":1},{"id":"445","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"446","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]},{"id":"20","food_name":"Thosai or Idli","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b81807298a18.jpeg","portions":[{"id":"447","portion":"0.50","measurement":"piece","portion_image_path":null,"selected":0},{"id":"448","portion":"1.00","measurement":"piece","portion_image_path":null,"selected":1},{"id":"449","portion":"2.00","measurement":"pieces","portion_image_path":null,"selected":0},{"id":"450","portion":"3.00","measurement":"pieces","portion_image_path":null,"selected":0}]},{"id":"23","food_name":"Wholemeal bread","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b8181a71e304.jpeg","portions":[{"id":"67","portion":"0.50","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b8181f829256.jpeg","selected":0},{"id":"68","portion":"1.00","measurement":"slice","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81822166bd2.jpeg","selected":1},{"id":"69","portion":"1.50","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b81824173357.jpeg","selected":0},{"id":"70","portion":"2.00","measurement":"slices","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b818266940ee.jpeg","selected":0}]}],"selected_option_others":[],"selected_drinks_option":[],"selected_fruits_option":[],"selected_desert_option":[],"selected_option_drinks_others":[],"selected_option_fruits_others":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"10:15 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 07:34:12'),
(221, NULL, 18, 4, 27, 'Sitting Activities', 'Indoor', 'sitting.png', 'Using your phone/tablet', 1, NULL, '2018-11-06 10:15:00', '2018-11-06 10:30:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Using your phone/tablet","others":""}', '2018-11-06 03:34:33', '2018-11-06 07:34:49'),
(222, 221, 18, 4, 28, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 10:30 AM', '2018-11-06 10:15:00', '2018-11-06 10:30:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"3","time_minute2":"0","time_am":"AM"}}', NULL, '2018-11-06 07:34:39');
INSERT INTO `activities` (`id`, `dual_activity_id`, `daily_log_id`, `user_kid_id`, `sequence`, `activity_type`, `location`, `icon`, `selected_activity_display`, `complete`, `param_display`, `start_date_time`, `complete_date_time`, `duration`, `json`, `created_date`, `updated_date`) VALUES
(223, NULL, 18, 4, 29, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'Curry meat or fish\n', 1, NULL, '2018-11-06 10:30:00', '2018-11-06 10:45:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"61","food_name":"Curry meat or fish","food_categories_id":"9","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b83aa2d02979.jpeg","portions":[{"id":"203","portion":"3.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa5da1f1f.jpeg","selected":0},{"id":"204","portion":"5.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa732ca2a.jpeg","selected":0},{"id":"205","portion":"7.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa8540abf.jpeg","selected":1},{"id":"206","portion":"9.00","measurement":"dessert spoons","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b83aa9788bfd.jpeg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Outdoor"}', '2018-11-06 03:35:10', '2018-11-06 07:36:00'),
(224, 223, 18, 4, 30, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 10:45 AM', '2018-11-06 10:30:00', '2018-11-06 10:45:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","time":{"time_hr1":"1","time_hr2":"0","time_minute1":"4","time_minute2":"5","time_am":"AM"}}', NULL, '2018-11-06 07:35:47'),
(225, NULL, 18, 4, 31, 'Eat and Drink', 'Outdoor', 'eat-and-drink.png', 'Bread spreads (e.g. butter, jam)\n', 1, NULL, '2018-11-06 10:45:00', '2018-11-06 11:00:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[{"id":"6","food_name":"Bread spreads (e.g. butter, jam)","food_categories_id":"3","image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/foods/5b80f9f0309f9.jpg","portions":[{"id":"6","portion":"1.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa169d87b.jpg","selected":0},{"id":"7","portion":"1.50","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa2ef37ae.jpg","selected":1},{"id":"8","portion":"2.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa4297ab6.jpg","selected":0},{"id":"9","portion":"3.00","measurement":"teasponn","portion_image_path":"http://47.88.174.149/frontend/nus_coc/backend/uploads/food_portions/5b80fa50c9525.jpg","selected":0}]}],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","location":"School","location_indoor_outdoor":"Outdoor"}', '2018-11-06 03:36:06', '2018-11-06 07:36:30'),
(226, 225, 18, 4, 32, 'Sitting Activities', 'Indoor', 'sitting.png', 'Watching TV', 1, 'Cool! What did you do after doing those activities at 11:00 AM', '2018-11-06 10:45:00', '2018-11-06 11:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Watching TV","others":"","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"11:00 AM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 07:36:18'),
(227, NULL, 18, 4, 33, 'Eat and Drink', 'Indoor', 'eat-and-drink.png', '', 1, 'Cool! What did you do after doing those activities at 11:15 AM', '2018-11-06 11:00:00', '2018-11-06 11:15:00', NULL, '{"current_page":"/page-food-record-main","next_page":"/page-food-record-main","selected_option":[],"selected_option_others":[],"selected_fruits_option":[],"selected_option_fruits_others":[],"selected_drinks_option":[],"selected_option_drinks_others":[],"selected_desert_option":[],"selected_option_deserts_others":[],"selectedCategories":"Breads, Spreads and Cereals","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"11:15 AM","moment_from":null,"moment_to":null}}', '2018-11-06 03:36:35', '2018-11-06 07:36:35'),
(228, NULL, 18, 4, 34, 'Travelling', '', 'travelling.png', 'Cycle', 1, 'Cool! What did you do after reaching your destination at 11:30 AM', '2018-11-06 11:15:00', '2018-11-06 11:30:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Cycle","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"1","time_minute1":"3","time_minute2":"0","time_am":"AM"}}', '2018-11-06 03:38:17', '2018-11-06 07:38:17'),
(229, NULL, 18, 4, 35, 'Travelling', '', 'travelling.png', 'Car/Taxi', 1, NULL, '2018-11-06 11:30:00', '2018-11-06 11:45:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-06 03:38:48', '2018-11-06 07:40:39'),
(230, 229, 18, 4, 36, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 11:45 AM?', '2018-11-06 11:30:00', '2018-11-06 11:45:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"1","time_sleep_hr2":"1","time_sleep_minute1":"4","time_sleep_minute2":"5","time_sleep_am":"AM"}}', NULL, '2018-11-06 07:39:06'),
(231, NULL, 18, 4, 37, 'Travelling', '', 'travelling.png', 'Cycle', 1, NULL, '2018-11-06 11:45:00', '2018-11-06 12:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-06 03:40:41', '2018-11-06 07:41:01'),
(232, 231, 18, 4, 38, 'Sitting Activities', 'Indoor', 'sitting.png', 'Tuition-Music-Lesson', 1, 'Cool! What did you do after doing those activities at 12:00 PM', '2018-11-06 11:45:00', '2018-11-06 12:00:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Tuition-Music-Lesson","others":"","form":{"minute1":"","minute2":"","from":"","to":"12:00 PM","moment_from":null,"moment_to":null}}', NULL, '2018-11-06 07:40:46'),
(233, NULL, 18, 4, 39, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 12:15 PM?', '2018-11-06 12:00:00', '2018-11-06 12:15:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"1","time_shower_hr2":"2","time_shower_minute1":"1","time_shower_minute2":"5","shower_am":"PM"}}', '2018-11-06 03:41:24', '2018-11-06 07:41:24'),
(234, NULL, 18, 4, 40, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 12:30 PM?', '2018-11-06 12:15:00', '2018-11-06 12:30:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"1","time_sleep_hr2":"2","time_sleep_minute1":"3","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-06 03:41:37', '2018-11-06 07:41:37'),
(236, NULL, 16, 8, 68, 'Sitting Activities', 'Indoor', 'sitting.png', 'Reading/Studying', 1, 'Cool! What did you do after doing those activities at 01:25 PM', '2018-11-04 07:25:00', '2018-11-04 13:25:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Reading/Studying","others":"","dual_selected_activity":"Nothing","location":"School","location_indoor_outdoor":"Indoor","form":{"minute1":"","minute2":"","from":"","to":"01:25 PM","moment_from":null,"moment_to":null}}', '2018-11-07 09:44:03', '2018-11-07 01:44:03'),
(237, NULL, 19, 104, 1, 'Shower/Washup', 'Indoor', 'shower-washup.png', NULL, 1, 'Cool! What did you do after showering at 11:00 AM?', '2018-11-06 06:05:00', '2018-11-06 11:00:00', NULL, '{"current_page":"/page-shower","next_page":"/","time":{"time_shower_hr1":"1","time_shower_hr2":"1","time_shower_minute1":"0","time_shower_minute2":"0","shower_am":"AM"}}', '2018-11-07 09:48:17', '2018-11-07 01:48:17'),
(238, NULL, 19, 104, 2, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 12:00 PM', '2018-11-06 11:00:00', '2018-11-06 12:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"2","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', '2018-11-07 09:48:26', '2018-11-07 01:48:26'),
(239, NULL, 19, 104, 3, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 01:00 PM?', '2018-11-06 12:00:00', '2018-11-06 13:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"0","time_sleep_hr2":"1","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-07 09:49:05', '2018-11-07 01:49:05'),
(245, NULL, 19, 104, 4, 'Sitting Activities', 'Outdoor', 'sitting.png', 'Reading/Studying', 1, 'Cool! What did you do after doing those activities at 01:05 PM', '2018-11-06 13:00:00', '2018-11-06 13:05:00', NULL, '{"current_page":"/page-activity-diary-sitting-options","next_page":"/","selected_option":"Reading/Studying","others":"","dual_selected_activity":"Nothing","location":"Home","location_indoor_outdoor":"Outdoor","form":{"minute1":"","minute2":"","from":"","to":"01:05 PM","moment_from":null,"moment_to":null}}', '2018-11-07 09:55:03', '2018-11-07 01:55:03'),
(246, NULL, 19, 104, 5, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 0, NULL, '2018-11-06 13:05:00', NULL, NULL, '{"current_page":"/page-activity-diary","next_page":"/page-activity-diary-sleep"}', '2018-11-07 09:55:19', '2018-11-07 01:55:19'),
(247, NULL, 20, 10, 4, 'Nap/Sleep', 'Indoor', 'nap-bed.png', NULL, 1, 'Cool! What did you do after waking up at 12:00 PM?', '2018-11-07 07:00:00', '2018-11-07 12:00:00', NULL, '{"current_page":"/page-activity-diary-sleep","next_page":"/","time":{"time_sleep_hr1":"1","time_sleep_hr2":"2","time_sleep_minute1":"0","time_sleep_minute2":"0","time_sleep_am":"PM"}}', '2018-11-07 10:06:16', '2018-11-07 02:06:16'),
(248, NULL, 20, 10, 5, 'Travelling', '', 'travelling.png', 'Walk', 0, NULL, '2018-11-07 12:00:00', NULL, NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing"}', '2018-11-07 10:10:49', '2018-11-07 02:10:49'),
(249, NULL, 1, 1, 4, 'Travelling', '', 'travelling.png', 'Walk', 1, 'Cool! What did you do after reaching your destination at 12:00 PM', '2018-11-02 10:15:00', '2018-11-02 12:00:00', NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":"","selected_activity":"Nothing","time":{"time_hr1":"1","time_hr2":"2","time_minute1":"0","time_minute2":"0","time_am":"PM"}}', '2018-11-07 10:12:39', '2018-11-07 02:12:39'),
(250, NULL, 1, 1, 5, 'Travelling', '', 'travelling.png', 'Walk', 0, NULL, '2018-11-02 12:00:00', NULL, NULL, '{"current_page":"/page-activity-diary-travelling-options","next_page":"/","selected_option":"Walk","others":""}', '2018-11-07 10:14:10', '2018-11-07 02:14:10');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `userId` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `userId`, `password`, `created_date`, `updated_date`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2014-05-18 12:49:38', '2018-04-06 01:38:45'),
(2, 'asdf', '912ec803b2ce49e4a541068d495ab570', '2014-07-08 16:37:07', '2018-04-05 12:40:41'),
(3, 'asdfasdf', '6a204bd89f3c8348afd5c77c717a097a', '2014-07-12 14:05:23', '2018-04-05 12:40:41'),
(4, 'sharon', '74d4c760a95d678a10f32870b132b002', '2014-10-26 16:09:18', '2018-04-05 12:40:41'),
(5, 'ahfen', '41aba10f01b8b5b9d797a6e872c00d4c', '2014-10-26 16:09:42', '2018-04-05 12:40:41'),
(7, 'huiying', '7881599e1bc28db5bb20e8955adbe6e8', '2016-06-20 13:01:56', '2018-04-05 12:40:41'),
(9, 'eason', 'b1e93021e6f5726e2668a2bb1a70d6b3', '2016-08-26 13:37:18', '2018-04-05 12:40:41'),
(10, 'Serene', 'e1f67778a5f8e0fdf6841f56cb19521f', '2016-11-25 10:50:57', '2018-04-05 12:40:41'),
(11, 'MARCO', '96e79218965eb72c92a549dd5a330112', '2017-01-15 11:56:39', '2018-04-05 12:40:41'),
(12, 'junsian', 'e7a9f1743c85e6b93f1f08ab9e8e2530', '2017-02-02 16:52:27', '2018-04-05 12:40:41');

-- --------------------------------------------------------

--
-- Table structure for table `daily_log`
--

CREATE TABLE `daily_log` (
  `id` int(11) NOT NULL,
  `day_status` int(11) DEFAULT NULL,
  `user_kid_id` int(11) NOT NULL,
  `selected_day` varchar(500) DEFAULT NULL,
  `day_log` varchar(500) NOT NULL,
  `date_log` date NOT NULL,
  `sleeping_done` tinyint(1) DEFAULT '0',
  `time_sleep_hr1` varchar(500) DEFAULT NULL,
  `time_sleep_hr2` varchar(500) DEFAULT NULL,
  `time_sleep_minute1` varchar(500) DEFAULT NULL,
  `time_sleep_minute2` varchar(500) DEFAULT NULL,
  `time_sleep_am` varchar(500) DEFAULT NULL,
  `wakeup_done` tinyint(1) DEFAULT '0',
  `time_wakeup_hr1` varchar(500) DEFAULT NULL,
  `time_wakeup_hr2` varchar(500) DEFAULT NULL,
  `time_wakeup_minute1` varchar(500) DEFAULT NULL,
  `time_wakeup_minute2` varchar(500) DEFAULT NULL,
  `time_wakeup_am` varchar(500) DEFAULT NULL,
  `shower_done` tinyint(1) DEFAULT '0',
  `time_shower_minute1` varchar(500) DEFAULT NULL,
  `time_shower_minute2` varchar(500) DEFAULT NULL,
  `shower_location` varchar(500) DEFAULT NULL,
  `shower_after_activity` varchar(500) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_log`
--

INSERT INTO `daily_log` (`id`, `day_status`, `user_kid_id`, `selected_day`, `day_log`, `date_log`, `sleeping_done`, `time_sleep_hr1`, `time_sleep_hr2`, `time_sleep_minute1`, `time_sleep_minute2`, `time_sleep_am`, `wakeup_done`, `time_wakeup_hr1`, `time_wakeup_hr2`, `time_wakeup_minute1`, `time_wakeup_minute2`, `time_wakeup_am`, `shower_done`, `time_shower_minute1`, `time_shower_minute2`, `shower_location`, `shower_after_activity`, `completed`, `created_date`, `updated_date`) VALUES
(1, 1, 1, 'Yesterday', 'Friday, 02 November 2018', '2018-11-02', 1, '0', '8', '1', '0', 'PM', 1, '0', '8', '1', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-03', '2018-11-03 10:37:47'),
(2, 1, 2, 'Yesterday', 'Saturday, 03 November 2018', '2018-11-03', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '2018-11-04', '2018-11-04 05:53:40'),
(3, 1, 8, 'Yesterday', 'Saturday, 03 November 2018', '2018-11-03', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-04', '2018-11-04 06:00:08'),
(4, 1, 9, 'Yesterday', 'Saturday, 03 November 2018', '2018-11-03', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-04', '2018-11-04 07:18:01'),
(5, 1, 10, 'Yesterday', 'Saturday, 03 November 2018', '2018-11-03', 1, '0', '7', '0', '0', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-04', '2018-11-04 11:25:12'),
(6, 2, 9, 'Yesterday', 'Saturday, 03 November 2018', '2018-11-03', 1, '1', '1', '0', '0', 'PM', 1, '0', '7', '', '', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-04', '2018-11-04 11:45:56'),
(7, 2, 8, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-05', '2018-11-05 01:22:53'),
(8, 3, 9, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '5', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 01:37:11'),
(9, 1, 5, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '0', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-05', '2018-11-05 01:51:48'),
(10, 3, 8, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '5', 'PM', 1, '0', '6', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-05', '2018-11-05 02:04:41'),
(11, 2, 5, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '0', '9', '0', '0', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-05', '2018-11-05 03:46:51'),
(12, 3, 5, 'Today', 'Monday, 05 November 2018', '2018-11-05', 1, '0', '9', '0', '0', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 04:02:43'),
(13, 1, 11, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '5', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 07:32:07'),
(14, 1, 102, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '5', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 08:02:35'),
(15, 1, 103, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 08:23:34'),
(16, 4, 8, 'Yesterday', 'Sunday, 04 November 2018', '2018-11-04', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-05', '2018-11-05 09:47:11'),
(17, 1, 4, 'Yesterday', 'Monday, 05 November 2018', '2018-11-05', 1, '0', '2', '0', '0', 'AM', 1, '0', '8', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 1, '2018-11-06', '2018-11-06 03:28:37'),
(18, 2, 4, 'Today', 'Tuesday, 06 November 2018', '2018-11-06', 1, '1', '2', '0', '0', 'AM', 1, '0', '8', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-06', '2018-11-06 03:53:26'),
(19, 1, 104, 'Yesterday', 'Tuesday, 06 November 2018', '2018-11-06', 1, '1', '0', '0', '0', 'PM', 1, '0', '6', '0', '5', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-07', '2018-11-07 01:46:21'),
(20, 2, 10, 'Today', 'Wednesday, 07 November 2018', '2018-11-07', 1, '0', '7', '0', '0', 'PM', 1, '0', '7', '0', '0', 'AM', 0, NULL, NULL, NULL, NULL, 0, '2018-11-07', '2018-11-07 02:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `food_categories_id` int(11) NOT NULL,
  `food_name` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '10',
  `created_date` datetime DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `food_categories_id`, `food_name`, `image`, `sequence`, `created_date`, `updated_date`) VALUES
(4, 1, 'Coffee or Tea', '/uploads/foods/5b7f8d6e6ffce.jpg', 10, NULL, NULL),
(5, 1, 'Cultured drinks (e.g. YakultÂ®, VitagenÂ®)', '/uploads/foods/5b7f8d7badd4c.jpg', 10, NULL, NULL),
(6, 3, 'Bread spreads (e.g. butter, jam)', '/uploads/foods/5b80f9f0309f9.jpg', 10, NULL, NULL),
(7, 8, 'Apple or pear', '/uploads/foods/5b80fbf8a48ce.jpg', 10, NULL, NULL),
(8, 5, 'Biscuits or cookies', '/uploads/foods/5b80fd7a7eef2.jpg', 10, NULL, NULL),
(11, 16, 'Malt beverages (e.g. MiloÂ®, OvaltineÂ®)', '/uploads/foods/5b817574b847d.jpeg', 10, NULL, NULL),
(12, 16, 'Full fat milk', '/uploads/foods/5bd13edd34eb9.jpg', 10, NULL, NULL),
(13, 1, 'Soya milk', '/uploads/foods/5b8176fbb182e.jpeg', 10, NULL, NULL),
(14, 1, 'Soft drinks (e.g. Coca-ColaÂ®, RibenaÂ®)', '/uploads/foods/5b8177e20970c.jpeg', 10, NULL, NULL),
(15, 1, 'Water', '/uploads/foods/5b817958d7e4e.jpeg', 10, NULL, NULL),
(16, 3, 'Bun or pow', '/uploads/foods/5bd28430e7916.jpg', 10, NULL, NULL),
(17, 3, 'Chapati', '/uploads/foods/5b817aefc4297.jpeg', 10, NULL, NULL),
(18, 3, 'Roti prata', '/uploads/foods/5b817bcebd866.jpeg', 10, NULL, NULL),
(20, 3, 'Thosai or Idli', '/uploads/foods/5b81807298a18.jpeg', 10, NULL, NULL),
(22, 3, 'White bread', '/uploads/foods/5b81810094826.jpeg', 10, NULL, NULL),
(23, 3, 'Wholemeal bread', '/uploads/foods/5b8181a71e304.jpeg', 10, NULL, NULL),
(24, 3, 'Breakfast cereals (e.g. Koko KrunchÂ®, Cornflakes)', '/uploads/foods/5b8182cda8cc6.jpeg', 10, NULL, NULL),
(25, 3, 'Oats', '/uploads/foods/5b81845677907.jpeg', 10, NULL, NULL),
(26, 5, 'Cakes, kueh kueh or waffles', '/uploads/foods/5bc983df71fe5.jpg', 10, NULL, NULL),
(27, 16, 'Cheese', '/uploads/foods/5b823f27ec85c.jpeg', 10, NULL, NULL),
(28, 5, 'Chips (e.g. potato chips, keropok)', '/uploads/foods/5b82415fb0c75.jpeg', 10, NULL, NULL),
(29, 5, 'Chocolates', '/uploads/foods/5b82428d3db5a.jpeg', 10, NULL, NULL),
(30, 5, 'Dessert in soup (e.g. green bean soup, ice kachang)', '/uploads/foods/5b82448cc829a.jpeg', 10, NULL, NULL),
(34, 5, 'Ice-cream', '/uploads/foods/5b825b73278c8.jpeg', 10, NULL, NULL),
(35, 5, 'Jelly or pudding (e.g. fruit jelly, fruit pudding)', '/uploads/foods/5b825d0195740.jpeg', 10, NULL, NULL),
(37, 5, 'Nuts', '/uploads/foods/5b82617990225.jpeg', 10, NULL, NULL),
(38, 5, 'Pastries (e.g. curry puff, apple pie)', '/uploads/foods/5b82627f87039.jpeg', 10, NULL, NULL),
(39, 5, 'Sweets', '/uploads/foods/5b8265be022e1.jpeg', 10, NULL, NULL),
(41, 16, 'Yoghurt', '/uploads/foods/5b8266b114aa0.jpeg', 10, NULL, NULL),
(42, 6, 'Boiled or steamed eggs', '/uploads/foods/5b82c530c0a36.jpeg', 10, NULL, NULL),
(43, 6, 'Fried or scrambled eggs', '/uploads/foods/5b82c5e9d5151.jpeg', 10, NULL, NULL),
(44, 7, 'Burger', '/uploads/foods/5b82c67a7d3ef.jpeg', 10, NULL, NULL),
(45, 7, 'French fries and hash brown', '/uploads/foods/5b82c6f4b7421.jpeg', 10, NULL, NULL),
(46, 7, 'Pizza', '/uploads/foods/5b82c74808b56.jpeg', 10, NULL, NULL),
(47, 8, 'Dried fruits (e.g. raisins, dried apricot)', '/uploads/foods/5b82c7e1d41fa.jpeg', 10, NULL, NULL),
(48, 8, 'Banana', '/uploads/foods/5b82c88d1a706.jpeg', 10, NULL, NULL),
(49, 8, 'Grapes or berries (e.g. strawberries, blueberries)', '/uploads/foods/5b82c933b01b3.jpeg', 10, NULL, NULL),
(50, 8, 'Melons (e.g. watermelon, honeydew)', '/uploads/foods/5b82c9c18e18a.jpeg', 10, NULL, NULL),
(51, 8, 'Orange', '/uploads/foods/5b82ca530fe32.jpeg', 10, NULL, NULL),
(52, 8, 'Tropical fruits (e.g. mango, guava)', '/uploads/foods/5b82caf48af1f.jpeg', 10, NULL, NULL),
(56, 9, 'Deep fried fish', '/uploads/foods/5b83a6b83a909.jpeg', 10, NULL, NULL),
(57, 9, 'Stir-fry or steamed fish', '/uploads/foods/5b83a706ce722.jpeg', 10, NULL, NULL),
(59, 9, 'Deep fried meat', '/uploads/foods/5b83a9065af59.jpeg', 10, NULL, NULL),
(60, 9, 'Stir-fry or steamed meat', '/uploads/foods/5b83a9a857d3b.jpeg', 10, NULL, NULL),
(61, 9, 'Curry meat or fish', '/uploads/foods/5b83aa2d02979.jpeg', 10, NULL, NULL),
(63, 9, 'Chicken nuggets or sausages or meatballs', '/uploads/foods/5bc9797718cd9.jpg', 10, NULL, NULL),
(64, 9, 'Ham or luncheon meat', '/uploads/foods/5bc979d322e2c.jpg', 10, NULL, NULL),
(67, 9, 'Prawn or other shellfish', '/uploads/foods/5b83addd1695d.jpeg', 10, NULL, NULL),
(70, 10, 'Dry noodles (e.g. wanton noodles, mee goreng)', '/uploads/foods/5b83af0c5adb5.jpeg', 10, NULL, NULL),
(71, 10, 'Instant noodles', '/uploads/foods/5b83afbf491d5.jpeg', 10, NULL, NULL),
(72, 10, 'Noodles in soup (e.g. beehoon soup, mee soto)', '/uploads/foods/5b83b054ae6e7.jpeg', 10, NULL, NULL),
(73, 10, 'Noodles with gravy (e.g. mee rebus, hor fun)', '/uploads/foods/5b83b1be157af.jpeg', 10, NULL, NULL),
(74, 10, 'Pasta in soup (e.g. macaroni or alphabet soup) ', '/uploads/foods/5b83b24703edb.jpeg', 10, NULL, NULL),
(75, 10, 'Pasta with sauce (e.g. bolognese, carbonara)', '/uploads/foods/5b83b2c014abe.jpeg', 10, NULL, NULL),
(78, 12, 'Brown rice', '/uploads/foods/5b83fe9a76432.jpeg', 10, NULL, NULL),
(79, 12, 'Fried rice', '/uploads/foods/5b83ffc76ece7.jpeg', 10, NULL, NULL),
(80, 12, 'Nasi lemak', '/uploads/foods/5b84016b9d79b.jpeg', 10, NULL, NULL),
(81, 12, 'Chicken or duck rice', '/uploads/foods/5b840337133f6.jpeg', 10, NULL, NULL),
(82, 12, 'Mixed white and brown rice', '/uploads/foods/5b840480660fc.jpeg', 10, NULL, NULL),
(83, 12, 'Plain porridge', '/uploads/foods/5b840714e6127.jpeg', 10, NULL, NULL),
(84, 12, 'Chicken or fish porridge', '/uploads/foods/5b8409c34d66c.jpeg', 10, NULL, NULL),
(85, 12, 'Sushi', '/uploads/foods/5b840af46a339.jpeg', 10, NULL, NULL),
(86, 12, 'White rice', '/uploads/foods/5b840c896478b.jpeg', 10, NULL, NULL),
(90, 15, 'Broccoli or cauliflower', '/uploads/foods/5b841426bb54b.jpeg', 10, NULL, NULL),
(94, 15, 'Dark green leafy vegetables (e.g. spinach, kailan)', '/uploads/foods/5b84189c667ff.jpeg', 10, NULL, NULL),
(99, 15, 'Peas and green beans', '/uploads/foods/5b841d47ef1e2.jpeg', 10, NULL, NULL),
(100, 15, 'Pumpkin ', '/uploads/foods/5bc97cd7582ea.jpg', 10, NULL, NULL),
(104, 16, 'Semi-skimmed milk', '/uploads/foods/5bc96fe204d34.jpg', 10, NULL, NULL),
(105, 16, 'Chocolate or strawberry milk', '/uploads/foods/5bc970be1a3ab.jpg', 10, NULL, NULL),
(106, 16, 'Yogurt drink or milk shake', '/uploads/foods/5bc973890c95e.jpg', 10, NULL, NULL),
(107, 10, 'Boiled or cooked potatoes (e.g. mashed or sweet potato)', '/uploads/foods/5bc975edd6dfa.jpg', 10, NULL, NULL),
(108, 10, 'French fries and hash brown', '/uploads/foods/5bc976b2322c0.jpg', 10, NULL, NULL),
(109, 6, 'Tofu', '/uploads/foods/5bc9775b5345a.jpg', 10, NULL, NULL),
(110, 9, 'Fishball or fishcake or crabstick', '/uploads/foods/5bc97a4badf45.jpg', 10, NULL, NULL),
(111, 15, 'Lentils and other beans (e.g. chickpeas, broad beans)', '/uploads/foods/5bc97b72d4e91.jpg', 10, NULL, NULL),
(112, 15, 'Light green vegetables (e.g. cabbage, beansprouts)', '/uploads/foods/5bc97c11e952a.jpg', 10, NULL, NULL),
(113, 15, 'Yellow, orange, red vegetables (e.g. corn, carrot, tomato)', '/uploads/foods/5bc97cfbc964d.jpg', 10, NULL, NULL),
(114, 8, 'Peach or plum', '/uploads/foods/5bc98056ca985.jpg', 10, NULL, NULL),
(115, 1, 'Sports drinks (e.g. 100 PlusÂ®, H-Two-OÂ®)', '/uploads/foods/5bc984e39edf5.jpg', 10, NULL, NULL),
(116, 1, 'Barley or Green Tea or Chrysanthemum Tea', '/uploads/foods/5bc98591d7122.jpg', 10, NULL, NULL),
(117, 1, 'Malt beverages (e.g. MiloÂ®, OvaltineÂ®)', '/uploads/foods/5bd13cbd0ad0e.jpg', 10, NULL, NULL),
(119, 14, 'Multivitamins', '/uploads/foods/5bd7f0ee71c98.png', 10, NULL, NULL),
(120, 14, 'Vitamin C', '/uploads/foods/5bd7f109a8a3f.png', 10, NULL, NULL),
(121, 14, 'Fish oil', '/uploads/foods/5bd7f13ddf4c9.png', 10, NULL, NULL),
(122, 14, 'Probiotics', '/uploads/foods/5bd7f14ded106.png', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `food_categories`
--

CREATE TABLE `food_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `food_type` int(11) NOT NULL DEFAULT '1' COMMENT '1. Food, 2. Drinks, 3. Fruits, 4. Deserts',
  `sequence` int(11) NOT NULL DEFAULT '10',
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_categories`
--

INSERT INTO `food_categories` (`id`, `name`, `food_type`, `sequence`, `created_date`) VALUES
(1, 'Drinks', 2, 10, NULL),
(3, 'Breads, Spreads and Cereals', 1, 10, NULL),
(5, 'Snacks and Desserts', 4, 10, NULL),
(6, 'Eggs and Tofu', 1, 10, NULL),
(7, 'Fast food', 1, 10, NULL),
(8, 'Fruits', 3, 10, NULL),
(9, 'Meat and Fish', 1, 10, NULL),
(10, 'Noodles, Pasta and Potatoes', 1, 10, NULL),
(12, 'Rice and Porridge', 1, 10, NULL),
(14, 'Supplements', 1, 10, NULL),
(15, 'Vegetables', 1, 10, NULL),
(16, 'Milk, Cheese and Yogurt', 2, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `food_portions`
--

CREATE TABLE `food_portions` (
  `id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `portion` float(10,2) NOT NULL,
  `measurement` varchar(50) NOT NULL,
  `portion_image` varchar(500) DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '10',
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_portions`
--

INSERT INTO `food_portions` (`id`, `food_id`, `portion`, `measurement`, `portion_image`, `sequence`, `created_date`) VALUES
(6, 6, 1.00, 'teasponn', '/uploads/food_portions/5b80fa169d87b.jpg', 10, NULL),
(7, 6, 1.50, 'teasponn', '/uploads/food_portions/5b80fa2ef37ae.jpg', 10, NULL),
(8, 6, 2.00, 'teasponn', '/uploads/food_portions/5b80fa4297ab6.jpg', 10, NULL),
(9, 6, 3.00, 'teasponn', '/uploads/food_portions/5b80fa50c9525.jpg', 10, NULL),
(10, 7, 0.25, 'apple / pear', '/uploads/food_portions/5b80fc2f12c07.jpg', 10, NULL),
(11, 7, 0.50, 'apple / pear', '/uploads/food_portions/5b80fc3bb954d.jpg', 10, NULL),
(12, 7, 0.75, 'apple / pear', '/uploads/food_portions/5b80fc4ca56d6.jpg', 10, NULL),
(13, 7, 1.00, 'apple / pear', '/uploads/food_portions/5b80fc59a09d4.jpg', 10, NULL),
(14, 8, 1.00, 'piece', '/uploads/food_portions/5b80fdb9ebc87.JPG', 10, NULL),
(15, 8, 3.00, 'pieces', '/uploads/food_portions/5b80fdc88d1ed.JPG', 10, NULL),
(16, 8, 6.00, 'pieces', '/uploads/food_portions/5b80fdd9df69e.JPG', 10, NULL),
(21, 4, 0.50, 'cup', '/uploads/food_portions/5b81711196e9c.jpeg', 10, NULL),
(22, 4, 1.00, 'cup', '/uploads/food_portions/5b81717207138.jpeg', 10, NULL),
(23, 4, 1.50, 'cup', '/uploads/food_portions/5b8171920064a.jpeg', 10, NULL),
(24, 4, 2.00, 'cup', '/uploads/food_portions/5b8171a9e017a.jpeg', 10, NULL),
(31, 11, 0.50, 'cup', '/uploads/food_portions/5b8175a27b986.jpeg', 10, NULL),
(32, 11, 1.00, 'cup', '/uploads/food_portions/5b8175bd45da4.jpeg', 10, NULL),
(33, 11, 1.50, 'cups', '/uploads/food_portions/5b8175dbb4b61.jpeg', 10, NULL),
(34, 11, 2.00, 'cups', '/uploads/food_portions/5b81760eb7186.jpeg', 10, NULL),
(39, 13, 0.50, 'cup / packet', '/uploads/food_portions/5b81773c6641a.jpeg', 10, NULL),
(40, 13, 1.00, 'cup / packet', '/uploads/food_portions/5b81775d2e2f5.jpeg', 10, NULL),
(41, 13, 1.50, 'cups / packets', '/uploads/food_portions/5b81779533ffc.jpeg', 10, NULL),
(42, 13, 2.00, 'cups / packets', '/uploads/food_portions/5b8177abc9618.jpeg', 10, NULL),
(43, 14, 0.50, 'cup / packet', '/uploads/food_portions/5b81781fb989b.jpeg', 10, NULL),
(44, 14, 1.00, 'cup / packet', '/uploads/food_portions/5b8178357f0b4.jpeg', 10, NULL),
(45, 14, 1.50, 'cups / packets', '/uploads/food_portions/5b817890934d4.jpeg', 10, NULL),
(46, 14, 2.00, 'cups / packets', '/uploads/food_portions/5b8178c77341d.jpeg', 10, NULL),
(51, 17, 0.50, 'piece', '/uploads/food_portions/5b817b21eaeca.jpeg', 10, NULL),
(52, 17, 1.00, 'piece', '/uploads/food_portions/5b817b34d1662.jpeg', 10, NULL),
(53, 17, 2.00, 'pieces', '/uploads/food_portions/5b817b58a7270.jpeg', 10, NULL),
(54, 17, 3.00, 'pieces', '/uploads/food_portions/5b817b6e03329.jpeg', 10, NULL),
(55, 18, 0.50, 'piece', '/uploads/food_portions/5b817c0ee4973.jpeg', 10, NULL),
(56, 18, 1.00, 'piece', '/uploads/food_portions/5b817ca488d40.jpeg', 10, NULL),
(57, 18, 2.00, 'pieces', '/uploads/food_portions/5b817cbc780e2.jpeg', 10, NULL),
(58, 18, 3.00, 'pieces', '/uploads/food_portions/5b817cd04d956.jpeg', 10, NULL),
(63, 22, 1.00, 'slice', '/uploads/food_portions/5b81812e8eda5.jpeg', 10, NULL),
(64, 22, 1.50, 'slices', '/uploads/food_portions/5b81814f602b4.jpeg', 10, NULL),
(65, 22, 2.00, 'slices', '/uploads/food_portions/5b81816c34838.jpeg', 10, NULL),
(66, 22, 3.00, 'slices', '/uploads/food_portions/5b81818039d12.jpeg', 10, NULL),
(67, 23, 0.50, 'slice', '/uploads/food_portions/5b8181f829256.jpeg', 10, NULL),
(68, 23, 1.00, 'slice', '/uploads/food_portions/5b81822166bd2.jpeg', 10, NULL),
(69, 23, 1.50, 'slices', '/uploads/food_portions/5b81824173357.jpeg', 10, NULL),
(70, 23, 2.00, 'slices', '/uploads/food_portions/5b818266940ee.jpeg', 10, NULL),
(74, 25, 0.50, 'rice bowl', '/uploads/food_portions/5b81847409b68.jpeg', 10, NULL),
(76, 25, 0.75, 'rice bowl', '/uploads/food_portions/5b81849ba79c3.jpeg', 10, NULL),
(77, 25, 1.00, 'rice bowl', '/uploads/food_portions/5b8184bbd4cd1.jpeg', 10, NULL),
(78, 25, 1.50, 'rice bowl', '/uploads/food_portions/5b8184d82ecc8.jpeg', 10, NULL),
(79, 8, 9.00, 'pieces', '/uploads/food_portions/5b81855198e7f.jpeg', 10, NULL),
(85, 27, 1.00, 'slice', '/uploads/food_portions/5b823f70b7cff.jpeg', 10, NULL),
(86, 27, 2.00, 'slices', '/uploads/food_portions/5b823fca10bea.jpeg', 10, NULL),
(87, 27, 3.00, 'slices', '/uploads/food_portions/5b823ffe3a901.jpeg', 10, NULL),
(88, 27, 4.00, 'slices', '/uploads/food_portions/5b82413789481.jpeg', 10, NULL),
(89, 28, 0.50, 'small packet', '/uploads/food_portions/5b824191146b2.jpeg', 10, NULL),
(90, 28, 0.75, 'small packet', '/uploads/food_portions/5b8241a75bddf.jpeg', 10, NULL),
(91, 28, 1.00, 'small packet', '/uploads/food_portions/5b8241e4d5a34.jpeg', 10, NULL),
(92, 28, 1.50, 'small packets', '/uploads/food_portions/5b82424e0b3d5.jpeg', 10, NULL),
(93, 29, 1.00, 'piece / mini bar/ sncak size', '/uploads/food_portions/5b8243c619329.jpeg', 10, NULL),
(94, 29, 2.00, 'pieces / mini bars/ sncak sizes', '/uploads/food_portions/5b8243ee3f0ef.jpeg', 10, NULL),
(95, 29, 3.00, 'pieces / mini bars/ sncak sizes', '/uploads/food_portions/5b82444175573.jpeg', 10, NULL),
(96, 29, 4.00, 'pieces / mini bars/ sncak sizes', '/uploads/food_portions/5b82446c63345.jpeg', 10, NULL),
(97, 30, 0.25, 'standard portion', '/uploads/food_portions/5b8244c27134b.jpeg', 10, NULL),
(98, 30, 0.50, 'standard portion', '/uploads/food_portions/5b8244e847f20.jpeg', 10, NULL),
(99, 30, 0.75, 'standard portion', '/uploads/food_portions/5b8245059a8d4.jpeg', 10, NULL),
(100, 30, 1.00, 'standard portion', '/uploads/food_portions/5b82452773cd0.jpeg', 10, NULL),
(113, 34, 0.50, 'scoop', '/uploads/food_portions/5b825ba783881.jpeg', 10, NULL),
(114, 34, 1.00, 'scoop', '/uploads/food_portions/5b825bbe85f9f.jpeg', 10, NULL),
(115, 34, 2.00, 'scoops', '/uploads/food_portions/5b825bf557d01.jpeg', 10, NULL),
(116, 34, 3.00, 'scoops', '/uploads/food_portions/5b825c0f1f199.jpeg', 10, NULL),
(117, 35, 1.00, 'piece', '/uploads/food_portions/5b825d3040433.jpeg', 10, NULL),
(118, 35, 2.00, 'pieces', '/uploads/food_portions/5b825d479f071.jpeg', 10, NULL),
(119, 35, 3.00, 'pieces', '/uploads/food_portions/5b825d5e70f23.jpeg', 10, NULL),
(120, 35, 4.00, 'pieces', '/uploads/food_portions/5b825d7bda33b.jpeg', 10, NULL),
(126, 37, 10.00, 'pieces', '/uploads/food_portions/5b8261993159d.jpeg', 10, NULL),
(127, 37, 15.00, 'pieces', '/uploads/food_portions/5b8261b2c57f5.jpeg', 10, NULL),
(128, 37, 20.00, 'pieces', '/uploads/food_portions/5b8261f925079.jpeg', 10, NULL),
(129, 37, 30.00, 'pieces', '/uploads/food_portions/5b82620b706a0.jpeg', 10, NULL),
(130, 38, 0.50, 'piece', '/uploads/food_portions/5b8262a5c67ac.jpeg', 10, NULL),
(131, 38, 1.00, 'piece', '/uploads/food_portions/5b8262bd5978b.jpeg', 10, NULL),
(132, 38, 1.50, 'pieces', '/uploads/food_portions/5b8262d68d73f.jpeg', 10, NULL),
(133, 38, 2.00, 'pieces', '/uploads/food_portions/5b8262efd5f0a.jpeg', 10, NULL),
(138, 42, 0.50, 'piece', '/uploads/food_portions/5b82c58092e17.jpeg', 10, NULL),
(139, 42, 1.00, 'piece', '/uploads/food_portions/5b82c5a2b8f99.jpeg', 10, NULL),
(140, 42, 1.50, 'pieces', '/uploads/food_portions/5b82c5b72764e.jpeg', 10, NULL),
(141, 42, 2.00, 'pieces', '/uploads/food_portions/5b82c5cc065a3.jpeg', 10, NULL),
(142, 43, 0.50, 'piece', '/uploads/food_portions/5b82c60e25d33.jpeg', 10, NULL),
(143, 43, 1.00, 'piece', '/uploads/food_portions/5b82c624b0eda.jpeg', 10, NULL),
(144, 43, 1.50, 'pieces', '/uploads/food_portions/5b82c63b67ecb.jpeg', 10, NULL),
(145, 43, 2.00, 'pieces', '/uploads/food_portions/5b82c65063cf6.jpeg', 10, NULL),
(148, 46, 1.00, 'slice', '/uploads/food_portions/5b82c773aa1fc.jpeg', 10, NULL),
(149, 46, 2.00, 'slices', '/uploads/food_portions/5b82c78816d01.jpeg', 10, NULL),
(150, 46, 3.00, 'slices', '/uploads/food_portions/5b82c79dda073.jpeg', 10, NULL),
(151, 46, 4.00, 'slices', '/uploads/food_portions/5b82c7b7c50b8.jpeg', 10, NULL),
(152, 48, 0.50, 'banana', '/uploads/food_portions/5b82c8bc33480.jpeg', 10, NULL),
(153, 48, 0.75, 'banana', '/uploads/food_portions/5b82c8d2a903d.jpeg', 10, NULL),
(154, 48, 1.00, 'banana', '/uploads/food_portions/5b82c8e61eda4.jpeg', 10, NULL),
(155, 48, 2.00, 'bananas', '/uploads/food_portions/5b82c8fe8bcf7.jpeg', 10, NULL),
(156, 49, 5.00, 'pieces', '/uploads/food_portions/5b82c9577cfbc.jpeg', 10, NULL),
(157, 49, 10.00, 'pieces', '/uploads/food_portions/5b82c96d4cc5b.jpeg', 10, NULL),
(158, 49, 15.00, 'pieces', '/uploads/food_portions/5b82c98403aba.jpeg', 10, NULL),
(159, 49, 20.00, 'pieces', '/uploads/food_portions/5b82c996c7448.jpeg', 10, NULL),
(160, 50, 0.50, 'wedge', '/uploads/food_portions/5b82c9f7e563e.jpeg', 10, NULL),
(161, 50, 0.75, 'wedge', '/uploads/food_portions/5b82ca0f86af3.jpeg', 10, NULL),
(162, 50, 1.00, 'wedge', '/uploads/food_portions/5b82ca1ff3cce.jpeg', 10, NULL),
(163, 50, 2.00, 'wedges', '/uploads/food_portions/5b82ca347ca8d.jpeg', 10, NULL),
(164, 51, 0.25, 'orange', '/uploads/food_portions/5b82ca83c171f.jpeg', 10, NULL),
(165, 51, 0.50, 'orange', '/uploads/food_portions/5b82ca9d1a38a.jpeg', 10, NULL),
(166, 51, 0.75, 'orange', '/uploads/food_portions/5b82cab3851f0.jpeg', 10, NULL),
(167, 51, 1.00, 'orange', '/uploads/food_portions/5b82cac815dc6.jpeg', 10, NULL),
(168, 52, 0.50, 'wedge', '/uploads/food_portions/5b82cb1d4cc58.jpeg', 10, NULL),
(169, 52, 0.75, 'wedge', '/uploads/food_portions/5b82cb3053d39.jpeg', 10, NULL),
(170, 52, 1.00, 'wedge', '/uploads/food_portions/5b82cb413dfbf.jpeg', 10, NULL),
(171, 52, 2.00, 'wedges', '/uploads/food_portions/5b82cb557b07e.jpeg', 10, NULL),
(183, 56, 0.25, 'piece', '/uploads/food_portions/5b83a74096de0.jpeg', 10, NULL),
(184, 56, 0.50, 'piece', '/uploads/food_portions/5b83a756aa2c9.jpeg', 10, NULL),
(185, 56, 0.75, 'piece', '/uploads/food_portions/5b83a78196976.jpeg', 10, NULL),
(186, 56, 1.00, 'piece', '/uploads/food_portions/5b83a7a606099.jpeg', 10, NULL),
(187, 57, 0.25, 'piece', '/uploads/food_portions/5b83a7d4d17fd.jpeg', 10, NULL),
(188, 57, 0.50, 'piece', '/uploads/food_portions/5b83a7f4df986.jpeg', 10, NULL),
(189, 57, 0.75, 'piece', '/uploads/food_portions/5b83a80a79eac.jpeg', 10, NULL),
(190, 57, 1.00, 'piece', '/uploads/food_portions/5b83a81b156aa.jpeg', 10, NULL),
(195, 59, 0.25, 'piece', '/uploads/food_portions/5b83a93fe14a3.jpeg', 10, NULL),
(196, 59, 0.50, 'piece', '/uploads/food_portions/5b83a94db7366.jpeg', 10, NULL),
(197, 59, 0.75, 'piece', '/uploads/food_portions/5b83a9665f6bd.jpeg', 10, NULL),
(198, 59, 1.00, 'piece', '/uploads/food_portions/5b83a9757ff2b.jpeg', 10, NULL),
(199, 60, 1.00, 'dessert spoon', '/uploads/food_portions/5b83a9d1ef632.jpeg', 10, NULL),
(200, 60, 3.00, 'dessert spoons', '/uploads/food_portions/5b83a9e8c2080.jpeg', 10, NULL),
(201, 60, 5.00, 'dessert spoons', '/uploads/food_portions/5b83a9ffd31ce.jpeg', 10, NULL),
(202, 60, 7.00, 'dessert spoons', '/uploads/food_portions/5b83aa1746008.jpeg', 10, NULL),
(203, 61, 3.00, 'dessert spoons', '/uploads/food_portions/5b83aa5da1f1f.jpeg', 10, NULL),
(204, 61, 5.00, 'dessert spoons', '/uploads/food_portions/5b83aa732ca2a.jpeg', 10, NULL),
(205, 61, 7.00, 'dessert spoons', '/uploads/food_portions/5b83aa8540abf.jpeg', 10, NULL),
(206, 61, 9.00, 'dessert spoons', '/uploads/food_portions/5b83aa9788bfd.jpeg', 10, NULL),
(226, 70, 0.50, 'rice bowl', '/uploads/food_portions/5b83af4b0d8fe.jpeg', 10, NULL),
(227, 70, 0.75, 'rice bowl', '/uploads/food_portions/5b83af59e98c5.jpeg', 10, NULL),
(228, 70, 1.00, 'rice bowl', '/uploads/food_portions/5b83af69cf763.jpeg', 10, NULL),
(229, 70, 1.50, 'rice bowls', '/uploads/food_portions/5b83af858e4a4.jpeg', 10, NULL),
(230, 71, 0.50, 'rice bowl', '/uploads/food_portions/5b83afe67018f.jpeg', 10, NULL),
(231, 71, 0.75, 'rice bowl', '/uploads/food_portions/5b83b008472be.jpeg', 10, NULL),
(232, 71, 1.00, 'rice bowl', '/uploads/food_portions/5b83b01bc9f45.jpeg', 10, NULL),
(233, 71, 1.50, 'rice bowls', '/uploads/food_portions/5b83b038e69e5.jpeg', 10, NULL),
(234, 72, 0.50, 'rice bowl', '/uploads/food_portions/5b83b07c6806d.jpeg', 10, NULL),
(235, 72, 0.75, 'rice bowl', '/uploads/food_portions/5b83b08fb47c9.jpeg', 10, NULL),
(236, 72, 1.00, 'rice bowl', '/uploads/food_portions/5b83b0a406a64.jpeg', 10, NULL),
(237, 72, 1.50, 'rice bowls', '/uploads/food_portions/5b83b0d87605b.jpeg', 10, NULL),
(238, 73, 0.50, 'rice bowl', '/uploads/food_portions/5b83b1e695c7b.jpeg', 10, NULL),
(239, 73, 0.75, 'rice bowl', '/uploads/food_portions/5b83b1f8274bf.jpeg', 10, NULL),
(240, 73, 1.00, 'rice bowl', '/uploads/food_portions/5b83b21031422.jpeg', 10, NULL),
(241, 73, 1.50, 'rice bowls', '/uploads/food_portions/5b83b22562b4d.jpeg', 10, NULL),
(242, 74, 0.50, 'rice bowl', '/uploads/food_portions/5b83b2656cc51.jpeg', 10, NULL),
(243, 74, 0.75, 'rice bowl', '/uploads/food_portions/5b83b2775c188.jpeg', 10, NULL),
(244, 74, 1.00, 'rice bowl', '/uploads/food_portions/5b83b28e79923.jpeg', 10, NULL),
(245, 74, 1.50, 'rice bowls', '/uploads/food_portions/5b83b2a00cb54.jpeg', 10, NULL),
(246, 75, 0.50, 'rice bowl', '/uploads/food_portions/5b83b2d5f1cc8.jpeg', 10, NULL),
(247, 75, 0.75, 'rice bowl', '/uploads/food_portions/5b83b2e64d738.jpeg', 10, NULL),
(248, 75, 1.00, 'rice bowl', '/uploads/food_portions/5b83b2f8451ae.jpeg', 10, NULL),
(249, 75, 1.50, 'rice bowl', '/uploads/food_portions/5b83b3112f046.jpeg', 10, NULL),
(254, 78, 0.50, 'rice bowl', '/uploads/food_portions/5b83feb09e52a.jpeg', 10, NULL),
(255, 78, 0.75, 'rice bowl', '/uploads/food_portions/5b83fec279cf8.jpeg', 10, NULL),
(256, 78, 1.00, 'rice bowl', '/uploads/food_portions/5b83fedc7e8a4.jpeg', 10, NULL),
(257, 79, 0.50, 'rice bowl', '/uploads/food_portions/5b8400c571db4.jpeg', 10, NULL),
(258, 79, 0.75, 'rice bowl', '/uploads/food_portions/5b8400e359e77.jpeg', 10, NULL),
(259, 79, 1.00, 'rice bowl', '/uploads/food_portions/5b8401082fc95.jpeg', 10, NULL),
(260, 79, 1.50, 'rice bowls', '/uploads/food_portions/5b8401409847f.jpeg', 10, NULL),
(261, 80, 0.50, 'rice bowl', '/uploads/food_portions/5b8401c1853a1.jpeg', 10, NULL),
(262, 80, 0.75, 'rice bowl', '/uploads/food_portions/5b84024f9614c.jpeg', 10, NULL),
(263, 80, 1.00, 'rice bowl', '/uploads/food_portions/5b8402e39bc9b.jpeg', 10, NULL),
(264, 80, 1.50, 'rice bowls', '/uploads/food_portions/5b8403122bd38.jpeg', 10, NULL),
(265, 81, 0.50, 'rice bowl', '/uploads/food_portions/5b840363e1b31.jpeg', 10, NULL),
(266, 81, 0.75, 'rice bowl', '/uploads/food_portions/5b84038596530.jpeg', 10, NULL),
(267, 81, 1.00, 'rice bowl', '/uploads/food_portions/5b84042dcaabf.jpeg', 10, NULL),
(268, 81, 1.50, 'rice bowls', '/uploads/food_portions/5b840454dadbe.jpeg', 10, NULL),
(269, 82, 0.50, 'rice bowl', '/uploads/food_portions/5b84059f55c22.jpeg', 10, NULL),
(270, 82, 0.75, 'rice bowl', '/uploads/food_portions/5b8405faae5e3.jpeg', 10, NULL),
(271, 82, 1.00, 'rice bowl', '/uploads/food_portions/5b84064c60d2b.jpeg', 10, NULL),
(272, 82, 1.50, 'rice bowls', '/uploads/food_portions/5b8406829f702.jpeg', 10, NULL),
(273, 83, 0.50, 'rice bowl', '/uploads/food_portions/5b84074802894.jpeg', 10, NULL),
(274, 83, 0.75, 'rice bowl', '/uploads/food_portions/5b84082492dfe.jpeg', 10, NULL),
(275, 83, 1.00, 'rice bowl', '/uploads/food_portions/5b8408e8312ae.jpeg', 10, NULL),
(276, 83, 1.50, 'rice bowls', '/uploads/food_portions/5b840928b3f97.jpeg', 10, NULL),
(277, 84, 0.50, 'rice bowl', '/uploads/food_portions/5b8409f25fe25.jpeg', 10, NULL),
(278, 84, 0.75, 'rice bowl', '/uploads/food_portions/5b840a3043711.jpeg', 10, NULL),
(279, 84, 1.00, 'rice bowl', '/uploads/food_portions/5b840a6c6d8f3.jpeg', 10, NULL),
(280, 84, 1.50, 'rice bowls', '/uploads/food_portions/5b840ab02c3ce.jpeg', 10, NULL),
(281, 85, 3.00, 'maki', '/uploads/food_portions/5b840b2fdd8be.jpeg', 10, NULL),
(282, 85, 6.00, 'maki', '/uploads/food_portions/5b840b8a2f616.jpeg', 10, NULL),
(283, 85, 12.00, 'maki', '/uploads/food_portions/5b840bbd539a9.jpeg', 10, NULL),
(284, 85, 18.00, 'maki', '/uploads/food_portions/5b840c2521dcd.jpeg', 10, NULL),
(285, 86, 0.50, 'rice bowl', '/uploads/food_portions/5b840caa7ebd8.jpeg', 10, NULL),
(286, 86, 0.75, 'rice bowl', '/uploads/food_portions/5b840d3471a31.jpeg', 10, NULL),
(287, 86, 1.00, 'rice bowl', '/uploads/food_portions/5b840d85228fd.jpeg', 10, NULL),
(288, 86, 1.50, 'rice bowls', '/uploads/food_portions/5b840e09606f7.jpeg', 10, NULL),
(301, 90, 2.00, 'dessert spoons', '/uploads/food_portions/5b84147944e14.jpeg', 10, NULL),
(302, 90, 4.00, 'dessert spoons', '/uploads/food_portions/5b84151146f40.jpeg', 10, NULL),
(303, 90, 6.00, 'dessert spoons', '/uploads/food_portions/5b8415bc593e3.jpeg', 10, NULL),
(304, 90, 8.00, 'dessert spoons', '/uploads/food_portions/5b841602920db.jpeg', 10, NULL),
(317, 94, 2.00, 'dessert spoons', '/uploads/food_portions/5b8418bec13c6.jpeg', 10, NULL),
(318, 94, 4.00, 'dessert spoons', '/uploads/food_portions/5b84191c17175.jpeg', 10, NULL),
(319, 94, 8.00, 'dessert spoons', '/uploads/food_portions/5b8419444a961.jpeg', 10, NULL),
(320, 94, 12.00, 'dessert spoons', '/uploads/food_portions/5b841971ad324.jpeg', 10, NULL),
(337, 99, 2.00, 'dessert spoons', '/uploads/food_portions/5bd7ecc80b06d.jpg', 10, NULL),
(338, 99, 4.00, 'dessert spoons', '/uploads/food_portions/5bd7ecda81557.jpg', 10, NULL),
(339, 99, 8.00, 'dessert spoons', '/uploads/food_portions/5bd7ece6a69d2.jpg', 10, NULL),
(340, 99, 12.00, 'dessert spoons', '/uploads/food_portions/5bd7ecf4edee1.jpg', 10, NULL),
(341, 100, 3.00, 'dessert spoons', '/uploads/food_portions/5b841e0e897b1.jpeg', 10, NULL),
(342, 100, 5.00, 'dessert spoons', '/uploads/food_portions/5b841e56081a4.jpeg', 10, NULL),
(343, 100, 7.00, 'dessert spoons', '/uploads/food_portions/5b841e7cc66c5.jpeg', 10, NULL),
(344, 100, 10.00, 'dessert spoons', '/uploads/food_portions/5b841ea671503.jpeg', 10, NULL),
(358, 12, 0.50, 'cup', '/uploads/food_portions/5bc96e449be65.jpg', 10, NULL),
(359, 12, 1.00, 'cup', '/uploads/food_portions/5bc96f4db9c08.jpg', 10, NULL),
(360, 12, 1.50, 'cup', '/uploads/food_portions/5bc96f950037d.jpg', 10, NULL),
(361, 12, 2.00, 'cup', '/uploads/food_portions/5bc96f73136f6.jpg', 10, NULL),
(362, 104, 0.50, 'cup', '/uploads/food_portions/5bc96ff9dcc6a.jpg', 10, NULL),
(363, 104, 1.00, 'cup', '/uploads/food_portions/5bc97007eeeea.jpg', 10, NULL),
(364, 104, 1.50, 'cup', '/uploads/food_portions/5bc97016bae8c.jpg', 10, NULL),
(365, 104, 2.00, 'cup', '/uploads/food_portions/5bc9702611f3e.jpg', 10, NULL),
(366, 105, 0.50, 'cup', '/uploads/food_portions/5bc9725145d98.jpg', 10, NULL),
(367, 105, 1.00, 'cup', '/uploads/food_portions/5bc97263b08ec.jpg', 10, NULL),
(368, 105, 1.50, 'cup', '/uploads/food_portions/5bc972701bf1c.jpg', 10, NULL),
(369, 105, 2.00, 'cup', '/uploads/food_portions/5bc9727ec655f.jpg', 10, NULL),
(370, 106, 0.50, 'cup', '/uploads/food_portions/5bc973a7f1023.jpg', 10, NULL),
(371, 106, 1.00, 'cup', '/uploads/food_portions/5bc973b5ba7de.jpg', 10, NULL),
(372, 106, 1.50, 'cup', '/uploads/food_portions/5bc973c6cf6fb.jpg', 10, NULL),
(373, 106, 2.00, 'cup', '/uploads/food_portions/5bc973d2d53bc.jpg', 10, NULL),
(374, 107, 3.00, 'dessert spoons', '/uploads/food_portions/5bc9761c0f10b.jpg', 10, NULL),
(375, 107, 5.00, 'dessert spoons', '/uploads/food_portions/5bc97648a823f.jpg', 10, NULL),
(376, 107, 7.00, 'dessert spoons', '/uploads/food_portions/5bc97657f32fa.jpg', 10, NULL),
(377, 107, 10.00, 'dessert spoons', '/uploads/food_portions/5bc976692d965.jpg', 10, NULL),
(378, 109, 0.50, 'piece', '/uploads/food_portions/5bc977943b800.jpg', 10, NULL),
(379, 109, 1.00, 'piece', '/uploads/food_portions/5bc977a314b53.jpg', 10, NULL),
(380, 109, 1.50, 'pieces', '/uploads/food_portions/5bc977c6cf315.jpg', 10, NULL),
(381, 109, 2.00, 'pieces', '/uploads/food_portions/5bc977d4981dd.jpg', 10, NULL),
(382, 112, 2.00, 'dessert spoons', '/uploads/food_portions/5bc97c3cb5ce5.jpg', 10, NULL),
(383, 112, 4.00, 'dessert spoons', '/uploads/food_portions/5bc97c4ad4fe2.jpg', 10, NULL),
(384, 112, 8.00, 'dessert spoons', '/uploads/food_portions/5bc97c5d7bb0d.jpg', 10, NULL),
(385, 112, 12.00, 'dessert spoons', '/uploads/food_portions/5bc97c6b71c52.jpg', 10, NULL),
(386, 113, 3.00, 'dessert spoons', '/uploads/food_portions/5bc97d1b7e95d.jpg', 10, NULL),
(387, 113, 5.00, 'dessert spoons', '/uploads/food_portions/5bc97dbf1b258.jpg', 10, NULL),
(388, 113, 7.00, 'dessert spoons', '/uploads/food_portions/5bc97ddd4bc91.jpg', 10, NULL),
(389, 113, 10.00, 'dessert spoons', '/uploads/food_portions/5bc97df72479d.jpg', 10, NULL),
(390, 114, 0.50, 'stone fruit', '/uploads/food_portions/5bc9817016f40.jpg', 10, NULL),
(391, 111, 5.00, 'dessert spoons', '/uploads/food_portions/5bc98177bca98.jpg', 10, NULL),
(392, 114, 1.00, 'stone fruit', '/uploads/food_portions/5bc9818c6f3cb.jpg', 10, NULL),
(393, 111, 8.00, 'dessert spoons', '/uploads/food_portions/5bc98193d92cb.jpg', 10, NULL),
(394, 114, 1.50, 'stone fruit', '/uploads/food_portions/5bc981a3a9f1c.jpg', 10, NULL),
(395, 111, 10.00, 'dessert spoons', '/uploads/food_portions/5bc981aa06dbc.jpg', 10, NULL),
(396, 114, 2.00, 'stone fruit', '/uploads/food_portions/5bc981b90a6e6.jpg', 10, NULL),
(397, 111, 15.00, 'dessert spoons', '/uploads/food_portions/5bc981bbe37ab.jpg', 10, NULL),
(398, 115, 0.50, 'cup / packet', '/uploads/food_portions/5bc984fce7292.jpg', 10, NULL),
(399, 115, 1.00, 'cup / packet', '/uploads/food_portions/5bc9850c2e99d.jpg', 10, NULL),
(400, 115, 1.50, 'cup / packet', '/uploads/food_portions/5bc9851d48bc6.jpg', 10, NULL),
(401, 115, 2.00, 'cup / packet', '/uploads/food_portions/5bc98529ed0f8.jpg', 10, NULL),
(402, 116, 0.50, 'cup / packet', '/uploads/food_portions/5bc98604255b2.jpg', 10, NULL),
(403, 116, 1.00, 'cup / packet', '/uploads/food_portions/5bc98622c86db.jpg', 10, NULL),
(404, 116, 1.50, 'cup / packet', '/uploads/food_portions/5bc986311401c.jpg', 10, NULL),
(405, 116, 2.00, 'cup / packet', '/uploads/food_portions/5bc9864295fb0.jpg', 10, NULL),
(406, 117, 0.50, 'cup', '/uploads/food_portions/5bd13d329f54b.jpg', 10, NULL),
(407, 117, 1.00, 'cup', '/uploads/food_portions/5bd13d48c3c0d.jpg', 10, NULL),
(408, 117, 1.50, 'cups', '/uploads/food_portions/5bd13d59c387a.jpg', 10, NULL),
(409, 117, 2.00, 'cups', '/uploads/food_portions/5bd13d63e8000.jpg', 10, NULL),
(410, 78, 2.00, 'rice bowls', '/uploads/food_portions/5bd13f64b3b63.jpg', 10, NULL),
(413, 44, 0.50, 'piece', NULL, 10, NULL),
(414, 44, 1.00, 'piece', NULL, 10, NULL),
(415, 44, 1.50, 'pieces', NULL, 10, NULL),
(416, 44, 2.00, 'pieces', NULL, 10, NULL),
(417, 24, 0.50, 'rice bowl', NULL, 10, NULL),
(418, 24, 0.75, 'rice bowl', NULL, 10, NULL),
(419, 24, 1.00, 'rice bowl', NULL, 10, NULL),
(421, 24, 1.50, 'rice bowls', NULL, 10, NULL),
(422, 5, 0.50, 'bottle', NULL, 10, NULL),
(423, 5, 1.00, 'bottle', NULL, 10, NULL),
(424, 5, 1.50, 'bottle', NULL, 10, NULL),
(425, 5, 2.00, 'bottle', NULL, 10, NULL),
(427, 15, 1.00, 'cup', NULL, 10, NULL),
(428, 15, 1.50, 'cup', NULL, 10, NULL),
(429, 15, 2.00, 'cup', NULL, 10, NULL),
(430, 15, 0.50, 'cup', NULL, 10, NULL),
(431, 41, 0.50, 'tub', NULL, 10, NULL),
(432, 41, 1.00, 'tub', NULL, 10, NULL),
(433, 41, 1.50, 'tubs', NULL, 10, NULL),
(434, 41, 2.00, 'tubs', NULL, 10, NULL),
(435, 47, 0.50, 'small box', NULL, 10, NULL),
(436, 47, 1.00, 'small box', NULL, 10, NULL),
(437, 47, 1.50, 'small box', NULL, 10, NULL),
(438, 47, 2.00, 'small box', NULL, 10, NULL),
(439, 39, 3.00, 'pieces', NULL, 10, NULL),
(440, 39, 6.00, 'pieces', NULL, 10, NULL),
(441, 39, 10.00, 'pieces', NULL, 10, NULL),
(442, 39, 20.00, 'pieces', NULL, 10, NULL),
(443, 16, 0.50, 'piece', NULL, 10, NULL),
(444, 16, 1.00, 'piece', NULL, 10, NULL),
(445, 16, 2.00, 'pieces', NULL, 10, NULL),
(446, 16, 3.00, 'pieces', NULL, 10, NULL),
(447, 20, 0.50, 'piece', NULL, 10, NULL),
(448, 20, 1.00, 'piece', NULL, 10, NULL),
(449, 20, 2.00, 'pieces', NULL, 10, NULL),
(450, 20, 3.00, 'pieces', NULL, 10, NULL),
(451, 63, 2.00, 'pieces', NULL, 10, NULL),
(452, 63, 4.00, 'pieces', NULL, 10, NULL),
(453, 63, 6.00, 'pieces', NULL, 10, NULL),
(454, 63, 8.00, 'pieces', NULL, 10, NULL),
(455, 64, 1.00, 'piece', NULL, 10, NULL),
(456, 64, 2.00, 'pieces', NULL, 10, NULL),
(457, 64, 3.00, 'pieces', NULL, 10, NULL),
(458, 64, 4.00, 'pieces', NULL, 10, NULL),
(459, 110, 2.00, 'pieces', NULL, 10, NULL),
(460, 110, 3.00, 'pieces', NULL, 10, NULL),
(461, 110, 4.00, 'pieces', NULL, 10, NULL),
(462, 110, 5.00, 'pieces', NULL, 10, NULL),
(463, 67, 1.00, 'piece', NULL, 10, NULL),
(464, 67, 2.00, 'pieces', NULL, 10, NULL),
(465, 67, 3.00, 'pieces', NULL, 10, NULL),
(466, 67, 4.00, 'pieces', NULL, 10, NULL),
(467, 26, 0.50, 'slice', NULL, 10, NULL),
(468, 26, 1.00, 'slice', NULL, 10, NULL),
(469, 26, 2.00, 'slices', NULL, 10, NULL),
(470, 26, 3.00, 'slices', NULL, 10, NULL),
(471, 45, 0.50, 'small packet', NULL, 10, NULL),
(472, 45, 1.00, 'small packet', NULL, 10, NULL),
(473, 45, 1.50, 'small packets', NULL, 10, NULL),
(474, 45, 2.00, 'small packets', NULL, 10, NULL),
(475, 108, 0.50, 'small packet', NULL, 10, NULL),
(476, 108, 1.00, 'small packet', NULL, 10, NULL),
(477, 108, 1.50, 'small packets', NULL, 10, NULL),
(478, 108, 2.00, 'small packets', NULL, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `path` varchar(500) NOT NULL,
  `image_type` varchar(500) NOT NULL,
  `image_type_primary` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_kid`
--

CREATE TABLE `user_kid` (
  `id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `character_type` varchar(500) DEFAULT NULL,
  `character_name` varchar(500) DEFAULT NULL,
  `where_character_live` varchar(500) DEFAULT NULL,
  `day_status` int(1) DEFAULT '1',
  `birthday` date DEFAULT NULL,
  `gender` varchar(500) DEFAULT NULL,
  `tutorial_done` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_kid`
--

INSERT INTO `user_kid` (`id`, `username`, `password`, `name`, `character_type`, `character_name`, `where_character_live`, `day_status`, `birthday`, `gender`, `tutorial_done`, `created_date`, `updated_date`) VALUES
(1, 'hendry', 'hendry', NULL, 'robot', 'hendry', 'space', 1, '2007-04-04', 'boy', 1, NULL, '2018-11-03 10:32:38'),
(3, 'nus_testuser_1', 'nus_testuser_1', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-04 05:57:18'),
(4, 'nus_testuser_2', 'nus_testuser_2', NULL, 'hippo', 'staff', 'forest', 2, '2006-11-08', 'girl', 1, NULL, '2018-11-04 05:57:25'),
(5, 'nus_testuser_3', 'nus_testuser_3', NULL, 'girl', 'Sarah', 'mountains', 3, '2006-10-03', 'girl', 1, NULL, '2018-11-04 05:57:35'),
(6, 'nus_testuser_4', 'nus_testuser_4', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-04 05:57:42'),
(7, 'nus_testuser_5', 'nus_testuser_5', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-04 05:57:49'),
(8, 'ming', 'ming', NULL, 'girl', 'Go', 'forest', 4, '2007-02-02', 'boy', 1, NULL, '2018-11-04 05:59:33'),
(9, 'ming2', 'ming2', NULL, 'zebra', 'Zeb', 'space', 3, '2007-04-05', 'girl', 1, NULL, '2018-11-04 07:17:07'),
(10, 'hendry2', 'hendry2', NULL, 'girl', 'girl', 'space', 2, '2006-02-04', 'girl', 1, NULL, '2018-11-04 11:23:50'),
(11, 'ming3', 'ming3', NULL, 'hippo', 'Test', 'space', 1, '2008-05-03', 'boy', 1, NULL, '2018-11-05 07:12:53'),
(12, 'FSP301', 'FSP301', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:28:32'),
(13, 'FSP302', 'FSP302', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:29:07'),
(14, 'FSP303', 'FSP303', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:29:18'),
(15, 'FSP304', 'FSP304', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:29:32'),
(16, 'FSP305', 'FSP305', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:29:43'),
(17, 'FSP306', 'FSP306', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:29:58'),
(18, 'FSP307', 'FSP307', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:30:10'),
(19, 'FSP308', 'FSP308', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:30:23'),
(20, 'FSP309', 'FSP309', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:30:33'),
(21, 'FSP310', 'FSP310', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:30:44'),
(22, 'FSP311', 'FSP311', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:30:54'),
(23, 'FSP312', 'FSP312', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:31:03'),
(24, 'FSP313', 'FSP313', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:31:12'),
(25, 'FSP314', 'FSP314', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:31:30'),
(26, 'FSP315', 'FSP315', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:31:44'),
(27, 'FSP316', 'FSP316', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:31:53'),
(28, 'FSP317', 'FSP317', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:32:06'),
(29, 'FSP318', 'FSP318', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:32:19'),
(30, 'FSP319', 'FSP319', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:32:37'),
(31, 'FSP320', 'FSP320', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:32:48'),
(32, 'FSP321', 'FSP321', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:33:03'),
(33, 'FSP322', 'FSP322', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:33:34'),
(34, 'FSP323', 'FSP323', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:34:18'),
(35, 'FSP324', 'FSP324', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:34:27'),
(36, 'FSP325', 'FSP325', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:34:37'),
(37, 'FSP326', 'FSP326', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:34:49'),
(38, 'FSP327', 'FSP327', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:34:59'),
(39, 'FSP328', 'FSP328', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:08'),
(40, 'FSP329', 'FSP329', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:18'),
(41, 'FSP330', 'FSP330', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:27'),
(42, 'FSP331', 'FSP331', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:36'),
(43, 'FSP332', 'FSP332', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:45'),
(44, 'FSP333', 'FSP333', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:35:54'),
(45, 'FSP334', 'FSP334', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:04'),
(46, 'FSP335', 'FSP335', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:12'),
(47, 'FSP336', 'FSP336', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:20'),
(48, 'FSP337', 'FSP337', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:27'),
(49, 'FSP338', 'FSP338', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:35'),
(50, 'FSP339', 'FSP339', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:44'),
(51, 'FSP340', 'FSP340', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:36:53'),
(52, 'FSP341', 'FSP341', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:37:03'),
(53, 'FSP342', 'FSP342', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:37:12'),
(54, 'FSP343', 'FSP343', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:37:20'),
(55, 'FSP344', 'FSP344', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:37:29'),
(56, 'FSP345', 'FSP345', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:37:40'),
(57, 'FSP601', 'FSP601', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:39:08'),
(58, 'FSP602', 'FSP602', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:41:46'),
(59, 'FSP603', 'FSP603', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:44:08'),
(60, 'FSP604', 'FSP604', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:44:34'),
(61, 'FSP605', 'FSP605', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:44:43'),
(62, 'FSP606', 'FSP606', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:44:54'),
(63, 'FSP607', 'FSP607', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:45:46'),
(64, 'FSP608', 'FSP608', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:47:27'),
(65, 'FSP609', 'FSP609', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:47:37'),
(66, 'FSP610', 'FSP610', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:47:49'),
(67, 'FSP611', 'FSP611', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:47:56'),
(68, 'FSP612', 'FSP612', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:12'),
(69, 'FSP613', 'FSP613', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:24'),
(70, 'FSP614', 'FSP614', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:33'),
(71, 'FSP615', 'FSP615', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:40'),
(72, 'FSP616', 'FSP616', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:47'),
(73, 'FSP617', 'FSP617', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:48:54'),
(74, 'FSP618', 'FSP618', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:03'),
(75, 'FSP619', 'FSP619', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:11'),
(76, 'FSP620', 'FSP620', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:21'),
(77, 'FSP621', 'FSP621', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:29'),
(78, 'FSP622', 'FSP622', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:52'),
(79, 'FSP623', 'FSP623', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:49:59'),
(80, 'FSP624', 'FSP624', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:50:08'),
(81, 'FSP625', 'FSP625', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:50:15'),
(82, 'FSP626', 'FSP626', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:50:48'),
(83, 'FSP627', 'FSP627', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:50:56'),
(84, 'FSP628', 'FSP628', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:03'),
(85, 'FSP629', 'FSP629', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:11'),
(86, 'FSP630', 'FSP630', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:18'),
(87, 'FSP631', 'FSP631', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:38'),
(88, 'FSP632', 'FSP632', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:45'),
(89, 'FSP633', 'FSP633', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:52'),
(90, 'FSP634', 'FSP634', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:51:59'),
(91, 'FSP635', 'FSP635', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:07'),
(92, 'FSP636', 'FSP636', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:16'),
(93, 'FSP637', 'FSP637', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:24'),
(94, 'FSP638', 'FSP638', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:32'),
(95, 'FSP639', 'FSP639', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:39'),
(96, 'FSP640', 'FSP640', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:47'),
(97, 'FSP641', 'FSP641', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:52:55'),
(98, 'FSP642', 'FSP642', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:53:02'),
(99, 'FSP643', 'FSP643', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:53:11'),
(100, 'FSP644', 'FSP644', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:53:19'),
(101, 'FSP645', 'FSP645', NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL, '2018-11-05 07:53:27'),
(102, 'jmjm', 'jmjm', NULL, 'girl', 'jm', 'forest', 1, '2009-06-06', 'boy', 1, NULL, '2018-11-05 08:02:04'),
(103, 'jm1', 'jm1', NULL, 'zebra', 'Zeb', 'forest', 1, '2008-08-05', 'boy', 1, NULL, '2018-11-05 08:22:45'),
(104, 'ming4', 'ming4', NULL, 'lion', 'Lion', 'space', 1, '2008-03-03', 'boy', 1, NULL, '2018-11-07 01:45:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_log`
--
ALTER TABLE `daily_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories` (`food_categories_id`);

--
-- Indexes for table `food_categories`
--
ALTER TABLE `food_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_portions`
--
ALTER TABLE `food_portions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `food_id` (`food_id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_kid`
--
ALTER TABLE `user_kid`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `daily_log`
--
ALTER TABLE `daily_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `food_categories`
--
ALTER TABLE `food_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `food_portions`
--
ALTER TABLE `food_portions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=479;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_kid`
--
ALTER TABLE `user_kid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `food`
--
ALTER TABLE `food`
  ADD CONSTRAINT `categories` FOREIGN KEY (`food_categories_id`) REFERENCES `food_categories` (`id`);

--
-- Constraints for table `food_portions`
--
ALTER TABLE `food_portions`
  ADD CONSTRAINT `food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
