<?php

class UserKidController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','export','download'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	private function actionDownload($exports){
		require_once('third_party/PHPExcel.php');
		
		foreach($exports as $key => $val){
			if(!empty($val['json'])){
				$json_decode = json_decode($val['json'], true);
				if(!empty($json_decode['location'])){
					$exports[$key]['location'] = $json_decode['location'].", ".$exports[$key]['location'];
					$exports[$key]['location'] = trim($exports[$key]['location'],",");
				}
			}
			if($val['concurrent_activity_type'] == 'Eat and Drink'){
				$json_decode = json_decode($val['dual_json'], true);
				if(!empty($json_decode['location'])){
					$exports[$key]['location'] = $json_decode['location'].", ".$exports[$key]['location'];
					$exports[$key]['location'] = trim($exports[$key]['location'],",");
				}
			}
			if($val['activity_type'] == 'Active Activities'){
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
					if(!empty($json_decode['intensity'])){
						$exports[$key]['intensity_activity'] = $json_decode['intensity'];
					}
				}
				
			}
			if($val['activity_type'] == 'Eat and Drink' || $val['concurrent_activity_type'] == 'Eat and Drink'){
				$portion = array();
				if($val['concurrent_activity_type'] == 'Eat and Drink'){
					$val['json'] = $val['dual_json'];
				}
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
						$selected_option = @$json_decode['selected_option'];
						if(is_array($selected_option)){
							foreach($selected_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_option_others = @$json_decode['selected_option_others'];
						if(is_array($selected_option_others)){
							foreach($selected_option_others as $r){
								if(!empty($r['name'])){
									$temp = $r;
									$temp['food_name'] = $r['name'];
									unset($temp['id']);
									unset($temp['portion_image']);
									unset($temp['portion_image_path']);
									$temp_str = $temp['food_name']." - ".$temp['portion'];
									array_push($portion, $temp_str);
								}
								
// 								if(!empty($r['portions'])){
// 									foreach($r['portions'] as $p){
// 										if($p['selected'] == '1' || $p['selected'] == 1){
// 											$temp = $p;
// 											$temp['food_name'] = $r['food_name'];
// 											unset($temp['id']);
// 											unset($temp['portion_image']);
// 											unset($temp['portion_image_path']);
// 											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
// 											array_push($portion, $temp_str);
// 											break;
// 										}
// 									}
// 								}
								
							}
						}
						$selected_fruits_option = @$json_decode['selected_fruits_option'];
						if(is_array($selected_fruits_option)){
							foreach($selected_fruits_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_option_fruits_others = @$json_decode['selected_option_fruits_others'];
						if(is_array($selected_option_fruits_others)){
							foreach($selected_option_fruits_others as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_drinks_option = @$json_decode['selected_drinks_option'];
						if(is_array($selected_drinks_option)){
							foreach($selected_drinks_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_option_drinks_others = @$json_decode['selected_option_drinks_others'];
						if(is_array($selected_option_drinks_others)){
							foreach($selected_option_drinks_others as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_desert_option = @$json_decode['selected_desert_option'];
						if(is_array($selected_desert_option)){
							foreach($selected_desert_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
						$selected_option_deserts_others = @$json_decode['selected_option_deserts_others'];
						if(is_array($selected_option_deserts_others)){
							foreach($selected_option_deserts_others as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
						}
// 					}

						if(!empty($portion)){
							$exports[$key]['portion_size'] = json_encode($portion);
						}
				}
				
			}
			unset($exports[$key]['json']);
			unset($exports[$key]['dual_json']);
		}
		
		
		array_unshift($exports,array(
				'Date Recorded',
				'Is Dual',
				'Day Log',
				'Last Login',
				'Username',
				'Character',
				'Start Time',
				'End Time',
				'Activities',
				'Suboption',
				'Concurrent Activity Recorded',
				'Sub-Concurrent Option Activity Recorded',
				'Portion Size',
				'Intensity Activity',
				'Plant',
				'Location'
		));
		
		// create php excel object
		$doc = new PHPExcel();
		
		// set active sheet
		$doc->setActiveSheetIndex(0);
		
		// read data to active sheet
		$doc->getActiveSheet()->fromArray($exports);
		
		//save our workbook as this file name
		$filename = 'exports.xls';
		//mime type
		header('Content-Type: application/vnd.ms-excel');
		//tell browser what's the file name
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		
		$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
		
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	public function actionPatchDailyLogSleeping(){
		$daily_log = Yii::app()->db->createCommand()
		->select('*')
		->from('daily_log')
		->where('sleeping_done = 1')
		->queryAll();
		foreach($daily_log as $row){
			$date = DateTime::createFromFormat('Y-m-d', $row['date_log']);
			$date->modify('-1 day');
			$sleeping_time = $date->format('Y-m-d')." ".$row['time_sleep_hr1'].$row['time_sleep_hr2'].":".$row['time_sleep_minute1'].$row['time_sleep_minute2']." ".$row['time_sleep_am'];
			$time_sleep = date("Y-m-d H:i:s",strtotime($sleeping_time));
			
			$wakeup_time = $row['date_log']." ".$row['time_wakeup_hr1'].$row['time_wakeup_hr2'].":".$row['time_wakeup_minute1'].$row['time_wakeup_minute2']." ".$row['time_wakeup_am'];
			$time_wakeup = date("Y-m-d H:i:s",strtotime($wakeup_time));
			
			if(strtotime($time_sleep) > strtotime('2000-01-01') && strtotime($time_wakeup) > strtotime('2000-01-01')){
				DailyLog::model()->updateByPk($row['id'], array('sleeping_time'=>$time_sleep,'wakeup_time'=>$time_wakeup));
				
			}
			
		}
	}
	
	public function actionExport(){
		$this->actionPatchDailyLogSleeping();
		$kid_array = array();
		$kid_string = '1=1';
		$kid_string_date_log = '1=1';
		$date_from = '';
		$date_to = '';
		$date_range = '1=1';
		$date_log = '1-1';
		$limit = 100;
		if(!empty($_POST['kid']) || !empty($_POST['date_from']) || !empty($_POST['date_to']) || !empty($_POST['limit'])){
			
			
			if(!empty($_POST['kid'])){
				$kid_array = $_POST['kid'];
				$kid_string= implode(",", $_POST['kid']);
				$kid_string = 't1.id in ('.$kid_string.')';
				$kid_string_date_log = 't2.id in ('.implode(",", $_POST['kid']).')';
			}
			if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
				$date_from = date("Y-m-d",strtotime($_POST['date_from']));
				$date_to = date("Y-m-d",strtotime($_POST['date_to']));
				$date_range = 'DATE(activity_date) BETWEEN \''.$date_from.'\' and \''.$date_to.'\'';
				$date_log = 't1.date_log BETWEEN \''.$date_from.'\' and \''.$date_to.'\'';
			}
			if(!empty($_POST['limit'])){
				$limit = $_POST['limit'];
			}
		}
		
		$daily_logs = Yii::app()->db->createCommand()
		->select('t2.day_status, t2.last_login_date, t1.sleeping_time, t1.wakeup_time, t2.username, t2.character_type, t1.created_date, t1.date_log as activity_date, TIMESTAMPDIFF(MINUTE,t1.sleeping_time,t1.wakeup_time) as sleep_duration')
		->from('daily_log t1')
		->join('user_kid t2','t1.user_kid_id = t2.id')
		->where($kid_string_date_log)
		->andWhere('t1.completed = 1')
		->queryAll();
// 		print_r($daily_logs);exit;
		
		$sleeping = array();
		foreach($daily_logs as $row){
			$temp = array(
					'activity_date' => $row['created_date'],
					'is_dual' => '-',
					'day_status' => $row['day_status'],
					'last_login_date' => $row['last_login_date'],
					'username' => $row['username'],
					'character_type' => $row['character_type'],
					'start_date_time' => $row['sleeping_time'],
					'complete_date_time' => $row['wakeup_time'],
					'activity_type' => 'Sleeping',
					'selected_activity_display' => '-',
					'concurrent_activity_type' => '-',
					'subconcurrent_activity' => '-',
					'portion_size' => '-',
					'intensity_activity' => '-',
					'plant' => '-',
					'location' => '-',
					'json' => '-'
			);
			array_push($sleeping,$temp);
			
		}
		
		$command = Yii::app()->db->createCommand()
		->select("IF(t3.dual_activity_id > 0, t4.created_date , t3.created_date ) as activity_date, IF(t3.dual_activity_id > 0, 'Yes', 'No') as is_dual, t1.day_status, t1.last_login_date, t1.username, t1.character_type, t3.start_date_time, t3.complete_date_time, t3.activity_type, t3.selected_activity_display, t4.activity_type as concurrent_activity_type, t4.selected_activity_display as subconcurrent_activity, '-' as portion_size, '-' as intensity_activity, t2.plant_randomize as plant, t3.location, t3.json as json, t4.json as dual_json")
		->from('user_kid t1')
		->join('daily_log t2', 't1.id = t2.user_kid_id')
		->join('activities t3', 't3.daily_log_id = t2.id')
		->leftJoin('activities t4', 't4.id = t3.dual_activity_id')
		->where('t3.complete = 1')
		->andWhere($kid_string)
		->having($date_range)
		->order('t1.id desc, t2.date_log desc, t3.start_date_time desc')
		->limit($limit);
// 		echo $command->getText();
		$exports = $command
				->queryAll();
		
				
		$exports = array_merge($exports, $sleeping);
		if(isset($_POST['download'])){
			$this->actionDownload($exports);
		}
	
		// 		echo '<pre>';
		// 		print_r($exports);exit;
		
		foreach($exports as $key => $val){
			if(!empty($val['json'])){
				$json_decode = json_decode($val['json'], true);
				if(!empty($json_decode['location'])){
					$exports[$key]['location'] = $json_decode['location'].", ".$exports[$key]['location'];
					$exports[$key]['location'] = trim($exports[$key]['location'],",");
				}
			}
			if($val['concurrent_activity_type'] == 'Eat and Drink'){
				$json_decode = json_decode($val['dual_json'], true);
				if(!empty($json_decode['location'])){
					$exports[$key]['location'] = $json_decode['location'].", ".$exports[$key]['location'];
					$exports[$key]['location'] = trim($exports[$key]['location'],",");
				}
			}
			if($val['activity_type'] == 'Active Activities'){
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
					if(!empty($json_decode['intensity'])){
						$exports[$key]['intensity_activity'] = $json_decode['intensity'];
					}
				}
				
			}
			
			if($val['activity_type'] == 'Eat and Drink' || $val['concurrent_activity_type'] == 'Eat and Drink'){
				$portion = array();
				if($val['concurrent_activity_type'] == 'Eat and Drink'){
					$val['json'] = $val['dual_json'];
				}
				if(!empty($val['json'])){
					$json_decode = json_decode($val['json'], true);
						$selected_option = @$json_decode['selected_option'];
						if(is_array($selected_option)){
							
							foreach($selected_option as $r){
								if(!empty($r['portions'])){
									foreach($r['portions'] as $p){
										if($p['selected'] == '1' || $p['selected'] == 1){
											$temp = $p;
											$temp['food_name'] = $r['food_name'];
											unset($temp['id']);
											unset($temp['portion_image']);
											unset($temp['portion_image_path']);
											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
											array_push($portion, $temp_str);
											break;
										}
									}
								}
								
							}
							
						}
						
					}
					$selected_option_others = @$json_decode['selected_option_others'];
					if(is_array($selected_option_others)){
						foreach($selected_option_others as $r){
							if(!empty($r['name'])){
								$temp = $r;
								$temp['food_name'] = $r['name'];
								unset($temp['id']);
								unset($temp['portion_image']);
								unset($temp['portion_image_path']);
								$temp_str = $temp['food_name']." - ".$temp['portion'];
								array_push($portion, $temp_str);
							}
							
							// 								if(!empty($r['portions'])){
							// 									foreach($r['portions'] as $p){
							// 										if($p['selected'] == '1' || $p['selected'] == 1){
							// 											$temp = $p;
							// 											$temp['food_name'] = $r['food_name'];
							// 											unset($temp['id']);
							// 											unset($temp['portion_image']);
							// 											unset($temp['portion_image_path']);
							// 											$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
							// 											array_push($portion, $temp_str);
							// 											break;
							// 										}
							// 									}
							// 								}
							
						}
					}
					$selected_fruits_option = @$json_decode['selected_fruits_option'];
					if(is_array($selected_fruits_option)){
						foreach($selected_fruits_option as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
					$selected_option_fruits_others = @$json_decode['selected_option_fruits_others'];
					if(is_array($selected_option_fruits_others)){
						foreach($selected_option_fruits_others as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
					$selected_drinks_option = @$json_decode['selected_drinks_option'];
					
					if(is_array($selected_drinks_option) && !empty($selected_drinks_option)){
						
						foreach($selected_drinks_option as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
					$selected_option_drinks_others = @$json_decode['selected_option_drinks_others'];
					if(is_array($selected_option_drinks_others)){
						foreach($selected_option_drinks_others as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
					$selected_desert_option = @$json_decode['selected_desert_option'];
					if(is_array($selected_desert_option)){
						foreach($selected_desert_option as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
					$selected_option_deserts_others = @$json_decode['selected_option_deserts_others'];
					if(is_array($selected_option_deserts_others)){
						foreach($selected_option_deserts_others as $r){
							if(!empty($r['portions'])){
								foreach($r['portions'] as $p){
									if($p['selected'] == '1' || $p['selected'] == 1){
										$temp = $p;
										$temp['food_name'] = $r['food_name'];
										unset($temp['id']);
										unset($temp['portion_image']);
										unset($temp['portion_image_path']);
										$temp_str = $temp['food_name']." - ".$temp['portion']." - ".$temp['measurement'];
										array_push($portion, $temp_str);
										break;
									}
								}
							}
							
						}
					}
// 				}
				
				if(!empty($portion)){
					$exports[$key]['portion_size'] = json_encode($portion);
				}
			}
			unset($exports[$key]['json']);
			unset($exports[$key]['dual_json']);
		}
		
		$this->render('export',array(
				'results'=>$exports,
				'date_from'=>$date_from,
				'date_to' => $date_to,
				'limit' => $limit,
				'kid_array' => $kid_array
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UserKid;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserKid']))
		{
			$model->attributes=$_POST['UserKid'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserKid']))
		{
			$model->attributes=$_POST['UserKid'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UserKid('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserKid']))
			$model->attributes=$_GET['UserKid'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UserKid the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UserKid::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UserKid $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-kid-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
