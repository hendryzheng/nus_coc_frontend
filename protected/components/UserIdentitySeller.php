<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentitySeller extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
        public $_id;
        public $_userType;
	public function authenticate() {

        /* EMERGENCY BACKDOOR! incase everything failz */
        //check backdoor
//        if ($this->username == Yii::app()->params['backdoor']['username'] && $this->password == Yii::app()->params['backdoor']['password']) {
//            $this->_id = 'backdoor';
//            $this->setState('idno', 0);
//            $this->setState('name', 'Backdoor');
//            $this->setState('type', 'backdoor');
//
//            $this->errorCode = self::ERROR_NONE;
//            return !$this->errorCode;
//        }
        $record = Seller::model()->findByAttributes(array('sellerUsername' => $this->username));
        
        
        //check hashes
        $this->password = md5($this->password);
        
        if ($record === null) {
            $this->_id = 'user Null';
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($record->sellerPassword !== $this->password) {
            $this->_id = $this->username;
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            if($record->sellerStatus == 7){
                $this->errorMessage = 'Account has been temporarily locked. Please contact admin for enquiries';
//                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            }else{
                $this->_id = $record['id'];
                $this->setState('idno', $record['id']);
                $this->setState('type', 'seller');
                $this->setState('role', 'seller');
                $this->errorCode = self::ERROR_NONE;
            }
            
        }

        return !$this->errorCode;
    }
    
    public function afterSave() {
        if (!Yii::app()->authManager->isAssigned(
                $this->type,$this->id)) {
            Yii::app()->authManager->assign($this->type,
            $this->id);
        }
        return parent::afterSave();
        }
}