<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EmailSmsHelper{
    /*
     * $to : receiver
     * $mailTemplateCode : Template Code for the email
     * $param : parameter of the content
     */
    
	public static function sendEmail($to, $subject, $html){
		
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: PropertyConnectAlliance<no-reply@propertyconnectalliance.com>' . "\r\n";
		'Reply-To: info@propertyconnectalliance.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
// 		$headers .= 'Cc:sales@friendlyvegetarian.com.sg'.  "\r\n";
		
// 		mail($to, $subject, $html, $headers);
		
		
		require_once('mail/phpmailer/class.phpmailer.php');
		
		
		
		
		//include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
// 		define('GUSER', 'rw@hendryzheng.com'); // Gmail username
// 		define('GPWD', '123qwe'); // Gmail password
		$mail = new PHPMailer();  // create a new object
		$mail->IsSMTP(); // enable SMTP
		$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true;  // authentication enabled
		$mail->SMTPSecure = ''; // secure transfer enabled REQUIRED for Gmail
		
		// 		$mail->SMTPSecure = 'tls';
		//         $mail->Host = 'smtp.gmail.com';
		//         $mail->Port = 587;
		//         $mail->Username = 'your@gmail.com';
		//         $mail->Password = 'gmailpassword';
		
		$mail->Host = 'mail.propertyconnectalliance.com';
		$mail->Port = 587;
		$mail->Username = 'no-reply@propertyconnectalliance.com';
		$mail->Password = '44iioc_{LTUp';
		$mail->SetFrom('no-reply@propertyconnectalliance.com', 'PropertyConnectAlliance');
		$mail->Subject = $subject;
		$mail->Body = $html;
		$mail->isHTML(true);
		$mail->AddAddress($to);
		if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo;
			echo $error;
			return false;
		} else {
			$error = 'Message sent!';
			return true;
		}
	}
	
	public static function sendMessage($title, $message ,$target, $token_array_user = array()){
		//FCM api URL
		$url = 'https://fcm.googleapis.com/fcm/send';
		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
		$server_key = 'AAAAfKSGxrQ:APA91bEVlIZb_8K4uoeX4RU9Qh5vjyT8k53JdqS8Q6qKhkxnrdENjcgPLsKxxD4xklgF50CdVcfjD0XjDSZ0_jN9U-ZMHWLLgkT39NlfCmJAUNow01xOg8vwpuOl2ZmS4XZddKAGmj0T';
		
		$fields = array();
		if(is_array($token_array_user) && !empty($token_array_user)){
			foreach($token_array_user as $row){
				$fields['to'] = $row['token'];
				$fields['priority'] = "high";
				$total_badge = Notification::model()->getTotalUnread($row['member_type'], $row['id']);
				$fields['notification'] = array ("title" => $title, "body"=> $message,"sound"=>"default","badge"=>$total_badge);
				//header with content_type api key
				$headers = array(
						'Content-Type:application/json',
						'Authorization:key='.$server_key
				);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				$result = curl_exec($ch);
				if ($result === FALSE) {
					// 			die('FCM Send Error: ' . curl_error($ch));
				}
				curl_close($ch);
				StringHelper::logMsg('Send Notification',json_encode($fields)." /r/n ".stripslashes($result),false);
			}
		}else{
			if(is_array($target)){
				$fields['registration_ids'] = $target;
			}else{
				$fields['to'] = $target;
			}
			$fields['priority'] = "high";
			$fields['notification'] = array ("title" => $title, "body"=> $message,"sound"=>"default","badge"=>"");
			//header with content_type api key
			$headers = array(
					'Content-Type:application/json',
					'Authorization:key='.$server_key
			);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			if ($result === FALSE) {
				// 			die('FCM Send Error: ' . curl_error($ch));
			}
			curl_close($ch);
			StringHelper::logMsg('Send Notification',json_encode($fields)." /r/n ".stripslashes($result),false);
			return $result;
		}
		// 		$fields['data'] = $data;
		
	}
 
//     public static function sendEmail($to, $mailTemplateCode, $param = array(), $attachment = null){
//         $mailTemplate = MailTemplate::model()->findByAttributes(array('code'=>$mailTemplateCode));
//         $subject = $mailTemplate->subject;
//         if(!empty($param['subject'])){
//             foreach($param['subject'] as $key=>$value){
//                 $subject = str_replace('{$'.$key.'}', $value, $subject);
//             }
//             $mailTemplate->subject = $subject;
//         }
//         $content = $mailTemplate->content;
//         if(!empty($param['content'])){
//             foreach($param['content'] as $key=>$value){
//                 $content = str_replace('{$'.$key.'}', $value, $content);
//             }
//             $mailTemplate->content = $content;
//         }
//         $cc = $mailTemplate->cc;
//         if(!empty($param['cc'])){
//             foreach($param['cc'] as $key=>$value){
//                 $cc = str_replace('{$'.$key.'}', $value, $cc);
//             }
//             $mailTemplate->cc = $cc;
//         }
//         if(!empty($attachment)){
//             $mailTemplate->attachment = $attachment;
//         }
//         $mailTemplate->sendEmail($to);
//     }
    
    public static function sendSMS($to, $smsTemplateCode, $param= array()){
        $smsTemplate = SmsTemplate::model()->findByAttributes(array('code'=>$smsTemplateCode));
        $content = $smsTemplate->message;
        if(!empty($param['content'])){
            foreach($param['content'] as $key=>$value){
                $content = str_replace('{$'.$key.'}', $value, $content);
            }
            $smsTemplate->message = $content;
        }
        SMSHelper::sendSMS($to, $smsTemplate->message);
    }
}