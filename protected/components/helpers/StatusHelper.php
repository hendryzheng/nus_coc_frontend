<?php 

/**
 * String Helper Class
 **/
 
class StatusHelper{


	/** 
	 * @param $str - the string to trim
	 * @param $len - the length of trim 	(Optional)(22)
	 * @param $rep - replacement trim	(Optional)(...)
	 *
	 * @return $str2 - resulting trim
	 */
	 
    public static function getStatus($id) {
        switch ($id) {
            case 1:
                return "active";
                break;
            case 2:
                return "inactive";
                break;
            case 3:
                return "pending verification";
                break;
        }
	}
	
	public static function getIdByName($status){
	    switch ($status) {
	        case "active":
	            return 1;
	            break;
	        case "inactive":
	            return 2;
	            break;
	        case "pending verification":
	            return 3;
	            break;
	    }
	}
	
	public static function getStatusList() {
	    return array(1=>"active", 2=>"inactive", 3=>"pending verification");
	}
	
	public static function getStatusDisplay($id) {
	    switch ($id) {
	        case 1:
	            return '<span class="label label-success">active</span>';
	            break;
	        case 2:
	            return '<span class="label label-default">inactive</span>';
	            break;
	        case 3:
	            return '<span class="label label-warning">pending verification</span>';
	            break;
	    }
	}
}

?>