<?php 

/**
 * String Helper Class
 **/
 
class StringHelper{


	/** 
	 * @param $str - the string to trim
	 * @param $len - the length of trim 	(Optional)(22)
	 * @param $rep - replacement trim	(Optional)(...)
	 *
	 * @return $str2 - resulting trim
	 */
	 
	public static function logMsg($action, $msg = null, $combine = true){
	
		$string = CVarDumper::dumpAsString($_REQUEST);
		$string_combine = '';
		if($combine){
			$string_combine = $action."\n".$string;
		}else{
			$string_combine = $action."\n";
			if(!empty($msg)){
				$string_combine .= "\n".$msg;
			}
		}		
		$log = new Log();
		$log->log = $string_combine;
		$log->save();
		return $string_combine;
	
	}
	
	
	

	   public static function get_month_list() {
			$resp = array();
			for ($m=1; $m<=12; $m++) {
				$month = date('F', mktime(0,0,0,$m, 1, date('Y')));
				$resp[$m] = $month;
			}
			return $resp;
		}
		

	   public static function get_year_list() {
			$resp = array();
			$year = intval(date('Y',strtotime("now"))) - 3;
			$yearTarget = $year+5;
			for ($m=$year; $m<=$yearTarget; $m++) {
				$resp[$m] = $m;
			}
			return $resp;
		}
		

		
	public static function getIP(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public static function trim($str, $len = 22, $rep = '...'){
	
		$str2 = $str;
		
		if(strlen($str) > $len){
			
			$str2 = substr($str,0,$len - strlen($rep)) . '...';
		
		}
		
		return $str2;
	}
	
	public static function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	/** 
	 * @param $url - the url of the youtube video
	 *
	 * @return $id - resulting id of link
	 */
	 
	public static function getYoutubeId($url){
	
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

		$id = $matches[1];
		
		return $id;
	}
	
	public static function removeSpace($str){
		return str_replace('/','',str_replace(' ', '-', $str));
	}
	
	public static function removeKeys($data){
		$array = array();
		foreach($data as $key=>$value){
			array_push($array, $value);
		}
		return $array;
	}
}

?>