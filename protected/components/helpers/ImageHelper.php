<?php 

/**
 * String Helper Class
 **/
 
class ImageHelper{

	function create( $filename ) {
		if (!file_exists($filename)) {
			throw new InvalidArgumentException('File "'.$filename.'" not found.');
		}
		switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
			case 'jpeg':
			case 'jpg':
				return imagecreatefromjpeg($filename);
			break;

			case 'png':
				return imagecreatefrompng($filename);
			break;

			case 'gif':
				return imagecreatefromgif($filename);
			break;

			default:
				throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
			break;
		}
	}
	
}