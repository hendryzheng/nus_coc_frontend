<?php

class CartSummaryWidget extends CWidget{

	public $title = '';
	public $content = '';
	
	public $container_class = '';
	
	public $container_attributes = '';
	
         public $purchase = '';
         public $purchaseItem = '';
         
         public $continueShopping = false;
         public $admin = false;
         public $include_css = false;
	public function run() {
	
        $this->render('cartSummary');
    }
}

?>