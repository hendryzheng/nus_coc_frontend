<?php
$product = $this->product;

?>
<!-- Bootstrap -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- style -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/app.css" rel="stylesheet">
	
<style>
h5,h4,.details{
	font-weight:bold !important;
	color:#000 !important;
}
</style>
    <div class="site-content"><!--[start:site-content]-->
      <div class="section-cart"><!--[start:section-cart]-->
        <div class="container"><!--[start:container]-->
        <div class="row">
		<div class="col-lg-2">
			<img src="<?php echo Yii::app()->getBaseUrl(true)?>/img/logo.png" alt="Friendly Vegetarian Logo" class="img-responsive" />
		</div>
		<div class="col-lg-10" style="margin-top:-20px;">
			<h1>Friendly Vegetarian Food Supplier</h1>
			<p>Block 421 ANG MO KIO AVE 10 #01-1165 SINGAPORE 560421<br/>
			TEL: 64566607 FAX: 64541632<br/>
			Block 14 Pasir Panjang #01-24 Wholesale Center Singapore 110014<br/>
			TEL: 67791488 FAX: 67792635<br/>
			Email: <a href="mailto:sales@friendlyvegetarian.com.sg">sales@friendlyvegetarian.com.sg</a>   Web: <a href="http://www.friendlyvegetarian.com.sg">http://www.friendlyvegetarian.com.sg</a><br/>
			<small>Company Registered number: 39100200K GST Reg. No. M8-8002457-4</small>
		</div>
	</div>
          <div class="row">
            <div class="col-lg-12">
              <div class="thumbnail pad20"><!--[start:thumbnail]-->
                <h3 class="entry-title text-semi marTop-0">
                <?php echo $product->name ?> is back !</h3>
                <div class="desc">
                <p>Quickly get it while stock last.</p>
                  <div class="carousel-inner">
                      <?php 
                      //carousel-inner>.item.active.right
                      	$images = Image::model()->getImages($product);
                      	$i = 0;
                      	foreach($images as $img){
                      		if($i == 0){
                      			$class= 'active';
                      		}else{
                      			$class = '';
                      		}
                      		$i++;
                      ?>
                      	<div style="display:inline-block;" class="item <?php echo $class?>" data-slide-number="0" align="left">
	                      	<img style="max-height:200px;" id="main-img" src="<?php echo URLHelper::getBackendBaseUrl()?>/<?php echo $img->path?>">
	                    </div>
                      <?php 		
                      	}
                      ?>
<!--                       <div class="active item" data-slide-number="0"> -->
<!--                       	<img src="http://placehold.it/770x650&text=one"> -->
<!--                       </div> -->
<!--                       <div class="item" data-slide-number="1"><img src="http://placehold.it/770x650&text=two"></div> -->
<!--                       <div class="item" data-slide-number="2"><img src="http://placehold.it/770x650&text=three"></div> -->
                    </div>    
                  <div class="classification-img-detail" align="left">
                  <?php 
                  $classification = Classification::model()->findAll();
                  
                  	foreach($classification as $row){
                  		if($product->classification == $row->id){
                  			$icon = $row['icon'];
                  		}else{
                  			$icon = $row['icon_grey'];
                  		}
                  		echo '<img src="'.Yii::app()->getBaseUrl(true).'/'.$icon.'"/>';
                  	}
                  ?>
                  </div>
                  <b>Description</b>
                  <p><?php echo strip_tags($product->description)?></p>    
                  <b>Ingredients</b>
                   <p><?php echo strip_tags($product->ingredients)?></p>        
                  <div class="entry-footer">
                     <h3 class="price-tags">$<?php echo $product->normal_price?></h3> 
                  </div>  
                  <a style="background: #f99516;padding:10px 30px;width:150px;margin-bottom: 5px;color: #fff;" href="<?php echo Yii::app()->getBaseUrl(true)?>/product/<?php echo $product->id?>">Buy Now</a>
                </div>


            </div>
          </div>  
        </div> <!--[end:continaer]-->     
      </div><!--[end:section-cart]-->
    </div><!--[end:site-content]-->
