<?php
    $purchase = $this->purchase;
    $purchaseItem = $this->purchaseItem;
    $admin = $this->admin;
?>
<?php
if($this->include_css){
?>
<!-- Bootstrap -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- style -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/app.css" rel="stylesheet">
<?php 
}
?>
<style>
h5,h4,.details{
	font-weight:bold !important;
	color:#000 !important;
}
</style>
                 <input type="hidden" id="purchase_id" value="<?=$purchase->id?>"/>
    <div class="site-content"><!--[start:site-content]-->
      <div class="section-cart"><!--[start:section-cart]-->
        <div class="container"><!--[start:container]-->
        <?php 
        	if($this->include_css){
        ?>
        <div class="row">
		<div class="col-lg-2">
			<img src="<?php echo Yii::app()->getBaseUrl(true)?>/img/logo.png" alt="Friendly Vegetarian Logo" class="img-responsive" />
		</div>
		<div class="col-lg-10">
			<h1>Friendly Vegetarian Food Supplier</h1>
			<p>Block 421 ANG MO KIO AVE 10 #01-1165 SINGAPORE 560421<br/>
			TEL: 64566607 FAX: 64541632<br/>
			Block 14 Pasir Panjang #01-24 Wholesale Center Singapore 110014<br/>
			TEL: 67791488 FAX: 67792635<br/>
			Email: <a href="mailto:sales@friendlyvegetarian.com.sg">sales@friendlyvegetarian.com.sg</a>   Web: <a href="http://www.friendlyvegetarian.com.sg">http://www.friendlyvegetarian.com.sg</a><br/>
			<small>Company Registered number: 39100200K GST Reg. No. M8-8002457-4</small>
		</div>
	</div>
        <?php }?>
          <div class="row">
            <div class="col-lg-12">
              <div class="thumbnail pad20"><!--[start:thumbnail]-->
                <h3 class="entry-title text-semi marTop-0">
                <?php 
                        if(!empty(Yii::app()->user->type) && Yii::app()->user->type == "corporate"){
                            echo 'PURCHASE ORDER';
                        }else{
                            echo 'ORDER CONFIRMATION';
                        }
                        
                    ?></h3>
                <p class="marBot20 small">
                  Your Order <b>#<?php echo $purchase->invoice_no; ?></b> is being processed. We will get back to you shortly.<br/>
                  A copy of your <?php 
                        if(!empty(Yii::app()->user->type) && Yii::app()->user->type == "corporate"){
                            echo 'PURCHASE ORDER';
                        }else{
                            echo 'ORDER CONFIRMATION';
                        }
                        
                    ?> is being sent to your email account.<br/>
                  Thank you for shopping with us..
                </p>
                <div class="personal-info"><!--[start:personal particulars]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Personal Particulars</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Name:</b></label>
                        <h5><?php echo $purchase->name; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Email:</b></label>
                        <h5><?php echo $purchase->email; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Telp:</b></label>
                        <h5><?php echo $purchase->tel; ?></h5>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label90"><b>Address:</b></label>
                        <h5><?php echo $purchase->address; ?></h5>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label90"><b>Postal Code:</b></label>
                        <h5><?php echo $purchase->postalcode; ?></h5>
                      </div>
                    </div>
                  </div>
                </div><!--[end:personal particulars] -->
                <div class="table-cart table-responsive table">
                  <div class="divTable clearfix">
                    <div class="divTableHeading clearfix">
                      <div class="divTableRow">
                        <div class="divTableHead pad10 col-lg-1 col-md-1 col-sm-1 text-regular text-left hidden-xs"><b>S/No.</b></div>
                        <div class="divTableHead pad10 col-lg-5 col-md-6 col-sm-5 text-regular"><b>Item</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-2 col-sm-2 text-regular text-center hidden-xs"><b>Unit Price</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-1 col-sm-2 text-regular text-center hidden-xs"><b>Quality</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-2 col-sm-2 text-regular text-right hidden-xs"><b>Sub Total</b></div>
                      </div>
                    </div>
                    <div class="divTableBody">
                    <?php
                    		$total_weight = 0;
			                $subtotal = 0;
			                $count = 0;
			                $counter= 0;
			                Yii::app()->session['after_gst'] = ConfigHelper::getVal('gst_inclusive');
			               foreach($purchaseItem as $item){
			               	$total_weight += @$item->product->weight_no * @$item->quantity;
			               	$counter++;
			               	$subtotal+= $item->price * Yii::app()->session['after_gst'] * $item->quantity; //200 (2*100)
			               	
			                 $image = Image::model()->getImagePreview($item->product);
			                 $image = Image::model()->findByAttributes(array('image_type'=>'product','image_type_primary'=>$item->product->id));
			        ?>
			        <div class="divTableRow clearfix">
                        <div class="divTableCell col-lg-1 col-md-1 col-sm-1 text-regular text-center hidden-xs"><?php echo $counter;?></div>
                        <div class="divTableCell col-lg-5 col-md-6 col-sm-5 text-regular">
                          <div class="media">
                          	<?php if($this->include_css === false){?>
                            <?php if(!empty($image->path)){?>
                            <div class="media-left">
                              <img src="<?php echo URLHelper::getBackendBaseUrl()."/".@$image->path?>" alt="dummy" class="img-responsive"/>
                            </div>
                            <?php }}?>
                            <div class="media-body">
                              <h4 class="marTop-0"><?php echo $item->product->name?> </h4>
                              Weight : <?php echo $item->weight?>
                            </div>
                          </div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular unit-price">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Unit Price:</b></div> 
                          <div class="details"> S$ <?php echo round($item->price * Yii::app()->session['after_gst'],2)?></div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-1 col-sm-2 text-regular quality">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Quantity:</b></div> 
                          <div class="details">X <?php echo $item->quantity?>
							</div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular sub-total">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Sub Total:</b></div> 
                          <div class="details">S$ <?php echo number_format($item->price * Yii::app()->session['after_gst']* $item->quantity,2)?> </div>
                        </div>
                      </div>
			        <?php }
			        if(@$purchase->promo_code_type=== '3'){
                      		
                      		$list= Product::model()->findByPk($purchase->promo_product_id);
                      		
                      		
                      		$image = @Image::model ()->getImagePreview ( $list );
                      		$name=$list->name;
                      		$description=$list->description;
                      		$qty=1;
                      		$price=$list->normal_price * Yii::app()->session['after_gst'];
                      		$sumPrice=1*($list->normal_price * Yii::app()->session['after_gst']);
                      		$weight = @$list->weight_no;
                      		$total_weight += $weight * 1;
                      		$savings = $sumPrice;
                      		$coupon = 'Your coupon has earned you a free item <b>'.$name.'</b>';
//                       		$subtotal += $sumPrice;
                      	?>
                      	 <div class="divTableRow clearfix">
                        <div class="divTableCell col-lg-1 col-md-1 col-sm-1 text-regular text-center hidden-xs"><?php echo $counter?></div>
                        <div class="divTableCell col-lg-3 col-md-6 col-sm-5 text-regular">
                          <div class="media">
                            <div class="media-left">
                              <img src="<?php echo $image?>" alt="dummy" class="img-responsive"/>
                            </div>
                            <div class="media-body">
                              <h4 class="marTop-0"><?php echo $name?><br/>
                              <span style="color:#f00;">COUPON CODE (<?php echo $purchase->promo_code?>) Free Item</span> </h4>
                              <div class="text-thin"><?php echo $description?></div>
                            </div>
                          </div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular unit-price">
                          <div class="hidden-lg hidden-md hidden-sm title">Weight:</div> 
                          <div class="details"> <?php echo $weight?> gram</div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular unit-price">
                          <div class="hidden-lg hidden-md hidden-sm title">Unit Price:</div> 
                          <div class="details"> S$ <?php echo $price?></div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-1 col-sm-2 text-regular quality">
                          <div class="hidden-lg hidden-md hidden-sm title">Quantity:</div> 
                          <div class="details">X <?php echo $qty?>
						</div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular sub-total">
                          <div class="hidden-lg hidden-md hidden-sm title">Sub Total:</div> 
                          <div class="details"><strike>S$ <?php echo number_format($sumPrice,2)?> </strike></div>
                        </div>
                      </div>
                      	<?php
                      		
                      	}
                      	
                      $calculationResult = Purchase::calculateTotalPurchaseApi ( $subtotal, $purchase->customer_id );
                      $rewards = @$calculationResult ['rewards'];
                      ?>
                    </div>
                  </div>
                  <hr/>
                  <div class="table-total clearfix">
                    <div class="col-lg-8 col-md-7 col-sm-5 marBot20">
                      <h4 class="marTop-0">Minimum Purchase for 1 reward point :
                        <span class="text-green"> S$ <?=$purchase->min_reward_purchase?> </span>
                      </h4>
                      <div class="text-thin">(S$1 = <?=$purchase->total_reward_per_dollar?> Reward Points) </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-7"> 
                    <div class="grand-total marBot10 clearfix">
                        <div class="text-left title">Total Weight: </div>
                        <div class=" text-thin details"><b><?php echo number_format($total_weight,0); ?> gram</b></div>
                      </div>
                    <div class="grand-total marBot10 clearfix">
                        <div class="text-left title">Sub Total : </div>
                        <div class=" text-thin details"><b>S$ <?php echo number_format($subtotal,2); ?> </b></div>
                      </div>
                    <?PHP
			            if(!empty($purchase->delivery_option) && $purchase->delivery_option !== "self collection"){
			            	if($purchase->delivery_charge > 0){
			        ?>
                      <div class="delivery-charge marBot10 clearfix">
                        <div class="text-left title">Delivery Charge :</div>
                        <div class="text-thin details"> <b>S$ <?php echo number_format((float)$purchase->delivery_charge,2); ?></b></div>
                      </div>
                     <?php }else{
                     	$config_deliverychg = Config::model()->findByAttributes(array('set'=>'delivery_charge'));
                     	?>
                     	<div class="delivery-charge marBot10 clearfix">
	                        <div class="text-left title"><strike>Delivery Charge :</strike></div>
	                        <div class="text-thin details"> <strike><b>S$ <?php echo number_format($config_deliverychg->val,2); ?></b></strike></div>
	                      </div>
                     	<?php
                     		}
			            }?>
			            <?php
                      if(!empty($purchase->existing_reward_point)){
						?>
						<div class="rewards marBot10 clearfix">
	                        <div class="text-left title">Existing Reward Points : </div>
	                        <div class="text-thin details"><b><?php echo $purchase->existing_reward_point; ?> Points</b></div>
	                      </div>
                    <?php
					}
					if(!empty($purchase->reward_earned)){
						?>
						<div class="rewards marBot10 clearfix">
	                        <div class="text-left title">Current Purchase Points Earned : </div>
	                        <div class="text-thin details"><b><?php echo $purchase->reward_earned; ?> Points</b></div>
	                      </div>
                    <?php
					}
                      if(!empty($purchase->reward_point)){
						?>
						<div class="rewards marBot10 clearfix">
	                        <div class="text-left title">Total Reward Points : </div>
	                        <div class="text-thin details"><b><?php echo $purchase->reward_point; ?> Points</b></div>
	                      </div>
                    <?php
					}
					if(!empty($purchase->reward_value)){
						?>
						<div class="rewards marBot10 clearfix">
	                        <div class="text-left title">Deduct From Reward Points :</div>
	                        <div class="text-thin details"><b>- S$ <?php echo $purchase->reward_value; ?></b></div>
	                     </div>
                    <?php
					}

					?>
					<?php 
					$sub = 'Grand Total';
					if(!empty($purchase->promo_code)){
						if($purchase->promo_code=== '1' || $purchase->promo_code=== '2'){
							$sub = 'Subtotal';
						}
					}
					?>
                      <div class="grand-total marBot10 clearfix">
                        <div class="text-left title"><?php echo $sub?> (Inclusive GST <?= $calculationResult['gst_percent'] ?>) : </div>
                        <div class=" text-thin details"><b>S$ <?php echo number_format($purchase->total,2); ?> </b></div>
                      </div>
                      <?php 
                      $savings = 0;
                      if(!empty($purchase->promo_code)){
                      	if($purchase->promo_code_type === '1'){
                      		$savings = $purchase->promo_off_total_bill;
                      		$subtotal= $purchase->total- $purchase->promo_off_total_bill;;
                      ?>
                      <div class="grand-total marBot10 clearfix">
                        <div class="text-left title"> <?php echo $purchase->promo_code?> (- $<?= $purchase->promo_off_total_bill ?>) : </div>
                        <div class=" text-thin details"><b style="color:#f00;">- S$ <?php echo $purchase->promo_off_total_bill; ?> </b></div>
                      </div>
                      <div class="grand-total marBot10 clearfix">
                        <div class="text-left title">Grand Total (After Promo) : </div>
                        <div class=" text-thin details"><b>S$ <?php echo $subtotal; ?> </b></div>
                      </div>
                      <?php
                      	}elseif($purchase->promo_code_type=== '2'){
                      		$percent = $purchase->total * $purchase->promo_discount_percent/100;
                      		$savings = $percent;
                      		$subtotal = $subtotal - $percent;
                      		
                      ?>
                      	<div class="grand-total marBot10 clearfix">
                        <div class="text-left title"> <?php echo $purchase->promo_code?> Discount (<?= $purchase->promo_discount_percent?>)% : </div>
                        <div class=" text-thin details"><b style="color:#f00;">- S$ <?php echo number_format($percent,2); ?> </b></div>
                      </div>
                      <div class="grand-total marBot10 clearfix">
                        <div class="text-left title">Grand Total (After Promo) : </div>
                        <div class=" text-thin details"><b>S$ <?php echo number_format($subtotal,2); ?> </b></div>
                      </div>
                      <?php 
                      	}
                      	$savings = $savings + $purchase->reward_value;
                      ?>
                      <div class="marBot10 clearfix"></div>
                      <div class="marBot10 clearfix"></div>
                      <div class="grand-total marBot10 clearfix">
                        <div class="text-left title text-green" style="font-style:italic;">Total Savings : </div>
                        <div class=" text-thin details text-green" style="font-style:italic;"><b>S$ <?php echo number_format($savings,2); ?> </b></div>
                      </div>
                      <?php
                      	
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <br/>
                <div class="delivery-intructions"><!--[start:payment reference]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Payment Reference</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Invoice No:</b></label>
                        <h5 class="text-uppercase"><?php echo $purchase->invoice_no; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Payment Option:</b></label>
                        <h5><?php echo $purchase->payment_option ?></h5>
                      </div>
                    </div>
                    <?php if($purchase->isPayment()){?>
                    <div class="col-lg-6 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Reference:</b></label>
                        <h5><?php echo !empty($purchase->getPayment()->transaction_id) ? $purchase->getPayment()->transaction_id: '-' ; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Status:</b></label>
                        <h5><?php echo $purchase->getPayment()->status; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Date:</b></label>
                        <h5><?php echo DateHelper::formatDate($purchase->getPayment()->created,'d F Y, h:i a'); ?></h5>
                      </div>
                    </div>
                    <?php }?>
                  </div>
                   <?php 
                        	if($purchase->payment_option === 'ATM TRANSFER'){
                       ?>
                       <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-8 col-md-9">
                       <p>
                       Please proceed to make payment to : </p>
                       <p>
                       <b>Bank Name : </b> DBS <br/>
                       <b>Account Name : </b> Friendly Vegetarian Food Supplier <br/>
                       <b>Account No : </b>005- 904255-5 (Current Account)</br/>
                      <p>Please reply to email receipt confirmation stating transaction number for successful payment. Thank you.</p>
                      </p>
                      </div>
                       <?php  		
                        	}
                        ?>
                </div><!--[end:delivery-intructions]-->
                <br/>
                <div class="delivery-intructions"><!--[start:delivery-intructions]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Delivery Instructions</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                      	<?PHP
                			if (!empty($model->delivery_option) && $model->delivery_option !== "self collection") {
	                    ?>
                        <label class="text-label150"><b>Delivery Date:</b></label>
                        <?php }else{?>
                        <label class="text-label150"><b>Collection Date:</b></label>
                        <?php }?>
                        <h5 class="text-uppercase"><?php echo empty($purchase->delivery_option) ? "DELIVERY" : strtoupper($purchase->delivery_option); ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Date:</b></label>
                        <h5><?php echo $purchase->delivery_date; ?></h5>
                      </div>
                      <div class="form-group">
                        <label class="text-label150"><b>Remark:</b></label>
                        <h5><?php echo empty($purchase->remark) ? '-' : $purchase->remark; ?></h5>
                      </div>
                    </div>
                  </div>
                  <hr/>
                  <p class="small padL20">
                    * Please read and understand the TERMS &amp; CONDITIONS.<br/>
                    * Delivery will only be made with a minimum purchase of S$100.<br/>
                    * All sales proceed will be made in Singapore.
                  </p>
                </div><!--[end:delivery-intructions]-->
                <div class="clearfix text-right">
                  <div class="col-lg-12">
                  <?php
                                if($this->admin){
                            ?>
                            <a onclick="window.open('<?=Yii::app()->request->baseUrl?>/purchase/print/<?=$purchase->id?>','width=600')" href="javascript:void(0);" id="print" class="btn btn-green btn-corner0 text-semi text-uppercase">PRINT REFERENCE</a>
                                <?php }else{
                                	
                            ?>
                    <a href="<?php echo Yii::app()->getBaseUrl(true)?>" class="btn btn-green btn-corner0 text-semi text-uppercase">Continue Shopping</a>&nbsp;&nbsp;
                            <?php 
                                }
                            ?>
                  </div>
                </div>
              </div><!--[end:thumbnail]-->
            </div>
          </div>  
        </div> <!--[end:continaer]-->     
      </div><!--[end:section-cart]-->
    </div><!--[end:site-content]-->
