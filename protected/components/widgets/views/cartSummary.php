<?php
    $purchase = $this->purchase;
    $purchaseItem = $this->purchaseItem;
    $admin = $this->admin;
    $customer = Customer::model()->findByPk($purchase->customer_id);
    $config = Config::model()->findByPk(3);
?>
<!-- Bootstrap -->
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true)?>/css/bootstrap.min.css" type="text/css" />
<style type="text/css">
	.invoice{
		font-family: Calibri;
		border: 1px solid #000000;
		width: 21cm;
		margin-top: 10px;
		margin-bottom: 10px;
		padding-top: 10px;
		padding-bottom: 10px;
		background: #ffffff;
	}
	hr.line{
		background-color: #000 !important;
		color: #000 !important;
		height: 1px !important;
		margin-top: 10px !important;
		margin-bottom: 0px !important;
	}
	.no-col{
		float: left;
		width: 5%;
	}
	.code-col{
		float: left;
		width: 14%;
	}
	.desc-col{
		float: left;
		width: 27%;
	}
	.qty-col{
		float: left;
		width: 10%;
	}
	.price-col{
		float: left;
		width: 10%;
	}
	.disc-col{
		float: left;
		width: 7%;
	}
	.excl-gst-col{
		float: left;
		width: 7%;
	}
	.gst-col{
		float: left;
		width: 7%;
	}
	.incl-gst-col{
		float: left;
		width: 7%;
	}
	.tax-col{
		float: left;
		width: 6%;
	}
	.header-invoice{
		padding-left: 120px;
	}
	.company-name{
		font-size: 28px;
		font-weight: bold;
	}
	.logo > img{
		float: left;
		margin-top: -95px;
		margin-left: 20px;
	}
	.header-tax-invoice{
		font-size: 24px;
		font-weight: bold;
	}
	.sub-header-invoice{
		margin-bottom: 10px;
	}
	.sub-header-invoice > .sub-left{
		float: left;
		width: 60%;
	}
	.sub-header-invoice > .sub-left > .sub-list{
		line-height: 18px;
	}
    .sub-header-invoice > .sub-left > .sub-list.address{
		max-width: 250px;
	}
	.sub-header-invoice > .sub-left > .sub-list.tel, .sub-header-invoice > .sub-left > .sub-list.cust-code{
		float: left;
		width: 50%;
	}
	.sub-header-invoice > .sub-right{
		float: left;
		width: 40%;
	}
	.sub-header-invoice > .sub-right > .sub-list{
		line-height: 25px;
	}
	.content-invoice{
		height: 400px;
	}
	.content-invoice-header{
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
	}
	.footer-invoice{
		margin-bottom: 50px;
	}
	.footer-left{
		float: left;
		width: 70%;
		padding-left: 10px;
	}
	.footer-right{
		float: left;
		width: 30%;
	}
	.footer-invoice > .footer-left > .header-left{
		padding-bottom: 15px;
	}
	.footer-invoice > .footer-left > .detail-left{
		line-height: 25px;
		font-size: 12px;
	}
	.footer-invoice > .footer-right > .box-top{
		border: 1px solid #000;
		margin-top: 5px;
		padding: 0px 5px;
	}
	.footer-invoice > .footer-right > .box{
		border: 1px solid #000;
		padding: 0px 5px;
		border-top: none;
	}
	.footer-invoice > .footer-right > .box .left, .footer-invoice > .footer-right > .box-top .left{
		float: left;
	}
	.footer-invoice > .footer-right > .box .right, .footer-invoice > .footer-right > .box-top .right{
		float: right;
	}
	.footer-gap{
		height: 80px;
	}
	.footer-signature{
		border-top: 1px solid #000;
		width: 30%;
		float: left;
		margin-right: 30px;
		font-weight: bold;
	}
</style>
</head>
<body>
<div align="center" class="container invoice">
	<div class="header-invoice">
    	<span class="company-name">CYCLE LIFE (M) SDN BHD</span>
		<span>(1235881-M)</span>
		<div>
			NO. 37 DATARAN MESTIKA, JALAN MAWAR 15, 56100 KUALA LUMPUR. <br />
            Phone: 012-4710005 &nbsp; Fax: 03-42921000 &nbsp;  Email: info@cyclelife.com.my <br />
            (GST No: 000718680064)
        </div>
    </div>
	<div class="logo">
		<img src="<?php echo URLHelper::getBackendBaseUrl()?>/images/logo.png" width="120px" />
		<div class="clearfix"></div>
	</div>
    <hr class="line" />
    <div class="header-tax-invoice">TAX INVOICE</div>
    <div align="left" class="sub-header-invoice">
		<div class="sub-left">
			<div class="sub-list address"><?= $purchase->address; ?></div>
			<div class="sub-list">Attn : <?= $purchase->name ?></div>
			<div class="sub-list tel">Tel : <?= $purchase->tel ?></div>
			<div class="sub-list cust-code">Customer Code : <?= empty($customer->customer_code) ? "-" : $customer->customer_code ?></div>
			<div class="clearfix"></div>
		</div>
		<div class="sub-right">
			<div class="sub-list">Inv No : <b><?= $purchase->invoice_no; ?></b></div>
			<div class="sub-list">Terms Date : <?= empty($purchase->terms) ? "-" : $purchase->terms; ?></div>
			<div class="sub-list">Page : 1 of 1</div>
			<div class="sub-list">Agent : CK</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="content-invoice">
		<div class="content-invoice-header">
			<div class="no-col">No</div>
			<div align="left" class="code-col">Item Code</div>
			<div align="left" class="desc-col">Description</div>
			<div class="qty-col">Qty</div>
			<div class="price-col">Price/Unit</div>
			<div class="disc-col">Disc</div>
			<div class="excl-gst-col">Total<br />Excl.GST</div>
			<div class="gst-col">GST <?= $config->val; ?></div>
			<div class="incl-gst-col">Total<br />Incl.GST</div>
			<div class="tax-col">Tax Code</div>
			<div class="clearfix"></div>
		</div>
		<?php
            $no=0;
            $total_excl = 0;
            $total_gst = 0;
            $total_incl = 0;
            foreach($purchaseItem as $item){
                $no++;
                $product=Product::model()->findByPk($item->product_id);
                $excl=$item->quantity*$item->price;
                $gst=$excl*$config->val/100;
                $incl=$excl+$gst;
                $total_excl=$total_excl+$excl;
                $total_gst=$total_gst+$gst;
                $total_incl=$total_incl+$incl;
        ?>
    	<div>
			<div class="no-col"><?php echo $no; ?></div>
			<div align="left" class="code-col"><?= $product->item_code; ?></div>
			<div align="left" class="desc-col"><?= $product->description; ?></div>
			<div class="qty-col"><?= $item->quantity; ?> UNIT</div>
			<div class="price-col"><?= $item->price; ?></div>
			<div class="disc-col">&nbsp;</div>
			<div class="excl-gst-col"><?= number_format($excl, 2); ?></div>
			<div class="gst-col"><?= number_format($gst, 2); ?></div>
			<div class="incl-gst-col"><?= number_format($incl, 2); ?></div>
			<div class="tax-col">SR</div>
			<div class="clearfix"></div>
		</div>
		<?php
            }
        ?>
	</div>
    <hr class="line" />
	<?php
	function Terbilang($x){
	    $abil = array("", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve");
	    if ($x < 13){
            return " ".$abil[$x];
	    }elseif ($x < 20){
	        if($x == 13){
	            return "thirteen";
	        }elseif($x == 15){
	            return "fifteen";
	        }elseif($x == 18){
	            return "eighteen";
	        }else{
                return Terbilang($x - 10) . "teen";
	        }
	    }elseif ($x < 100){
	        if(floor($x / 10) == 2){
	            return "twenty" . Terbilang($x % 10);
	        }elseif(floor($x / 10) == 3){
	            return "thirty" . Terbilang($x % 10);
	        }elseif(floor($x / 10) == 4){
	            return "forty" . Terbilang($x % 10);
	        }elseif(floor($x / 10) == 5){
	            return "fifty" . Terbilang($x % 10);
	        }elseif(floor($x / 10) == 8){
	            return "eighty" . Terbilang($x % 10);
	        }else{
                return Terbilang($x / 10) . "ty" . Terbilang($x % 10);
	        }
	    }elseif ($x < 1000){
	        return Terbilang($x / 100) . " hundred " . Terbilang($x % 100);
	    }elseif ($x < 1000000){
	        return Terbilang($x / 1000) . " thousand " . Terbilang($x % 1000);
	    }
	}
	
	$num = $total_incl;
	$int = intval($num);
	$dec = $num * 100 % 100;
	
	if($dec>0){
	    $total_label = strtoupper(Terbilang($num)." and ".Terbilang($dec)." cents")." ONLY";
	}else{
	    $total_label = strtoupper(Terbilang($num))." ONLY";
	}
    ?>
	<div align="left" class="footer-invoice">
		<div class="footer-left">
			<div class="header-left">RINGGIT MALAYSIA : <?= $total_label; ?></div>
			<div class="detail-left">All cheques should be crossed and made payable to CYCLE LIFE (M) SDN BHD</div>
			<div class="detail-left">For online payment, please pay to:</div>
			<div class="detail-left"><b>CIMB BANK: 800 882 8776</b></div>
		</div>
		<div class="footer-right">
			<div class="box-top">
				<div class="left">Sub Total</div>
				<div class="right"><?= number_format($total_excl, 2); ?></div>
				<div class="clearfix"></div>
				<div class="left">Total Discount</div>
				<div class="right"><?= number_format(0, 2); ?></div>
				<div class="clearfix"></div>
			</div>
			<div class="box">
				<div class="left">Total Excl. GST</div>
				<div class="right"><?= number_format($total_excl, 2); ?></div>
				<div class="clearfix"></div>
				<div class="left">Add GST <?= $config->val; ?></div>
				<div class="right"><?= number_format($total_gst, 2); ?></div>
				<div class="clearfix"></div>
			</div>
			<div class="box">
				<div class="left">Total Incl. GST</div>
				<div class="right"><b><?= number_format($total_incl, 2); ?></b></div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="footer-gap">&nbsp;</div>
		<div align="center" class="footer-signature">Authorized Signatrure</div>
		<div align="center" class="footer-signature">Recieved By</div>
		<div class="clearfix"></div>
	</div>
</div>
</body>
</html>