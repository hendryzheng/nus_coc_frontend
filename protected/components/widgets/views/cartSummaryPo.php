<?php
    $purchase = $this->purchase;
    $purchaseItem = $this->purchaseItem;
?>
<?php
if($this->include_css){
?>
<!-- Bootstrap -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- style -->
    <link href="<?php echo Yii::app()->getBaseUrl(true)?>/css/app.css" rel="stylesheet">
    
<?php 
}
?>
<style>
h5,h4,.details{
	font-weight:bold !important;
	color:#000 !important;
}
</style>
                 <input type="hidden" id="purchase_id" value="<?=$purchase->id?>"/>
    <div class="site-content"><!--[start:site-content]-->
      <div class="section-cart"><!--[start:section-cart]-->
        <div class="container"><!--[start:container]-->
        <?php 
        	if($this->include_css){
        ?>
        <div class="row">
		<div class="col-lg-2">
			<img src="<?php echo Yii::app()->getBaseUrl(true)?>/img/logo.png" alt="Friendly Vegetarian Logo" class="img-responsive" />
		</div>
		<div class="col-lg-10">
			<h1>Friendly Vegetarian Food Supplier</h1>
			<p>Block 421 ANG MO KIO AVE 10 #01-1165 SINGAPORE 560421<br/>
			TEL: 64566607 FAX: 64541632<br/>
			Block 14 Pasir Panjang #01-24 Wholesale Center Singapore 110014<br/>
			TEL: 67791488 FAX: 67792635<br/>
			Email: <a href="mailto:sales@friendlyvegetarian.com.sg">sales@friendlyvegetarian.com.sg</a>   Web: <a href="http://www.friendlyvegetarian.com.sg">http://www.friendlyvegetarian.com.sg</a><br/>
			<small>Company Registered number: 39100200K GST Reg. No. M8-8002457-4</small>
		</div>
	</div>
        <?php }?>
          <div class="row">
            <div class="col-lg-12">
              <div class="thumbnail pad20"><!--[start:thumbnail]-->
                <h3 class="entry-title text-semi marTop-0">
                <?php 
                        if(!empty(Yii::app()->user->type) && Yii::app()->user->type == "corporate"){
                            echo 'PURCHASE ORDER';
                        }else{
                            echo 'ORDER CONFIRMATION';
                        }
                        
                    ?></h3>
                <p class="marBot20 small">
                  Your Order <b>#<?php echo $purchase->invoice_no; ?></b> is being processed. We will get back to you shortly.<br/>
                  A copy of your <?php 
                        if(!empty(Yii::app()->user->type) && Yii::app()->user->type == "corporate"){
                            echo 'PURCHASE ORDER';
                        }else{
                            echo 'ORDER CONFIRMATION';
                        }
                        
                    ?> is being sent to your email account.<br/>
                  Thank you for shopping with us..
                </p>
                <div class="personal-info"><!--[start:personal particulars]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Personal Particulars</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Name:</b></label>
                        <h5><?php echo $purchase->name; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Email:</b></label>
                        <h5><?php echo $purchase->email; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label70"><b>Telp:</b></label>
                        <h5><?php echo $purchase->tel; ?></h5>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label90"><b>Address:</b></label>
                        <h5><?php echo $purchase->address; ?></h5>
                      </div>
                    </div>
                  </div>
                </div><!--[end:personal particulars] -->
                <div class="table-cart table-responsive table">
                  <div class="divTable clearfix">
                    <div class="divTableHeading clearfix">
                      <div class="divTableRow">
                        <div class="divTableHead pad10 col-lg-1 col-md-1 col-sm-1 text-regular text-left hidden-xs"><b>S/No.</b></div>
                        <div class="divTableHead pad10 col-lg-5 col-md-6 col-sm-5 text-regular"><b>Item</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-2 col-sm-2 text-regular text-center hidden-xs"><b>Unit Price</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-1 col-sm-2 text-regular text-center hidden-xs"><b>Quality</b></div>
                        <div class="divTableHead pad10 col-lg-2 col-md-2 col-sm-2 text-regular text-right hidden-xs"><b>Sub Total</b></div>
                      </div>
                    </div>
                    <div class="divTableBody">
                    <?php
			                $subtotal = 0;
			                $count = 0;
			                Yii::app()->session['after_gst'] = ConfigHelper::getVal('gst_inclusive');
			               foreach($purchaseItem as $item){
			               	$subtotal+= $item->price * Yii::app()->session['after_gst'] * $item->quantity; //200 (2*100)
			               	$image = Image::model()->getImagePreview($item->product);
			                 $image = Image::model()->findByAttributes(array('image_type'=>'product','image_type_primary'=>$item->product->id));
			        ?>
			        <div class="divTableRow clearfix">
                        <div class="divTableCell col-lg-1 col-md-1 col-sm-1 text-regular text-center hidden-xs">1</div>
                        <div class="divTableCell col-lg-5 col-md-6 col-sm-5 text-regular">
                          <div class="media">
                            <?php if($this->include_css === false){?>
                            <div class="media-left">
                              <img src="<?php echo URLHelper::getBackendBaseUrl()."/".@$image->path?>" alt="dummy" class="img-responsive"/>
                            </div>
                            <?php }?>
                            <div class="media-body">
                              <h4 class="marTop-0"><?php echo $item->product->name?> </h4>
                            </div>
                          </div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular unit-price">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Unit Price:</b></div> 
                          <div class="details"> S$ <?php echo round($item->price * Yii::app()->session['after_gst'],2)?></div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-1 col-sm-2 text-regular quality">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Quantity:</b></div> 
                          <div class="details">X <?php echo $item->quantity?>
							</div>
                        </div>
                        <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular sub-total">
                          <div class="hidden-lg hidden-md hidden-sm title"><b>Sub Total:</b></div> 
                          <div class="details">S$ <?php echo number_format($item->price * Yii::app()->session['after_gst'] * $item->quantity,2)?> </div>
                        </div>
                      </div>
			        <?php }?>
			        <?php 
                      $calculationResult = PurchaseOrder::calculateTotalPurchase ( $subtotal );
//                       $rewards = $calculationResult ['rewards'];
                      ?>
<!--                       <div class="divTableRow clearfix"> -->
<!--                         <div class="divTableCell col-lg-1 col-md-1 col-sm-1 text-regular text-center hidden-xs">1</div> -->
<!--                         <div class="divTableCell col-lg-5 col-md-6 col-sm-5 text-regular"> -->
<!--                           <div class="media"> -->
<!--                             <div class="media-left"> -->
<!--                               <img src="img/dummy-pic3.png" alt="dummy" class="img-responsive"/> -->
<!--                             </div> -->
<!--                             <div class="media-body"> -->
<!--                               <h4 class="marTop-0">Monkeyhead Mushroom #lactoveg 1.8kg 白猴頭菇 猴头菇 </h4> -->
<!--                               <div class="text-thin">1.8kg Friendly Vegetarian 善缘素食</div> -->
<!--                             </div> -->
<!--                           </div> -->
<!--                         </div> -->
<!--                         <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular unit-price"> -->
<!--                           <div class="hidden-lg hidden-md hidden-sm title">Unit Price:</div>  -->
<!--                           <div class="details"> S$ 28.50</div> -->
<!--                         </div> -->
<!--                         <div class="divTableCell col-lg-2 col-md-1 col-sm-2 text-regular quality"> -->
<!--                           <div class="hidden-lg hidden-md hidden-sm title">Quality:</div>  -->
<!--                           <div class="details">X 5</div> -->
<!--                         </div> -->
<!--                         <div class="divTableCell col-lg-2 col-md-2 col-sm-2 text-regular sub-total"> -->
<!--                           <div class="hidden-lg hidden-md hidden-sm title">Sub Total:</div>  -->
<!--                           <div class="details">S$ 142.50 </div> -->
<!--                         </div> -->
<!--                       </div> -->
                    </div>
                  </div>
                  <hr/>
                  <div class="table-total clearfix">
                    <div class="col-lg-8 col-md-7 col-sm-5 marBot20">
                      

                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-7"> 
                    <?PHP
			            if(!empty($purchase->delivery_option) && $purchase->delivery_option !== "self collection"){
			        ?>
                      <div class="delivery-charge marBot10 clearfix">
                        <div class="text-left title"><strike>Delivery Charge :</strike></div>
                        <div class="text-thin details"> <b><strike>S$ <?php echo number_format((float)$purchase->delivery_charge); ?></strike></b></div>
                      </div>
                     <?php }?>

                      <div class="grand-total clearfix">
                        <div class="text-left title">Grand Total (Inclusive GST <?= $calculationResult['gst_percent'] ?>): </div>
                        <div class=" text-thin details"><b>S$ <?php echo number_format($purchase->total,2); ?> </b></div>
                      </div>
                    </div>
                  </div>
                </div>
                <br/>
                <div class="delivery-intructions"><!--[start:payment reference]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Payment Reference</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Invoice No:</b></label>
                        <h5 class="text-uppercase"><?php echo $purchase->invoice_no; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Payment Option:</b></label>
                        <h5><?php echo $purchase->payment_option ?></h5>
                        
                      </div>
                    </div>
                    
                    <?php if($purchase->isPayment()){?>
                    <div class="col-lg-6 col-md-6">
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Reference:</b></label>
                        <h5><?php echo !empty($purchase->getPayment()->transaction_id) ? $purchase->getPayment()->transaction_id: '-' ; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Status:</b></label>
                        <h5><?php echo $purchase->getPayment()->status; ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label200"><b>Payment Date:</b></label>
                        <h5><?php echo DateHelper::formatDate($purchase->getPayment()->created,'d F Y, h:i a'); ?></h5>
                      </div>
                    </div>
                    <?php }?>
                  </div>
                  <?php 
                        	if($purchase->payment_option === 'ATM TRANSFER'){
                       ?>
                       <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-8 col-md-9">
                       <p>
                       Please proceed to make payment to : </p>
                       <p>
                       <b>Bank Name : </b> DBS <br/>
                       <b>Account Name : </b> Friendly Vegetarian Food Supplier <br/>
                       <b>Account No : </b>005- 904255-5 (Current Account)</br/>
                      <p>Please reply to email receipt confirmation stating transaction number for successful payment. Thank you.</p>
                      </p>
                      </div>
                       <?php  		
                        	}
                        ?>
                </div><!--[end:delivery-intructions]-->
                <br/>
                <div class="delivery-intructions"><!--[start:delivery-intructions]-->
                  <div class="divTableHeading pad10">
                    <h4 class="marTop-0 marBot-0"><b>Delivery Instructions</b></h4>
                  </div>
                  <div class="marTop20 marBot20 clearfix">
                    <div class="col-lg-5 col-md-6">
                      <div class="form-group clearfix">
                      	<?PHP
                			if (!empty($model->delivery_option) && $model->delivery_option !== "self collection") {
	                    ?>
                        <label class="text-label150"><b>Delivery Date:</b></label>
                        <?php }else{?>
                        <label class="text-label150"><b>Collection Date:</b></label>
                        <?php }?>
                        <h5 class="text-uppercase"><?php echo empty($purchase->delivery_option) ? "DELIVERY" : strtoupper($purchase->delivery_option); ?></h5>
                      </div>
                      <div class="form-group clearfix">
                        <label class="text-label150"><b>Date:</b></label>
                        <h5><?php echo $purchase->delivery_date; ?></h5>
                      </div>
                      <div class="form-group">
                        <label class="text-label150"><b>Remark:</b></label>
                        <h5><?php echo empty($purchase->remark) ? '-' : $purchase->remark; ?></h5>
                      </div>
                    </div>
                  </div>
                  <hr/>
                  <p class="small padL20">
                    * Please read and understand the TERMS &amp; CONDITIONS.<br/>
                    * Delivery will only be made with a minimum purchase of S$100.<br/>
                    * All sales proceed will be made in Singapore.
                  </p>
                </div><!--[end:delivery-intructions]-->
                <div class="clearfix text-right">
                  <div class="col-lg-12">
                  <?php
                                if($this->admin){
                            ?>
                            <a onclick="window.open('<?=Yii::app()->request->baseUrl?>/purchase/print/<?=$purchase->id?>','width=600')" href="javascript:void(0);" id="print" class="btn btn-green btn-corner0 text-semi text-uppercase">PRINT REFERENCE</a>
                                <?php }else{
                                	
                            ?>
                    <a href="<?php echo Yii::app()->getBaseUrl(true)?>" class="btn btn-green btn-corner0 text-semi text-uppercase">Continue Shopping</a>&nbsp;&nbsp;
                            <?php 
                                }
                            ?>
                  </div>
                </div>
              </div><!--[end:thumbnail]-->
            </div>
          </div>  
        </div> <!--[end:continaer]-->     
      </div><!--[end:section-cart]-->
    </div><!--[end:site-content]-->
