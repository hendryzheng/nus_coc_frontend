<?php
    if (!empty(Yii::app()->user->idno)) {
        if(Yii::app()->user->type == "corporate"){
            $cart_id = UserCart::model()->findByAttributes(array('corp_id' => Yii::app()->user->idno,'status' => '0'),array('order'=>'created desc'),array('limit'=>'1'));
        }else{
            $cart_id = UserCart::model()->findByAttributes(array('user_id' => Yii::app()->user->idno,'status' => '0'),array('order'=>'created desc'),array('limit'=>'1'));
        }
        if(Yii::app()->user->type != "admin"){
        	if(!empty($cart_id)){
        		$positions = UserCartProduct::model()->findAllByAttributes(array('user_cart_id' => $cart_id->id));
        		$count = UserCartProduct::model()->countByAttributes(array('user_cart_id' => $cart_id->id));
        	}
            
        }else{
            $positions = Yii::app()->shoppingCart->getPositions();
            $count = Yii::app()->shoppingCart->getCount();
        }
    }else{
	    $positions = Yii::app()->shoppingCart->getPositions();
	    $count = Yii::app()->shoppingCart->getCount();
    }
	if(empty($count)){
		$count = 0;
	}
	$subtotal = 0;
	
	$config_minorder = Config::model()->findByAttributes(array('set' => 'total_purchase_for_waive'));
	$config_deliverychg = Config::model()->findByAttributes(array('set' => 'delivery_charge'));
	$config_gst = Config::model()->findByAttributes(array('set' => 'gst'));
	
	$minOrder = intval($config_minorder->val);
	$delivery_charge = floatval($config_deliverychg->val);
	$gst = intval($config_gst->val) / 100;
	$progress=0;
	$addUp = $minOrder;
?>
<a href="javascript:void(0)" class="dropdown-toggle"><span class="icon-shopcart"></span><span><?php echo $count?></span></a>
<div class="dropdown-menu drop-cart">
	<!--[start:drop-cart]-->
	<div class="drop-head pad20">
		<!--[start:drop-head]-->
		<h5 class="text-semi mar0">ORDER Items (<?php echo $count;?>)</h5>
		<p>YOUR ORDER Items arrive via Friendly Uncle in 1-3 Business Days after payment</p>
	</div>
	<!--[end:drop-head]-->
	<?php if(!empty($positions)){?>
	<div class="drop-item pad20">
		<!--[start:drop-item]-->
		<?php
		foreach ($positions as $row) {
		    if (!empty(Yii::app()->user->idno) && Yii::app()->user->type !== "admin") {
                $item = Product::model()->findByPk($row->product_id);
                
                $image=Image::model()->getImagePreview($item);
                $id=$item->id;
                $name=$item->name;
                $qty=$row->qty;
                $price=$item->normal_price * Yii::app()->session['after_gst'];
                $sumPrice=$row->qty*($item->normal_price * Yii::app()->session['after_gst']);
                
                $subtotal+= $sumPrice;
		    }else{
		        $image=Image::model()->getImagePreview($row);
		        $id=$row->id;
		        $name=$row->name;
		        $qty=$row->getQuantity();
		        $price=$row->getPrice() * Yii::app()->session['after_gst'];
		        $sumPrice=$row->getSumPrice() * Yii::app()->session['after_gst'];
		        
		        $subtotal+= $row->getSumPrice() * Yii::app()->session['after_gst'];
		    }
		?>
		<div class="list-item clearfix">
			<!--[start:list-item]-->
			<div class="thumb-item text-center">
				<img src="<?php echo $image?>" alt="thumb" class="img-responsive" style="max-height:150px;" />
			</div>
			<div class="desc-item">
				<h4 class="item-name"><?php echo $name?></h4>
				<div class="price-tags text-semi">$<?php echo number_format($price,2)?></div>
			</div>
			<div class="quantity-item text-right">
				<div class="quantity_box">
					<span>Qty</span>
					<div class="custom-select" style="border:none;">
						<input style="width:50px;" type="number" data-id="<?php echo $id?>" class="quantity-cart" name="quantity" value="<?php echo $qty?>"/>
					</div>
				</div>
				<div class="total-qty">$<?php echo $sumPrice?></div>
				<a href="javascript:void(0)" style="color:#f00;" data-id="<?php echo $id?>" data-title="<?php echo $name?>" class="removecart remove-item">Remove <img alt="delete" src="<?php echo URLHelper::getAppUrl()?>img/delete.png"></a>
			</div>
		</div>
		<!--[end:list-item]-->
		<?php 
		}
		$addUp = 0.00;
		$progress = 100;
		if($subtotal < $minOrder){
			$addUp = $minOrder - $subtotal;
			$progress = intval(floor($subtotal / $minOrder * 100));
			
			if(is_float($progress/10)){
				$progress = floor($progress/10)*10;
			}
		}
		?>
		
<!-- 		<div class="list-item clearfix"> -->
			<!--[start:list-item]-->
<!-- 			<div class="thumb-item text-center"> -->
<!-- 				<img src="img/dummy-pic2.png" alt="thumb" class="img-responsive" /> -->
<!-- 			</div> -->
<!-- 			<div class="desc-item"> -->
<!-- 				<h4 class="item-name">Veggie Meat Ball 600g 獅子頭</h4> -->
<!-- 				<div class="price-tags text-semi">$7.00</div> -->
<!-- 			</div> -->
<!-- 			<div class="quantity-item text-right"> -->
<!-- 				<div class="quantity_box"> -->
<!-- 					<span>Qty</span> -->
<!-- 					<div class="custom-select np-ico-caret"> -->
<!-- 						<select id="quantity" name="quantity" -->
<!-- 							placeholder="Choose Quantity"> -->
<!-- 							<option>1</option> -->
<!-- 							<option>2</option> -->
<!-- 							<option>3</option> -->
<!-- 							<option>4</option> -->
<!-- 							<option>5</option> -->
<!-- 							<option>6</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="total-qty">$7.00</div> -->
<!-- 				<a href="#" class="remove-item">Remove</a> -->
<!-- 			</div> -->
<!-- 		</div> -->
		<!--[end:list-item]-->
	</div>
	<?php } ?>
	<!--[end:drop-item]-->
	<div class="order-calc">
		<!--[start:order-calc]-->
		<div class="pad20 clearfix">
			<h5 class="pull-left text-uppercase mar0">Your Order</h5>
			<div class="progressBarContainer">
				<div class="progressBarValue value-<?php echo $progress?>"></div>
			</div>
			<h5 class="order-count pull-right text-uppercase text-semi">$<?php echo $subtotal?></h5>
			<span class="order-notice">Add $<?php echo $addUp?> to meet FREE DELIVERY</span>
			
			<p align="center"><small><strong>Pay $<?php echo $delivery_charge?> to purchase now or choose self collection to checkout.</strong></small></p>
		</div>
	</div>
	<?php if(!empty($positions)){ ?>		
	<!--[end:order-calc]-->
	<div class="order-total">
		<!--[start:order-total]-->
		<div class="pad20">
			<div class="clearfix">
				<h5 class="pull-left text-uppercase marTop-0">Sub total</h5>
				<h5 class="pull-right text-uppercase text-semi marTop-0">$<?php echo $subtotal?></h5>
			</div>
			<a href="<?php echo Yii::app()->getBaseUrl(true)?>/cart" class="btn btn-green btn-md btn-block text-semi">Checkout</a>
		</div>
	</div>
	<!--[end:order-total]-->
	<?php }else{ ?>
	<div class="order-total">
		<!--[start:order-total]-->
		<div class="pad20">
		<a href="<?php echo Yii::app()->getBaseUrl(true)?>/" class="btn btn-green btn-md btn-block text-semi">Continue Shopping</a>
	</div>
	</div>
	<?php } ?>
</div>
 