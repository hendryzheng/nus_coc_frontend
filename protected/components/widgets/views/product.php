<?php
$product = $this->product;
$image = Image::model()->getImagePreview($product);
$classification = Classification::model()->findAll();
?>
			<div class="col-lg-3 col-md-3 col-sm-6 product">
			  <?php 
			    /*
			  	if(!empty($this->badge)){
			  		echo ' <div class="badges text-uppercase text-center text-semi">sale</div>';
			  	}
			  	*/
			  ?>
			  <?php /* if(!empty($this->sale)){?>
			  <div class="badges text-uppercase text-center text-semi">sale</div>
			  <?php } */?>
              <div class="thumbnail thumb-md">
              	<div class="img-wrapper" align="center">
                	<img src="<?php echo $image?>">
                </div>
                <div class="desc">
                  <div class="classification-img" align="center">
                  <?php
                  	foreach($classification as $row){
                  		if($product->classification == $row->id){
                  			$icon = $row['icon'];
                  		}else{
                  			$icon = $row['icon_grey'];
                  		}
                  		echo '<img src="'.Yii::app()->getBaseUrl(true).'/'.$icon.'"/>';
                  	}
                  ?>
                  </div>
                  <h2 class="entry-title text-semi text-md"><a href="<?php echo Yii::app()->getBaseUrl(true)?>/product/<?php echo $product->id?>/<?php echo StringHelper::removeSpace($product->name)?>"><?php echo StringHelper::trim($product->name,60)?></a></h2>
                  <p><?php echo StringHelper::trim(strip_tags($product->description), 65)?></p>
                  <?php 
                  	if($this->gooddeals === true){
                  		if($product->usual_price < 1){
                  			$product->usual_price = ($product->normal_price + 2) * Yii::app()->session['after_gst'];
                  		}
                  ?>
                  <h4 class="text-semi" style="text-decoration: line-through;">$ <?php echo number_format($product->usual_price,2)?></h4>
                  <?php }?>
                  <h4 class="price-tags text-semi">
                  $
                  <?php
                  	if(!empty(Yii::app()->user->type) && Yii::app()->user->type === 'corporate'){
                  		echo number_format($product->corp_price * Yii::app()->session['after_gst'],2);
                  	}else{
                  		echo number_format($product->normal_price * Yii::app()->session['after_gst'],2);
                  	}
                  ?>
                   <span class="weight">(<?php echo $product->weight?>)</span>
                   </h4>
				   <?php // if(!empty(Yii::app()->user->idno)){ ?>
				   <a data-id="<?php echo $product->id?>" data-title="<?php echo $product->name?>" href="javascript:void(0)" style="text-align: left" class="addtowishlist">
					<div class="btn-wishlist text-semi" align="center">
						<span></span>
						<span class="wishlist-text"><i class="fa fa-heart-o" style="color: white;" aria-hidden="true"></i> Add to wishlist</span>
					</div>
				   </a>
				   <?php /* }else{ ?>
				   <a href="#" data-toggle="modal" data-target="#Signin">
				   	<div class="btn-wishlist text-semi">
						<span class="icon-list"></span>
						<span class="wishlist-text">Add to wishlist</span>
					</div>
				   </a>
				   <?php } */ ?>
                </div>
                <div class="entry-footer btn-green text-center clearfix">
                  <input class="qty<?php echo $product->id?>" type="number" value="1"/>
                  <a data-id="<?php echo $product->id?>" data-title="<?php echo $product->name?>" href="javascript:void(0)" class="addtocart btn text-semi">
                  	<span class="icon-cart"></span>Add to cart
				  </a>
                </div>
              </div>
            </div>