<?php
    if (!empty(Yii::app()->user->idno)) {
        if (Yii::app()->user->type == "corporate") {
            $wishlist = Wishlist::model()->findAllByAttributes(array('corporate_id'=>Yii::app()->user->idno,'status'=>'1'),array('order'=>'created_date desc'));
            $count = Wishlist::model()->countByAttributes(array('corporate_id'=>Yii::app()->user->idno,'status'=>'1'));
        } else {
            $wishlist = Wishlist::model()->findAllByAttributes(array('customer_id'=>Yii::app()->user->idno,'status'=>'1'),array('order'=>'created_date desc'));
            $count = Wishlist::model()->countByAttributes(array('customer_id'=>Yii::app()->user->idno,'status'=>'1'));
        }
    }else{
        if(!empty(Yii::app()->session['wishlistItem'])){
            $wishlist = Yii::app()->session['wishlistItem'];
            $count = count(Yii::app()->session['wishlistItem']);
        }else{
            $count = 0;
        }
    }
?>
<a href="javascript:void(0)" class="dropdown-toggle"><span><i class="fa fa-heart-o" style="color: black;" aria-hidden="true"></i></span>&nbsp; <span><?php echo $count; ?></span></a>
<div class="dropdown-menu drop-cart">
	<!--[start:drop-cart]-->
	<div class="drop-head pad20">
		<!--[start:drop-head]-->
		<h5 class="text-semi mar0">Wishlist Items (<?php echo $count;?>)</h5>
	</div>
	<!--[end:drop-head]-->
	<?php if(!empty($wishlist)){ ?>
	<div class="drop-item pad20">
		<!--[start:drop-item]-->
		<?php
		foreach ($wishlist as $item) {
		    if (!empty(Yii::app()->user->idno)) {
                $product_id=$item->product_id;
                $id=$item->id;
		    }else{
		        $product_id=$item;
		        $id=$item;
		    }
		    
		    $product_detail = Product::model()->findByPk($product_id);
		    $image = Image::model()->getImagePreview($product_detail);
		?>
		<div class="list-item clearfix">
			<!--[start:list-item]-->
			<div class="thumb-item text-center">
				<img src="<?php echo $image?>" alt="thumb" class="img-responsive" style="max-height:150px;" />
			</div>
			<div class="desc-item">
				<h4 class="item-name"><?php echo $product_detail->name?></h4>
				<?php if (!empty(Yii::app()->user->idno)) { ?>
					<?php if (Yii::app()->user->type == "corporate") { ?>
					<div class="price-tags text-semi">$<?php echo number_format($product_detail->corp_price * Yii::app()->session['after_gst'],2);?></div>
					<?php }else{ ?>
					<div class="price-tags text-semi">$<?php echo number_format($product_detail->normal_price * Yii::app()->session['after_gst'],2);?></div>
					<?php } ?>
				<?php }else{ ?>
					<div class="price-tags text-semi">$<?php echo number_format($product_detail->normal_price * Yii::app()->session['after_gst'],2);?></div>
				<?php } ?>
			</div>
			<div class="quantity-item text-right">
				<div class="quantity_box">
					<span>Qty</span>
					<div class="custom-select" style="border:none;">
						<input style="width:50px;" type="number" class="quantity-cart qty<?php echo $product_id?>" name="quantity" value="1"/>
					</div>
				</div>
				<a href="javascript:void(0)" style="color:#f99516;" data-id="<?php echo $product_id?>" data-title="<?php echo $product_detail->name?>" class="addtocart add-item">Add to Cart</a>
				<a href="javascript:void(0)" style="color:#f00;" data-id="<?php echo $id?>" data-title="<?php echo $product_detail->name?>" class="removewishlist remove-item">Remove <img alt="delete" src="img/delete.png"></a>
			</div>
		</div>
		<!--[end:list-item]-->
		<?php 
		}
		?>
	</div>
	<?php } ?>
	<div class="order-total">
		<!--[start:order-total]-->
		<div class="pad20">
			<?php if($count!=0){ ?>
			<a href="<?php echo Yii::app()->getBaseUrl(true)?>/wishlist" class="btn btn-green btn-md btn-block text-semi">View Wishlist</a>
			<?php }else{ ?>
			<a href="<?php echo Yii::app()->getBaseUrl(true)?>/" class="btn btn-green btn-md btn-block text-semi">Add Wishlist</a>
			<?php } ?>
		</div>
	</div>
</div>
 