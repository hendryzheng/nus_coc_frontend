<?php
    $purchase = $this->purchase;
    $purchaseItem = $this->purchaseItem;
    Yii::app()->session['after_gst'] = ConfigHelper::getVal('gst_inclusive');
?>
<meta charset="UTF-8"/>
<style>
    .navlinks ul, .header, .navlinks{
        display:none;
    }
    .login{
        visibility: hidden;
    }
    .container{
        width:100%;
    }
    td{
    	font-size:12px;
    }
    .head-title td{
    	color:#fff;
    }
</style>
<style type="text/css">
	body { margin:0; padding:0; font-family:Georgia, "Times New Roman", Times, serif; font-size:12px; color:#000; }
</style>
<table width="800" border="0" cellspacing="15" cellpadding="0">
  <tr>
    <td align="left" valign="top"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/logo.png" alt="" width="" height="90" longdesc="<?=URLHelper::getBackendBaseUrl()?>" /></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    Block 421 Ang Mo Kio Avenue 10 #01-1165 S(560421)<br />
    Tel: 64566607 / 64566749 Fax:64541632<br /><br />
    Block 14 Pasir Panjang Wholesale Center #01-23/24 S(110014)<br />
    Tel: 67791488 / 67798341 Fax: 67792635<br/><br/>
    Co.Reg. No. 39100200k     GST Reg.No. M8-8002457-4
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#999"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" width="1" height="4" style="display:block; margin:0; border:none;" /></td>
  </tr>
  <tr>
      <td align="center" valign="top" style="font-size:20px;">THANK YOU FOR SHOPPING WITH US</td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#999"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" width="1" height="4" style="display:block; margin:0; border:none;" /></td>
  </tr>
  <tr>
      <td align="left" valign="top">
    Invoice : <strong>#<?php echo $purchase->invoice_no; ?></strong><br />
    Thank you for shopping with us. <br/>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#43b02a">
    <table width="570" border="0" cellspacing="0" cellpadding="0" class="head-title">
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="26" style="display:block; margin:0; border:none;" /></td>
        <td width="554" align="left" valign="middle"><strong>Personal Particulars</strong></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="570" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="10" style="display:block; margin:0; border:none;" /></td>
        <td width="658" align="left" valign="top">
        <table width="658" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="1" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Name </td>
            <td width="357" align="left" valign="top">:<?php echo $purchase->name; ?></td>
          </tr>
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="2" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Email </td>
            <td width="357" align="left" valign="top">:<?php echo $purchase->email; ?></td>
          </tr>
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="2" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Tel </td>
            <td width="357" align="left" valign="top">:<?php echo $purchase->tel; ?></td>
          </tr>
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="2" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Delivery Address </td>
            <td width="357" align="left" valign="top">:<?php echo $purchase->address; ?></td>
          </tr>
        </table><br/><br/>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#43b02a">
    <table width="750" border="0" cellspacing="0" cellpadding="0" class="head-title">
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="26" style="display:block; margin:0; border:none;" /></td>
        <td width="60" align="left" valign="middle"><strong>S/No.</strong></td>
        <td width="388" align="left" valign="middle"><strong>Item</strong></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
        <td width="120" align="left" valign="middle"><strong>Unit Price</strong></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
        <td width="100" align="left" valign="middle"><strong>Quantity</strong></td>
        <td width="80" align="right" valign="middle"><strong>Subtotal</strong></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
    </table>
    </td>
  </tr>
  <!-- -->
  <tr>
    <td align="left" valign="top">
    <table width="750" border="0" cellspacing="0" cellpadding="0">
      <?php
            $count = 0;
            $subTotal = 0;
            foreach($purchaseItem as $item){
            	$subTotal += (float)(intval($item->quantity) * (float)$item->price) * Yii::app()->session['after_gst'];
            	$priceTotal = number_format((float)(intval($item->quantity) * (float)$item->price) * Yii::app()->session['after_gst'], 2, '.', '');
      ?>
      <tr>
        <td width="12" align="left" valign="middle"><!--<img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="20" style="display:block; margin:0; border:none;" />--></td>
        <td width="60" align="left" valign="top"><?php echo ($count + 1); ?>.</td>
        <td width="388" align="left" valign="top"><strong><?php echo $item->name; ?></strong><br />(<?php echo $item->weight; ?>) by <?php echo $item->brand; ?></td>
        <td width="10" align="right" valign="middle"><!--<img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" />--></td>
        <td width="120" align="left" valign="top">S$ <?php echo $item->price; ?></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
        <td width="100" align="left" valign="top">x <?php echo $item->quantity; ?></td>
        <td width="80" align="right" valign="top">S$ <?php echo number_format((float)(intval($item->quantity) * (float)$item->price * Yii::app()->session['after_gst']), 2, '.', ''); ?></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="left" valign="middle"><hr /></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
      <?php
	  		$count++;
		}
             $calculationResult = Purchase::calculateTotalPurchase($subTotal, $purchase->delivery_option);    
	  ?>
	  <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Subtotal :&nbsp;&nbsp;&nbsp;</strong> S$ <?php echo number_format($subTotal,2); ?><br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
      <?php
            if(!empty($purchase->reward_value) && $purchase->reward_value > 0){
        ?>
            <tr>
                <td colspan="7" width="12" align="left" valign="middle">Minimum Purchase for 1 reward point : <span class="required">S$ <?=$purchase->min_reward_purchase?></span> <br/>(<?=$purchase->total_reward_per_dollar?> Reward Points = S$1)</td>
                
            </tr>
        <?php
            }
        ?>
      <?PHP
            if(!empty($purchase->delivery_option) && $purchase->delivery_option !== "self collection"){
        ?>
      <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Delivery Charge :&nbsp;&nbsp;&nbsp;</strong> S$ <?php echo $calculationResult['delivery_charge']; ?><br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
            <?php } ?>
        <?php
        if(!empty($purchase->existing_reward_point)){
        ?>
       <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Existing Reward Points </strong>: <?php echo number_format($purchase->existing_reward_point,2); ?> Points<br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr> 
        <?php
            }
        ?> 
        <?php
        if(!empty($purchase->reward_earned)){
        ?>
       <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Current Purchase Points Earned </strong>: <?php echo number_format($purchase->reward_earned,2); ?> Points<br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr> 
        <?php
            }
        ?> 
       <?php
       if(!empty($purchase->reward_point)){
        ?>
       <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Total Reward Points </strong>: <?php echo number_format($purchase->reward_point,2); ?> Points<br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr> 
        <?php
            }
        ?>   
        <?php
            if(!empty($purchase->reward_value)){
        ?>
      <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Discounted From Reward Points </strong>: - S$ <?php echo number_format($purchase->reward_value,2); ?><br/><br/></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr> 
        <?php
            }
        ?>
      <tr>
      	<td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="1" style="display:block; margin:0; border:none;" /></td>
        <td colspan="7" align="right" valign="middle"><strong>Grand Total :&nbsp;&nbsp;&nbsp;</strong> S$ <?php echo number_format($purchase->total,2); ?></td>
        <td width="10" align="right" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="10" height="1" style="display:block; margin:0; border:none;" /></td>
      </tr>
    </table><br/><br/>
    </td>
  </tr>
  <!-- -->
  <tr>
    <td align="left" valign="top" bgcolor="#43b02a">
    <table width="570" border="0" cellspacing="0" cellpadding="0" class="head-title">
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="26" style="display:block; margin:0; border:none;" /></td>
        <td width="554" align="left" valign="middle"><strong>Delivery Instruction</strong></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="570" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="12" height="10" style="display:block; margin:0; border:none;" /></td>
        <td width="558" align="left" valign="top">
        <table width="558" border="0" cellspacing="0" cellpadding="0">
           <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="1" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Delivery Option :</td>
            <td width="357" align="left" valign="top"><?php echo empty($purchase->delivery_option) ? "DELIVERY" : strtoupper($purchase->delivery_option); ?></td>
          </tr> 
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="1" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Date :</td>
            <td width="357" align="left" valign="top"><?php echo $purchase->delivery_date; ?></td>
          </tr>
          <tr>
            <td width="1" align="left" valign="middle"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" alt="" width="2" height="20" style="display:block; margin:0; border:none;" /></td>
            <td width="100" align="left" valign="top">Remark :</td>
            <td width="357" align="left" valign="top"><?php echo $purchase->remark; ?></td>
          </tr>
        </table>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#999"><img src="<?=URLHelper::getBackendBaseUrl()?>/img/spacer.gif" width="1" height="1" style="display:block; margin:0; border:none;" /></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    * Please read and understand the TERMS &amp; CONDITIONS.<br />
    * Delivery will only be made with a minimum purchase of S$100.<br />
    * All sales proceed will be made in Singapore and Cash on Delivery only.
    </td>
  </tr>
</table>