<?php

class EmailPromoWidget extends CWidget{
	
	public $title = '';
	public $content = '';
	
	public $term = '';
	public $promo_code = '';
	
	public $container_attributes = '';
	
	public $purchase = '';
	public $purchaseItem = '';
	
	public $continueShopping = false;
	public $admin = false;
	public $include_css = false;
	public function run() {
		
		$this->render('email_promo');
	}
}

?>