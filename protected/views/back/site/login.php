
<div class="login-box">
      <div class="login-logo">
      
        Welcome <b>Administrator</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body form ">
      <div align="center">
      <h1>KOBBY (NUS COC)</h1>
      </div>
        <p class="login-box-msg">Sign in</p>
       <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'login-form',
                                'enableClientValidation'=>true,
                                'clientOptions'=>array(
                                        'validateOnSubmit'=>true,
                                ),
                        )); ?>
            <?php echo $form->errorSummary($model); ?>
                                <div class="form-group has-feedback">
                                    <?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'Username')); ?>
                                	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>                                
                                	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
								<div class="row">
            <div align="center" class="col-xs-13">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
                        <?php $this->endWidget(); ?>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    
