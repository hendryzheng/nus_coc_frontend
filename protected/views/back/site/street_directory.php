
<html>
<head>
<title>Streetdirectory - Javascript Map API Example: Marker Simple</title>
<style type="text/css">
body { margin: 0px; padding: 0px; }
</style>
<script type="text/javascript" language="javascript" src="http://www.streetdirectory.com/js/map_api/m.php"></script>
<script type="text/javascript" language="javascript">
	function initialize() {
		var latlng = new GeoPoint(<?php echo @$_GET['lat']?>, <?php echo @$_GET['lng']?>);
		
		var myOptions = {
			zoom: 7,
			center: latlng,
			showCopyright: false
		};
		
		var map = new SD.genmap.Map(
			document.getElementById("map_canvas"),
			myOptions
		);
		
		var navControl = new CompleteMapControl();
		map.addControl(navControl);
		
		var image = new SD.genmap.MarkerImage({
			image: "../image/redpin.png",
			title: "Red Pin",
			iconSize: new Size(40, 46),
			iconAnchor: new Point(40,46)
		});
		
		var markerManager = new SD.genmap.MarkerStaticManager({
			map: map
		});
		
		var marker = markerManager.add({
			position: latlng,
			map: map,
			icon: image
		});
	}
</script>
</head>

<body onLoad="initialize()">
<div id="map_canvas" style="width:100%; height:100%"></div>

</html>