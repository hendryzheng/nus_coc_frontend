<?php
/* @var $this FoodCategoriesController */
/* @var $model FoodCategories */

$this->breadcrumbs=array(
	'Food Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FoodCategories', 'url'=>array('index')),
	array('label'=>'Create FoodCategories', 'url'=>array('create')),
	array('label'=>'View FoodCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FoodCategories', 'url'=>array('admin')),
);
?>

<h1>Update FoodCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>