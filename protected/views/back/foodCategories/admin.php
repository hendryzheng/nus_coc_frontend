<?php
/* @var $this FoodCategoriesController */
/* @var $model FoodCategories */

$this->breadcrumbs=array(
	'Food Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FoodCategories', 'url'=>array('index')),
	array('label'=>'Create FoodCategories', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#food-categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Food Categories</h1>

<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'food-categories-grid',
	'dataProvider'=>$model->search(),
	'htmlOptions'=>array('class'=>'table-responsive no-padding'),
	'itemsCssClass'=>'table table-hover',
	'filter'=>$model,
	'columns'=>array(
		'name',
		'food_type',
		'sequence',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
