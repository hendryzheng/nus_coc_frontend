<?php
/* @var $this FoodCategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Food Categories',
);

$this->menu=array(
	array('label'=>'Create FoodCategories', 'url'=>array('create')),
	array('label'=>'Manage FoodCategories', 'url'=>array('admin')),
);
?>

<h1>Food Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
