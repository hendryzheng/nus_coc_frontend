<?php
/* @var $this FoodCategoriesController */
/* @var $model FoodCategories */

$this->breadcrumbs=array(
	'Food Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FoodCategories', 'url'=>array('index')),
	array('label'=>'Manage FoodCategories', 'url'=>array('admin')),
);
?>

<h1>Create FoodCategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>