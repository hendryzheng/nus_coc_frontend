<?php
/* @var $this FoodCategoriesController */
/* @var $model FoodCategories */

$this->breadcrumbs=array(
	'Food Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List FoodCategories', 'url'=>array('index')),
	array('label'=>'Create FoodCategories', 'url'=>array('create')),
	array('label'=>'Update FoodCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FoodCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FoodCategories', 'url'=>array('admin')),
);
?>

<h1>View FoodCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'sequence',
		'created_date',
	),
)); ?>
