<?php
/* @var $this FoodController */
/* @var $model Food */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'food-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'runat'=>'server'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'food_categories_id'); ?>
		<?php echo $form->dropDownList($model,'food_categories_id', CHtml::listData(FoodCategories::model()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'food_categories_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'food_name'); ?>
		<?php echo $form->textField($model,'food_name',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'food_name'); ?>
	</div>

	<div class="row">
    	<div class="form-group">
			<?php echo $form->labelEx($model,'image'); ?>
        	<?php if (!empty($model->image)) {?>
        	<img id="pv_image" style="max-width:200px;" class="img-responsive" src="<?php echo URLHelper::getBackendBaseUrl() . $model->image ?>" style="max-width: -webkit-fill-available" alt="Photo">
        	<br>
        	<?php } else {?>
			<img id="pv_image" style="max-width:200px;" alt="your image" style="max-width: -webkit-fill-available" src=""/>
			<?php }?>
        	<?php echo $form->fileField($model, 'image', array('accept'=>'.png,.jpg,.jpeg,.gif'))?>
			<?php echo $form->error($model,'image'); ?>
    	</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sequence'); ?>
		<?php echo $form->textField($model,'sequence'); ?>
		<?php echo $form->error($model,'sequence'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#pv_image').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#Food_image").change(function(){
        readURL(this);
    });
</script>	