<?php
/* @var $this FoodController */
/* @var $model Food */

$this->breadcrumbs=array(
	'Foods'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Food', 'url'=>array('index')),
	array('label'=>'Create Food', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#food-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Foods</h1>
<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'food-grid',
	'htmlOptions'=>array('class'=>'table-responsive no-padding'),
	'itemsCssClass'=>'table table-hover',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
					'header'=>'Food Category',
					'name'=>'food_categories_id',
					'filter'=>CHtml::listData(FoodCategories::model()->findAll(), 'id', 'name'),
					'value'=>'$data->foodCategories->name',
			),
		'food_name',
			array(
					'name'=>'image',
					'value'=>'$data->getImg()',
					'type'=>'raw'
			),
		'sequence',
		'created_date',
		/*
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
