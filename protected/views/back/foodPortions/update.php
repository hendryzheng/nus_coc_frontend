<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */

$this->breadcrumbs=array(
	'Food Portions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FoodPortions', 'url'=>array('index')),
	array('label'=>'Create FoodPortions', 'url'=>array('create')),
	array('label'=>'View FoodPortions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FoodPortions', 'url'=>array('admin')),
);
?>

<h1>Update FoodPortions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>