<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){

	$('select').selectpicker({
		liveSearch: true
	});
});
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'food-portions-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'runat'=>'server'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'food_id'); ?>
		<?php echo $form->dropDownList($model,'food_id', CHtml::listData(Food::model()->findAll(array('order'=>'food_name asc')), 'id', 'food_name')); ?>
		<?php echo $form->error($model,'food_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'portion'); ?>
		<?php echo $form->textField($model,'portion'); ?>
		<?php echo $form->error($model,'portion'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value'); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'measurement'); ?>
		<?php echo $form->textField($model,'measurement'); ?>
		<?php echo $form->error($model,'measurement'); ?>
	</div>
	<div class="row">
    	<div class="form-group">
			<?php echo $form->labelEx($model,'portion_image'); ?>
        	<?php if (!empty($model->portion_image)) {?>
        	<img id="pv_image" style="max-width:200px;" class="img-responsive" src="<?php echo URLHelper::getBackendBaseUrl() . $model->portion_image ?>" style="max-width: -webkit-fill-available" alt="Photo">
        	<br>
        	<?php } else {?>
			<img id="pv_image" style="max-width:200px;" alt="your image" style="max-width: -webkit-fill-available" src=""/>
			<?php }?>
        	<?php echo $form->fileField($model, 'portion_image', array('accept'=>'.png,.jpg,.jpeg,.gif'))?>
			<?php echo $form->error($model,'portion_image'); ?>
    	</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sequence'); ?>
		<?php echo $form->textField($model,'sequence'); ?>
		<?php echo $form->error($model,'sequence'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#pv_image').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#FoodPortions_portion_image").change(function(){
        readURL(this);
    });
</script>	