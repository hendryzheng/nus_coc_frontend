<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */

$this->breadcrumbs=array(
	'Food Portions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FoodPortions', 'url'=>array('index')),
	array('label'=>'Manage FoodPortions', 'url'=>array('admin')),
);
?>

<h1>Create FoodPortions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>