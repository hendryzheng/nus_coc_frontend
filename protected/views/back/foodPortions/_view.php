<?php
/* @var $this FoodPortionsController */
/* @var $data FoodPortions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('food_id')); ?>:</b>
	<?php echo CHtml::encode($data->food_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('portion')); ?>:</b>
	<?php echo CHtml::encode($data->portion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('portion_image')); ?>:</b>
	<?php echo CHtml::encode($data->portion_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sequence')); ?>:</b>
	<?php echo CHtml::encode($data->sequence); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />


</div>