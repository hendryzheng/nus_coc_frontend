<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */

$this->breadcrumbs=array(
	'Food Portions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FoodPortions', 'url'=>array('index')),
	array('label'=>'Create FoodPortions', 'url'=>array('create')),
	array('label'=>'Update FoodPortions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FoodPortions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FoodPortions', 'url'=>array('admin')),
);
?>

<h1>View FoodPortions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'food_id',
		'portion',
		'value',
		'portion_image',
		'sequence',
		'created_date',
	),
)); ?>
