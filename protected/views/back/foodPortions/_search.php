<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'food_id'); ?>
		<?php echo $form->textField($model,'food_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'portion'); ?>
		<?php echo $form->textField($model,'portion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'portion_image'); ?>
		<?php echo $form->textField($model,'portion_image',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sequence'); ?>
		<?php echo $form->textField($model,'sequence'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->