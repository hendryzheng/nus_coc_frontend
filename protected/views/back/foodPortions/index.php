<?php
/* @var $this FoodPortionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Food Portions',
);

$this->menu=array(
	array('label'=>'Create FoodPortions', 'url'=>array('create')),
	array('label'=>'Manage FoodPortions', 'url'=>array('admin')),
);
?>

<h1>Food Portions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
