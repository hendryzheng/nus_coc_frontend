<?php
/* @var $this FoodPortionsController */
/* @var $model FoodPortions */

$this->breadcrumbs=array(
	'Food Portions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FoodPortions', 'url'=>array('index')),
	array('label'=>'Create FoodPortions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('select').selectpicker({
		liveSearch: true
	});
$('.search-form form').submit(function(){
	$('#food-portions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Food Portions</h1>

<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'food-portions-grid',
	'htmlOptions'=>array('class'=>'table-responsive no-padding'),
	'itemsCssClass'=>'table table-hover',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
					'header'=>'Food',
					'name'=>'food_id',
					'filter'=>CHtml::listData(Food::model()->findAll(array('order'=>'food_name')), 'id', 'food_name'),
					'value'=>'$data->food->food_name',
			),
		'portion',
		'value',
		'measurement',
			array(
					'name'=>'portion_image',
					'value'=>'$data->getImg()',
					'type'=>'raw'
			),
		'sequence',
		'created_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
