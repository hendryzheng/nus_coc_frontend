<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Account', 'url'=>array('index')),
	array('label'=>'Create Account', 'url'=>array('create')),
	array('label'=>'Update Account', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Account', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Account', 'url'=>array('admin')),
);
?>

<h1>View Member - <?php echo $model->email; ?></h1>

<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>
<div class="box box-danger">
	<div class="box-body">
		<table class="table table-hover">
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('email')?></th>
				<td><?php echo empty($model->email) ? "-" : $model->email?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('firstname')?></th>
				<td><?php echo empty($model->firstname) ? "-" : $model->firstname?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('lastname')?></th>
				<td><?php echo empty($model->lastname) ? "-" : $model->lastname?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('mobile')?></th>
				<td><?php echo empty($model->mobile) ? "-" : $model->mobile?></td>
			</tr>
			<tr>
				<th><?php echo $model->getAttributeLabel('status')?></th>
				<td><?php echo empty($model->status) ? "-" : StatusHelper::getStatus($model->status)?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('created_date')?></th>
				<td><?php echo empty($model->created_date) ? "-" : $model->created_date?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('updated_date')?></th>
				<td><?php echo empty($model->updated_date) ? "-" : $model->updated_date?></td>
			</tr>
		</table>
	</div>
</div>
