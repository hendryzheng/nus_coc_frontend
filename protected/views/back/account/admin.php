<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Account', 'url'=>array('index')),
	array('label'=>'Create Account', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#account-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Members</h1>

<div class="box box-danger">
	<div class="box-body">
        <p>
        You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
        or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
        </p>
        
        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search',array(
        	'model'=>$model,
        )); ?>
        </div><!-- search-form -->
        
        <?php $this->widget('zii.widgets.grid.CGridView', array(
        	'id'=>'account-grid',
            'dataProvider'=>$model->search(),
            'htmlOptions'=>array('class'=>'table-responsive no-padding'),
            'itemsCssClass'=>'table table-hover',
//         	'filter'=>$model,
        	'columns'=>array(
        		'email',
        		'firstname',
        		'lastname',
        	    'mobile',
        	    array(
        	        'name' => 'status',
        	        'type'=>'raw',
        	        'value' => 'StatusHelper::getStatusDisplay($data->status)',
        	    ),
        		'created_date',
        		'updated_date',
        		array(
        			'class'=>'CButtonColumn',
        		),
        	),
        )); ?>
	</div>
</div>