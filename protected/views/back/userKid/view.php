<?php
/* @var $this UserKidController */
/* @var $model UserKid */

$this->breadcrumbs=array(
	'User Kids'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List UserKid', 'url'=>array('index')),
	array('label'=>'Create UserKid', 'url'=>array('create')),
	array('label'=>'Update UserKid', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserKid', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserKid', 'url'=>array('admin')),
);
?>

<h1>View UserKid #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'name',
		'character_name',
		'where_character_live',
		'day_status',
		'birthday',
		'gender',
		'created_date',
		'updated_date',
	),
)); ?>
