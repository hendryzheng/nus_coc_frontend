<?php
/* @var $this UserKidController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Kids',
);

$this->menu=array(
	array('label'=>'Create UserKid', 'url'=>array('create')),
	array('label'=>'Manage UserKid', 'url'=>array('admin')),
);
?>

<h1>User Kids</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
