<?php
/* @var $this UserKidController */
/* @var $model UserKid */

$this->breadcrumbs=array(
	'User Kids'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserKid', 'url'=>array('index')),
	array('label'=>'Create UserKid', 'url'=>array('create')),
	array('label'=>'View UserKid', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserKid', 'url'=>array('admin')),
);
?>

<h1>Update UserKid <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>