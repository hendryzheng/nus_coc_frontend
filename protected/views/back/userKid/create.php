<?php
/* @var $this UserKidController */
/* @var $model UserKid */

$this->breadcrumbs=array(
	'User Kids'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserKid', 'url'=>array('index')),
	array('label'=>'Manage UserKid', 'url'=>array('admin')),
);
?>

<h1>Create UserKid</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>