<?php
/* @var $this UserKidController */
/* @var $model UserKid */

$this->breadcrumbs=array(
	'User Kids'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UserKid', 'url'=>array('index')),
	array('label'=>'Create UserKid', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-kid-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage User Kids</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-kid-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions'=>array('class'=>'table-responsive no-padding'),
	'itemsCssClass'=>'table table-hover',
	'columns'=>array(
		'username',
		'password',
		'name',
		'character_name',
		'where_character_live',
		'day_status',
		'birthday',
		'gender',
		'created_date',
		'updated_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
