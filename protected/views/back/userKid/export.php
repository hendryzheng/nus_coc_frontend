
<h1>Data Export</h1>
<hr/>
<form method="POST">
	<div class="form">
		<div class="row">
			<div class="col-xs-2">
				<div style="margin-top:10px;font-weight:bold;">Select Kid: </div>
			</div>
			<div class="col-xs-5">
				
				<?php echo CHtml::dropDownList('kid[]', $kid_array, CHtml::listData(UserKid::model()->findAll(), 'id', 'username'), array('class'=>'select2','multiple'=>'multiple','style'=>'width:300px;'))?>
			</div>
		</div><br/>
		<div class="row">
			<div class="col-xs-2">
				<div style="margin-top:10px;font-weight:bold;">Date Range: </div>
			</div>
			<div class="col-xs-5">
				<?php echo CHtml::dateField('date_from',$date_from,array('class'=>'form-control','placeholder'=>'Date From','style'=>'width:40%;display:inline-block;'))?>
				<?php echo CHtml::dateField('date_to',$date_to,array('class'=>'form-control','placeholder'=>'Date To','style'=>'width:40%;display:inline-block;'))?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2">
				<div style="margin-top:10px;font-weight:bold;">Record Limit: </div>
			</div>
			<div class="col-xs-5">
				<?php echo CHtml::textField('limit',$limit,array('class'=>'form-control','placeholder'=>'Limit','style'=>'width:40%;display:inline-block;','max'=>'1000','min'=>'50'))?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-5">
				<button type="submit" name="filter" class="btn btn-primary">Filter</button>
				<button type="submit" name="download" class="btn btn-warning">Download</button>
			</div>
		</div>
	</div>
</form>
<style>
	.table-display > thead td{font-weight:bold;}
	.table-display{width:2300px;}
</style><br/><br/>
<div class="table-responsive">
	<table class="table-display table table-striped table-bordered">
		<thead>
		<tr>
			<td>Date Recorded</td>
			<td>Is Dual</td>
			<td>Day Log</td>
			<td>Last Login</td>
			<td>Username</td>
			<td>Character</td>
			<td>Start Time</td>
			<td>End Time</td>
			<td>Activities</td>
			<td>Suboption</td>
			<td>Concurrent Activity Recorded</td>
			<td>Sub-Concurrent Option Activity Recorded</td>
			<td>Portion Size</td>
			<td>Intensity</td>
			<td>Plant</td>
			<td>Location</td>
		</tr>
		</thead>
		<tbody>
		<?php if(!empty($results)){
			foreach($results as $row){
		?>
			<tr>
				<td><?php echo $row['activity_date']?></td>
				<td><?php echo $row['is_dual']?></td>
				<td><?php echo $row['day_status']?></td>
				<td><?php echo $row['last_login_date']?></td>
				<td><?php echo $row['username']?></td>
				<td><?php echo $row['character_type']?></td>
				<td><?php echo $row['start_date_time']?></td>
				<td><?php echo $row['complete_date_time']?></td>
				<td><?php echo $row['activity_type']?></td>
				<td><?php echo '<div style="width:200px;">'.$row['selected_activity_display'].'</div>'?></td>
				<td><?php echo '<div style="width:200px;">'.$row['concurrent_activity_type'].'</div>'?></td>
				<td><?php echo '<div style="width:200px;">'.$row['subconcurrent_activity'].'</div>'?></td>
				<td><?php echo '<div style="width:200px;">'.$row['portion_size'].'</div>'?></td>
				<td><?php echo $row['intensity_activity']?></td>
				<td><?php echo $row['plant']?></td>
				<td><?php echo $row['location']?></td>
			</tr>
		<?php 		
			}
		}else{
		
			echo '<tr><td colspan="14">No record found</td></tr>';
		}?>
		</tbody>
	</table>
</div>
