<?php
/* @var $this AdminController */
/* @var $model Admin */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>
<div class="box box-danger">
	<div class="box-body">
    	<p class="note">Fields with <span class="required">*</span> are required.</p>
    
    	<?php echo $form->errorSummary($model); ?>
    
    	<div class="form-group">
    		<?php echo $form->labelEx($model,'userId', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-sm-9">
    		<?php echo $form->textField($model,'userId',array('placeholder'=>'Username','class'=>'form-control','size'=>60,'maxlength'=>500)); ?>
    		<?php echo $form->error($model,'userId'); ?>
    		</div>
    	</div>
    	<div class="form-group row">
    		<label class="col-sm-2 control-label" for="new_password">New Password</label>
    		<div class="col-sm-8">
    			<input type="password" size="60" maxlength="500" class="form-control" placeholder="Insert new password" name="new_password" id="new_password">
    		</div>
    	</div>
    	<div class="form-group row">
    		<label class="col-sm-2 control-label" for="confirm_password">Confirm New Password</label>
    		<div class="col-sm-8">
    			<input type="password" size="60" maxlength="500" class="form-control" placeholder="Confirm new password" name="confirm_password" id="confirm_password">
    		</div>
    	</div>
	</div>
	<div class="box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('name'=>'action','class'=>'btn btn-primary pull-right')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->