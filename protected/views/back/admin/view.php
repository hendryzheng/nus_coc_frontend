<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Update Admin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>

<h1>View Admin - <?php echo $model->userId; ?></h1>
<?php 
	$this->widget('application.components.widgets.NotificationMessageWidget');
?>
<div class="box box-danger">
	<div class="box-body">
		<table class="table table-hover">
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('userId')?></th>
				<td><?php echo empty($model->userId) ? "-" : $model->userId?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('created_date')?></th>
				<td><?php echo empty($model->created_date) ? "-" : $model->created_date?></td>
			</tr>
			<tr>
				<th style="width: 15%"><?php echo $model->getAttributeLabel('updated_date')?></th>
				<td><?php echo empty($model->updated_date) ? "-" : $model->updated_date?></td>
			</tr>
		</table>
	</div>
</div>