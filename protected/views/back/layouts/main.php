<?php include("_header.php"); ?>
<?php
$proceed = false;
if(!Yii::app()->user->isGuest && !empty(Yii::app()->user->type)){
	if(Yii::app()->user->type == "admin" || Yii::app()->user->type == "user"){
		$proceed= true;
	}
}
if ($proceed) {
    ?>
     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="overflow:auto;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
<!--           <h1> -->
<!--             Dashboard -->
<!--             <small>Control panel</small> -->
<!--           </h1> -->

        <!-- Main content -->
        <section class="content">
          <?php echo $content;?>

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php
} else {
    echo $content;
}
?>
</div>
<?php include("_footer.php"); ?>