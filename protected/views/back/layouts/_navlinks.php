<?php
if (!Yii::app()->user->isGuest && Yii::app()->user->type == "admin" ) {
    $this->widget('zii.widgets.CMenu', array(
        'items' => array(
            
            array('label' => '<i class="fa fa-list-alt"></i> <span>The Kid</span> <i class="fa fa-angle-left pull-right"></i>',
                'url' => '#',
                'active' =>(Yii::app()->controller->id == 'userKid'),
                'items' => array(
                    array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>Create New Acc</span>',
                        'url' => array('/userKid/create'),
                        'active' => (Yii::app()->controller->action->getId() == 'create')
                    ),
                    array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>List Kid Account</span>',
                        'url' => array('/userKid'),
                        'active' => (Yii::app()->controller->action->getId() == 'index')
                    ),
                ), 'htmlOptions' => array('class' => 'treeview-menu')),
        	array('label' => '<i class="fa fa-list-alt"></i> <span>FoodCategories</span> <i class="fa fa-angle-left pull-right"></i>',
        		'url' => '#',
        		'active' =>(Yii::app()->controller->id == 'foodCategories'),
        		'items' => array(
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>Create New Category</span>',
        				'url' => array('/foodCategories/create'),
        				'active' => (Yii::app()->controller->action->getId() == 'create')
        			),
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>List Food Categories</span>',
        				'url' => array('/foodCategories'),
        				'active' => (Yii::app()->controller->action->getId() == 'index')
        			),
        		), 'htmlOptions' => array('class' => 'treeview-menu')),
        	array('label' => '<i class="fa fa-list-alt"></i> <span>Food</span> <i class="fa fa-angle-left pull-right"></i>',
        		'url' => '#',
        		'active' =>(Yii::app()->controller->id == 'food'),
        		'items' => array(
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>Create New Food</span>',
        				'url' => array('/food/create'),
        				'active' => (Yii::app()->controller->action->getId() == 'create')
        			),
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>List Food</span>',
        				'url' => array('/food'),
        				'active' => (Yii::app()->controller->action->getId() == 'index')
        			),
        		), 'htmlOptions' => array('class' => 'treeview-menu')),
        	array('label' => '<i class="fa fa-list-alt"></i> <span>Food Portions</span> <i class="fa fa-angle-left pull-right"></i>',
        		'url' => '#',
        		'active' =>(Yii::app()->controller->id == 'foodPortions'),
        		'items' => array(
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>Create New Food Portion</span>',
        				'url' => array('/foodPortions/create'),
        				'active' => (Yii::app()->controller->action->getId() == 'create')
        			),
        			array('label' => '<i class="fa fa-fw fa-circle-o"></i> <span>List Food Portion</span>',
        								'url' => array('/foodPortions'),
        								'active' => (Yii::app()->controller->action->getId() == 'index')
        						),
        				), 'htmlOptions' => array('class' => 'treeview-menu')),
        	array('label' => '<i class="fa fa-external-link"></i> <span>Export</span>', 'url' => array('/userKid/export'), 'active' => Yii::app()->controller->id == 'site'),
        	array('label' => '<i class="fa fa-fw fa-sign-out"></i> <span>Sign Out</span>', 'url' => array('/site/logout'), 'active' => Yii::app()->controller->id == 'site'),
            ),
            'encodeLabel' => false,
            'htmlOptions' => array('class' => 'sidebar-menu'),
            'id' => 'side-menu',
            'submenuHtmlOptions' => array(
            'class' => 'treeview-menu',
        ),
    ));
}
?> 