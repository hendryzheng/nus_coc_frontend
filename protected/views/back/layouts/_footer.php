<?php
$proceed = false;
if(!Yii::app()->user->isGuest && !empty(Yii::app()->user->type)){
	if(Yii::app()->user->type == "admin" || Yii::app()->user->type == "user"){
		$proceed= true;
	}
}
if ($proceed) {
            	echo '</div>';
            }
                ?>
<!-- DataTables JavaScript -->
<?php
$cs = Yii::app ()->getClientScript ();
$cs->packages = array (
		'jquery.ui' => array (
				'js' => array (
						'jui/js/jquery-ui.min.js' 
				),
				'css' => array (
						'jui/css/base/jquery-ui.css' 
				),
				'depends' => array (
						'jquery' 
				) 
		) 
);
$cs->registerCoreScript ( 'jquery.ui' );
?>
 	<script>
		$.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/morris/morris.min.js"></script>
    <!-- Select2 -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/select2/select2.full.min.js"></script>
    <!-- Sparkline -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- datepicker -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="<?=URLHelper::getBackendBaseUrl()?>/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?=URLHelper::getBackendBaseUrl()?>/dist/js/demo.js"></script>
	<script type="text/javascript" src="<?=URLHelper::getBackendBaseUrl()?>/js/back.js"></script>
	<script type="text/javascript" src="<?=URLHelper::getBackendBaseUrl()?>/js/canvasjs.min.js"></script>
	<script src="<?=URLHelper::getBackendBaseUrl()?>/js/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="<?=URLHelper::getBackendBaseUrl()?>/js/amcharts/serial.js" type="text/javascript"></script>
	<!-- Dropzone for photo -->
	<script type="text/javascript" src="<?php echo URLHelper::getBackendBaseUrl(); ?>/js/dropzone.js"></script>
	<!-- Bootstrap for Select Input -->
	<script type="text/javascript" src="<?php echo URLHelper::getBackendBaseUrl(); ?>/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
    $(function () {
        $(".select2").select2();
        $(".textarea").wysihtml5();
    	$(".icon-image").select2({
    	    templateResult: function (option) {
    	        return '<img src="<?php echo URLHelper::getBackendBaseUrl()?>' + option.text + '" style="max-width:90px"/>';
    	    },
    	    templateSelection: function (option) {
    	        return '<img src="<?php echo URLHelper::getBackendBaseUrl()?>' + option.text + '" style="max-width:20px"/>';
    	    },
    	    escapeMarkup: function (m) {
    			return m;
    		}
    	});
    });
    </script>
</body>
</html>