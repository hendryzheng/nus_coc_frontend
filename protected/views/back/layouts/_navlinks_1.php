<div class="container">
			<div class="span11">
				<?php
                                    if(!Yii::app()->user->isGuest && Yii::app()->user->type == "admin"){
                                        $this->widget('zii.widgets.CMenu',array(
                                'items'=>array(
                                        array('label'=>'Admin', 'url'=>array('/admin'),'active'=>Yii::app()->controller->id == 'admin'),
                                        array('label'=>'Account', 'url'=>array('/account'),'active'=>Yii::app()->controller->id == 'account'),
                                        array('label'=>'Status', 'url'=>array('/status'),'active'=>Yii::app()->controller->id == 'status'),
                                        array('label'=>'Category', 'url'=>array('/category'),'active'=>Yii::app()->controller->id == 'category'),
                                        array('label'=>'Config', 'url'=>array('/config'),'active'=>Yii::app()->controller->id == 'config'),
                                        array('label'=>'Slider', 'url'=>array('/slider'),'active'=>Yii::app()->controller->id == 'slider'),
                                ),
                                'htmlOptions'=>array('class'=>'left')
                                ));
                                    }
                                ?>
			</div>
			
			<div class="span3 pull-right">
				<?php $this->widget('zii.widgets.CMenu',array(
                                'items'=>array(
                                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                                ),
                                'htmlOptions'=>array('class'=>'right')
                                )); ?>
			</div>
			<div class="clearfix">
			
			</div>
		</div>