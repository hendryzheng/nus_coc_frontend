<!DOCTYPE html>
<html>
    <head>
        <title><?= Yii::app()->name ?> - Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo URLHelper::getBackendBaseUrl(); ?>/css/form.css" rel="stylesheet">
        <?php /*<link href="<?php echo URLHelper::getBackendBaseUrl(); ?>/css/back.css" rel="stylesheet">*/ ?>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	    <!-- Bootstrap 3.3.5 -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/bootstrap/css/bootstrap.min.css">
	    <!-- Font Awesome -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	    <!-- Ionicons -->
	    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Select2 -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/select2/select2.min.css">
	    <!-- Theme style -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/dist/css/AdminLTE.min.css">
	    <!-- AdminLTE Skins. Choose a skin from the css/skins
	         folder instead of downloading all of them to reduce the load. -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/dist/css/skins/_all-skins.min.css">
	    <!-- iCheck -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/iCheck/flat/blue.css">
	    <!-- Morris chart -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/morris/morris.css">
	    <!-- jvectormap -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	    <!-- Date Picker -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/datepicker/datepicker3.css">
	    <!-- Daterange picker -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- Bootstrap time Picker -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/timepicker/bootstrap-timepicker.min.css">
	    <!-- bootstrap wysihtml5 - text editor -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	    <!-- dropzone photo css -->
		<link href="<?php echo URLHelper::getBackendBaseUrl(); ?>/css/dropzone.css" rel="stylesheet">
	    <!-- Custom CSS -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl()?>/css/back.css">
	    <!-- Selectpicker CSS -->
	    <link rel="stylesheet" href="<?php echo URLHelper::getBackendBaseUrl(); ?>/css/bootstrap-select.css">
        
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
        <script>
            var site_url = '<?= URLHelper::getBackendBaseUrl() ?>';
        </script>
        <style>
        .content-wrapper, .right-side{
        background:#fff;
        }
        </style>
    </head>
    
            <?php
            $proceed = false;
            if(!Yii::app()->user->isGuest && !empty(Yii::app()->user->type)){
            	if(Yii::app()->user->type == "admin" || Yii::app()->user->type == "user"){
            		$proceed= true;
            	}
            }
            if ($proceed) {
            	$admin = Admin::model()->findByPk(Yii::app()->user->idno);
                ?>
          <body class="hold-transition skin-blue sidebar-mini">
    		<div class="wrapper">
                <header class="main-header">
			        <!-- Logo -->
			        <a href="<?= URLHelper::getBackendAppUrl() ?>" class="logo">
			          <!-- mini logo for sidebar mini 50x50 pixels -->
				          <span class="logo-mini">KOBBY</span>
				          <!-- logo for regular state and mobile devices -->
				          <span class="logo-lg"><b>KOBBY</b></span>
			        </a>
                <!-- Header Navbar: style can be found in header.less -->
		        <nav class="navbar navbar-static-top" role="navigation">
		          <!-- Sidebar toggle button-->
		          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		            <span class="sr-only">Toggle navigation</span>
		          </a>
		          <div class="navbar-custom-menu">
		            <ul class="nav navbar-nav">
		              <!-- User Account: style can be found in dropdown.less -->
		              <li class="dropdown user user-menu">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                  <?php /* ?><img src="<?php echo URLHelper::getBackendBaseUrl() ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"><?php */ ?>
		                  <span class="hidden-xs"><?php echo $admin->userId?></span>
		                </a>
		                <ul class="dropdown-menu">
						  <?php /* ?>
		                  <!-- User image -->
		                  <li class="user-header">
		                    <img src="<?php echo URLHelper::getBackendBaseUrl() ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
		                    <p>
		                      <?php echo $admin->userId; ?>
		                      <small>Member since <?php echo date("j Y",strtotime($admin->created_date))?></small>
		                    </p>
		                  </li>
		                  <?php */ ?>
		                  <!-- Menu Footer-->
		                  <li class="user-footer">
<!-- 		                    <div class="pull-left"> -->
<!-- 		                      <a href="#" class="btn btn-default btn-flat">Profile</a> -->
<!-- 		                    </div> -->
		                    <div align="center">
		                      <a href="<?php echo URLHelper::getBackendAppUrl()?>/site/logout" class="btn btn-default btn-flat">Sign out</a>
		                    </div>
		                  </li>
		                </ul>
		              </li>
		              <!-- Control Sidebar Toggle Button -->
<!-- 		              <li> -->
<!-- 		                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
<!-- 		              </li> -->
		            </ul>
		          </div>
		        </nav>
		      </header>
              <!-- Left side column. contains the logo and sidebar -->
		      <aside class="main-sidebar">
		        <!-- sidebar: style can be found in sidebar.less -->
		        <section class="sidebar">
		          <!-- Sidebar user panel -->
<!-- 		          <div class="user-panel"> -->
<!-- 		            <div class="pull-left image"> -->
<!-- 		              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
<!-- 		            </div> -->
<!-- 		            <div class="pull-left info"> -->
<!-- 		              <p>Alexander Pierce</p> -->
<!-- 		              <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
<!-- 		            </div> -->
<!-- 		          </div> -->
				  <?php /* ?>
		          <!-- search form -->
		          <form action="#" method="get" class="sidebar-form">
		            <div class="input-group">
		              <input type="text" name="q" class="form-control" placeholder="Search...">
		              <span class="input-group-btn">
		                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
		              </span>
		            </div>
		          </form>
		          <!-- /.search form -->
		          <?php */ ?>
		          <!-- sidebar menu: : style can be found in sidebar.less -->
		          <?php
                            if (!Yii::app()->user->isGuest && (Yii::app()->user->type == "admin" || Yii::app()->user->type == "user")) {
                                include_once '_navlinks.php';
                            }
                            ?>
<!-- 		          <ul class="sidebar-menu"> -->
<!-- 		            <li class="header">MAIN NAVIGATION</li> -->
		            
<!-- 		          </ul> -->
		        </section>
		        <!-- /.sidebar -->
		      </aside>
                <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
                <?php
            }else{
            	echo '<body class="hold-transition login-page">';
            }
            ?>