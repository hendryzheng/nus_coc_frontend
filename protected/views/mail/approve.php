
<table cellspacing="0" cellpadding="10" style="color:#333;font:13px Arial;line-height:1.4em;width:500px;">
	<tbody>
<!--		<tr>
            <td style="color:#4D90FE;font-size:22px;border-bottom: 2px solid #4D90FE;">
				<?php echo CHtml::encode(Yii::app()->name); ?>
            </td>
		</tr>-->
<!--		<tr>
            <td style="color:#777;font-size:16px;padding-top:5px;">
            	<?php if(isset($data['description'])) echo $data['description'];  ?>
            </td>
		</tr>-->
		<tr>
			<td style="border-bottom:1px solid #dfdfdf;">
				<div align="center">
	<img style="max-width:150px;" src="http://disrupt.hendryzheng.com/assets/img/Logo.png"/>
</div>
			</td>
		</tr>
		<tr>
            <td>
            	<h2 style="color:#55b6a5;text-align:center;padding-top:15px;">Welcome to Disrupt Property</h2>
            	<p>
				Thanks for submitting your startup. We have reviewed your submission and uploaded <span style="color:#55b6a5"><?php echo @$startup_name?></span>  on our portal. You may check your listing by clicking link below and if you would like any changes please do not hesitate to get in touch.</p>
            	<div style="margin:30px;" align="center">
            		<a style="color:#fff;background:#55b6a5;padding:10px;width:150px;text-decoration:none;border-radius:3px;" href="<?php echo @$url?>">View My Listing</a>
            	</div>
            	<p>We started Disrupt Property to help answer the question 'I wonder what startups are addressing crowdfunding? Or 3D visualisation? Or parking? Or smart homes?'. These are the questions we ask ourselves and the people asking these questions need to find you. Our aim is to connect startups to the industry on a single platform where we can find and compare global startups across all asset classes. We hope listing on Disrupt Property can help you on your journey. Maybe you will connect with a new client, scope out a competitor or even attract an investor. If there is anything we can do to help please let us know.</p>
            	
            	<p>Please do not hesitate to contact us if you have any questions or want to change any details regarding to your listing.</p>
            </td>
		</tr>
		<tr>
            <td style="">
                                Thank you,<br/>
                                Disrupt Property Team<br/>
                                <a href="mailto:hello@disruptproperty.com">hello@disruptproperty.com</a>
			</td>
		</tr>
	</tbody>
</table>
