
<table cellspacing="0" cellpadding="10" style="color:#333;font:15px Arial;line-height:1.4em;width:500px;">
	<tbody>
		<tr>
			<td>
				<h2 style="text-align:center;padding-top:15px;">Disrupt Property New Enquiry</h2>
			</td>
		</tr>
		<tr>
            <td>
            	<p>You have received a new message from your contact form on Disrupt Property.</p>
            </td>
		</tr>
		<tr>
		
			<table cellspacing="0" cellpadding="10" style="color:#333;font:15px Arial;line-height:1.4em;width:600px;">
				<tr>
					<td><b>Name : </b></td>
					<td><?php echo @$name?></td>
				</tr>
				<tr>
					<td><b>Email : </b></td>
					<td><?php echo @$email?></td>
				</tr>
				<tr>
					<td valign="top" width="20%"><b>Message :</b></td>
					<td><?php echo @$message?></td>
				</tr>
			</table>
		</tr>
	</tbody>
</table>
