<?php

/**
 * This is the model class for table "activities".
 *
 * The followings are the available columns in table 'activities':
 * @property integer $id
 * @property integer $dual_activity_id
 * @property integer $daily_log_id
 * @property integer $user_kid_id
 * @property integer $sequence
 * @property string $activity_type
 * @property string $location
 * @property string $selected_activity_display
 * @property integer $complete
 * @property string $param_display
 * @property string $icon
 * @property string $start_date_time
 * @property string $complete_date_time
 * @property integer $duration
 * @property string $json
 * @property string $created_date
 * @property string $updated_date
 */
class Activities extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'activities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('daily_log_id, user_kid_id, activity_type', 'required'),
			array('dual_activity_id, daily_log_id, user_kid_id, sequence, complete, duration', 'numerical', 'integerOnly'=>true),
			array('location, activity_type, icon, param_display', 'length', 'max'=>500),
			array('start_date_time, complete_date_time, json,selected_activity_display, created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dual_activity_id, icon, daily_log_id, user_kid_id, sequence, location, activity_type, selected_activity_display, complete, param_display, start_date_time, complete_date_time, duration, json, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dual_activity_id' => 'Dual Activity',
			'daily_log_id' => 'Daily Log',
			'user_kid_id' => 'User Kid',
			'sequence' => 'Sequence',
			'activity_type' => 'Activity Type',
			'selected_activity_display' => 'Selected Activity Display',
			'complete' => 'Complete',
			'param_display' => 'Param Display',
			'start_date_time' => 'Start Date Time',
			'complete_date_time' => 'Complete Date Time',
			'duration' => 'Duration',
			'json' => 'Json',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dual_activity_id',$this->dual_activity_id);
		$criteria->compare('daily_log_id',$this->daily_log_id);
		$criteria->compare('user_kid_id',$this->user_kid_id);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('activity_type',$this->activity_type,true);
		$criteria->compare('selected_activity_display',$this->selected_activity_display,true);
		$criteria->compare('complete',$this->complete);
		$criteria->compare('param_display',$this->param_display,true);
		$criteria->compare('start_date_time',$this->start_date_time,true);
		$criteria->compare('complete_date_time',$this->complete_date_time,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('json',$this->json,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Activities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
