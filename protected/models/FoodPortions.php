<?php

/**
 * This is the model class for table "food_portions".
 *
 * The followings are the available columns in table 'food_portions':
 * @property integer $id
 * @property integer $food_id
 * @property double $portion
 * @property string $measurement
 * @property string $portion_image
 * @property double $value
 * @property integer $sequence
 * @property string $created_date
 * 
 * @property Food $food
 */
class FoodPortions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'food_portions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		    array('food_id, portion', 'required'),
		    array('portion_image', 'file', 'types'=>'png, jpg, jpeg, gif', 'safe' => false,'allowEmpty'=>'true'),
			array('food_id, sequence', 'numerical', 'integerOnly'=>true),
			array('measurement', 'length', 'max'=>50),
			array('portion, value', 'numerical'),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, food_id, portion, value, portion_image, measurement, sequence, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'food' => array(self::BELONGS_TO, 'Food', 'food_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'food_id' => 'Food',
			'portion' => 'Portion',
			'portion_image' => 'Portion Image',
			'sequence' => 'Sequence',
			'created_date' => 'Created Date',
		);
	}
	
	public function getImg(){
		return '<a target="_new" href="'.URLHelper::getBackendBaseUrl().$this->portion_image.'"><img style="width:100px;" src="'.URLHelper::getBackendBaseUrl().$this->portion_image.'"/></a>';
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('food_id',$this->food_id);
		$criteria->compare('portion',$this->portion);
		$criteria->compare('portion_image',$this->portion_image,true);
		
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FoodPortions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
