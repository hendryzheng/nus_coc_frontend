<?php

/**
 * This is the model class for table "daily_log".
 *
 * The followings are the available columns in table 'daily_log':
 * @property integer $id
 * @property integer $day_status
 * @property integer $user_kid_id
 * @property string $selected_day
 * @property string $day_log
 * @property string $date_log
 * @property integer $sleeping_done
 * @property string $time_sleep_hr1
 * @property string $time_sleep_hr2
 * @property string $time_sleep_minute1
 * @property string $time_sleep_minute2
 * @property string $time_sleep_am
 * @property integer $wakeup_done
 * @property string $time_wakeup_hr1
 * @property string $time_wakeup_hr2
 * @property string $time_wakeup_minute1
 * @property string $time_wakeup_minute2
 * @property string $time_wakeup_am
 * @property integer $shower_done
 * @property string $time_shower_minute1
 * @property string $time_shower_minute2
 * @property string $shower_location
 * @property string $shower_after_activity
 * @property integer $completed
 * @property string $plant_randomize
 * @property string $created_date
 * @property string $updated_date
 */
class DailyLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daily_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_kid_id, day_log, date_log', 'required'),
			array('day_status, user_kid_id, sleeping_done, wakeup_done, shower_done, completed', 'numerical', 'integerOnly'=>true),
			array('selected_day, day_log, time_sleep_hr1, time_sleep_hr2, time_sleep_minute1, time_sleep_minute2, time_sleep_am, time_wakeup_hr1, time_wakeup_hr2, time_wakeup_minute1, time_wakeup_minute2, time_wakeup_am, time_shower_minute1, time_shower_minute2, shower_location, shower_after_activity, plant_randomize', 'length', 'max'=>500),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, day_status, user_kid_id, selected_day, plant_randomize, day_log, date_log, sleeping_done, time_sleep_hr1, time_sleep_hr2, time_sleep_minute1, time_sleep_minute2, time_sleep_am, wakeup_done, time_wakeup_hr1, time_wakeup_hr2, time_wakeup_minute1, time_wakeup_minute2, time_wakeup_am, shower_done, time_shower_minute1, time_shower_minute2, shower_location, shower_after_activity, completed, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'day_status' => 'Day Status',
			'user_kid_id' => 'User Kid',
			'selected_day' => 'Selected Day',
			'day_log' => 'Day Log',
			'date_log' => 'Date Log',
			'sleeping_done' => 'Sleeping Done',
			'time_sleep_hr1' => 'Time Sleep Hr1',
			'time_sleep_hr2' => 'Time Sleep Hr2',
			'time_sleep_minute1' => 'Time Sleep Minute1',
			'time_sleep_minute2' => 'Time Sleep Minute2',
			'time_sleep_am' => 'Time Sleep Am',
			'wakeup_done' => 'Wakeup Done',
			'time_wakeup_hr1' => 'Time Wakeup Hr1',
			'time_wakeup_hr2' => 'Time Wakeup Hr2',
			'time_wakeup_minute1' => 'Time Wakeup Minute1',
			'time_wakeup_minute2' => 'Time Wakeup Minute2',
			'time_wakeup_am' => 'Time Wakeup Am',
			'shower_done' => 'Shower Done',
			'time_shower_minute1' => 'Time Shower Minute1',
			'time_shower_minute2' => 'Time Shower Minute2',
			'shower_location' => 'Shower Location',
			'shower_after_activity' => 'Shower After Activity',
			'completed' => 'Completed',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('day_status',$this->day_status);
		$criteria->compare('user_kid_id',$this->user_kid_id);
		$criteria->compare('selected_day',$this->selected_day,true);
		$criteria->compare('day_log',$this->day_log,true);
		$criteria->compare('date_log',$this->date_log,true);
		$criteria->compare('sleeping_done',$this->sleeping_done);
		$criteria->compare('time_sleep_hr1',$this->time_sleep_hr1,true);
		$criteria->compare('time_sleep_hr2',$this->time_sleep_hr2,true);
		$criteria->compare('time_sleep_minute1',$this->time_sleep_minute1,true);
		$criteria->compare('time_sleep_minute2',$this->time_sleep_minute2,true);
		$criteria->compare('time_sleep_am',$this->time_sleep_am,true);
		$criteria->compare('wakeup_done',$this->wakeup_done);
		$criteria->compare('time_wakeup_hr1',$this->time_wakeup_hr1,true);
		$criteria->compare('time_wakeup_hr2',$this->time_wakeup_hr2,true);
		$criteria->compare('time_wakeup_minute1',$this->time_wakeup_minute1,true);
		$criteria->compare('time_wakeup_minute2',$this->time_wakeup_minute2,true);
		$criteria->compare('time_wakeup_am',$this->time_wakeup_am,true);
		$criteria->compare('shower_done',$this->shower_done);
		$criteria->compare('time_shower_minute1',$this->time_shower_minute1,true);
		$criteria->compare('time_shower_minute2',$this->time_shower_minute2,true);
		$criteria->compare('shower_location',$this->shower_location,true);
		$criteria->compare('shower_after_activity',$this->shower_after_activity,true);
		$criteria->compare('completed',$this->completed);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DailyLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
