<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;
        public $isAdmin;
	private $_identity;
        public $userType;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password,', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
                        'userType'=>'Login Type'
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
// 		echo $this->userType;exit;
		if(!$this->hasErrors())
		{
                    if($this->isAdmin){
                        $this->_identity=new UserIdentityAdmin($this->username,$this->password);
                    }else{
                        if($this->userType == "customer"){
                            $this->_identity=new UserIdentityCustomer($this->username,$this->password);
                        }elseif($this->userType == "corporate"){
                            $this->_identity=new UserIdentityCorporate($this->username,$this->password);
                        }elseif($this->userType == "delivery_men"){
                            $this->_identity=new UserIdentityDeliveryMen($this->username,$this->password);
                        }elseif($this->userType == "manager"){
                            $this->_identity=new UserIdentityManager($this->username,$this->password);
                        }
                        
                    }
			
			if(!$this->_identity->authenticate())
				$this->addError('password','Incorrect username or password.');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
            Yii::app()->user->logout();
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
