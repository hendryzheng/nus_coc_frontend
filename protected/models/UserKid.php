<?php

/**
 * This is the model class for table "user_kid".
 *
 * The followings are the available columns in table 'user_kid':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $character_type
 * @property string $character_name
 * @property string $where_character_live
 * @property integer $day_status
 * @property string $birthday
 * @property string $gender
 * @property integer $tutorial_done
 * @property string $last_login_date
 * @property string $created_date
 * @property string $updated_date
 */
class UserKid extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_kid';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password', 'required'),
			array('day_status, tutorial_done', 'numerical', 'integerOnly'=>true),
			array('username, password, name, character_type, character_name, where_character_live, gender', 'length', 'max'=>500),
			array('birthday, created_date, last_login_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, name, character_type, character_name, where_character_live, day_status, birthday, gender, tutorial_done, last_login_date, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'name' => 'Name',
			'character_type' => 'Character Type',
			'character_name' => 'Character Name',
			'where_character_live' => 'Where Character Live',
			'day_status' => 'Day Status',
			'birthday' => 'Birthday',
			'gender' => 'Gender',
			'tutorial_done' => 'Tutorial Done',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('character_type',$this->character_type,true);
		$criteria->compare('character_name',$this->character_name,true);
		$criteria->compare('where_character_live',$this->where_character_live,true);
		$criteria->compare('day_status',$this->day_status);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('tutorial_done',$this->tutorial_done);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserKid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
