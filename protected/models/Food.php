<?php

/**
 * This is the model class for table "food".
 *
 * The followings are the available columns in table 'food':
 * @property integer $id
 * @property integer $food_categories_id
 * @property string $food_name
 * @property string $image
 * @property integer $sequence
 * @property string $created_date
 * @property string $updated_date
 * 
 * The followings are the available model relations:
 * @property FoodCategories $foodCategories
 */
class Food extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'food';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		    array('food_categories_id, food_name', 'required'),
		    array('image', 'file', 'types'=>'png, jpg, jpeg, gif', 'safe' => false, 'on'=>'insert'),
			array('food_categories_id, sequence', 'numerical', 'integerOnly'=>true),
			array('food_name', 'length', 'max'=>500),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, food_categories_id, foodCategories.name, food_name, image, sequence, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'foodCategories' => array(self::BELONGS_TO, 'FoodCategories', 'food_categories_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'food_categories_id' => 'Food Categories',
			'food_name' => 'Food Name',
			'image' => 'Image',
			'sequence' => 'Sequence',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}
	
	public function getImg(){
		return '<a target="_new" href="'.URLHelper::getBackendBaseUrl().$this->image.'"><img style="width:100px;" src="'.URLHelper::getBackendBaseUrl().$this->image.'"/></a>';
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('food_categories_id',$this->food_categories_id, true);
		$criteria->compare('food_name',$this->food_name,true);
		
// 		$criteria->compare('foodCategories.name',$this->food_categories_id,true); 
		$criteria->compare('image',$this->image,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->with = 'foodCategories';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Food the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
