import { Angular2HnPage } from './app.po';

describe('angular2-hn App', () => {
  let page: Angular2HnPage;

  beforeEach(() => {
    page = new Angular2HnPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
