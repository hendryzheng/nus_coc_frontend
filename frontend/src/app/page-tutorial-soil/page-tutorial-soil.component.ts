import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-page-tutorial-soil',
  templateUrl: './page-tutorial-soil.component.html',
  styleUrls: ['./page-tutorial-soil.component.scss']
})
export class PageTutorialSoilComponent implements OnInit {

  selected_char: any = '';
  kid_account: any = null;
  image_type: any = 'png';
  photo: any = '';

  constructor(private _commonService: CommonService) { }

  ngOnInit() {
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }


    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let params = {
      kid_id: this.kid_account.id
    }
    if (this.kid_account.character_type !== null) {
      this.selected_char = this.kid_account.character_type;
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_9);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_10);
        this._commonService.audio.play();

        this._commonService.audio.onended = () => {
          this._commonService.audio = new Audio(this._commonService.audioList.audio_11);
          this._commonService.audio.play();

          this._commonService.audio.onended = () => {
            this.image_type = 'png';
          };

        };
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
