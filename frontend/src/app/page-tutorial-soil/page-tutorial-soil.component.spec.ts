import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialSoilComponent } from './page-tutorial-soil.component';

describe('PageTutorialSoilComponent', () => {
  let component: PageTutorialSoilComponent;
  let fixture: ComponentFixture<PageTutorialSoilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialSoilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialSoilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
