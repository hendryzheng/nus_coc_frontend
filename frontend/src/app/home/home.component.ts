import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private router: Router) { }


  private key: any = '';
  color: any = '';
  hoverEnter: boolean = false;
  active: boolean = false;
  photo: any = 'assets/img/background/mountains/mountains@2x.png';
  background_map = {
    'beach' : 'assets/img/background/beach/beach@2x.png',
    'carnival' : 'assets/img/background/carnival/carnival@2x.png',
    'forest' : 'assets/img/background/forest/forest@2x.png',
    'mountains' : 'assets/img/background/mountains/mountains@2x.png',
    'arcade': 'assets/img/background/video-game/video-game@2x.png',
    'space': 'assets/img/background/space/space@2x.png'
  }

  form = {
    username: '',
    password: ''
  }

  ngOnInit() {

  }

  onKey(ev) {
    if (this.form.username !== '' && this.form.password !== '') {
      this.active = true;
    } else {
      this.active = false;
    }
  }

  submit() {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.login, this.form).then(response => {
      this._commonService.showLoading = false;
      localStorage.setItem('account', JSON.stringify(response.data));
      localStorage.setItem('token', response.data.token);
      if(typeof response.data.where_character_live !== 'undefined'){
        if(response.data.where_character_live !== null){
          this.photo = this.background_map[response.data.where_character_live];
          localStorage.setItem('bg_photo', this.photo);
        }
      }
      if(response.data.character_type !== null && response.data.character_name !== null && response.data.tutorial_done === '1'){
        this.router.navigate(['/page-home-day']);
      }else{
        this.router.navigate(['/profile-choose-avatar']);
      }
    }, error => {
      this._commonService.showLoading = false;
      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  ngOnDestroy() {
  }




}
