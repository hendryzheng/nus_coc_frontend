import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialActivityDiaryComponent } from './page-tutorial-activity-diary.component';

describe('PageTutorialActivityDiaryComponent', () => {
  let component: PageTutorialActivityDiaryComponent;
  let fixture: ComponentFixture<PageTutorialActivityDiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialActivityDiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialActivityDiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
