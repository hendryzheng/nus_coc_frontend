import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryIndoorsOutdoorsActiveComponent } from './page-activity-diary-indoors-outdoors-active.component';

describe('PageActivityDiaryIndoorsOutdoorsActiveComponent', () => {
  let component: PageActivityDiaryIndoorsOutdoorsActiveComponent;
  let fixture: ComponentFixture<PageActivityDiaryIndoorsOutdoorsActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryIndoorsOutdoorsActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryIndoorsOutdoorsActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
