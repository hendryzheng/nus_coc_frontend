import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-activity-diary-indoors-outdoors-active',
  templateUrl: './page-activity-diary-indoors-outdoors-active.component.html',
  styleUrls: ['./page-activity-diary-indoors-outdoors-active.component.scss']
})
export class PageActivityDiaryIndoorsOutdoorsActiveComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  selected_activity: any = '-';
  param: any = '';


  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;
  location: any = '-';
  location_indoor_outdoor: any = '-';
  json: any = [];
  isDualBefore: boolean = false;

  updateActivity: boolean = false;
  activity: any = [];

  photo: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    this.activatedRoute.queryParams
      .subscribe(params => {
        console.log(params); // {order: "popular"}
        this.param = params.param;
        if (typeof params.id !== 'undefined') {
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            
            this.initData();
          });
        } else {
          // this.location_indoor_outdoor = '';
        }
        if(typeof params.update !== 'undefined'){
          if(params.update == 'true'){
            this.updateActivity = true;
            this.proceed = true;
          }
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      if (!this.updateActivity) {
        if (this.kid_account.last_incomplete_activity.json !== '') {
          this.json = JSON.parse(this.kid_account.last_incomplete_activity.json);
          this.activity = this.kid_account.last_incomplete_activity;
        }
        if (typeof this.kid_account.root_activity !== 'undefined') {
          this.isDualBefore = true;
        }
      }
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);
    this.json = json;
    this.location_indoor_outdoor = this.json.location_indoor_outdoor;
    this.location = this.json.location;

  }

  selectActivityIndoorOutdoor(type) {
    console.log(type);
    if (this.location_indoor_outdoor === type) {
      this.location_indoor_outdoor = '-';
    } else {
      this.location_indoor_outdoor = type;
    }

    if (this.location_indoor_outdoor !== '-' && this.location !== '-') {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  selectActivityLocation(type) {
    console.log(type);
    if (this.location === type) {
      this.location = '-';
    } else {
      this.location = type;
    }

    if (this.location_indoor_outdoor !== '-' && this.location !== '-') {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  proceedNext() {
    if (this.proceed) {
      this.json.location = this.location;
      this.json.location_indoor_outdoor = this.location_indoor_outdoor;
      let params = {
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[location]': this.location_indoor_outdoor,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };

      if (this.updateActivity) {
        let params = {
          'kid_id': this.kid_account.id,
          'Activities[location]': this.location_indoor_outdoor,
          'Activities[json]': JSON.stringify(this.json)
        };
        let next_page = '/page-activity-summary';
        params['id'] = this.activity.id;
        params['Activities[complete]'] = 1;
        this._apiService.updateSpecificActivity(params, () => {
          this.router.navigate([next_page], { queryParams: { navigate: 'page-activity-diary' } });
        });
      } else {
          this.json.next_page = '/page-activity-diary-duration-active';
        this._apiService.updateActivity(params, () => {
          this.router.navigate([this.json.next_page], { queryParams: { param: '' } });
        });
      }
    }


  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }



    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  goBack(){
    this.router.navigate(['/page-activity-diary-active-intensity-options'], { queryParams: { id: this.activity.id } })
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}