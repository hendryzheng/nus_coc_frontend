import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRewardsForkComponent } from './page-rewards-fork.component';

describe('PageRewardsForkComponent', () => {
  let component: PageRewardsForkComponent;
  let fixture: ComponentFixture<PageRewardsForkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRewardsForkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRewardsForkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
