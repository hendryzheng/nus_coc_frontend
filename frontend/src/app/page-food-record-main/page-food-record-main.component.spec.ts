import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainComponent } from './page-food-record-main.component';

describe('PageFoodRecordMainComponent', () => {
  let component: PageFoodRecordMainComponent;
  let fixture: ComponentFixture<PageFoodRecordMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
