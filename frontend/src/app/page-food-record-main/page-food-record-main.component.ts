import { Router, ActivatedRoute, Params } from "@angular/router";
import { Component, OnInit, ViewChild } from "@angular/core";
import { CommonService } from "../services/common.service";
import { HttpService } from "../services/http.service";
import { CONFIGURATION } from "../services/config.service";
import { SubmissionApiService } from "../services/submission-api.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-page-food-record-main",
  templateUrl: "./page-food-record-main.component.html",
  styleUrls: ["./page-food-record-main.component.scss"]
})
export class PageFoodRecordMainComponent implements OnInit {
  kid_account: any = null;
  character_type: any = "";
  daily_log: any = [];
  proceed: boolean = false;
  nothing_selected: boolean = false;
  selected_option: any = [];
  selected_option_others: any = [];
  selected_drinks_option: any = [];
  selected_option_drinks_others: any = [];
  selected_fruits_option: any = [];
  selected_option_fruits_others: any = [];
  selected_desert_option: any = [];
  selected_option_deserts_others: any = [];
  image_type: any = "png";

  selectedCategories: any = "Breads, Spreads and Cereals";
  selectedCategoriesIndex: number = 0;
  selectedCategoriesType: number = 1;
  foodCategories: any = [];
  foodList: any = [];
  activity: any = [];
  json: any = [];
  updateActivity = false;

  others_selected: boolean = false;
  others: any = "";

  photo: any = "";
  search: any = "";

  showForgetDrink: boolean = false;

  @ViewChild("list") list: any;

  constructor(
    private _commonService: CommonService,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private location: Location,
    private router: Router
  ) {}

  ngOnInit() {
    let photo = localStorage.getItem("bg_photo");
    if (photo) {
      this.photo = photo;
    }
    this.route.queryParams.subscribe(params => {
      console.log(params);
      let param = {
        id: params.id
      };
      if (typeof params.update !== "undefined") {
        this.updateActivity = true;
      }
      if (typeof params.id !== "undefined") {
        this._apiService.getActivity(param, () => {
          this.activity = this._apiService.activity;
          this.json = JSON.parse(this.activity.json);
          console.log(this.json);
          if (this.json.selected_option_others.length > 0) {
            this.selected_option_others = this.json.selected_option_others;
            if (this.selected_option_others.length > 0) {
              this.proceed = true;
            }
          } else if (this.json.selected_option.length > 0) {
            this.selected_option = this.json.selected_option;
            console.log(this.selected_option);
            if (this.selected_option.length > 0) {
              this.proceed = true;
            }
            this.selectedCategories = this.json.selectedCategories;
          }
          if (this.json.selected_option_drinks_others.length > 0) {
            this.selected_option_drinks_others = this.json.selected_option_drinks_others;
            if (this.selected_option_drinks_others.length > 0) {
              this.proceed = true;
            }
          } else if (this.json.selected_drinks_option.length > 0) {
            this.selected_drinks_option = this.json.selected_drinks_option;

            if (this.selected_drinks_option.length > 0) {
              this.proceed = true;
            } else {
              this.nothing_selected = true;
              this.proceed = true;
            }
          }
          if (this.json.selected_option_fruits_others.length > 0) {
            this.selected_option_fruits_others = this.json.selected_option_fruits_others;
            if (this.json.selected_option_fruits_others.length > 0) {
              this.proceed = true;
            }
          } else if (this.json.selected_fruits_option.length > 0) {
            this.selected_fruits_option = this.json.selected_fruits_option;
            if (this.selected_fruits_option.length > 0) {
              this.proceed = true;
            } else {
              this.nothing_selected = true;
              this.proceed = true;
            }
          }
          if (this.json.selected_option_deserts_others.length > 0) {
            this.selected_option_deserts_others = this.json.selected_option_deserts_others;
            if (this.selected_option_deserts_others.length > 0) {
              this.proceed = true;
            }
          } else if (this.json.selected_desert_option.length > 0) {
            this.selected_desert_option = this.json.selected_desert_option;

            if (this.selected_option_deserts_others.length > 0) {
              this.proceed = true;
            } else {
              this.nothing_selected = true;
              this.proceed = true;
            }
          }
        });
      }
    });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem("account");
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem("daily_log");
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._commonService.showLoading = true;
    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.daily_log = this.kid_account.daily_log;
      if (typeof this.activity.id == "undefined") {
        let param = {
          id: this.kid_account.last_incomplete_activity.id
        };
        this._apiService.getActivity(param, () => {
          this.activity = this._apiService.activity;
          this.json = JSON.parse(this.activity.json);
          console.log(this.json);
          if (typeof this.json !== "undefined" && this.json != null) {
            if (typeof this.json.selected_option !== "undefined") {
              this.selected_option = this.json.selected_option;
              this.selectedCategories = this.json.selectedCategories;
            }
            if (this.selected_option.length == 0) {
              this.nothing_selected = true;
              this.proceed = true;
            }
            if (
              typeof this.json.selected_option_drinks_others !== "undefined"
            ) {
              this.selected_option_drinks_others = this.json.selected_option_drinks_others;
              if (this.selected_option_drinks_others.length > 0) {
                this.proceed = true;
              }
            } else if (
              typeof this.json.selected_drinks_option !== "undefined"
            ) {
              this.selected_drinks_option = this.json.selected_drinks_option;

              if (this.selected_drinks_option.length > 0) {
                this.proceed = true;
              } else {
                this.nothing_selected = true;
                this.proceed = true;
              }
            }
            if (
              typeof this.json.selected_option_fruits_others !== "undefined"
            ) {
              this.selected_option_fruits_others = this.json.selected_option_fruits_others;
              if (this.json.selected_option_fruits_others.length > 0) {
                this.proceed = true;
              }
            } else if (
              typeof this.json.selected_fruits_option !== "undefined"
            ) {
              this.selected_fruits_option = this.json.selected_fruits_option;
              if (this.selected_fruits_option.length > 0) {
                this.proceed = true;
              } else {
                this.nothing_selected = true;
                this.proceed = true;
              }
            }
            if (
              typeof this.json.selected_option_deserts_others !== "undefined"
            ) {
              this.selected_option_deserts_others = this.json.selected_option_deserts_others;
              if (this.selected_option_deserts_others.length > 0) {
                this.proceed = true;
              }
            } else if (
              typeof this.json.selected_desert_option !== "undefined"
            ) {
              this.selected_desert_option = this.json.selected_desert_option;

              if (this.selected_option_deserts_others.length > 0) {
                this.proceed = true;
              } else {
                this.nothing_selected = true;
                this.proceed = true;
              }
            }
          } else {
            this.activity = this.kid_account.last_incomplete_activity;
          }
        });
      }

      this._apiService.getFoodMain(() => {
        this.foodCategories = this._apiService.foodCategories;
        this.foodList = this._apiService.foodMain;
        this._commonService.showLoading = false;
      });
    });
  }

  prevCategory() {
    let maxIndex = this.foodCategories.length;
    if (this.selectedCategoriesIndex > 0) {
      this.selectedCategoriesIndex = this.selectedCategoriesIndex - 1;
      let item = this.foodCategories[this.selectedCategoriesIndex];
      this.selectedCategories = item.name;
      this.selectedCategoriesType = item.food_type;
      this.list.nativeElement.scrollTo({
        left: this.list.nativeElement.scrollLeft - 80,
        behavior: "smooth"
      });
      this.foodList = item.food_list;
    }
  }

  nextCategory() {
    let maxIndex = this.foodCategories.length;
    if (this.selectedCategoriesIndex < maxIndex) {
      this.selectedCategoriesIndex = this.selectedCategoriesIndex + 1;
      let item = this.foodCategories[this.selectedCategoriesIndex];
      this.selectedCategories = item.name;
      this.selectedCategoriesType = item.food_type;
      this.list.nativeElement.scrollTo({
        left: this.list.nativeElement.scrollLeft + 80,
        behavior: "smooth"
      });
      this.foodList = item.food_list;
    }
  }

  eventHandler(event) {
    console.log(event, event.keyCode, event.keyIdentifier);
    console.log(this.search);
    if (this.search !== "") {
      let params = {
        keyword: event.target.value,
        food_type: 1,
        cat_id: this.foodCategories[this.selectedCategoriesIndex].id
      };
      this._apiService.searchFood(params, response => {
        this.foodList = response.data;
        // this._commonService.showLoading = false;
      });
    } else {
      console.log(this.foodCategories[this.selectedCategoriesIndex]);
      this.foodList = this.foodCategories[
        this.selectedCategoriesIndex
      ].food_list;
    }
  }

  initData() {}

  goBack() {
    let params = {
      id: this.kid_account.last_incomplete_activity.id
    };
    this._apiService.deleteActivity(params, () => {
      this.location.back();
      console.log("goBack()...");
    });
  }

  nothing() {
    this.nothing_selected = true;
    this.selected_option = [];
    this.proceed = true;
  }

  isInArray(item) {
    var res = false;
    let category = this.foodCategories[this.selectedCategoriesIndex];
    if (category.food_type == "1") {
      for (var i = 0; i < this.selected_option.length; i++) {
        if (this.selected_option[i].id == item.id) {
          res = true;
        }
      }
    } else if (category.food_type == "2") {
      for (var i = 0; i < this.selected_drinks_option.length; i++) {
        if (this.selected_drinks_option[i].id == item.id) {
          res = true;
        }
      }
    } else if (category.food_type == "3") {
      for (var i = 0; i < this.selected_fruits_option.length; i++) {
        if (this.selected_fruits_option[i].id == item.id) {
          res = true;
        }
      }
    } else if (category.food_type == "4") {
      for (var i = 0; i < this.selected_desert_option.length; i++) {
        if (this.selected_desert_option[i].id == item.id) {
          res = true;
        }
      }
    } else {
    }

    return res;
  }

  selectCategory(item, i) {
    this.foodList = item.food_list;
    this.selectedCategories = item.name;
    this.selectedCategoriesIndex = i;
    this.selectedCategoriesType = item.food_type;
  }

  isSelected(){
    var selected = false;
    if (this.selected_option.length > 0){
      selected = true;
    }
    if (this.selected_drinks_option.length > 0){
      selected = true;
    }
    if (this.selected_fruits_option.length > 0){
      selected = true;
    }
    if (this.selected_desert_option.length > 0){
      selected = true;
    }
    if(this.selected_option_deserts_others.length > 0){
      selected = true;
    }
    if(this.selected_option_drinks_others.length > 0){
      selected = true;
    }
    if(this.selected_option_fruits_others.length > 0){
      selected = true;
    }
    if(this.selected_option_others.length > 0){
      selected = true;
    }
    return selected;

  }

  selectActivity(item) {
    console.log(item);
    this.nothing_selected = false;
    let category = this.foodCategories[this.selectedCategoriesIndex];
    if (category.food_type == "1") {
      for (var i = 0; i < this.selected_option.length; i++) {
        if (this.selected_option[i].id == item.id) {
          this.selected_option.splice(i, 1);
          return;
        }
      }
      this.selected_option.push(item);
      this.proceed = true;
      console.log(this.selected_option);
    } else if (category.food_type == "2") {
      for (var i = 0; i < this.selected_drinks_option.length; i++) {
        if (this.selected_drinks_option[i].id == item.id) {
          this.selected_drinks_option.splice(i, 1);
          return;
        }
      }
      this.selected_drinks_option.push(item);
      this.proceed = true;
      console.log(this.selected_drinks_option);
    } else if (category.food_type == "3") {
      for (var i = 0; i < this.selected_fruits_option.length; i++) {
        if (this.selected_fruits_option[i].id == item.id) {
          this.selected_fruits_option.splice(i, 1);
          return;
        }
      }
      this.selected_fruits_option.push(item);
      this.proceed = true;
      console.log(this.selected_fruits_option);
    } else if (category.food_type == "4") {
      for (var i = 0; i < this.selected_desert_option.length; i++) {
        if (this.selected_desert_option[i].id == item.id) {
          this.selected_desert_option.splice(i, 1);
          return;
        }
      }
      this.selected_desert_option.push(item);
      this.proceed = true;
      console.log(this.selected_desert_option);
    } else {
    }
  }

  checkOtherWasForgotten() {
    if(!this.isSelected()){
      alert('Oops ! You have not selected any food item yet !');
      return;
    }
    this.showForgetDrink = false;
    if (
      this.selected_drinks_option.length < 1 ||
      this.selected_option_drinks_others.length < 1
    ) {
      this.showForgetDrink = true;
    }
    if (
      this.selected_desert_option.length < 1 ||
      this.selected_option_deserts_others.length < 1
    ) {
      this.showForgetDrink = true;
    }
    if (
      this.selected_fruits_option.length < 1 ||
      this.selected_option_fruits_others.length < 1
    ) {
      this.showForgetDrink = true;
    }
    if (this.showForgetDrink == false) {
      this.proceedNext();
    }
  }

  resetForget() {
    this.showForgetDrink = false;
  }

  proceedNext() {
    if(!this.isSelected()){
      alert('Oops ! You have not selected any food item yet !');
      return;
    }
    if (this.proceed) {
      var selectedOption = "";

      if (this.json == null) {
        this.json = {
          current_page: "/page-food-record-main",
          selected_option: this.selected_option,
          selected_option_others: this.selected_option_others,
          selected_drinks_option: this.selected_drinks_option,
          selected_fruits_option: this.selected_fruits_option,
          selected_desert_option: this.selected_desert_option,
          selected_option_drinks_others: this.selected_option_drinks_others,
          selected_option_fruits_others: this.selected_option_fruits_others,
          selected_option_deserts_others: this.selected_option_deserts_others,
          selectedCategories: this.selectedCategories
        };
      } else {
        this.json.current_page = "/page-food-record-main";
        this.json.selected_option = this.selected_option;
        this.json.selected_option_others = this.selected_option_others;
        this.json.selected_fruits_option = this.selected_fruits_option;
        this.json.selected_option_fruits_others = this.selected_option_fruits_others;
        this.json.selected_drinks_option = this.selected_drinks_option;
        this.json.selected_option_drinks_others = this.selected_option_drinks_others;
        this.json.selected_desert_option = this.selected_desert_option;
        this.json.selected_option_deserts_others = this.selected_option_deserts_others;
        this.json.selectedCategories = this.selectedCategories;
      }

      let params = {
        kid_id: this.kid_account.id,
        "Activities[daily_log_id]": this.kid_account.daily_log.id,
        "Activities[user_kid_id]": this.kid_account.id,
        "Activities[complete]": 0,
        "Activities[json]": JSON.stringify(this.json)
      };
      this.json.next_page = "/page-food-record-main-portion";
      params["id"] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([this.json.next_page], {
          queryParams: { id: this.activity.id, update: this.updateActivity }
        });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== "undefined") {
        this._commonService.audio.pause();
      }
      this.image_type = "gif";
      if (this.kid_account.daily_log.day_status === "1") {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_33
        );
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === "2") {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_34
        );
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_35
        );
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = "png";
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = "png";
    } else {
      this.playVoiceOver();
    }
  }

  toggleOthersSelected() {
    this.others_selected = true;
  }

  saveOthers() {
    if (this.others !== "") {
      if (typeof this.selected_option_others == "undefined") {
        this.selected_option_others = [];
      }
      let temp = {
        name: this.others,
        portion: 0
      };
      let category = this.foodCategories[this.selectedCategoriesIndex];
      if (category.food_type == "1") {
        this.selected_option_others.push(temp);
      } else if (category.food_type == "2") {
        this.selected_option_drinks_others.push(temp);
      } else if (category.food_type == "3") {
        this.selected_option_fruits_others.push(temp);
      } else if (category.food_type == "4") {
        this.selected_option_deserts_others.push(temp);
      } else {
      }

      this.others_selected = false;
      this.others = "";
      this.proceed = true;
    }
  }

  remove(key) {
    this.selected_option_others.splice(key, 1);
    if (this.selected_option_others.length > 0) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  removeDrinksOthers(key) {
    this.selected_option_drinks_others.splice(key, 1);
    if (this.selected_option_drinks_others.length > 0) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  removeFruitsOthers(key) {
    this.selected_option_fruits_others.splice(key, 1);
    if (this.selected_option_fruits_others.length > 0) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  removeDesertsOthers(key) {
    this.selected_option_deserts_others.splice(key, 1);
    if (this.selected_option_deserts_others.length > 0) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  cancelOthers() {
    this.others_selected = false;
    this.others = "";
  }
}
