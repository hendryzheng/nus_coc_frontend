import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAboutYourselfComponent } from './page-about-yourself.component';

describe('PageAboutYourselfComponent', () => {
  let component: PageAboutYourselfComponent;
  let fixture: ComponentFixture<PageAboutYourselfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAboutYourselfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAboutYourselfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
