import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';


@Component({
  selector: 'app-page-about-yourself',
  templateUrl: './page-about-yourself.component.html',
  styleUrls: ['./page-about-yourself.component.scss']
})
export class PageAboutYourselfComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';
  day_status: number = 1;
  selected_gender: any = '-';
  day_list: any = [];
  month_list: any = [];
  year_list: any = [];
  birthday = {
    day: '',
    month: '',
    year: ''
  };
  image_type: any = 'png';
  photo: any = '';

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _apiService: SubmissionApiService,
    private _httpService: HttpService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    for (var i = 1; i <= 31; i++) {
      let temp = {
        key: i,
        val: i
      };
      this.day_list.push(temp);
    }
    for (var i = 1; i <= 11; i++) {
      let temp = {
        key: 2004 + i,
        val: 2004 + i
      };
      this.year_list.push(temp);
    }
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      this.character_type = this.kid_account.character_type;
      if (this.kid_account.birthday !== null) {
        var split = this.kid_account.birthday.split("-");
        this.birthday.year = split[0];
        this.birthday.month = split[1];
        this.birthday.day = split[2];
        this.selected_gender = this.kid_account.gender;
      }
    });
  }

  proceed() {
    if (this.birthday.day !== '' && this.birthday.month !== '' && this.birthday.year !== '' && this.selected_gender !== '') {
      return true;
    } else {
      return false;
    }
  }

  selectGender(gender) {
    this.selected_gender = gender;
  }

  proceedNext() {
    if (this.proceed()) {
      let params = {
        'kid_id': this.kid_account.id,
        'UserKid[birthday]': this.birthday.year + "-" + this.birthday.month + "-" + this.birthday.day,
        'UserKid[gender]': this.selected_gender
      };
      this._apiService.updateKid(params, () => {
        this.router.navigate(['/page-record-day']);
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_29);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
