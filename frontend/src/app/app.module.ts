import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { routing } from './app.routes';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';



import { SubmissionApiService } from './services/submission-api.service';
import { CommonService } from './services/common.service';
import { HttpService } from './services/http.service';

import { AngularDraggableModule } from 'angular2-draggable';
import { MomentModule } from 'angular2-moment';

import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { ProfileChooseAvatarComponent } from './profile-choose-avatar/profile-choose-avatar.component';
import { ProfileCharacterLiveComponent } from './profile-character-live/profile-character-live.component';
import { PageTutorialIntroComponent } from './page-tutorial-intro/page-tutorial-intro.component';
import { PageTutorialSoilComponent } from './page-tutorial-soil/page-tutorial-soil.component';
import { PageTutorialSoil2Component } from './page-tutorial-soil2/page-tutorial-soil2.component';
import { PageTutorialActivityDiaryComponent } from './page-tutorial-activity-diary/page-tutorial-activity-diary.component';
import { PageTutorialScreenRewardComponent } from './page-tutorial-screen-reward/page-tutorial-screen-reward.component';
import { PageTutorialScreenFinalComponent } from './page-tutorial-screen-final/page-tutorial-screen-final.component';
import { PageHomeDayComponent } from './page-home-day/page-home-day.component';
import { PageAboutYourselfComponent } from './page-about-yourself/page-about-yourself.component';
import { PageRecordDayComponent } from './page-record-day/page-record-day.component';
import { PageAboutYourselfSleepComponent } from './page-about-yourself-sleep/page-about-yourself-sleep.component';
import { PageAboutYourselfWakeupComponent } from './page-about-yourself-wakeup/page-about-yourself-wakeup.component';
import { PageActivityDiaryComponent } from './page-activity-diary/page-activity-diary.component';
import { PageShowerComponent } from './page-shower/page-shower.component';
import { PageActivityDiaryTravellingOptionsComponent } from './page-activity-diary-travelling-options/page-activity-diary-travelling-options.component';
import { PageActivityDiaryDualTravellingComponent } from './page-activity-diary-dual-travelling/page-activity-diary-dual-travelling.component';
import { PageActivityDiaryDurationTravellingComponent } from './page-activity-diary-duration-travelling/page-activity-diary-duration-travelling.component';
import { PageActivityDiarySittingOptionsComponent } from './page-activity-diary-sitting-options/page-activity-diary-sitting-options.component';
import { PageActivityDiaryDualSittingComponent } from './page-activity-diary-dual-sitting/page-activity-diary-dual-sitting.component';
import { PageActivityDiaryLocationSittingComponent } from './page-activity-diary-location-sitting/page-activity-diary-location-sitting.component';
import { PageActivityDiaryIndoorsOutdoorsSittingComponent } from './page-activity-diary-indoors-outdoors-sitting/page-activity-diary-indoors-outdoors-sitting.component';
import { PageActivityDiaryDurationSittingComponent } from './page-activity-diary-duration-sitting/page-activity-diary-duration-sitting.component';
import { PageActivityDiarySleepComponent } from './page-activity-diary-sleep/page-activity-diary-sleep.component';
import { PageActivityDiaryActiveOptionsComponent } from './page-activity-diary-active-options/page-activity-diary-active-options.component';
import { PageActivityDiaryActiveIntensityComponent } from './page-activity-diary-active-intensity/page-activity-diary-active-intensity.component';
import { PageActivityDiaryDualActiveComponent } from './page-activity-diary-dual-active/page-activity-diary-dual-active.component';
import { PageActivityDiaryDurationActiveComponent } from './page-activity-diary-duration-active/page-activity-diary-duration-active.component';
import { PageActivitySummaryComponent } from './page-activity-summary/page-activity-summary.component';
import { PageRewardSeedComponent } from './page-reward-seed/page-reward-seed.component';
import { PageGrowthDay1Component } from './page-growth-day-1/page-growth-day-1.component';

import { DragAndDropModule } from 'angular-draggable-droppable';
import { PageRewardsWateringCanComponent } from './page-rewards-watering-can/page-rewards-watering-can.component';
import { PageRewardsFertilizerComponent } from './page-rewards-fertilizer/page-rewards-fertilizer.component';
import { PageRewardsForkComponent } from './page-rewards-fork/page-rewards-fork.component';
import { PageGrowthDay2Component } from './page-growth-day-2/page-growth-day-2.component';
import { PageGrowthDay3Component } from './page-growth-day-3/page-growth-day-3.component';
import { PageGrowthDay4Component } from './page-growth-day-4/page-growth-day-4.component';
import { PageHomeDoneComponent } from './page-home-done/page-home-done.component';
import { PageHomeDay1DoneComponent } from './page-home-day-1-done/page-home-day-1-done.component';
import { PageHomeDay2DoneComponent } from './page-home-day-2-done/page-home-day-2-done.component';
import { PageHomeDay3DoneComponent } from './page-home-day-3-done/page-home-day-3-done.component';
import { PageFoodRecordMainComponent } from './page-food-record-main/page-food-record-main.component';
import { PageFoodRecordMainDrinksComponent } from './page-food-record-main-drinks/page-food-record-main-drinks.component';
import { PageFoodRecordMainFruitsComponent } from './page-food-record-main-fruits/page-food-record-main-fruits.component';
import { PageFoodRecordMainDesertsComponent } from './page-food-record-main-deserts/page-food-record-main-deserts.component';
import { PageFoodRecordMainPortionComponent } from './page-food-record-main-portion/page-food-record-main-portion.component';
import { PageFoodRecordMainSummaryComponent } from './page-food-record-main-summary/page-food-record-main-summary.component';
import { PageFoodRecordSummaryComponent } from './page-food-record-summary/page-food-record-summary.component';
import { PageFoodRecordLocationComponent } from './page-food-record-location/page-food-record-location.component';
import { PageFoodRecordDurationComponent } from './page-food-record-duration/page-food-record-duration.component';
import { PageActivityDiaryIndoorsOutdoorsActiveComponent } from './page-activity-diary-indoors-outdoors-active/page-activity-diary-indoors-outdoors-active.component';
import { PageActivityDiaryDualEatingComponent } from './page-activity-diary-dual-eating/page-activity-diary-dual-eating.component';
import { LifestyleReportComponent } from './lifestyle-report/lifestyle-report.component';
import { PageHomeCompleteComponent } from './page-home-complete/page-home-complete.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ProfileChooseAvatarComponent,
    ProfileCharacterLiveComponent,
    PageTutorialIntroComponent,
    PageTutorialSoilComponent,
    PageTutorialSoil2Component,
    PageTutorialActivityDiaryComponent,
    PageTutorialScreenRewardComponent,
    PageTutorialScreenFinalComponent,
    PageHomeDayComponent,
    PageAboutYourselfComponent,
    PageRecordDayComponent,
    PageAboutYourselfSleepComponent,
    PageAboutYourselfWakeupComponent,
    PageActivityDiaryComponent,
    PageShowerComponent,
    PageActivityDiaryTravellingOptionsComponent,
    PageActivityDiaryDualTravellingComponent,
    PageActivityDiaryDurationTravellingComponent,
    PageActivityDiarySittingOptionsComponent,
    PageActivityDiaryDualSittingComponent,
    PageActivityDiaryLocationSittingComponent,
    PageActivityDiaryIndoorsOutdoorsSittingComponent,
    PageActivityDiaryDurationSittingComponent,
    PageActivityDiarySleepComponent,
    PageActivityDiaryActiveOptionsComponent,
    PageActivityDiaryActiveIntensityComponent,
    PageActivityDiaryDualActiveComponent,
    PageActivityDiaryDurationActiveComponent,
    PageActivitySummaryComponent,
    PageRewardSeedComponent,
    PageGrowthDay1Component,
    PageRewardsWateringCanComponent,
    PageRewardsFertilizerComponent,
    PageRewardsForkComponent,
    PageGrowthDay2Component,
    PageGrowthDay3Component,
    PageGrowthDay4Component,
    PageHomeDoneComponent,
    PageHomeDay1DoneComponent,
    PageHomeDay2DoneComponent,
    PageHomeDay3DoneComponent,
    PageFoodRecordMainComponent,
    PageFoodRecordMainDrinksComponent,
    PageFoodRecordMainFruitsComponent,
    PageFoodRecordMainDesertsComponent,
    PageFoodRecordMainPortionComponent,
    PageFoodRecordMainSummaryComponent,
    PageFoodRecordSummaryComponent,
    PageFoodRecordLocationComponent,
    PageFoodRecordDurationComponent,
    PageActivityDiaryIndoorsOutdoorsActiveComponent,
    PageActivityDiaryDualEatingComponent,
    LifestyleReportComponent,
    PageHomeCompleteComponent
  ],
  imports: [
    DragAndDropModule.forRoot(),
    routing,
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    AngularDraggableModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    MomentModule
  ],
  exports: [

  ],
  providers: [
    SubmissionApiService,
    CommonService,
    HttpService
    // { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
