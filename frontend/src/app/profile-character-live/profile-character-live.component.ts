import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-profile-character-live',
  templateUrl: './profile-character-live.component.html',
  styleUrls: ['./profile-character-live.component.scss']
})
export class ProfileCharacterLiveComponent implements OnInit {

  selected: any = '-';
  proceed: boolean = false;

  name: any = '';
  kid_account: any = null;
  selected_char: any = '';
  image_type: any = 'png';
  photo: any = 'assets/img/background/mountains/mountains@2x.png';
  background_map = {
    'beach' : 'assets/img/background/beach/beach@2x.png',
    'carnival' : 'assets/img/background/carnival/carnival@2x.png',
    'forest' : 'assets/img/background/forest/forest@2x.png',
    'mountains' : 'assets/img/background/mountains/mountains@2x.png',
    'arcade': 'assets/img/background/video-game/video-game@2x.png',
    'space': 'assets/img/background/space/space@2x.png'
  }

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      if (this.kid_account.where_character_live !== null) {
        this.selected = this.kid_account.where_character_live;
      }
      if (this.kid_account.character_type !== null) {
        this.selected_char = this.kid_account.character_type;
      }
    });
  }

  select(type) {
    this.selected = type;
    this.photo = this.background_map[type];
    localStorage.setItem('bg_photo', this.photo);
  }

  isProceed() {
    return this.selected !== '-';
  }

  next() {
    if (this.isProceed()) {
      let params = {
        'kid_id': this.kid_account.id,
        'UserKid[where_character_live]': this.selected
      };
      this._apiService.updateKid(params, () => {
        this.router.navigate(['/page-tutorial-intro']);
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_6);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
