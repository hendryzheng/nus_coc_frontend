import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCharacterLiveComponent } from './profile-character-live.component';

describe('ProfileCharacterLiveComponent', () => {
  let component: ProfileCharacterLiveComponent;
  let fixture: ComponentFixture<ProfileCharacterLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCharacterLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCharacterLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
