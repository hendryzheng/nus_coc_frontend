import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDurationActiveComponent } from './page-activity-diary-duration-active.component';

describe('PageActivityDiaryDurationActiveComponent', () => {
  let component: PageActivityDiaryDurationActiveComponent;
  let fixture: ComponentFixture<PageActivityDiaryDurationActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDurationActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDurationActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
