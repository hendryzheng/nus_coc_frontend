import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainDesertsComponent } from './page-food-record-main-deserts.component';

describe('PageFoodRecordMainDesertsComponent', () => {
  let component: PageFoodRecordMainDesertsComponent;
  let fixture: ComponentFixture<PageFoodRecordMainDesertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainDesertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainDesertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
