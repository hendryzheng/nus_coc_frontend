import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeDay3DoneComponent } from './page-home-day-3-done.component';

describe('PageHomeDay3DoneComponent', () => {
  let component: PageHomeDay3DoneComponent;
  let fixture: ComponentFixture<PageHomeDay3DoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeDay3DoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeDay3DoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
