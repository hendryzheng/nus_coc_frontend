import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRecordDayComponent } from './page-record-day.component';

describe('PageRecordDayComponent', () => {
  let component: PageRecordDayComponent;
  let fixture: ComponentFixture<PageRecordDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRecordDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRecordDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
