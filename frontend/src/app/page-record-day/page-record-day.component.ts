import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import * as moment from 'moment';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-record-day',
  templateUrl: './page-record-day.component.html',
  styleUrls: ['./page-record-day.component.scss']
})
export class PageRecordDayComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';
  yesterday: any = [];
  today: any = [];
  selected_day: any = '-';
  selected_date: any = [];
  image_type: any = 'png';

  photo : any = '';
  yesterday_disabled: boolean = false;
  today_disabled: boolean = false;

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    this.today = {
      date: moment().format('dddd, DD MMMM YYYY'),
      day: moment().format('DD'),
      db_date: moment().format('YYYY-MM-DD')
    };
    this.yesterday = {
      date: moment().subtract(1, 'day').format('dddd, DD MMMM YYYY'),
      day: moment().subtract(1, 'day').format('DD'),
      db_date: moment().subtract(1, 'day').format('YYYY-MM-DD')
    };

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
    });
    let params = {
      yesterday: this.yesterday.db_date,
      today: this.today.db_date
    }
    this._apiService.checkDateExist(params, ()=>{
        this.yesterday_disabled = this._apiService.yesterday;
        this.today_disabled = this._apiService.today;
    });
  }

  next() {
    if (this.selected_day == 'Today') {
      if(this.today_disabled == true){
        alert('Today date was logged before');
        return;
      }
    }else{
      if(this.yesterday_disabled == true){
        alert('Yesterday date was logged before');
        return;
      }
    }
    if (this.selected_day !== '' && this.selected_day !== '-') {
      let params = {
        'kid_id': this.kid_account.id,
        // 'UserKid[day_status]': parseInt(this.kid_account.day_status) + 1,
        'DailyLog[selected_day]': this.selected_day,
        'DailyLog[user_kid_id]': this.kid_account.id,
        'DailyLog[day_log]': this.selected_date.date,
        'DailyLog[date_log]': this.selected_date.db_date
      };
      this._apiService.updateKid(params, () => {
        // localStorage.setItem('daily_log', JSON.stringify(this.kid_account.daily_log));
        this.router.navigate(['/page-about-yourself-sleep']);
      });

    }
  }

  setDate(date) {
    if (date == 'Today') {
      if(this.today_disabled == true){
        alert('Today date was logged before');
      }else{
        this.selected_date = this.today;
        this.selected_day = date;
      }
    } else {
      if(this.yesterday_disabled == true){
        alert('Yesterday date was logged before');
      }else{
        this.selected_date = this.yesterday;
        this.selected_day = date;
      }
    }
    
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_30);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
