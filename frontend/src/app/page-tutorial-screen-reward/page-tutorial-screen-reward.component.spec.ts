import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialScreenRewardComponent } from './page-tutorial-screen-reward.component';

describe('PageTutorialScreenRewardComponent', () => {
  let component: PageTutorialScreenRewardComponent;
  let fixture: ComponentFixture<PageTutorialScreenRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialScreenRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialScreenRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
