import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryComponent } from './page-activity-diary.component';

describe('PageActivityDiaryComponent', () => {
  let component: PageActivityDiaryComponent;
  let fixture: ComponentFixture<PageActivityDiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
