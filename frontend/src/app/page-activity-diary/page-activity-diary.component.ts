import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-activity-diary',
  templateUrl: './page-activity-diary.component.html',
  styleUrls: ['./page-activity-diary.component.scss']
})
export class PageActivityDiaryComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];
  odd_timing: boolean = false;

  proceed: boolean = false;

  selected_activity: any = '-';
  other_activities: any = '';
  image_type: any = 'png';
  param: any = '';
  time: any = '';
  start_date_time: any = '';
  last_activity: any = '';
  png: any = '';
  photo: any = '';

  showSuggestion: boolean = false;
  suggestion: any = [];

  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if (photo) {
      this.photo = photo;
    }


    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }


    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.daily_log = this.kid_account.daily_log;
      localStorage.setItem('daily_log', JSON.stringify(this.daily_log));
      this.character_type = this.kid_account.character_type;
      if (this.kid_account.last_completed_activity !== false) {
        this.last_activity = this.kid_account.last_completed_activity.activity_type;
      }
      // let time = this.time.split(' ');
      // this.start_date_time = this.daily_log.date_log+' '+time[0];
      this.time = this.kid_account.time;
      this.param = this.kid_account.param_display;
      this.start_date_time = this.kid_account.start_date_time;
    });
  }

  selectActivity(type, png) {
    console.log(type);
    this.png = png;
    if (this.selected_activity === type) {
      this.selected_activity = '-';
    } else {
      this.selected_activity = type;
      this.proceed = true;
    }
  }

  proceedNext() {
    if (this.proceed) {
      let json = {
        current_page: '/page-activity-diary',
        next_page: '/'
      };

      if (this.selected_activity === 'Shower/Washup') {
        json.next_page = '/page-shower'
      } else if (this.selected_activity === 'Travelling') {
        json.next_page = '/page-activity-diary-travelling-options';
      } else if (this.selected_activity === 'Sitting Activities') {
        json.next_page = '/page-activity-diary-sitting-options';
      } else if (this.selected_activity === 'Active Activities') {
        json.next_page = '/page-activity-diary-active-options';
      } else if (this.selected_activity === 'Nap/Sleep') {
        json.next_page = '/page-activity-diary-sleep';
      } else if (this.selected_activity === 'Eat and Drink') {
        json.next_page = '/page-food-record-main';
      }

      let params = {
        'kid_id': this.kid_account.id,
        'new': 'true',
        'Activities[icon]': this.png,
        'Activities[start_date_time]': this.start_date_time,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[activity_type]': this.selected_activity,
        'Activities[json]': JSON.stringify(json)
      };
      console.log(params);
      this._apiService.updateActivity(params, () => {
        this.router.navigate([json.next_page], { queryParams: { param: '' } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  eventHandler(ev) {
    if (this.suggestion.length > 0) {
      this.showSuggestion = true;
    } else {
      this.showSuggestion = false;
    }
    if (this.other_activities !== '') {
      this.selected_activity = '-';
    }

    console.log(ev);
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
