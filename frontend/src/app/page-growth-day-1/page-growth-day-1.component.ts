import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-growth-day-1',
  templateUrl: './page-growth-day-1.component.html',
  styleUrls: ['./page-growth-day-1.component.scss']
})
export class PageGrowthDay1Component implements OnInit {

  kid_account: any = null;
  day_status: number = 1;
  character_type: any = '';
  image_type: any = 'png';
  droppedData: boolean = false;

  photo: any = '';

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this._commonService.soundActive = true;
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      this.character_type = this.kid_account.character_type;
    });

  }

  dragEnd(event) {
    this.droppedData = true;
    console.log('Element was dragged', event);
    this.playVoiceOverEnd();
  }

  drop() {
    this.droppedData = true;
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOverEnd() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_44);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_43);
        this._commonService.audio.play();

        this._commonService.audio.onended = () => {
          this.image_type = 'png';
        };
      }


    }
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_39);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  goToHome() {
    if (this.droppedData) {
      this.router.navigate(['/page-home-day-1-done'])
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }
}
