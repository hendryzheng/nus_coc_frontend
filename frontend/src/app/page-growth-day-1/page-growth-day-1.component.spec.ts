import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGrowthDay1Component } from './page-growth-day-1.component';

describe('PageGrowthDay1Component', () => {
  let component: PageGrowthDay1Component;
  let fixture: ComponentFixture<PageGrowthDay1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGrowthDay1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGrowthDay1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
