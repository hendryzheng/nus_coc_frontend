import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDurationSittingComponent } from './page-activity-diary-duration-sitting.component';

describe('PageActivityDiaryDurationSittingComponent', () => {
  let component: PageActivityDiaryDurationSittingComponent;
  let fixture: ComponentFixture<PageActivityDiaryDurationSittingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDurationSittingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDurationSittingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
