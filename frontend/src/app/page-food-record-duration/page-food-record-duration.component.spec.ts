import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordDurationComponent } from './page-food-record-duration.component';

describe('PageFoodRecordDurationComponent', () => {
  let component: PageFoodRecordDurationComponent;
  let fixture: ComponentFixture<PageFoodRecordDurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordDurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordDurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
