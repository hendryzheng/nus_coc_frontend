import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-food-record-location',
  templateUrl: './page-food-record-location.component.html',
  styleUrls: ['./page-food-record-location.component.scss']
})
export class PageFoodRecordLocationComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  location: any = '-';
  location_indoor_outdoor: any = '-';
  image_type: any = 'png';
  activity: any = [];
  json: any = [];
  updateActivity: boolean = false;

  photo: any = '';

  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.update !== 'undefined') {
          if (params.update == 'true')
            this.updateActivity = true;
        }
        if (typeof params.id !== 'undefined') {
          let param = {
            id: params.id
          };
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.json = JSON.parse(this.activity.json);
            if (typeof this.json.location !== 'undefined') {
              this.location = this.json.location;
              this.proceed = true;
            }
            if (typeof this.json.location_indoor_outdoor !== 'undefined') {
              this.location_indoor_outdoor = this.json.location_indoor_outdoor;
              this.proceed = true;
            } else {
              // this.location_indoor_outdoor = 'Idoor';
            }
            console.log(this.json);
          });
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
    });
  }

  selectActivityIndoorOutdoor(type) {
    console.log(type);
    if (this.location_indoor_outdoor === type) {
      this.location_indoor_outdoor = '-';
    } else {
      this.location_indoor_outdoor = type;
    }

    if (this.location_indoor_outdoor !== '-' && this.location !== '-') {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  selectActivityLocation(type) {
    console.log(type);
    if (this.location === type) {
      this.location = '-';
    } else {
      this.location = type;
    }

    if (this.location_indoor_outdoor !== '-' && this.location !== '-') {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  proceedNext() {
    if (this.proceed) {
      this.json.location = this.location;
      this.json.location_indoor_outdoor = this.location_indoor_outdoor;

      let params = {
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[location]': this.location_indoor_outdoor,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };

      if (this.updateActivity) {
        this.json.next_page = '/page-activity-summary';
        params['Activities[complete]'] = 1;
      } else {
        this.json.next_page = '/page-food-record-duration';
      }
      params['id'] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        if (this.updateActivity) {
          this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id, navigate: '/page-activity-diary' } });
        } else {
          this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id } });
        }
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
