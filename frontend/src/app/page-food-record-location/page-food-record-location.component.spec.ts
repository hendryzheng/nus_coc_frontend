import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordLocationComponent } from './page-food-record-location.component';

describe('PageFoodRecordLocationComponent', () => {
  let component: PageFoodRecordLocationComponent;
  let fixture: ComponentFixture<PageFoodRecordLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
