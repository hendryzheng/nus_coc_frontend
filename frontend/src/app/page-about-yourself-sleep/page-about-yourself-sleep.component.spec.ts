import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAboutYourselfSleepComponent } from './page-about-yourself-sleep.component';

describe('PageAboutYourselfSleepComponent', () => {
  let component: PageAboutYourselfSleepComponent;
  let fixture: ComponentFixture<PageAboutYourselfSleepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAboutYourselfSleepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAboutYourselfSleepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
