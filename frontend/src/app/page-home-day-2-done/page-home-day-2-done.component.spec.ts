import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeDay2DoneComponent } from './page-home-day-2-done.component';

describe('PageHomeDay2DoneComponent', () => {
  let component: PageHomeDay2DoneComponent;
  let fixture: ComponentFixture<PageHomeDay2DoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeDay2DoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeDay2DoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
