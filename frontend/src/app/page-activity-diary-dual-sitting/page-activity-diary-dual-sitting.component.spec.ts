import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDualSittingComponent } from './page-activity-diary-dual-sitting.component';

describe('PageActivityDiaryDualSittingComponent', () => {
  let component: PageActivityDiaryDualSittingComponent;
  let fixture: ComponentFixture<PageActivityDiaryDualSittingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDualSittingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDualSittingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
