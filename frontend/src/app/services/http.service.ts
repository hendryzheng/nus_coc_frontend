import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import { CONFIGURATION } from './config.service';

declare var device: any;
declare var navigator: any;

@Injectable()
export class HttpService {

    constructor(public http: Http) {

    }

    deviceModel: string = '';
    ipInfo: any = [];

    private ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };

    fetchIp() {
        this.http.get(CONFIGURATION.GET_IP)
            .map(response => response.json())
            .subscribe((res: Response) => {
                let stringify = JSON.stringify(res);
                localStorage.setItem('ipInfo',JSON.stringify(res));
                console.log(stringify);
                this.ipInfo = res;
            });
    }


    private handleError(error: Response | any): Promise<any> {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            // alert(body.message);
            // this._utilitiesService.alertMessage('Oops ! Something Happened',body.message);
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        // console.error(errMsg);
        // return Observable.throw(errMsg);
        // alert(errMsg);
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error);
    }

    getRequest(url: string) {
        // url += '?client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(CONFIGURATION.apiEndpoint + url)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError)
            .toPromise();
    }

    getRequestPlain(url: string) {
        // url += '?client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError)
            .toPromise();
    }

    postRequest(url: string, params: any) {
        if (typeof device !== 'undefined') {
            this.deviceModel = device.model + " " + device.platform + " " + device.version;
        }
        
        // let sessionEncrypted = this._encryptionService.encrypt(paramSession);
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

        let options = new RequestOptions({ headers: headers });
        let account = localStorage.getItem('account');
        var token = localStorage.getItem('token');
        params['token'] = token;
        params['account_json'] = account;
        
        params['nav_agent'] = navigator.userAgent+ ' | '+navigator.platform+ ' | '+navigator.appVersion;
        // params['ip_address'] = this.ipInfo.ip;
        // params['agent_ip_obj'] = localStorage['ipInfo'];
        return this.http.post(CONFIGURATION.apiEndpoint + url, this.ObjecttoParams(params), options)
            .map(response => {
                let responseJson = response.json();
                return responseJson
            })
            .catch(this.handleError)
            .toPromise();
    }

}