import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {

  showLoading: boolean = false;
  soundActive: boolean = true;
  audio: any;

  audioList = {
    audio_1: 'assets/vo/1. Hi there - Ellie VO.mp3',
    audio_2: 'assets/vo/2. Before we start, choose a character! - Ellie VO.mp3',
    audio_3: 'assets/vo/3. This characer will be helping through your journey - Ellie VO.mp3',
    audio_4: 'assets/vo/4. Cool - Ellie VO.mp3',
    audio_5: 'assets/vo/5. What_s your character_s name - Ellie VO.mp3',
    audio_6: 'assets/vo/6. Now, where does your character live - Ellie VO.mp3',
    audio_7: 'assets/vo/7. Hello there - Ellie VO.mp3',
    audio_8: 'assets/vo/8. I need your help with a task! - Ellie VO.mp3',
    audio_9: 'assets/vo/9. See this patch of soil - Ellie VO.mp3',
    audio_10: 'assets/vo/10. I want to grow a plant - Ellie VO.mp3',
    audio_11: 'assets/vo/11. But I don_t have any tools. - Ellie VO.mp3',
    audio_12: 'assets/vo/12. I know how we can get them! - Ellie VO.mp3',
    audio_13: 'assets/vo/13. But I can_t do it without you. - Ellie VO.mp3',
    audio_14: 'assets/vo/14. Will you help me - Ellie VO.mp3',
    audio_15: 'assets/vo/15. Fantasrtic! - Ellie VO.mp3',
    audio_16: 'assets/vo/16. It_s very simple. - Ellie VO.mp3',
    audio_17: 'assets/vo/17. All we have to do it so fill up this activity diary with our daily activities - Ellie VO.mp3',
    audio_18: 'assets/vo/18. Everyday after filling up the diary, we will unlock tools to grow our plant. - Ellie VO.mp3',
    audio_19: 'assets/vo/19. We_ll get all we need if we do this for 3 days. - Ellie VO.mp3',
    audio_20: 'assets/vo/20. Let_s do this - Ellie VO.mp3',
    audio_21: 'assets/vo/21. Ready, get set, go - Ellie VO.mp3',
    audio_22: 'assets/vo/22. Let_s record your first day - Ellie VO.mp3',
    audio_23: 'assets/vo/23. Let_s record your second day - Ellie VO.mp3',
    audio_24: 'assets/vo/24. Let_s record your third day - Ellie VO.mp3',
    audio_25: 'assets/vo/25. Take a break! - Ellie VO.mp3',
    audio_26: 'assets/vo/26. You can come back later to record the rest. - Ellie VO.mp3',
    audio_27: 'assets/vo/27. Good job! - Ellie VO.mp3',
    audio_28: 'assets/vo/28. Come back tomorrow to unlock another tool - Ellie VO.mp3',
    audio_29: 'assets/vo/29. Tell me more about yourself - Ellie VO.mp3',
    audio_30: 'assets/vo/30. So... which day are you recording for - Ellie VO.mp3',
    audio_31: 'assets/vo/31. Great! You are recording for today - Ellie VO.mp3',
    audio_32: 'assets/vo/32. Great! You are recording for yesterday - Ellie VO.mp3',
    audio_33: '',
    audio_34: '',
    audio_35: '',
    audio_36: 'assets/vo/36. Yippee! You have unlocked your first reward! - Ellie VO.mp3',
    audio_37: 'assets/vo/37. Yippee! You have unlocked your second reward! - Ellie VO.mp3',
    audio_38: 'assets/vo/38. Yippee! You have unlocked your third reward! - Ellie VO.mp3',
    audio_39: 'assets/vo/39. Let_s drag your seed into the soil and see what happens! - Ellie VO.mp3',
    audio_40: 'assets/vo/40. Let_s drag your fertiliser onto the plant and see what happens! - Ellie VO.mp3',
    audio_41: 'assets/vo/41. Let_s drag your watering can onto the plant and see what happens! - Ellie VO.mp3',
    audio_42: 'assets/vo/42. Awesome! Your seed has sprouted! - Ellie VO.mp3',
    audio_43: 'assets/vo/43. Come back tomorrow for your next reward. - Ellie VO.mp3',
    audio_44: 'assets/vo/44. Awesome! Your plant has grown! - Ellie VO.mp3',
    audio_45: 'assets/vo/Let_s record your fourth day.wav',
    audio_46: 'assets/vo/Thank you for your effort over the past 4 days.wav',
    audio_47: 'assets/vo/We_ll get all we need if we do this for 4 days..wav',
    audio_48: 'assets/vo/You have unlocked your fourth reward.wav',
    audio_49: 'assets/vo/It has borne fruit from your care and concern.wav',
    audio_50: 'assets/vo/Look at your plant.wav',
    audio_51: 'assets/vo/Let_s drag your gardening tool onto the plant and see what happens.wav'

  };

  seekTo: number = 0;
  stopAt: number = 0;
  skipTo: any = '';

  src: any = 'assets/images/Video/CCE_CNY_EnglishPT1.mp4';
  video1 = 'assets/images/Video/CCE_CNY_EnglishPT1.mp4';
  video2 = 'assets/images/Video/CCE_CNY_EnglishPT2.mp4';
  video3 = 'assets/images/Video/CCE_CNY_EnglishPT3.mp4';
  videoFull = 'assets/images/Video/CCE_CNY_English.mp4';

  gameType: any = '';

  voiceOverPlaying: any = null;

  constructor() {

  }

  toggleSound() {
    this.soundActive = !this.soundActive;
    if (!this.soundActive) {
      this.audio.pause();
    } else {
      this.audio.volume = 0.7;
      this.audio.play();
    }
  }

  toggleSoundCallback(callback: () => void) {
    this.soundActive = !this.soundActive;
    if (!this.soundActive) {
      this.audio.pause();
    } else {
      callback();
    }
  }

  




}
