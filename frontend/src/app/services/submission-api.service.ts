import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseRequestOptions, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { HttpService } from './http.service';
import { CONFIGURATION } from './config.service';
import { CommonService } from './common.service';

@Injectable()
export class SubmissionApiService {
  baseUrl: string;
  stringKey: string;
  kid_account: any = [];
  summary_list: any = [];
  activity: any = [];
  foodCategories: any = [];
  foodMain: any = [];
  drinks: any = [];
  fruits: any = [];
  deserts: any = [];
  yesterday: boolean = false;
  today: boolean = false;


  constructor(private http: Http, private _httpService: HttpService, private _commonService: CommonService) {
    this.baseUrl = 'https://hacker-news.firebaseio.com/v0';
  }

  submit(param: any) {
    return this._httpService.postRequest(CONFIGURATION.URL.submitVerification, param);

  }

  getCaptcha() {
    return this._httpService.postRequest(CONFIGURATION.URL.getCaptcha, {});
  }

  submitv1(param: any) {
    return this._httpService.postRequest(CONFIGURATION.URL.submitVerificationv1, param);

  }

  queryKey(param: any) {
    return this._httpService.postRequest(CONFIGURATION.URL.checkKey, param);
  }

  queryKeyParam(param: any) {
    return this._httpService.postRequest(CONFIGURATION.URL.checkKeyParam, param);
  }

  updateKid(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.updateKid, params).then(response => {
      this.kid_account = response.data;
      localStorage.setItem('account', JSON.stringify(response.data));
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getFoodMain(callback: () => void) {
    let params = {};
    this._httpService.postRequest(CONFIGURATION.URL.getFoodMain, params).then(response => {
      this.foodCategories = response.data.categories;
      this.foodMain = response.data.main;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  searchFood(params, callback: (response) => void) {
    this._httpService.postRequest(CONFIGURATION.URL.searchFood, params).then(response => {
      callback(response);
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getDrinks(callback: () => void) {
    let params = {};
    this._httpService.postRequest(CONFIGURATION.URL.getDrinks, params).then(response => {
      this.drinks = response.data;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getReport(params, callback: (response) => void) {
    this._httpService.postRequest(CONFIGURATION.URL.getReport, params).then(response => {
      callback(response);
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  deleteActivity(params, callback: () => void) {
    this._httpService.postRequest(CONFIGURATION.URL.deleteActivity, params).then(response => {
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  deleteLog(params, callback: () => void) {
    this._httpService.postRequest(CONFIGURATION.URL.deleteLog, params).then(response => {
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getFruits(callback: () => void) {
    let params = {};
    this._httpService.postRequest(CONFIGURATION.URL.getFruits, params).then(response => {
      this.fruits = response.data;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getDeserts(callback: () => void) {
    let params = {};
    this._httpService.postRequest(CONFIGURATION.URL.getDeserts, params).then(response => {
      this.deserts = response.data;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  updateActivity(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.updateActivity, params).then(response => {
      this.kid_account = response.data;
      localStorage.setItem('account', JSON.stringify(response.data));
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  updateSpecificActivity(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.updateSpecificActivity + '/' + params['id'], params).then(response => {
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  completeDailyLog(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.completeDailyLog + '/' + params['id'], params).then(response => {
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  updatePlant(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.updatePlant, params).then(response => {
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getActivity(params, callback: () => void) {
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.getActivity + '/' + params['id'], params).then(response => {
      this._commonService.showLoading = false;
      this.activity = response.data;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getKid(callback: () => void) {
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let params = {
      kid_id: this.kid_account.id
    }
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.getKid, params).then(response => {
      localStorage.setItem('account', JSON.stringify(response.data));
      this.kid_account = response.data;
      this._commonService.showLoading = false;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  checkDateExist(params, callback: () => void) {
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    params['kid_id'] = this.kid_account.id;
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.checkDateExist, params).then(response => {
      this._commonService.showLoading = false;
      this.yesterday = response.data.yesterday;
      this.today = response.data.today;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

  getSummary(callback: () => void) {
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let params = {
      kid_id: this.kid_account.id,
      daily_log_id: this.kid_account.daily_log.id
    }
    this._commonService.showLoading = true;
    this._httpService.postRequest(CONFIGURATION.URL.getSummary, params).then(response => {
      this._commonService.showLoading = false;
      this.summary_list = response.data;
      callback();
    }, error => {

      const body = error.json() || '';
      // alert(body.message);
      // return Observable.throw(errMsg);
      if (body.message !== '') {
        alert(body.message);
      }
    })
  }

}
