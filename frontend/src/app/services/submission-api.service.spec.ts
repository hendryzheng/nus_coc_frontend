import { TestBed, inject } from '@angular/core/testing';

import { SubmissionApiService } from './submission-api.service';

describe('SubmissionApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubmissionApiService]
    });
  });

  it('should be created', inject([SubmissionApiService], (service: SubmissionApiService) => {
    expect(service).toBeTruthy();
  }));
});
