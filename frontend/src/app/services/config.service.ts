export let CONFIGURATION = {
    // apiEndpoint: 'http://localhost/nus_coc/backend/',
    // apiEndpoint: 'http://161.117.224.196//frontend/nus_coc/backend/',
    apiEndpoint: 'https://medal.nus.edu.sg/medal/backend/index.php/',

    // apiEndpoint: 'https://www.candyb.co/back/',
    // apiEndpoint: 'https://wellous.com/backend/api',
    //prod
    // apiEndpoint: 'https://app-api.emcapital2.org/',
    
    // apiEndpoint: 'http://hendryzheng.com/projects/epc/',

    //staging
    // apiEndpoint: 'http://121.121.47.174/epc_web/',
    URL: {
        login: 'api/login',
        updateKid: 'api/updateKid',
        updateActivity: 'api/updateActivity',
        getKid: 'api/getKid',
        submitVerification: 'api/submitVerification',
        submitVerificationv1: 'api/submitVerificationV1',
        checkKey: 'api/checkKey',
        checkKeyParam: 'api/checkKeyExist',
        getCaptcha: 'api/getCaptcha',
        getSummary: 'api/getSummary',
        getActivity: 'api/getActivity',
        updateSpecificActivity: 'api/updateSpecificActivity',
        completeDailyLog: 'api/completeDailyLog',
        updatePlant: 'api/updatePlant',
        getFoodMain: 'api/getFoodMain',
        getDrinks: 'api/getDrinks',
        getFruits: 'api/getFruits',
        getDeserts: 'api/getDeserts',
        searchFood: 'api/searchFood',
        getReport: 'api/getReport',
        uploadReport: 'api/uploadReport',
        deleteActivity: 'api/deleteActivity',
        deleteLog: 'api/deleteLog',
        checkDateExist: 'api/checkDateExist'
    },
    GET_IP:'https://freegeoip.net/json/'

}