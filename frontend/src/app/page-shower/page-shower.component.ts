import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-page-shower',
  templateUrl: './page-shower.component.html',
  styleUrls: ['./page-shower.component.scss']
})
export class PageShowerComponent implements OnInit {


  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];
  odd_timing: boolean = false;

  proceed: boolean = false;

  time = {
    time_shower_hr1: '',
    time_shower_hr2: '',
    time_shower_minute1: '',
    time_shower_minute2: '',
    shower_am: 'AM'
  };

  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;
  start_time: any = '';
  oddTiming: boolean = false;
  falseTiming: boolean = false;

  updateActivity: boolean = false;
  activity: any = [];

  photo: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private route: ActivatedRoute,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.id !== 'undefined') {
          this.updateActivity = true;
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.initData();
          });
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.start_time = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('hh:mm A');
      this.time.shower_am = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('A');
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);
    this.time = json.time;
  }

  hourClick(hr1, hr2) {
    this.time.time_shower_hr1 = hr1;
    this.time.time_shower_hr2 = hr2;
    this.showHour = false;
    this.showMinute = true;
  }

  minuteClick(min1, min2) {
    this.time.time_shower_minute1 = min1;
    this.time.time_shower_minute2 = min2;
    let form_to = this.time.time_shower_hr1 + this.time.time_shower_hr2 + ':' + this.time.time_shower_minute1 + this.time.time_shower_minute2 + ' ' + this.time.shower_am;
    if(this.start_time !== form_to)
      this.proceed = true;
    else
      this.proceed = false;
  }

  toggleTimeSleepAm(type) {
    this.time.shower_am = type;
  }

  reset() {
    this.time.time_shower_hr1 = '';
    this.time.time_shower_hr2 = '';
    this.time.time_shower_minute1 = '';
    this.time.time_shower_minute2 = '';
    this.showHour = true;
    this.showMinute = false;
    this.proceed = false;
    this.falseTiming = false;
    this.oddTiming = false;
  }

  yup() {
    this.proceed = true;
    this.proceedAction();
  }

  proceedNext() {
    if(this.proceed){
      this.checkOddTiming();
      if (!this.oddTiming && !this.falseTiming) {
        this.proceedAction();
      }
    }
  }

  proceedAction(){
    let json = {
      current_page: '/page-shower',
      next_page: '/',
      time: this.time
    };
    let time = this.time.time_shower_hr1.toString() + this.time.time_shower_hr2.toString() + ':' + this.time.time_shower_minute1.toString() + this.time.time_shower_minute2.toString() + ' ' + this.time.shower_am;
    let param_display = 'Cool! What did you do after that at ' + time + '?';
    let complete_date_time_str = this.kid_account.daily_log.date_log + ' ' + this.time.time_shower_hr1 + this.time.time_shower_hr2 + ':' + this.time.time_shower_minute1 + this.time.time_shower_minute2 + ' ' + this.time.shower_am;
    let complete_date_time = moment(complete_date_time_str, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DD HH:mm:ss');
    let params = {
      'kid_id': this.kid_account.id,
      'Activities[daily_log_id]': this.kid_account.daily_log.id,
      'Activities[user_kid_id]': this.kid_account.id,
      'Activities[complete]': 1,
      'Activities[complete_date_time]': complete_date_time,
      'Activities[param_display]': param_display,
      'Activities[json]': JSON.stringify(json)
    };

    if (this.updateActivity) {
      let next_page = '/page-activity-summary';
      params['id'] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([next_page], { queryParams: { navigate: 'page-activity-diary' } });
      });
    } else {

      json.next_page = '/page-activity-diary';

      this._apiService.updateActivity(params, () => {
        this.router.navigate([json.next_page], { queryParams: { param: param_display, time: time } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      // if (this.daily_log.selected_day === 'Today') {
      //   this._commonService.audio = new Audio(this._commonService.audioList.audio_31);
      //   this._commonService.audio.play();
      // } else {
      //   this._commonService.audio = new Audio(this._commonService.audioList.audio_32);
      //   this._commonService.audio.play();
      // }

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

  checkOddTiming() {
    let startTimeString = this.kid_account.start_date_time;
    let endTimeString = this.daily_log.date_log + ' ' + this.time.time_shower_hr1 + this.time.time_shower_hr2 + ':' + this.time.time_shower_minute1 + this.time.time_shower_minute2 + ' ' + this.time.shower_am;
    let startTime = moment(startTimeString, 'YYYY-MM-DD HH:mm:ss');
    let end = moment(endTimeString, 'YYYY-MM-DD hh:mm A');
    var duration = moment.duration(end.diff(startTime));
    var hours = duration.asHours();
    console.log(startTime);
    console.log(end);
    console.log(hours);
    if (hours > 3) {
      this.oddTiming = true;
    } else {
      if (hours < 0) {
        this.falseTiming = true;
      }
    }
  }

}
