import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageShowerComponent } from './page-shower.component';

describe('PageShowerComponent', () => {
  let component: PageShowerComponent;
  let fixture: ComponentFixture<PageShowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageShowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageShowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
