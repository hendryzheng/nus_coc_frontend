import { Component, HostBinding, ViewEncapsulation, NgZone, Renderer, ElementRef, ViewChild, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Compiler } from '@angular/core';
import {
  Router,
  // import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { HttpService } from './services/http.service';
import { CommonService } from './services/common.service';
import { fadeAnimation } from 'app/fade.animation';


declare var navigator: any;

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation]

})
export class AppComponent implements OnInit, AfterViewChecked {
  title = 'app';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  @ViewChild('spinnerElement') spinnerElement: ElementRef;

  constructor(private router: Router,
    private ngZone: NgZone,
    private renderer: Renderer,
    private _httpService: HttpService,
    private _commonService: CommonService,
    private _compiler: Compiler) {

    router.events.subscribe((event: RouterEvent) => {
      this._navigationInterceptor(event);
    });
  }

  ngOnInit() {
    // console.log(JSON.stringify(navigator));
    this._compiler.clearCache();
    // this._httpService.fetchIp();
    // document.addEventListener('contextmenu', event => event.preventDefault());

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    // this._commonService.showLoading = true;
  }
  onActivate(e, outlet) {
    outlet.scrollTop = 0;
  }
  ngAfterViewInit() {
    // this._commonService.showLoading = false;
    window.scrollTo(0, 0);
  }
  ngAfterViewChecked() {

  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  // Shows and hides the loading spinner during RouterEvent changes
  private _navigationInterceptor(event: RouterEvent): void {

    if (event instanceof NavigationStart) {

      // We wanna run this function outside of Angular's zone to
      // bypass change detection
      this.ngZone.runOutsideAngular(() => {

        // For simplicity we are going to turn opacity on / off
        // you could add/remove a class for more advanced styling
        // and enter/leave animation of the spinner
        this._commonService.showLoading = true;
      });
    }
    if (event instanceof NavigationEnd) {
      this._hideSpinner();
    }

    // Set loading state to false in both of the below events to
    // hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this._hideSpinner
    }
    if (event instanceof NavigationError) {
      this._hideSpinner();
    }
  }

  private _hideSpinner(): void {

    // We wanna run this function outside of Angular's zone to
    // bypass change detection, 
    this.ngZone.runOutsideAngular(() => {

      // For simplicity we are going to turn opacity on / off
      // you could add/remove a class for more advanced styling
      // and enter/leave animation of the spinner
      this._commonService.showLoading = false;
    });
  }

  showLoading() {
    return this._commonService.showLoading;
  }
}
