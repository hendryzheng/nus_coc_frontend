import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDualTravellingComponent } from './page-activity-diary-dual-travelling.component';

describe('PageActivityDiaryDualTravellingComponent', () => {
  let component: PageActivityDiaryDualTravellingComponent;
  let fixture: ComponentFixture<PageActivityDiaryDualTravellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDualTravellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDualTravellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
