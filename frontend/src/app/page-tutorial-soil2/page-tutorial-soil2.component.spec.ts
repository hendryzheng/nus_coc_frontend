import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialSoil2Component } from './page-tutorial-soil2.component';

describe('PageTutorialSoil2Component', () => {
  let component: PageTutorialSoil2Component;
  let fixture: ComponentFixture<PageTutorialSoil2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialSoil2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialSoil2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
