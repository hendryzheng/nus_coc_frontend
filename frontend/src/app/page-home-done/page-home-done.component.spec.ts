import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeDoneComponent } from './page-home-done.component';

describe('PageHomeDoneComponent', () => {
  let component: PageHomeDoneComponent;
  let fixture: ComponentFixture<PageHomeDoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeDoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeDoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
