import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-home-done',
  templateUrl: './page-home-done.component.html',
  styleUrls: ['./page-home-done.component.scss']
})
export class PageHomeDoneComponent implements OnInit {

  kid_account: any = null;
  day_status: number = 1;
  character_type: any = '';
  image_type: any = 'png';
  droppedData: boolean = false;
  photo: any = '';
  plant= {
    'tomato' : 'soil-done.png',
    'orange' : 'soil-done-orange.png',
    'lemon' : 'soil-done-lemon.png',
    'apple' : 'soil-done-apple.png'
  };
  
  selectedPlant: any = 'soil-done.png';
  selectedPlantName: any = 'tomato';

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    this._commonService.soundActive = true;
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      this.character_type = this.kid_account.character_type;
      this.selectedPlant = this.plant[this.kid_account.plant];
    });

  }

  signOut() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  dragEnd(event) {
    this.droppedData = true;
    console.log('Element was dragged', event);
  }

  drop() {
    this.droppedData = true;
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_48);
      this._commonService.audio.play();
      this._commonService.audio.onended = () => {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_50);
        this._commonService.audio.play();
        this._commonService.audio.onended = () => {
          this._commonService.audio = new Audio(this._commonService.audioList.audio_49);
          this._commonService.audio.play();
          this._commonService.audio.onended = () => {
            this.image_type = 'png';
          };
        };
      }

    }
  }

  goToComplete(){
    this.router.navigate(['/page-home-complete']);
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }
}
