import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDurationTravellingComponent } from './page-activity-diary-duration-travelling.component';

describe('PageActivityDiaryDurationTravellingComponent', () => {
  let component: PageActivityDiaryDurationTravellingComponent;
  let fixture: ComponentFixture<PageActivityDiaryDurationTravellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDurationTravellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDurationTravellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
