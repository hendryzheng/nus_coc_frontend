import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialIntroComponent } from './page-tutorial-intro.component';

describe('PageTutorialIntroComponent', () => {
  let component: PageTutorialIntroComponent;
  let fixture: ComponentFixture<PageTutorialIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
