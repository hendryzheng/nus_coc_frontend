import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryActiveOptionsComponent } from './page-activity-diary-active-options.component';

describe('PageActivityDiaryActiveOptionsComponent', () => {
  let component: PageActivityDiaryActiveOptionsComponent;
  let fixture: ComponentFixture<PageActivityDiaryActiveOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryActiveOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryActiveOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
