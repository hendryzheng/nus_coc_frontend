import { Router, ActivatedRoute, Params } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { CommonService } from "../services/common.service";
import { HttpService } from "../services/http.service";
import { CONFIGURATION } from "../services/config.service";
import { SubmissionApiService } from "../services/submission-api.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-page-activity-diary-active-options",
  templateUrl: "./page-activity-diary-active-options.component.html",
  styleUrls: ["./page-activity-diary-active-options.component.scss"]
})
export class PageActivityDiaryActiveOptionsComponent implements OnInit {
  kid_account: any = null;
  character_type: any = "";

  daily_log: any = [];

  proceed: boolean = false;
  selected_activity: any = "-";
  param: any = "";

  image_type: any = "png";
  showMinute: boolean = false;
  showHour: boolean = true;

  updateActivity: boolean = false;
  activity: any = [];
  json: any = [];
  photo: any = "";
  showSuggestion: boolean = false;
  suggestion: any = [];

  other_activities: any = "";

  constructor(
    private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private location: Location,
    private router: Router
  ) {}

  ngOnInit() {
    let photo = localStorage.getItem("bg_photo");
    if (photo) {
      this.photo = photo;
    }

    this.activatedRoute.queryParams.subscribe(params => {
      console.log(params); // {order: "popular"}
      this.param = params.param;
      if (typeof params.id !== "undefined") {
        let param = {
          id: params.id
        };
        this._apiService.getActivity(param, () => {
          this.activity = this._apiService.activity;
          this.initData();
        });
      }
      if (typeof params.update !== "undefined") {
        if (params.update == "true") {
          this.updateActivity = true;
          this.proceed = true;
        }
      }
    });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem("account");
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem("daily_log");
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);

    this.json = json;
    let selected_activity = json.selected_option;
    this.selected_activity = selected_activity;
    if (typeof this.json.other_activities !== "undefined") {
      if (this.json.other_activities !== "") {
        this.other_activities = this.json.other_activities;
        this.selected_activity = this.other_activities;
        this.proceed = true;
      }
    }
  }

  selectActivity(type) {
    console.log(type);
    if (this.selected_activity === type) {
      this.selected_activity = "-";
    } else {
      this.selected_activity = type;
      this.proceed = true;
    }
    this.other_activities = "";
  }

  goBack() {
    let params = {
      id: this.kid_account.last_incomplete_activity.id
    };
    this._apiService.deleteActivity(params, () => {
      this.location.back();
      console.log("goBack()...");
    });
  }

  eventHandler(ev) {
    if (this.other_activities !== "" && this.other_activities.length >= 3) {
      this.selected_activity = this.other_activities;
      this.proceed = true;
    } else {
      this.proceed = false;
    }

    console.log(ev);
  }

  proceedNext() {
    // let json = {
    //   current_page: '/page-activity-diary-active-options',
    //   next_page: '/',
    //   selected_option: this.selected_activity
    // };
    if (this.proceed) {
      if (this.updateActivity) {
        this.json.selected_option = this.selected_activity;
        this.json.other_activities = this.other_activities;
      } else {
        this.json = {
          current_page: "/page-activity-diary-active-options",
          next_page: "/",
          selected_option: this.selected_activity,
          other_activities: this.other_activities
        };
      }
      if (this.other_activities !== "") {
        this.json.other_activities = this.other_activities;
        let temp = {
          key: this.other_activities,
          value: this.other_activities
        };
        let suggest_list = localStorage.getItem("suggestion_list");
      }

      let params = {
        kid_id: this.kid_account.id,
        "Activities[daily_log_id]": this.kid_account.daily_log.id,
        "Activities[user_kid_id]": this.kid_account.id,
        "Activities[selected_activity_display]": this.selected_activity,
        "Activities[complete]": 0,
        "Activities[json]": JSON.stringify(this.json)
      };

      if (this.updateActivity) {
        this.json.next_page = "/page-activity-diary-active-intensity-options";
        params["id"] = this.activity.id;
        this._apiService.updateSpecificActivity(params, () => {
          this.router.navigate([this.json.next_page], {
            queryParams: { param: "", id: this.activity.id, update: "true" }
          });
        });
      } else {
        this.json.next_page = "/page-activity-diary-active-intensity-options";
        this._apiService.updateActivity(params, () => {
          this.router.navigate([this.json.next_page], {
            queryParams: { param: "" }
          });
        });
      }
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== "undefined") {
        this._commonService.audio.pause();
      }
      this.image_type = "gif";
      if (this.kid_account.daily_log.day_status === "1") {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_33
        );
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === "2") {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_34
        );
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(
          this._commonService.audioList.audio_35
        );
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = "png";
      };
    }

    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = "png";
    } else {
      this.playVoiceOver();
    }
  }
}
