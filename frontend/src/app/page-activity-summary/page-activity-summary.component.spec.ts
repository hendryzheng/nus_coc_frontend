import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivitySummaryComponent } from './page-activity-summary.component';

describe('PageActivitySummaryComponent', () => {
  let component: PageActivitySummaryComponent;
  let fixture: ComponentFixture<PageActivitySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivitySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivitySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
