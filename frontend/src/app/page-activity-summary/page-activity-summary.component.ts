import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-activity-summary',
  templateUrl: './page-activity-summary.component.html',
  styleUrls: ['./page-activity-summary.component.scss']
})
export class PageActivitySummaryComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];
  odd_timing: boolean = false;

  proceed: boolean = false;

  selected_activity: any = '-';
  other_activities: any = '';
  image_type: any = 'png';
  param: any = '';
  time: any = '';
  start_date_time: any = '';
  last_activity: any = '';
  png: any = '';
  list_activity: any = [];
  navigate_to: any = '';

  sleptTime: any = '';
  isBedTimeNotLogged: boolean = false;
  bedTimeNotPassed: boolean = false;
  last_completed_activity: any = [];
  passed: boolean = false;

  sleeping_time: any = '';
  wakeup_time: any = '';

  photo: any = '';


  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private location: Location,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    localStorage.removeItem('update_activity_json');

    this.route.queryParams
      .subscribe(params => {
        console.log(params); // {order: "popular"}
        if (typeof params.navigate !== 'undefined') {
          this.navigate_to = '/' + params.navigate;
        }
      });


    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getSummary(() => {
      this.list_activity = this._apiService.summary_list;
    });

    this._commonService.showLoading = true;
    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.sleeping_time = this.kid_account.daily_log.time_sleep_hr1+this.kid_account.daily_log.time_sleep_hr2+':'+this.kid_account.daily_log.time_sleep_minute1+this.kid_account.daily_log.time_sleep_minute2+' '+this.kid_account.daily_log.time_sleep_am;
      this.wakeup_time = this.kid_account.daily_log.time_wakeup_hr1+this.kid_account.daily_log.time_wakeup_hr2+':'+this.kid_account.daily_log.time_wakeup_minute1+this.kid_account.daily_log.time_wakeup_minute2+' '+this.kid_account.daily_log.time_wakeup_am;
      this.daily_log = this.kid_account.daily_log;

      this.character_type = this.kid_account.character_type;
      if (this.kid_account.last_completed_activity !== false) {
        this.last_completed_activity = this.kid_account.last_completed_activity;
        this.last_activity = this.kid_account.last_completed_activity.activity_type;

      }
      this._commonService.showLoading = false;
    });
  }

  goBack() {
    // window.history.back();
    if (this.navigate_to !== '') {
      this.router.navigate([this.navigate_to]);
    } else {
      this.location.back();
    }

    console.log('goBack()...');
  }

  navigateTo(item) {
    let json = JSON.parse(item.json);
    console.log(json);
    this.router.navigate([json.current_page], { queryParams: { id: item.id,update:'true' } });
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

  keepgoing() {
    this.router.navigate(['/page-activity-diary'])
  }

  cancel(){
    this.passed = false;
  }

  saveexit() {
    this.isBedTimeNotLogged = false;
    localStorage.clear();
    this.router.navigate(['/']);
  }

  proceedReward(){
    this.bedTimeNotPassed = false;
    this.passed = true;
  }

  proceedNext() {
    let params = {
      'id': this.kid_account.daily_log.id
    };
    this._apiService.completeDailyLog(params, () => {
      if(this.kid_account.day_status == '1'){
        this.router.navigate(['/page-reward-seed']);
      }else if(this.kid_account.day_status == '2'){
        this.router.navigate(['/page-rewards-watering-can']);
      }else if(this.kid_account.day_status == '3'){
        this.router.navigate(['/page-rewards-fertilizer']);
      }else{
        this.router.navigate(['/page-rewards-fork']);
      }
    });
  }

  editSleep() {
    let activity = this.last_completed_activity;
    let json = JSON.parse(activity.json);
    this.router.navigate([json.current_page], { queryParams: { id: activity.id } });
  }

  checkProceed() {
    if (this.last_activity !== 'Nap/Sleep') {
      this.isBedTimeNotLogged = true;
    } else {
      let activity = this.last_completed_activity;
      let json = JSON.parse(activity.json);
      let time = json.time;
      this.sleptTime = time.time_sleep_hr1 + time.time_sleep_hr2 + ':' + time.time_sleep_minute1 + time.time_sleep_minute2 + ' ' + time.time_sleep_am;
      console.log(time);
      if (time.time_sleep_am === 'AM') {
        this.bedTimeNotPassed = true;
      } else {
        let hr = time.time_sleep_hr1 + time.time_sleep_hr2;
        if (parseInt(hr) < 7 || parseInt(hr) > 12) {
          this.bedTimeNotPassed = true;
        } else {
          this.passed = true;
          // this.proceedNext();
        }
      }
    }
  }
}
