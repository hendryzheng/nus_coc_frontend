import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGrowthDay3Component } from './page-growth-day-3.component';

describe('PageGrowthDay3Component', () => {
  let component: PageGrowthDay3Component;
  let fixture: ComponentFixture<PageGrowthDay3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGrowthDay3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGrowthDay3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
