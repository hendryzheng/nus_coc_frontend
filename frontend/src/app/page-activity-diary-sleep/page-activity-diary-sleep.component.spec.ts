import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiarySleepComponent } from './page-activity-diary-sleep.component';

describe('PageActivityDiarySleepComponent', () => {
  let component: PageActivityDiarySleepComponent;
  let fixture: ComponentFixture<PageActivityDiarySleepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiarySleepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiarySleepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
