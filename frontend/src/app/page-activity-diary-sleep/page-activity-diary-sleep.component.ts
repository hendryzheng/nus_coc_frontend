import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import * as moment from 'moment';
@Component({
  selector: 'app-page-activity-diary-sleep',
  templateUrl: './page-activity-diary-sleep.component.html',
  styleUrls: ['./page-activity-diary-sleep.component.scss']
})
export class PageActivityDiarySleepComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];
  odd_timing: boolean = false;

  proceed: boolean = false;
  image_type: any = 'png';
  bedTime: boolean = false;
  endMyDay: boolean = false;

  time = {
    time_sleep_hr1: '',
    time_sleep_hr2: '',
    time_sleep_minute1: '',
    time_sleep_minute2: '',
    time_sleep_am: 'AM'
  };
  complete_date_time: any = '';

  showMinute: boolean = false;
  showHour: boolean = true;
  start_time: any = '';
  oddTiming: boolean = false;
  falseTiming: boolean = false;
  textDisplay: any = 'What time did you end?';
  textNap: any = 'What time did you wake up from your nap?';
  textWake: any = 'What time did you wake up in the morning?';

  updateActivity: boolean = false;
  activity: any = [];

  photo: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _apiService: SubmissionApiService,
    private _httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if (photo) {
      this.photo = photo;
    }
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.id !== 'undefined') {
          this.updateActivity = true;
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.initData();
          });
        }
      });
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.daily_log = this.kid_account.daily_log;
      this.character_type = this.kid_account.character_type;
      this.start_time = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('hh:mm A');
      let last_end_time = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('hh');
      let last_end_time_minute = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('mm');
      let last_end_ampm = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('A');
      if(parseInt(last_end_time) == 7 && parseInt(last_end_time_minute) > 0 && last_end_ampm == 'PM'){
        this.bedTime = true;
      }
      if(parseInt(last_end_time) >= 7 && last_end_ampm == 'PM'){
        this.bedTime = true;
      }
      this.time.time_sleep_am = moment(this.kid_account.start_date_time, 'YYYY-MM-DD HH:mm:ss').format('A');
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);
    this.time = json.time;
  }

  hourClick(hr1, hr2) {
    this.time.time_sleep_hr1 = hr1;
    this.time.time_sleep_hr2 = hr2;
    this.showHour = false;
    this.showMinute = true;
  }

  minuteClick(min1, min2) {
    this.time.time_sleep_minute1 = min1;
    this.time.time_sleep_minute2 = min2;
    let form_to = this.time.time_sleep_hr1 + this.time.time_sleep_hr2 + ':' + this.time.time_sleep_minute1 + this.time.time_sleep_minute2 + ' ' + this.time.time_sleep_am;
    if(this.start_time !== form_to)
      this.proceed = true;
    else
      this.proceed = false;

  }

  toggleTimeSleepAm(type) {
    this.time.time_sleep_am = type;
  }

  reset() {
    this.time.time_sleep_hr1 = '';
    this.time.time_sleep_hr2 = '';
    this.time.time_sleep_minute1 = '';
    this.time.time_sleep_minute2 = '';
    this.showHour = true;
    this.showMinute = false;
    this.proceed = false;
    this.falseTiming = false;
    this.oddTiming = false;
  }

  nope(){
    this.bedTime = false;
    this.textDisplay = this.textNap;
    this.endMyDay = false;
  }

  right(){
    this.bedTime = false;
    this.textDisplay = this.textWake;
    this.endMyDay = true;
    this.time.time_sleep_am = 'AM';
  }

  yup() {
    this.proceed = true;
    this.proceedAction();
  }

  proceedNext() {
    this.checkOddTiming();
    if (!this.oddTiming) {
      if (this.proceed) {
        this.proceedAction();
      }
    }
  }

  proceedAction() {
    let json = {
      current_page: '/page-activity-diary-sleep',
      next_page: '/',
      time: this.time
    };
    let time_display = this.time.time_sleep_hr1 + this.time.time_sleep_hr2 + ':' + this.time.time_sleep_minute1 + this.time.time_sleep_minute2 + ' ' + this.time.time_sleep_am;

    let param_display = 'Cool! What did you do after that at ' + time_display + '?';
    this.complete_date_time = this.kid_account.daily_log.date_log + ' ' + this.time.time_sleep_hr1 + this.time.time_sleep_hr2 + ':' + this.time.time_sleep_minute1 + this.time.time_sleep_minute2 + ' ' + this.time.time_sleep_am;
    let complete_date_time = moment(this.complete_date_time, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DD HH:mm:ss');
    let params = {
      'kid_id': this.kid_account.id,
      'Activities[daily_log_id]': this.kid_account.daily_log.id,
      'Activities[user_kid_id]': this.kid_account.id,
      'Activities[complete]': 1,
      'Activities[complete_date_time]': complete_date_time,
      'Activities[param_display]': param_display,
      'Activities[json]': JSON.stringify(json)
    };
    if (this.updateActivity) {
      let next_page = '/page-activity-summary';
      params['id'] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([next_page], { queryParams: { navigate: 'page-activity-diary' } });
      });
    } else {
      if(this.endMyDay){
        json.next_page = '/page-activity-summary';
      }else{
        json.next_page = '/page-activity-diary';
      }
      let time = this.time.time_sleep_hr1 + this.time.time_sleep_hr2 + ':' + this.time.time_sleep_minute1 + this.time.time_sleep_minute2 + ' ' + this.time.time_sleep_am;
      this._apiService.updateActivity(params, () => {
        this.router.navigate([json.next_page], { queryParams: { param: param_display, time: time } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
    } else {
      this.playVoiceOver();
    }
  }

  cancelOdd() {
    this.odd_timing = false;
    this.reset();
  }

  checkOddTiming() {
    if (!this.updateActivity) {
      let startTimeString = this.kid_account.start_date_time;
      let endTimeString = this.daily_log.date_log + ' ' + this.time.time_sleep_hr1 + this.time.time_sleep_hr2 + ':' + this.time.time_sleep_minute1 + this.time.time_sleep_minute2 + ' ' + this.time.time_sleep_am;
      let startTime = moment(startTimeString, 'YYYY-MM-DD HH:mm:ss');
      let end = moment(endTimeString, 'YYYY-MM-DD hh:mm A');
      var duration = moment.duration(end.diff(startTime));
      var hours = duration.asHours();
      console.log(startTime);
      console.log(end);
      console.log(hours);
      var format = 'hh:mm:ss A'
      // var time = moment() gives you current time. no format required.
      var time = moment(endTimeString, format),
        beforeTime = moment('5:00:00 PM', format),
        afterTime = moment('7:00:00 PM', format);

      if (time.isBetween(beforeTime, afterTime)) {
        console.log('odd timing');
        this.oddTiming = true;

      } else{
        if (hours > 3) {
          this.oddTiming = true;
        }
      }
      
    }
  }
}
