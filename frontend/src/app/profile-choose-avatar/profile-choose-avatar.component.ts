import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-profile-choose-avatar',
  templateUrl: './profile-choose-avatar.component.html',
  styleUrls: ['./profile-choose-avatar.component.scss']
})
export class ProfileChooseAvatarComponent implements OnInit {

  selected: any = '-';
  selectedImg: any = 'assets/img/no-image/no-image@2x.png';
  proceed: boolean = false;

  name: any = '';
  kid_account: any = null;

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {

    setTimeout(() => {
      this.playVoiceOver();
    }, 500);
    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      if (this.kid_account.character_type !== null && this.kid_account.character_type !== '') {
        this.selected = this.kid_account.character_type;
        this.chooseAvatar(this.selected, false);
      }
      if (this.kid_account.character_name !== null) {
        this.name = this.kid_account.character_name;
      }
    });

  }

  chooseAvatar(type, play) {
    if (play) {
      this._commonService.audio = new Audio(this._commonService.audioList.audio_5);
      this._commonService.audio.play();
    }
    this.selected = type;
    this.selectedImg = 'assets/img/avatar/' + this.selected + '/' + this.selected + '@2x.png';
  }



  isProceed() {
    return this.name !== '' && this.selected !== '-';
  }

  next() {
    if (this.isProceed()) {
      let params = {
        'kid_id': this.kid_account.id,
        'UserKid[character_type]': this.selected,
        'UserKid[character_name]': this.name
      };
      this._apiService.updateKid(params, () => {
        this.router.navigate(['/profile-character-live']);
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this._commonService.audio = new Audio(this._commonService.audioList.audio_1);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_2);
        this._commonService.audio.play();

        this._commonService.audio.onended = () => {
          this._commonService.audio = new Audio(this._commonService.audioList.audio_3);
          this._commonService.audio.play();
        };
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
    } else {
      this.playVoiceOver();
    }
  }



}
