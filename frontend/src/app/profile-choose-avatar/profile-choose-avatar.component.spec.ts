import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileChooseAvatarComponent } from './profile-choose-avatar.component';

describe('ProfileChooseAvatarComponent', () => {
  let component: ProfileChooseAvatarComponent;
  let fixture: ComponentFixture<ProfileChooseAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileChooseAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileChooseAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
