import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiarySittingOptionsComponent } from './page-activity-diary-sitting-options.component';

describe('PageActivityDiarySittingOptionsComponent', () => {
  let component: PageActivityDiarySittingOptionsComponent;
  let fixture: ComponentFixture<PageActivityDiarySittingOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiarySittingOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiarySittingOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
