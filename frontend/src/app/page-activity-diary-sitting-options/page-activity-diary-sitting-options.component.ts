import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-activity-diary-sitting-options',
  templateUrl: './page-activity-diary-sitting-options.component.html',
  styleUrls: ['./page-activity-diary-sitting-options.component.scss']
})
export class PageActivityDiarySittingOptionsComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  selected_activity: any = '-';
  param: any = '';

  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;

  updateActivity: boolean = false;
  activity: any = [];
  json: any = [];
  isDualBefore: boolean = false;

  dualActivity: any = '';

  photo: any = '';
  others: any = '';



  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private location: Location,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if (photo) {
      this.photo = photo;
    }

    this.activatedRoute.queryParams
      .subscribe(params => {
        console.log(params); // {order: "popular"}
        this.param = params.param;
        if (typeof params.id !== 'undefined') {
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            if(typeof params.update !== 'undefined'){
              if(params.update == 'false'){
                // this.updateActivity = false;
              }else{
                this.initData();
              }
            }
          });
        }
        if(typeof params.update !== 'undefined'){
          if(params.update == 'false'){
            this.updateActivity = false;
          }else{
            this.updateActivity = true;
          }
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      if (typeof this.kid_account.root_activity !== 'undefined') {
        this.dualActivity = this.kid_account.root_activity.activity_type;
      }
      if (typeof this.kid_account.root_activity !== 'undefined') {
        this.isDualBefore = true;
      }
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);
    console.log(json);
    this.json = json;
    this.selected_activity = json.selected_option;
    if (typeof json.others !== 'undefined') {
      if (json.others !== '') {
        this.others = json.others;
        this.selected_activity = this.others;
      }
    }
    this.proceed = true;
    console.log(this.activity.dual);
    if (typeof this.activity.dual !== 'undefined') {
      this.dualActivity = this.activity.dual.activity_type;
    }

  }

  selectActivity(type) {
    console.log(type);
    if (this.selected_activity === type) {
      this.selected_activity = '-';
    } else {
      this.selected_activity = type;
      this.proceed = true;
    }
    this.others = '';
  }

  onKey(event) {
    this.selected_activity = this.others;
    if (this.others.length >= 3) {
      this.proceed = true;
    }else{
      this.proceed = false;
    }
  }

  proceedNext() {
    if (this.proceed) {
      if (this.updateActivity) {
        this.json.selected_option = this.selected_activity;
        this.json.others = this.others;
      } else {
        this.json = {
          current_page: '/page-activity-diary-sitting-options',
          next_page: '/',
          selected_option: this.selected_activity,
          others: this.others
        };
      }

      let params = {
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[selected_activity_display]': this.selected_activity,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };

      if (this.updateActivity) {
        if (this.dualActivity == 'Travelling') {
          this.json.next_page = '/page-activity-summary';
          params['Activities[complete]'] = 1;
          params['id'] = this.activity.id;
          this._apiService.updateSpecificActivity(params, () => {
            this.router.navigate([this.json.next_page], { queryParams: { navigate: 'page-activity-diary' } });
          });
        } else {

          this.json.next_page = '/page-activity-diary-indoors-outdoors-sitting';
          params['id'] = this.activity.id;
          this._apiService.updateSpecificActivity(params, () => {
            this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id, update: this.updateActivity} });
          });
        }

      } else {
        if (this.dualActivity == 'Travelling') {
          this.json.next_page = '/page-activity-diary-duration-sitting';
        } else {
          if (this.isDualBefore) {
            // this.json.next_page = '/page-activity-diary-indoors-outdoors-sitting';
            this.json.next_page = '/page-activity-diary-indoors-outdoors-sitting';
          } else {
            this.json.next_page = '/page-activity-diary-dual-sitting';
          }
        }

        this._apiService.updateActivity(params, () => {
          this.router.navigate([this.json.next_page], { queryParams: { param: '' } });
        });
      }
    }


  }

  goBack() {
    // window.history.back();
    let params = {
      id: this.kid_account.last_incomplete_activity.id
    }
    this._apiService.deleteActivity(params, ()=>{
      this.location.back();
      console.log('goBack()...');

    });

  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }



}
