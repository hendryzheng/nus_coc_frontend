// app.routes.ts

import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileChooseAvatarComponent } from './profile-choose-avatar/profile-choose-avatar.component';
import { ProfileCharacterLiveComponent } from './profile-character-live/profile-character-live.component';
import { PageTutorialIntroComponent } from './page-tutorial-intro/page-tutorial-intro.component';
import { PageTutorialSoilComponent } from './page-tutorial-soil/page-tutorial-soil.component';
import { PageTutorialSoil2Component } from './page-tutorial-soil2/page-tutorial-soil2.component';
import { PageTutorialActivityDiaryComponent } from './page-tutorial-activity-diary/page-tutorial-activity-diary.component';
import { PageTutorialScreenRewardComponent } from './page-tutorial-screen-reward/page-tutorial-screen-reward.component';
import { PageTutorialScreenFinalComponent } from './page-tutorial-screen-final/page-tutorial-screen-final.component';
import { PageHomeDayComponent } from './page-home-day/page-home-day.component';
import { PageAboutYourselfComponent } from './page-about-yourself/page-about-yourself.component';
import { PageRecordDayComponent } from './page-record-day/page-record-day.component';
import { PageAboutYourselfSleepComponent } from './page-about-yourself-sleep/page-about-yourself-sleep.component';
import { PageAboutYourselfWakeupComponent } from './page-about-yourself-wakeup/page-about-yourself-wakeup.component';
import { PageActivityDiaryComponent } from './page-activity-diary/page-activity-diary.component';
import { PageShowerComponent } from './page-shower/page-shower.component';
import { PageActivityDiaryTravellingOptionsComponent } from './page-activity-diary-travelling-options/page-activity-diary-travelling-options.component';
import { PageActivityDiaryDualTravellingComponent } from './page-activity-diary-dual-travelling/page-activity-diary-dual-travelling.component';
import { PageActivityDiaryDurationTravellingComponent } from './page-activity-diary-duration-travelling/page-activity-diary-duration-travelling.component';
import { PageActivityDiarySittingOptionsComponent } from './page-activity-diary-sitting-options/page-activity-diary-sitting-options.component';
import { PageActivityDiaryDualSittingComponent } from './page-activity-diary-dual-sitting/page-activity-diary-dual-sitting.component';
import { PageActivityDiaryIndoorsOutdoorsSittingComponent } from './page-activity-diary-indoors-outdoors-sitting/page-activity-diary-indoors-outdoors-sitting.component';
import { PageActivityDiaryDurationSittingComponent } from './page-activity-diary-duration-sitting/page-activity-diary-duration-sitting.component';
import { PageActivityDiarySleepComponent } from './page-activity-diary-sleep/page-activity-diary-sleep.component';
import { PageActivityDiaryActiveOptionsComponent } from './page-activity-diary-active-options/page-activity-diary-active-options.component';
import { PageActivityDiaryActiveIntensityComponent } from './page-activity-diary-active-intensity/page-activity-diary-active-intensity.component';
import { PageActivityDiaryDualActiveComponent } from './page-activity-diary-dual-active/page-activity-diary-dual-active.component';
import { PageActivityDiaryDurationActiveComponent } from './page-activity-diary-duration-active/page-activity-diary-duration-active.component';
import { PageActivitySummaryComponent } from './page-activity-summary/page-activity-summary.component';
import { PageRewardSeedComponent } from './page-reward-seed/page-reward-seed.component';
import { PageGrowthDay1Component } from './page-growth-day-1/page-growth-day-1.component';
import { PageRewardsWateringCanComponent } from './page-rewards-watering-can/page-rewards-watering-can.component';
import { PageGrowthDay2Component } from './page-growth-day-2/page-growth-day-2.component';
import { PageRewardsFertilizerComponent } from './page-rewards-fertilizer/page-rewards-fertilizer.component';
import { PageGrowthDay3Component } from './page-growth-day-3/page-growth-day-3.component';
import { PageGrowthDay4Component } from './page-growth-day-4/page-growth-day-4.component';
import { PageRewardsForkComponent } from './page-rewards-fork/page-rewards-fork.component';
import { PageHomeDoneComponent } from './page-home-done/page-home-done.component';
import { PageHomeDay1DoneComponent } from './page-home-day-1-done/page-home-day-1-done.component';
import { PageHomeDay2DoneComponent } from './page-home-day-2-done/page-home-day-2-done.component';
import { PageHomeDay3DoneComponent } from './page-home-day-3-done/page-home-day-3-done.component';
import { PageFoodRecordMainComponent } from './page-food-record-main/page-food-record-main.component';
import { PageFoodRecordMainDrinksComponent } from './page-food-record-main-drinks/page-food-record-main-drinks.component';
import { PageFoodRecordMainFruitsComponent } from './page-food-record-main-fruits/page-food-record-main-fruits.component';
import { PageFoodRecordMainDesertsComponent } from './page-food-record-main-deserts/page-food-record-main-deserts.component';
import { PageFoodRecordMainPortionComponent } from './page-food-record-main-portion/page-food-record-main-portion.component';
import { PageFoodRecordSummaryComponent } from './page-food-record-summary/page-food-record-summary.component';
import { PageFoodRecordLocationComponent } from './page-food-record-location/page-food-record-location.component';
import { PageFoodRecordDurationComponent } from './page-food-record-duration/page-food-record-duration.component';
import { PageActivityDiaryIndoorsOutdoorsActiveComponent } from './page-activity-diary-indoors-outdoors-active/page-activity-diary-indoors-outdoors-active.component';
import { PageActivityDiaryDualEatingComponent } from './page-activity-diary-dual-eating/page-activity-diary-dual-eating.component';
import { LifestyleReportComponent } from './lifestyle-report/lifestyle-report.component';
import { PageHomeCompleteComponent } from './page-home-complete/page-home-complete.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'profile-choose-avatar', component: ProfileChooseAvatarComponent},
  { path: 'profile-character-live', component: ProfileCharacterLiveComponent},
  { path: 'page-tutorial-intro', component: PageTutorialIntroComponent},
  { path: 'page-tutorial-soil', component: PageTutorialSoilComponent},
  { path: 'page-tutorial-soil2', component: PageTutorialSoil2Component},
  { path: 'page-tutorial-activity-diary', component: PageTutorialActivityDiaryComponent},
  { path: 'page-tutorial-screen-reward', component: PageTutorialScreenRewardComponent},
  { path: 'page-tutorial-screen-final', component: PageTutorialScreenFinalComponent},
  { path: 'page-home-day', component: PageHomeDayComponent},
  { path: 'page-about-yourself', component: PageAboutYourselfComponent},
  { path: 'page-record-day', component: PageRecordDayComponent},
  { path: 'page-about-yourself-sleep', component: PageAboutYourselfSleepComponent},
  { path: 'page-about-yourself-wakeup', component: PageAboutYourselfWakeupComponent},
  { path: 'page-activity-diary', component: PageActivityDiaryComponent},
  { path: 'page-shower', component: PageShowerComponent},
  { path: 'page-activity-diary-travelling-options', component: PageActivityDiaryTravellingOptionsComponent},
  { path: 'page-activity-diary-dual-travelling', component: PageActivityDiaryDualTravellingComponent},
  { path: 'page-activity-diary-duration-travelling', component: PageActivityDiaryDurationTravellingComponent},
  { path: 'page-activity-diary-sitting-options', component: PageActivityDiarySittingOptionsComponent},
  { path: 'page-activity-diary-duration-sitting', component: PageActivityDiaryDurationSittingComponent},
  { path: 'page-activity-diary-dual-sitting', component: PageActivityDiaryDualSittingComponent},
  { path: 'page-activity-diary-indoors-outdoors-sitting', component: PageActivityDiaryIndoorsOutdoorsSittingComponent},
  { path: 'page-activity-diary-sleep', component: PageActivityDiarySleepComponent},
  { path: 'page-activity-diary-active-options', component: PageActivityDiaryActiveOptionsComponent},
  { path: 'page-activity-diary-active-intensity-options', component: PageActivityDiaryActiveIntensityComponent},
  { path: 'page-activity-dual-active', component: PageActivityDiaryDualActiveComponent},
  { path: 'page-activity-diary-duration-active', component: PageActivityDiaryDurationActiveComponent},
  { path: 'page-activity-summary', component: PageActivitySummaryComponent},
  { path: 'page-reward-seed', component: PageRewardSeedComponent},
  { path: 'page-growth-day-1', component: PageGrowthDay1Component},
  { path: 'page-rewards-watering-can', component: PageRewardsWateringCanComponent},
  { path: 'page-growth-day-2', component: PageGrowthDay2Component},
  { path: 'page-rewards-fertilizer', component: PageRewardsFertilizerComponent},
  { path: 'page-growth-day-3', component: PageGrowthDay3Component},
  { path: 'page-rewards-fork', component: PageRewardsForkComponent},
  { path: 'page-growth-day-4', component: PageGrowthDay4Component},
  { path: 'page-home-day-1-done', component: PageHomeDay1DoneComponent},
  { path: 'page-home-day-2-done', component: PageHomeDay2DoneComponent},
  { path: 'page-home-day-3-done', component: PageHomeDay3DoneComponent},
  { path: 'page-home-done', component: PageHomeDoneComponent},
  { path: 'page-food-record-main', component: PageFoodRecordMainComponent},
  { path: 'page-food-record-main-drinks', component: PageFoodRecordMainDrinksComponent},
  { path: 'page-food-record-main-fruits', component: PageFoodRecordMainFruitsComponent},
  { path: 'page-food-record-main-deserts', component: PageFoodRecordMainDesertsComponent},
  { path: 'page-food-record-main-portion', component: PageFoodRecordMainPortionComponent},
  { path: 'page-food-record-summary', component: PageFoodRecordSummaryComponent},
  { path: 'page-food-record-location', component: PageFoodRecordLocationComponent},
  { path: 'page-food-record-duration', component: PageFoodRecordDurationComponent},
  { path: 'page-activity-diary-indoors-outdoors-active', component: PageActivityDiaryIndoorsOutdoorsActiveComponent},
  { path: 'page-activity-diary-dual-eating', component: PageActivityDiaryDualEatingComponent},
  { path: 'page-home-complete', component: PageHomeCompleteComponent},
  { path: 'lifestyle-report', component: LifestyleReportComponent}








  // {path: '', redirectTo: 'news/1', pathMatch : 'full'},
  // {path: 'news/:page', component: StoriesComponent, data: {storiesType: 'news'}},
  // {path: 'newest/:page', component: StoriesComponent, data: {storiesType: 'newest'}},
  // {path: 'show/:page', component: StoriesComponent, data: {storiesType: 'show'}},
  // {path: 'ask/:page', component: StoriesComponent, data: {storiesType: 'ask'}},
  // {path: 'jobs/:page', component: StoriesComponent, data: {storiesType: 'jobs'}},
  // {path: 'item/:id', component: ItemCommentsComponent}
];

export const routing = RouterModule.forRoot(routes, { useHash: true });