import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryTravellingOptionsComponent } from './page-activity-diary-travelling-options.component';

describe('PageActivityDiaryTravellingOptionsComponent', () => {
  let component: PageActivityDiaryTravellingOptionsComponent;
  let fixture: ComponentFixture<PageActivityDiaryTravellingOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryTravellingOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryTravellingOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
