import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-activity-diary-travelling-options',
  templateUrl: './page-activity-diary-travelling-options.component.html',
  styleUrls: ['./page-activity-diary-travelling-options.component.scss']
})
export class PageActivityDiaryTravellingOptionsComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  selected_activity: any = '-';
  selected_option: any = '-';


  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;
  isDualBefore: boolean = false;

  updateActivity: boolean = false;
  activity: any = [];
  json: any = [];

  photo: any = '';
  others: any = '';


  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private location: Location,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.id !== 'undefined') {
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            if(typeof params.update !== 'undefined'){
              if(params.update == 'false'){
                // this.updateActivity = false;
              }else{
                this.initData();
              }
            }
          });
        }
        if(typeof params.update !== 'undefined'){
          if(params.update == 'false'){
            this.updateActivity = false;
          }else{
            this.updateActivity = true;
          }
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.daily_log = this.kid_account.daily_log;
      if (typeof this.kid_account.root_activity !== 'undefined') {
        this.isDualBefore = true;
      }
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);

    this.json = json;
    let selected_activity = json.selected_option;
    this.selected_option = selected_activity;
    if (typeof json.others !== 'undefined') {
      if (json.others !== '') {
        this.others = json.others;
        this.selected_option = this.others;
      }
    }
    // let split_selected_activity = selected_activity.split(',');
    // for (var i = 0; i < split_selected_activity.length; i++) {
    //   this.selected_option.push(split_selected_activity[i]);
    // }
    this.proceed = true;
  }

  isInArray(value) {
    return this.selected_option.indexOf(value) > -1;
  }

  selectActivity(type) {
    this.selected_option = type;
    this.proceed = true;
    this.others = '';
    // console.log(type);
    // let index = this.selected_option.indexOf(type);
    // console.log(index);
    // if (index > -1) {
    //   this.selected_option.splice(index);
    // } else {
    //   this.selected_option.push(type);
    // }
    // console.log(this.selected_option);
    // if (this.selected_option.length > 0) {
    //   this.proceed = true;
    // } else {
    //   this.proceed = false;
    // }
  }

  onKey(event) {
    this.selected_option = this.others;
    if (this.others.length >= 3) {
      this.proceed = true;
    }else{
      this.proceed = false;
    }
    if(this.others == ''){
      this.selected_option = '-';
    }
  }

  proceedNext() {
    if (this.proceed) {
      // var selectedOption = '';
      // for (var i = 0; i < this.selected_option.length; i++) {
      //   selectedOption += this.selected_option[i] + ',';
      // }
      // selectedOption = selectedOption.substring(0, selectedOption.length - 1);;
      this.json.others = this.others;
      if (this.updateActivity) {
        this.json.selected_option = this.selected_option;
      } else {
        this.json = {
          current_page: '/page-activity-diary-travelling-options',
          next_page: '/',
          selected_option: this.selected_option,
          others: this.others
        };
      }

      this.json.selected_option = this.selected_option;

      if (this.updateActivity) {
        let params = {
          'kid_id': this.kid_account.id,
          'Activities[selected_activity_display]': this.selected_option,
          'Activities[json]': JSON.stringify(this.json)
        };
        let next_page = '/page-activity-summary';
        params['id'] = this.activity.id;
        this._apiService.updateSpecificActivity(params, () => {
          this.router.navigate([next_page], { queryParams: { navigate: 'page-activity-diary' } });
        });
      } else {
        let params = {
          'kid_id': this.kid_account.id,
          'Activities[daily_log_id]': this.kid_account.daily_log.id,
          'Activities[user_kid_id]': this.kid_account.id,
          'Activities[selected_activity_display]': this.selected_option,
          'Activities[complete]': 0,
          'Activities[json]': JSON.stringify(this.json)
        };
        if (this.isDualBefore) {
          this.json.next_page = '/page-activity-diary-duration-travelling';
        } else {
          this.json.next_page = '/page-activity-diary-dual-travelling';
        }

        this._apiService.updateActivity(params, () => {
          this.router.navigate([this.json.next_page], { queryParams: { param: 'Were you doing other activities at the same time?' } });
        });
      }
      


    }
  }

  goBack() {
    let params = {
      id: this.kid_account.last_incomplete_activity.id
    }
    this._apiService.deleteActivity(params, ()=>{
      this.location.back();
      console.log('goBack()...');

    });
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }


}
