import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeCompleteComponent } from './page-home-complete.component';

describe('PageHomeCompleteComponent', () => {
  let component: PageHomeCompleteComponent;
  let fixture: ComponentFixture<PageHomeCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
