import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGrowthDay4Component } from './page-growth-day-4.component';

describe('PageGrowthDay4Component', () => {
  let component: PageGrowthDay4Component;
  let fixture: ComponentFixture<PageGrowthDay4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGrowthDay4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGrowthDay4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
