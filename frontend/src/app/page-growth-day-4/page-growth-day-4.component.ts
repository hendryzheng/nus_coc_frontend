import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-growth-day-4',
  templateUrl: './page-growth-day-4.component.html',
  styleUrls: ['./page-growth-day-4.component.scss']
})
export class PageGrowthDay4Component implements OnInit {

  kid_account: any = null;
  day_status: number = 1;
  character_type: any = '';
  image_type: any = 'png';
  droppedData: boolean = false;
  plant: any = [
    {
      name: 'tomato',
      image: 'soil-done.png'
    },
    {
      name: 'orange',
      image: 'soil-done-orange.png'
    },
    {
      name: 'lemon',
      image: 'soil-done-lemon.png'
    },
    {
      name: 'apple',
      image: 'soil-done-apple.png'
    }
  ];
  selectedPlant: any = 'soil-done.png';
  selectedPlantName: any = 'tomato';
  photo: any = '';

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this._commonService.soundActive = true;
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      this.character_type = this.kid_account.character_type;
    });

  }

  dragEnd(event) {
    
    let index = Math.floor(Math.random() * this.plant.length);
    this.selectedPlantName = this.plant[index].name;
    this.selectedPlant = this.plant[index].image;
    let params = {
      'kid_id': this.kid_account.id,
      'plant': this.selectedPlantName
    };
    this._apiService.updatePlant(params, () => {
      this.droppedData = true;
      console.log('Element was dragged', event);
      this.playVoiceOverEnd();
    });
    
  }


  drop() {
    this.droppedData = true;
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_41);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
  }

  playVoiceOverEnd() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      this._commonService.audio = new Audio(this._commonService.audioList.audio_51);
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      }


    }
  }

  goToHome() {
    if (this.droppedData) {
      this.router.navigate(['/page-home-done'])
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }
}
