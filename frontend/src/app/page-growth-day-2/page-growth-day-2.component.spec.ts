import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGrowthDay2Component } from './page-growth-day-2.component';

describe('PageGrowthDay2Component', () => {
  let component: PageGrowthDay2Component;
  let fixture: ComponentFixture<PageGrowthDay2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGrowthDay2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGrowthDay2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
