import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryIndoorsOutdoorsSittingComponent } from './page-activity-diary-indoors-outdoors-sitting.component';

describe('PageActivityDiaryIndoorsOutdoorsSittingComponent', () => {
  let component: PageActivityDiaryIndoorsOutdoorsSittingComponent;
  let fixture: ComponentFixture<PageActivityDiaryIndoorsOutdoorsSittingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryIndoorsOutdoorsSittingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryIndoorsOutdoorsSittingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
