import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainDrinksComponent } from './page-food-record-main-drinks.component';

describe('PageFoodRecordMainDrinksComponent', () => {
  let component: PageFoodRecordMainDrinksComponent;
  let fixture: ComponentFixture<PageFoodRecordMainDrinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainDrinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainDrinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
