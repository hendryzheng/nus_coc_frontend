import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-food-record-main-drinks',
  templateUrl: './page-food-record-main-drinks.component.html',
  styleUrls: ['./page-food-record-main-drinks.component.scss']
})
export class PageFoodRecordMainDrinksComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';
  daily_log: any = [];
  proceed: boolean = false;
  nothing_selected: boolean = false;
  
  

  image_type: any = 'png';
  updateActivity: boolean = false;


  drinks: any = [];
  activity: any = [];
  json: any = [];

  photo: any = '';
  search: any = '';
  initial: any = [];

  selected_drinks_option: any = [];
  selected_option_drinks_others: any = [];
  others_selected: boolean = false;
  others: any = '';


  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if(typeof params.update !== 'undefined'){
          if(params.update == 'true')
            this.updateActivity = true;
        }
        if(typeof params.id !== 'undefined'){
          let param = {
            id: params.id
          };
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.json = JSON.parse(this.activity.json);
            console.log(this.json);
            if(typeof this.json.selected_option_drinks_others !== 'undefined'){
              this.selected_option_drinks_others = this.json.selected_option_drinks_others;
              if(this.selected_option_drinks_others.length > 0){
                this.proceed = true;
              }
            }else if (typeof this.json.selected_drinks_option !== 'undefined') {
              this.selected_drinks_option = this.json.selected_drinks_option;
              
              if(this.selected_drinks_option.length > 0){
                this.proceed = true;
              }else{
                this.nothing_selected = true;
                this.proceed = true;
              }
            }
          });
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }
    this._commonService.showLoading = true;
    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.daily_log = this.kid_account.daily_log;

      this._apiService.getDrinks(() => {
        this._commonService.showLoading = false;
        this.drinks = this._apiService.drinks;
        this.initial = this.drinks;
      });
    });
  }

  eventHandler(event) {
    console.log(event, event.keyCode, event.keyIdentifier);
    console.log(this.search);
    if (this.search !== '') {
      let params = {
        keyword: event.target.value,
        food_type: 2,
        cat_id: 0
      }
      this._apiService.searchFood(params, response => {
        this.drinks = response.data;
        // this._commonService.showLoading = false;
      });
    } else {
      this.drinks = this.initial;
    }
  }

  initData() {

  }

  nothing() {
    this.nothing_selected = true;
    this.selected_drinks_option = [];
    this.proceed = true;
  }

  isInArray(item) {
    var res = false;
    for (var i = 0; i < this.selected_drinks_option.length; i++) {
      if (this.selected_drinks_option[i].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  selectActivity(item) {
    console.log(item);
    this.nothing_selected = false;
    for (var i = 0; i < this.selected_drinks_option.length; i++) {
      if (this.selected_drinks_option[i].id == item.id) {
        this.selected_drinks_option.splice(i, 1);
        return;
      }
    }
    this.selected_drinks_option.push(item);
    this.proceed = true;
    console.log(this.selected_drinks_option);
  }

  goBack() {
    this.router.navigate(['/page-food-record-main'], { queryParams: { id: this.activity.id } });
  }

  proceedNext() {
    if (this.proceed) {
      var selectedOption = '';
      this.json = JSON.parse(this.activity.json);
      this.json.selected_drinks_option = this.selected_drinks_option;
      this.json.selected_option_drinks_others = this.selected_option_drinks_others;

      let params = {
        'id': this.activity.id,
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };
      this.json.next_page = '/page-food-record-main-fruits';

      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id,  update: this.updateActivity } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

  toggleOthersSelected(){
    this.others_selected = true;
  }

  saveOthers(){
    if(typeof this.selected_option_drinks_others == 'undefined'){
      this.selected_option_drinks_others = [];
    }
    let temp = {
      name: this.others,
      portion: 0
    };
    this.selected_option_drinks_others.push(temp);
    this.others_selected = false;
    this.others = '';
    this.proceed = true;
  }

  remove(key){
    this.selected_option_drinks_others.splice(key,1);
    if(this.selected_option_drinks_others.length > 0){
      this.proceed = true;
    }else{
      this.proceed = false;
    }
  }

  cancelOthers(){
    this.others_selected = false;
    this.others = '';
  }

}
