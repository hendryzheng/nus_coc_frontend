import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeDay1DoneComponent } from './page-home-day-1-done.component';

describe('PageHomeDay1DoneComponent', () => {
  let component: PageHomeDay1DoneComponent;
  let fixture: ComponentFixture<PageHomeDay1DoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeDay1DoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeDay1DoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
