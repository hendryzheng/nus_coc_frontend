import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDualEatingComponent } from './page-activity-diary-dual-eating.component';

describe('PageActivityDiaryDualEatingComponent', () => {
  let component: PageActivityDiaryDualEatingComponent;
  let fixture: ComponentFixture<PageActivityDiaryDualEatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDualEatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDualEatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
