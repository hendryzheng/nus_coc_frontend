import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainSummaryComponent } from './page-food-record-main-summary.component';

describe('PageFoodRecordMainSummaryComponent', () => {
  let component: PageFoodRecordMainSummaryComponent;
  let fixture: ComponentFixture<PageFoodRecordMainSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
