import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTutorialScreenFinalComponent } from './page-tutorial-screen-final.component';

describe('PageTutorialScreenFinalComponent', () => {
  let component: PageTutorialScreenFinalComponent;
  let fixture: ComponentFixture<PageTutorialScreenFinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutorialScreenFinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutorialScreenFinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
