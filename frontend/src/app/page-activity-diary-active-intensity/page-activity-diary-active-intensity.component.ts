import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-activity-diary-active-intensity',
  templateUrl: './page-activity-diary-active-intensity.component.html',
  styleUrls: ['./page-activity-diary-active-intensity.component.scss']
})
export class PageActivityDiaryActiveIntensityComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  param: any = '';


  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;
  intensity: any = '-';
  json: any = [];
  isDualBefore: boolean = false;

  updateActivity: boolean = false;
  activity: any = [];
  photo: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    this.activatedRoute.queryParams
      .subscribe(params => {
        console.log(params); // {order: "popular"}
        this.param = params.param;
        if (typeof params.id !== 'undefined') {
          // this.updateActivity = true;
          let param = {
            id: params.id
          }
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.initData();
          });
          if(typeof params.update !== 'undefined'){
            if(params.update == 'true'){
              this.updateActivity = true;
              this.proceed = true;
            }
          }
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      if (this.kid_account.last_incomplete_activity.json !== '') {
        this.json = JSON.parse(this.kid_account.last_incomplete_activity.json);
        this.activity = this.kid_account.last_incomplete_activity;
      }
      if (typeof this.kid_account.root_activity !== 'undefined') {
        this.isDualBefore = true;
      }
    });
  }

  initData() {
    let json = JSON.parse(this.activity.json);
    this.json = json;
    this.intensity = this.json.intensity;
    if(this.intensity !== ''){
      this.proceed = true;
    }

  }

  selectIntensity(type) {
    console.log(type);
    if (this.intensity === type) {
      this.intensity = '-';
    } else {
      this.intensity = type;
    }

    if (this.intensity !== '-') {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
  }

  proceedNext() {
    if(this.proceed){
      this.json.intensity = this.intensity;
    let params = {
      'kid_id': this.kid_account.id,
      'Activities[daily_log_id]': this.kid_account.daily_log.id,
      'Activities[user_kid_id]': this.kid_account.id,
      'Activities[complete]': 0,
      'Activities[json]': JSON.stringify(this.json)
    };

    if (this.updateActivity) {
      this.json.next_page = '/page-activity-diary-indoors-outdoors-active';
      params['id'] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([this.json.next_page], { queryParams: { param: '', id: this.activity.id,update:'true' } });
      });
    } else {
      // if (this.isDualBefore) {
      //   this.json.next_page = '/page-activity-diary-duration-active';
      // } else {
      //   this.json.next_page = '/page-activity-dual-active';
      // }
      this.json.next_page = '/page-activity-diary-indoors-outdoors-active';

      this._apiService.updateActivity(params, () => {
        this.router.navigate([this.json.next_page], { queryParams: { param: '' } });
      });
    }
    }
    
  }

  goBack(){
    this.router.navigate(['/page-activity-diary-active-options'], { queryParams: { id: this.activity.id } })
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }



    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
