import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryActiveIntensityComponent } from './page-activity-diary-active-intensity.component';

describe('PageActivityDiaryActiveIntensityComponent', () => {
  let component: PageActivityDiaryActiveIntensityComponent;
  let fixture: ComponentFixture<PageActivityDiaryActiveIntensityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryActiveIntensityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryActiveIntensityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
