import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifestyleReportComponent } from './lifestyle-report.component';

describe('LifestyleReportComponent', () => {
  let component: LifestyleReportComponent;
  let fixture: ComponentFixture<LifestyleReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifestyleReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifestyleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
