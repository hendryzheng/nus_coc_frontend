import { Component, OnInit } from '@angular/core';
import { CommonService } from 'app/services/common.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'app/services/http.service';
import { SubmissionApiService } from 'app/services/submission-api.service';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import { CONFIGURATION } from 'app/services/config.service';

@Component({
  selector: 'app-lifestyle-report',
  templateUrl: './lifestyle-report.component.html',
  styleUrls: ['./lifestyle-report.component.scss']
})
export class LifestyleReportComponent implements OnInit {

  kid_account: any = null;
  day_status: number = 1;
  character_type: any = '';
  image_type: any = 'png';
  droppedData: boolean = false;
  outdoor_duration: number = 0;
  outdoor_duration_hour: any = 0;
  outdoor_duration_percentage: number = 0;
  outdoor_msg = '';
  pa_duration: number = 0;
  pa_duration_percentage: number = 0;
  pa_msg = '';
  sleeping_duration: number = 0;
  sleeping_duration_percentage: number = 0;
  sleeping_msg = '';
  screen_viewing: number = 0;
  screen_viewing_percentage: number = 0;
  screen_viewing_msg = '';
  selectedPlant: any = 'soil-done.png';
  selectedPlantName: any = 'tomato';
  photo: any = '';
  dataApi: any = [];
  pdf: any = '';
  showPdf: boolean = false;

  constructor(
    private _commonService: CommonService,
    private http: Http,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router
  ) { }

  ngOnInit() {
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      this.character_type = this.kid_account.character_type;
      this.getReport();
    });
  }


  pdfDownload() {
    var myOffscreenEl = document.getElementById('evaluation-report-pdf');
    var useWidth = document.getElementById('evaluation-report-pdf').style.width;
    var useHeight = document.getElementById('evaluation-report-pdf').style.height;
    this._commonService.showLoading = true;
    myOffscreenEl.style.maxHeight = '100%';
    html2canvas(document.getElementById('evaluation-report-pdf'), {
      width: useWidth,
      height: useHeight
    }).then(canvas => {

      // you should get a canvas that includes the entire element -- not just the visible portion
      myOffscreenEl.style.maxHeight = '400px';
      var img = canvas.toDataURL('image/png');
      this.pdf = img;
      let params = {
        file: img
      };
      this._httpService.postRequest(CONFIGURATION.URL.uploadReport,params).then(resp => {
        this._commonService.showLoading = false;
        console.log(resp);
        window.location.href=resp.data.imageUrl;

      }).catch(error => {
        this._commonService.showLoading = false;
        console.log(error);
        alert(JSON.stringify(error));
      });



    });
  }

  togglePdfShow() {
    this.showPdf = !this.showPdf;
  }

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ' hours and ' + rminutes + ' minutes';
  }

  getReport() {
    let params = {
      kid_id: this.kid_account.id
    };
    this._apiService.getReport(params, response => {
      console.log(response);
      this.dataApi = response.data;
      this.pa_duration = response.data.pa;
      if (this.pa_duration < 120) {
        this.pa_duration_percentage = this.pa_duration / 120 * 100;
      } else {
        this.pa_duration_percentage = 100;
      }
      this.pa_msg = response.data.pa_msg;
      this.screen_viewing = response.data.screen_viewing;
      if (this.screen_viewing < 4) {
        this.screen_viewing_percentage = this.screen_viewing / 4 * 100;
      } else {
        this.screen_viewing_percentage = 100;
      }
      this.screen_viewing_msg = response.data.screen_viewing_msg;
      this.outdoor_duration = response.data.outdoor;
      this.outdoor_duration_hour = this.outdoor_duration/60;
      if (this.outdoor_duration < 360) {
        this.outdoor_duration_percentage = this.outdoor_duration / 360 * 100;
      } else {
        this.outdoor_duration_percentage = 100;
      }
      this.outdoor_msg = response.data.outdoor_msg;
      this.sleeping_duration = response.data.sleeping;
      if (this.sleeping_duration < 18) {
        this.sleeping_duration_percentage = this.sleeping_duration / 18 * 100;
      } else {
        this.sleeping_duration_percentage = 100;
      }
      this.sleeping_msg = response.data.sleeping_msg;
    })
  }

}
