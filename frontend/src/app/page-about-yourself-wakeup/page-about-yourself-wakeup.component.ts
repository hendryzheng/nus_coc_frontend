import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-page-about-yourself-wakeup',
  templateUrl: './page-about-yourself-wakeup.component.html',
  styleUrls: ['./page-about-yourself-wakeup.component.scss']
})
export class PageAboutYourselfWakeupComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];
  odd_timing: boolean = false;

  proceed: boolean = false;

  time = {
    time_wakeup_hr1: '',
    time_wakeup_hr2: '',
    time_wakeup_minute1: '',
    time_wakeup_minute2: '',
    time_wakeup_am: 'AM'
  };

  showMinute: boolean = false;
  showHour: boolean = true;
  image_type: any = 'png';
  photo: any = '';
  slept_day: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }


    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.daily_log = this.kid_account.daily_log;
      this.character_type = this.kid_account.character_type;
      this.slept_day = moment(this.daily_log.date_log, 'YYYY-MM-DD').format('dddd');
    });
  }

  hourClick(hr1, hr2) {
    this.time.time_wakeup_hr1 = hr1;
    this.time.time_wakeup_hr2 = hr2;
    this.showHour = false;
    this.showMinute = true;
  }

  minuteClick(min1, min2) {
    this.time.time_wakeup_minute1 = min1;
    this.time.time_wakeup_minute2 = min2;
    this.proceed = true;
  }

  checkOddTiming() {
    let hr_string = this.time.time_wakeup_hr1.toString()+this.time.time_wakeup_hr2.toString();
    let hr = parseInt(hr_string);
    console.log(hr);
    if (this.time.time_wakeup_am == 'AM') {
      if(hr <= 4 || hr >= 11 )
      this.odd_timing = true;
    } else {
      this.odd_timing = true;
    }
  }

  toggleTimeSleepAm(type) {
    this.time.time_wakeup_am = type;
  }

  reset() {
    this.time.time_wakeup_hr1 = '';
    this.time.time_wakeup_hr2 = '';
    this.time.time_wakeup_minute1 = '';
    this.time.time_wakeup_minute2 = '';
    this.showHour = true;
    this.showMinute = false;
    this.proceed = false;
  }

  proceedNext() {
    // this.checkOddTiming();
    // if(!this.odd_timing){
      this.proceedAction();
    // }
    
  }

  proceedAction(){
    if(this.proceed){
      let params = {
        'kid_id': this.kid_account.id,
        'DailyLog[day_status]': this.kid_account.daily_log.day_status,
        'DailyLog[date_log]': this.kid_account.daily_log.date_log,
        'DailyLog[user_kid_id]': this.kid_account.id,
        'DailyLog[time_wakeup_am]': this.time.time_wakeup_am,
        'DailyLog[time_wakeup_hr1]': this.time.time_wakeup_hr1,
        'DailyLog[time_wakeup_hr2]': this.time.time_wakeup_hr2,
        'DailyLog[time_wakeup_minute1]': this.time.time_wakeup_minute1,
        'DailyLog[time_wakeup_minute2]': this.time.time_wakeup_minute2,
        'DailyLog[wakeup_done]': '1'
      };
  
      this._apiService.updateKid(params, () => {
        let time = this.time.time_wakeup_hr1.toString() + this.time.time_wakeup_hr2.toString() + ':' + this.time.time_wakeup_minute1.toString() + this.time.time_wakeup_minute2.toString() + ' ' + this.time.time_wakeup_am;
        let param_display = 'Alright, what did you do after waking up at ' + time + '?';
        this.router.navigate(['/page-activity-diary']);
      });
    }
    
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {

      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.daily_log.selected_day === 'Today') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_31);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_32);
        this._commonService.audio.play();
      }

      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }
    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

  yup(){
    this.proceed = true;
    this.proceedAction();
  }

  cancelOdd() {
    this.odd_timing = false;
    this.reset();
  }

}
