import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAboutYourselfWakeupComponent } from './page-about-yourself-wakeup.component';

describe('PageAboutYourselfWakeupComponent', () => {
  let component: PageAboutYourselfWakeupComponent;
  let fixture: ComponentFixture<PageAboutYourselfWakeupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAboutYourselfWakeupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAboutYourselfWakeupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
