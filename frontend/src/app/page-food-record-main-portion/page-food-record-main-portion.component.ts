import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-food-record-main-portion',
  templateUrl: './page-food-record-main-portion.component.html',
  styleUrls: ['./page-food-record-main-portion.component.scss']
})
export class PageFoodRecordMainPortionComponent implements OnInit {



  kid_account: any = null;
  character_type: any = '';
  daily_log: any = [];
  proceed: boolean = false;
  nothing_selected: boolean = false;
  selected_desert_option: any = [];
  image_type: any = 'png';


  deserts: any = [];
  activity: any = [];
  json: any = [];
  selected_item: any = [];
  updateActivity: boolean = false;

  photo: any = '';


  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if (photo) {
      this.photo = photo;
    }
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.update !== 'undefined') {
          if (params.update == 'true') {
            this.updateActivity = true;
            this.proceed = true;
          }
        }
        if (typeof params.edit !== 'undefined') {
          this.proceed = true;
        }
        let param = {
          id: params.id
        };
        this._apiService.getActivity(param, () => {
          this.activity = this._apiService.activity;
          this.json = JSON.parse(this.activity.json);

          console.log(this.json);
        });
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.daily_log = this.kid_account.daily_log;

      // this._apiService.getDeserts(() => {
      //   this.deserts = this._apiService.deserts;
      // });
    });
  }

  initData() {

  }

  nothing() {
    this.nothing_selected = true;
    this.selected_desert_option = [];
  }

  goBack() {
    this.router.navigate(['/page-food-record-main'], { queryParams: { id: this.kid_account.last_incomplete_activity.id } });
  }

  selectPortionMain(i, j) {
    for (var k = 0; k < this.json.selected_option[i].portions.length; k++) {
      this.json.selected_option[i].portions[k].selected = 0;
    }
    this.json.selected_option[i].portions[j].selected = 1;
    this.proceed = this.checkIfAllSelected();
  }

  selectPortionDrinks(i, j) {
    for (var k = 0; k < this.json.selected_drinks_option[i].portions.length; k++) {
      this.json.selected_drinks_option[i].portions[k].selected = 0;
    }
    this.json.selected_drinks_option[i].portions[j].selected = 1;
    this.proceed = this.checkIfAllSelected();
  }

  selectPortionFruits(i, j) {
    for (var k = 0; k < this.json.selected_fruits_option[i].portions.length; k++) {
      this.json.selected_fruits_option[i].portions[k].selected = 0;
    }
    this.json.selected_fruits_option[i].portions[j].selected = 1;
    this.proceed = this.checkIfAllSelected();
  }

  selectPortionDeserts(i, j) {
    for (var k = 0; k < this.json.selected_desert_option[i].portions.length; k++) {
      this.json.selected_desert_option[i].portions[k].selected = 0;
    }
    this.json.selected_desert_option[i].portions[j].selected = 1;
    this.proceed = this.checkIfAllSelected();
  }

  isInArray(item) {
    var res = false;
    for (var i = 0; i < this.selected_desert_option.length; i++) {
      if (this.selected_desert_option[i].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  checkIfAllSelected() {
    var total_count = 0;
    var total_passed = 0;
    for (var i = 0; i < this.json.selected_option.length; i++) {
      var total_portion = 0;
      for (var k = 0; k < this.json.selected_option[i].portions.length; k++) {
        total_portion++;
        if (this.json.selected_option[i].portions[k].selected == 1) {
          total_passed++;
          break;
        }
      }
      if (total_portion > 0) {
        total_count++;
      }

    }
    for (var i = 0; i < this.json.selected_drinks_option.length; i++) {
      var total_portion = 0;
      for (var k = 0; k < this.json.selected_drinks_option[i].portions.length; k++) {
        total_portion++;
        if (this.json.selected_drinks_option[i].portions[k].selected == 1) {
          total_passed++;
          break;
        }
      }
      if (total_portion > 0) {
        total_count++;
      }

    }
    for (var i = 0; i < this.json.selected_fruits_option.length; i++) {
      var total_portion = 0;
      for (var k = 0; k < this.json.selected_fruits_option[i].portions.length; k++) {
        total_portion++;
        if (this.json.selected_fruits_option[i].portions[k].selected == 1) {
          total_passed++;
          break;
        }
      }
      if (total_portion > 0) {
        total_count++;
      }
    }
    for (var i = 0; i < this.json.selected_desert_option.length; i++) {
      var total_portion = 0;
      for (var k = 0; k < this.json.selected_desert_option[i].portions.length; k++) {
        total_portion++;
        if (this.json.selected_desert_option[i].portions[k].selected == 1) {
          total_passed++;
          break;
        }
      }
      if (total_portion > 0) {
        total_count++;
      }
    }
    for (var i = 0; i < this.json.selected_option_others.length; i++) {
      var total_portion = 0;
      if (this.json.selected_option_others[i].portion > 0) {
        total_passed++;
        total_count++;
        break;
      }
    }
    for (var i = 0; i < this.json.selected_option_drinks_others.length; i++) {
      var total_portion = 0;
      if (this.json.selected_option_drinks_others[i].portion > 0) {
        total_passed++;
        total_count++;
        break;
      }
    }
    for (var i = 0; i < this.json.selected_option_deserts_others.length; i++) {
      var total_portion = 0;
      if (this.json.selected_option_deserts_others[i].portion > 0) {
        total_passed++;
        total_count++;
        break;
      }
    }
    for (var i = 0; i < this.json.selected_option_fruits_others.length; i++) {
      if (this.json.selected_option_fruits_others[i].portion > 0) {
        total_passed++;
        total_count++;
        break;
      }
    }
    console.log(total_count);
    console.log('-');
    console.log(total_passed);
    return total_count == total_passed;
  }

  selectOtherPortionMain(key, portion) {
    this.json.selected_option_others[key].portion = portion;
    console.log(this.json);
    this.checkIfAllSelected();
    this.proceed = this.checkIfAllSelected();
  }

  selectOtherDrinkPortion(key, portion) {
    this.json.selected_option_drinks_others[key].portion = portion;
    console.log(this.json);
    this.checkIfAllSelected();
    this.proceed = this.checkIfAllSelected();
  }

  selectOtherDesertPortion(key, portion) {
    this.json.selected_option_deserts_others[key].portion = portion;
    console.log(this.json);
    this.checkIfAllSelected();
    this.proceed = this.checkIfAllSelected();
  }

  selectOtherFruitPortion(key, portion) {
    this.json.selected_option_fruits_others[key].portion = portion;
    console.log(this.json);
    this.checkIfAllSelected();
    this.proceed = this.checkIfAllSelected();
  }

  selectActivity(item) {
    console.log(item);
    this.nothing_selected = false;
    for (var i = 0; i < this.selected_desert_option.length; i++) {
      if (this.selected_desert_option[i].id == item.id) {
        this.selected_desert_option.splice(i, 1);
        return;
      }
    }
    this.selected_desert_option.push(item);
    this.proceed = true;
    console.log(this.selected_desert_option);
  }

  proceedNext() {
    if (this.proceed) {
      var selectedOption = '';

      let params = {
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };
      this.json.next_page = '/page-food-record-summary';
      params['id'] = this.activity.id;
      this._apiService.updateSpecificActivity(params, () => {
        this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id, update: this.updateActivity } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }


}
