import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainPortionComponent } from './page-food-record-main-portion.component';

describe('PageFoodRecordMainPortionComponent', () => {
  let component: PageFoodRecordMainPortionComponent;
  let fixture: ComponentFixture<PageFoodRecordMainPortionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainPortionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainPortionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
