import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRewardSeedComponent } from './page-reward-seed.component';

describe('PageRewardSeedComponent', () => {
  let component: PageRewardSeedComponent;
  let fixture: ComponentFixture<PageRewardSeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRewardSeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRewardSeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
