import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRewardsWateringCanComponent } from './page-rewards-watering-can.component';

describe('PageRewardsWateringCanComponent', () => {
  let component: PageRewardsWateringCanComponent;
  let fixture: ComponentFixture<PageRewardsWateringCanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRewardsWateringCanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRewardsWateringCanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
