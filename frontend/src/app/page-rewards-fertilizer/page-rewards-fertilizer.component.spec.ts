import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRewardsFertilizerComponent } from './page-rewards-fertilizer.component';

describe('PageRewardsFertilizerComponent', () => {
  let component: PageRewardsFertilizerComponent;
  let fixture: ComponentFixture<PageRewardsFertilizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRewardsFertilizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRewardsFertilizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
