import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordSummaryComponent } from './page-food-record-summary.component';

describe('PageFoodRecordSummaryComponent', () => {
  let component: PageFoodRecordSummaryComponent;
  let fixture: ComponentFixture<PageFoodRecordSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
