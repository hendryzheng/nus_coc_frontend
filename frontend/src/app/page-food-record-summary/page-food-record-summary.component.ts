import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';


@Component({
  selector: 'app-page-food-record-summary',
  templateUrl: './page-food-record-summary.component.html',
  styleUrls: ['./page-food-record-summary.component.scss']
})
export class PageFoodRecordSummaryComponent implements OnInit {

  kid_account: any = null;
  character_type: any = '';
  daily_log: any = [];
  proceed: boolean = false;
  nothing_selected: boolean = false;
  image_type: any = 'png';
  showMenu: boolean = false;
  updateActivity: boolean = false;

  deleteDrinksItem: any = [];
  deleteFruitsItem: any = [];
  deleteDesertsItem: any = [];
  deleteMainItem: any = [];

  deleteMainOthersItem: any = [];
  deleteDrinkOthersItem: any = [];
  deleteFruitOthersItem: any = [];
  deleteDesertOthersItem: any = [];

  selected_options: any = [];
  selected_items: any = [];
  activity: any = [];
  json: any = [];

  photo: any = '';

  isDualBefore: boolean = false;
  dualActivity: any = '';


  constructor(private _commonService: CommonService,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (typeof params.update !== 'undefined') {
          if (params.update == 'true')
            this.updateActivity = true;
        }
        if (typeof params.edit !== 'undefined') {
          if (params.edit == 'true') {
            this.updateActivity = true;
          }
        }
        if (typeof params.id !== 'undefined') {
          let param = {
            id: params.id
          };
          this._apiService.getActivity(param, () => {
            this.activity = this._apiService.activity;
            this.json = JSON.parse(this.activity.json);
            console.log(this.json);


            console.log(this.selected_options);
          });
        }
      });

    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      this.daily_log = this.kid_account.daily_log;
      if (typeof this.kid_account.root_activity !== 'undefined') {
        this.dualActivity = this.kid_account.root_activity.activity_type;
        this.isDualBefore = true;
      }
    });
  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  initData() {

  }

  addMainMeals() {
    this.router.navigate(['/page-food-record-main'], { queryParams: { id: this.activity.id } })
  }

  addDrinks() {
    this.router.navigate(['/page-food-record-main-drinks'], { queryParams: { id: this.activity.id } })
  }

  addFruits() {
    this.router.navigate(['/page-food-record-main-fruits'], { queryParams: { id: this.activity.id } })
  }

  addDesserts() {
    this.router.navigate(['/page-food-record-main-deserts'], { queryParams: { id: this.activity.id } })
  }


  delete() {
    console.log(this.deleteDrinksItem);
    if (confirm('Are you sure to delete these records?')) {
      for (var j = 0; j < this.deleteDrinksItem.length; j++) {
        for(var i=0; i<this.json.selected_drinks_option.length; i++){
          if(this.json.selected_drinks_option[i].id == this.deleteDrinksItem[j].id){
            this.json.selected_drinks_option.splice(i,1);
            break;
          }
        }
      }
      this.deleteDrinksItem = [];
      for (var j = 0; j < this.deleteMainItem.length; j++) {
        for(var i=0; i<this.json.selected_option.length; i++){
          if(this.json.selected_option[i].id == this.deleteMainItem[j].id){
            this.json.selected_option.splice(i,1);
            break;
          }
        }
      }
      this.deleteMainItem = [];
      for (var j = 0; j < this.deleteFruitsItem.length; j++) {
        for(var i=0; i<this.json.selected_fruits_option.length; i++){
          if(this.json.selected_fruits_option[i].id == this.deleteFruitsItem[j].id){
            this.json.selected_fruits_option.splice(i,1);
            break;
          }
        }
      }
      this.deleteFruitsItem = [];
      for (var j = 0; j < this.deleteDesertsItem.length; j++) {
        for(var i=0; i<this.json.selected_desert_option.length; i++){
          if(this.json.selected_desert_option[i].id == this.deleteDesertsItem[j].id){
            this.json.selected_desert_option.splice(i,1);
            break;
          }
        }
      }
      this.deleteDesertsItem = [];
      for (var j = 0; j < this.deleteMainOthersItem.length; j++) {
        for(var i=0; i<this.json.selected_option_others.length; i++){
          if(this.json.selected_option_others[i].name == this.deleteMainOthersItem[j].name){
            this.json.selected_option_others.splice(i,1);
            break;
          }
        }
      }
      this.deleteMainOthersItem = [];
      for (var j = 0; j < this.deleteDrinkOthersItem.length; j++) {
        for(var i=0; i<this.json.selected_option_drinks_others.length; i++){
          if(this.json.selected_option_drinks_others[i].name == this.deleteDrinkOthersItem[j].name){
            this.json.selected_option_drinks_others.splice(i,1);
            break;
          }
        }
      }
      this.deleteDrinkOthersItem = [];
      for (var j = 0; j < this.deleteFruitOthersItem.length; j++) {
        for(var i=0; i<this.json.selected_option_fruits_others.length; i++){
          if(this.json.selected_option_fruits_others[i].name == this.deleteFruitOthersItem[j].name){
            this.json.selected_option_fruits_others.splice(i,1);
            break;
          }
        }
      }
      this.deleteFruitOthersItem = [];
      for (var j = 0; j < this.deleteDesertOthersItem.length; j++) {
        for(var i=0; i<this.json.selected_option_deserts_others.length; i++){
          if(this.json.selected_option_deserts_others[i].name == this.deleteDesertOthersItem[j].name){
            this.json.selected_option_deserts_others.splice(i,1);
            break;
          }
        }
      }
      this.deleteDesertOthersItem = [];
    }
  }

  selectDrinksOption(item) {
    for (var j = 0; j < this.deleteDrinksItem.length; j++) {
      if (this.deleteDrinksItem[j].id == item.id) {
        this.deleteDrinksItem.splice(j, 1);
        return;
      }
    }
    this.deleteDrinksItem.push(item);

  }

  selectMainMealOption(item) {
    for (var j = 0; j < this.deleteMainItem.length; j++) {
      if (this.deleteMainItem[j].id == item.id) {
        this.deleteMainItem.splice(j, 1);
        return;
      }
    }
    this.deleteMainItem.push(item);
  }

  selectFruitsOption(item) {
    for (var j = 0; j < this.deleteFruitsItem.length; j++) {
      if (this.deleteFruitsItem[j].id == item.id) {
        this.deleteFruitsItem.splice(j, 1);
        return;
      }
    }
    this.deleteFruitsItem.push(item);
  }

  selectDesertsOption(item) {
    for (var j = 0; j < this.deleteDesertsItem.length; j++) {
      if (this.deleteDesertsItem[j] == item.id) {
        this.deleteDesertsItem.splice(j, 1);
        return;
      }
    }
    this.deleteDesertsItem.push(item);
  }

  selectOtherMainOption(item) {
    for (var j = 0; j < this.deleteMainOthersItem.length; j++) {
      if (this.deleteMainOthersItem[j].name == item.name) {
        this.deleteMainOthersItem.splice(j, 1);
        return;
      }
    }
    this.deleteMainOthersItem.push(item);
  }

  selectOtherDrinkOption(item) {
    for (var j = 0; j < this.deleteDrinkOthersItem.length; j++) {
      if (this.deleteDrinkOthersItem[j].name == item.name) {
        this.deleteDrinkOthersItem.splice(j, 1);
        return;
      }
    }
    this.deleteDrinkOthersItem.push(item);
  }

  selectOtherFruitOption(item) {
    for (var j = 0; j < this.deleteFruitOthersItem.length; j++) {
      if (this.deleteFruitOthersItem[j].name == item.name) {
        this.deleteFruitOthersItem.splice(j, 1);
        return;
      }
    }
    this.deleteFruitOthersItem.push(item);
  }

  selectOtherDesertOption(item) {
    for (var j = 0; j < this.deleteDesertOthersItem.length; j++) {
      if (this.deleteDesertOthersItem[j].name == item.name) {
        this.deleteDesertOthersItem.splice(j, 1);
        return;
      }
    }
    this.deleteDesertOthersItem.push(item);
  }

  isInArraySelectedOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteMainItem.length; j++) {
      if (this.deleteMainItem[j].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedDrinksOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteDrinksItem.length; j++) {
      if (this.deleteDrinksItem[j].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedFruitsOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteFruitsItem.length; j++) {
      if (this.deleteFruitsItem[j].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedDesertsOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteDesertsItem.length; j++) {
      if (this.deleteDesertsItem[j].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedOthersOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteMainOthersItem.length; j++) {
      if (this.deleteMainOthersItem[j].name == item.name) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedDrinkOthersOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteDrinkOthersItem.length; j++) {
      if (this.deleteDrinkOthersItem[j].name == item.name) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedFruitOthersOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteFruitOthersItem.length; j++) {
      if (this.deleteFruitOthersItem[j].name == item.name) {
        res = true;
      }
    }
    return res;
  }

  isInArraySelectedDesertOthersOption(item) {
    var res = false;
    for (var j = 0; j < this.deleteDesertOthersItem.length; j++) {
      if (this.deleteDesertOthersItem[j].name == item.name) {
        res = true;
      }
    }
    return res;
  }

  isInArray(item) {
    var res = false;
    for (var i = 0; i < this.selected_items.length; i++) {
      if (this.selected_items[i].id == item.id) {
        res = true;
      }
    }
    return res;
  }

  selectActivity(item) {
    console.log(item);
  }

  goBack() {
    this.router.navigate(['/page-food-record-main-portion'], { queryParams: { id: this.activity.id, edit: true } });
  }

  goBackMain() {
    this.router.navigate(['/page-food-record-main'], { queryParams: { id: this.activity.id, edit: true } });
  }

  proceedNext() {
    // if (this.proceed) {
    var selectedOption = '';
    // this.json = JSON.parse(this.activity.json);
    var selected_display = '';
    for (var i = 0; i < this.json.selected_option.length; i++) {
      selected_display += this.json.selected_option[i].food_name + '\n';
    }
    for (var i = 0; i < this.json.selected_drinks_option.length; i++) {
      selected_display += this.json.selected_drinks_option[i].food_name + '\n';
    }
    for (var i = 0; i < this.json.selected_fruits_option.length; i++) {
      selected_display += this.json.selected_fruits_option[i].food_name + '\n';
    }
    for (var i = 0; i < this.json.selected_desert_option.length; i++) {
      selected_display += this.json.selected_desert_option[i].food_name + '\n';
    }
    for (var i = 0; i < this.json.selected_option_others.length; i++) {
      selected_display += this.json.selected_option_others[i].name + '\n';
    }
    if(typeof this.json.selected_option_drinks_others !== 'undefined'){
      for (var i = 0; i < this.json.selected_option_drinks_others.length; i++) {
        selected_display += this.json.selected_option_drinks_others[i].name + '\n';
      }
    }
    if(typeof this.json.selected_option_fruits_others !== 'undefined'){
      for (var i = 0; i < this.json.selected_option_fruits_others.length; i++) {
        selected_display += this.json.selected_option_fruits_others[i].name + '\n';
      }
    }
    if(typeof this.json.selected_option_deserts_others !== 'undefined'){
      for (var i = 0; i < this.json.selected_option_deserts_others.length; i++) {
        selected_display += this.json.selected_option_deserts_others[i].name + '\n';
      }
    }
    
    
    

    let params = {
      'id': this.activity.id,
      'kid_id': this.kid_account.id,
      'Activities[daily_log_id]': this.kid_account.daily_log.id,
      'Activities[user_kid_id]': this.kid_account.id,
      'Activities[complete]': 0,
      'Activities[selected_activity_display]': selected_display,
      'Activities[json]': JSON.stringify(this.json)
    };
    if (this.isDualBefore) {
      // this.json.next_page = '/page-activity-diary-indoors-outdoors-sitting';
      if (this.dualActivity == 'Travelling') {
        if (this.updateActivity) {
          this.json.next_page = '/page-activity-summary';
          params['Activities[complete]'] = 1;
        } else {
          this.json.next_page = '/page-food-record-duration';
        }
      } else {
        this.json.next_page = '/page-food-record-location';
      }
    } else {
      if (this.updateActivity) {
        this.json.next_page = '/page-food-record-location';
      } else {
        this.json.next_page = '/page-activity-diary-dual-eating';
      }
    }
    this._apiService.updateSpecificActivity(params, () => {
      this.router.navigate([this.json.next_page], { queryParams: { id: this.activity.id, update: this.updateActivity } });
    });
    // }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }
}
