import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFoodRecordMainFruitsComponent } from './page-food-record-main-fruits.component';

describe('PageFoodRecordMainFruitsComponent', () => {
  let component: PageFoodRecordMainFruitsComponent;
  let fixture: ComponentFixture<PageFoodRecordMainFruitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFoodRecordMainFruitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFoodRecordMainFruitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
