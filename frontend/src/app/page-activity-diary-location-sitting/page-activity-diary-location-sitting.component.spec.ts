import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryLocationSittingComponent } from './page-activity-diary-location-sitting.component';

describe('PageActivityDiaryLocationSittingComponent', () => {
  let component: PageActivityDiaryLocationSittingComponent;
  let fixture: ComponentFixture<PageActivityDiaryLocationSittingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryLocationSittingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryLocationSittingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
