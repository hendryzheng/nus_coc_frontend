import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-home-day',
  templateUrl: './page-home-day.component.html',
  styleUrls: ['./page-home-day.component.scss']
})
export class PageHomeDayComponent implements OnInit {

  kid_account: any = null;
  day_status: number = 1;
  day_name: any = 'first';
  character_type: any = '';
  image_type: any = 'png';
  new: boolean = true;
  initial: boolean = false;

  photo: any = '';

  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {

    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.day_status = parseInt(this.kid_account.day_status);
      if(this.day_status == 1){
        this.day_name = 'first';
      }else if (this.day_status == 2) {
        this.day_name = 'second';
      } else if (this.day_status == 3) {
        this.day_name = 'third';
      } else if(this.day_status == 4){
        this.day_name = 'fourth';
      }else{
        this.router.navigate(['/page-home-done']);
      }
      this.character_type = this.kid_account.character_type;
      if (this.kid_account.daily_log !== false) {
        this.new = false;
      }
      if (this.kid_account.birthday == null || this.kid_account.last_completed_activity == null) {
        this.initial = true;
      }
      setTimeout(() => {
        this.playVoiceOver();
      }, 1000);
    });

  }

  next() {
    if (this.kid_account.birthday !== null && this.kid_account.day_status <=4) {
      if (typeof this.kid_account.daily_log !== 'undefined' && this.kid_account.daily_log !== false) {
        if (this.kid_account.daily_log.sleeping_done === '0' || this.kid_account.daily_log.wakeup_done === '0') {
          this.router.navigate(['/page-record-day']);
        } else {
          let time = this.kid_account.daily_log.time_wakeup_hr1.toString() + this.kid_account.daily_log.time_wakeup_hr2.toString() + ':' + this.kid_account.daily_log.time_wakeup_minute1.toString() + this.kid_account.daily_log.time_wakeup_minute2.toString() + ' ' + this.kid_account.daily_log.time_wakeup_am;
          let param_display = this.kid_account.param_display;
          let start_date_time =
            this.router.navigate(['/page-activity-diary']);
        }
      } else {
        this.router.navigate(['/page-record-day']);
      }

    } else {
      this.router.navigate(['/page-about-yourself']);
    }
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/']);
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  reviewSummary(){
    if(!this.initial){
      this.router.navigate(['/page-activity-summary']);
    }
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      // if (this.new) {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_21);
      // } else {
      //   this._commonService.audio = new Audio(this._commonService.audioList.audio_25);
      // }
      this._commonService.audio.play();

      this._commonService.audio.onended = () => {

        if (this.new) {
          if (this.day_status == 1) {
            this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
          } else if (this.day_status == 2) {
            this._commonService.audio = new Audio(this._commonService.audioList.audio_23);
          } else if (this.day_status == 3) {
            this._commonService.audio = new Audio(this._commonService.audioList.audio_24);
          } else {
            this._commonService.audio = new Audio(this._commonService.audioList.audio_45);
          }
        } else {
          this._commonService.audio = new Audio(this._commonService.audioList.audio_26);
        }

        this._commonService.audio.play();

        this._commonService.audio.onended = () => {
          this.image_type = 'png';
        };
      };
    }
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
