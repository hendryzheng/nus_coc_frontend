import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeDayComponent } from './page-home-day.component';

describe('PageHomeDayComponent', () => {
  let component: PageHomeDayComponent;
  let fixture: ComponentFixture<PageHomeDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
