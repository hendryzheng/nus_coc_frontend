import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageActivityDiaryDualActiveComponent } from './page-activity-diary-dual-active.component';

describe('PageActivityDiaryDualActiveComponent', () => {
  let component: PageActivityDiaryDualActiveComponent;
  let fixture: ComponentFixture<PageActivityDiaryDualActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageActivityDiaryDualActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageActivityDiaryDualActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
