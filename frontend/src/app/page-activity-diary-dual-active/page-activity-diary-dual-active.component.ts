import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { SubmissionApiService } from '../services/submission-api.service';

@Component({
  selector: 'app-page-activity-diary-dual-active',
  templateUrl: './page-activity-diary-dual-active.component.html',
  styleUrls: ['./page-activity-diary-dual-active.component.scss']
})
export class PageActivityDiaryDualActiveComponent implements OnInit {


  kid_account: any = null;
  character_type: any = '';

  daily_log: any = [];

  proceed: boolean = false;
  selected_activity: any = '-';


  image_type: any = 'png';
  showMinute: boolean = false;
  showHour: boolean = true;
  json: any = [];

  photo: any = '';


  constructor(private _commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private _httpService: HttpService,
    private _apiService: SubmissionApiService,
    private router: Router) { }

  ngOnInit() {
    let photo = localStorage.getItem('bg_photo');
    if(photo){
      this.photo = photo;
    }
    
    setTimeout(() => {
      this.playVoiceOver();
    }, 1000);

    let acc = localStorage.getItem('account');
    this.kid_account = JSON.parse(acc);
    let dailyLog = localStorage.getItem('daily_log');
    if (dailyLog) {
      this.daily_log = JSON.parse(dailyLog);
    }

    this._apiService.getKid(() => {
      this.kid_account = this._apiService.kid_account;
      this.character_type = this.kid_account.character_type;
      if (this.kid_account.last_incomplete_activity.json !== '') {
        this.json = JSON.parse(this.kid_account.last_incomplete_activity.json);
      }
    });
  }

  selectActivity(type) {
    console.log(type);
    this.selected_activity = type;
    this.proceed = true;
  }

  proceedNext() {
    if (this.proceed) {
      var next_page = '/page-activity-diary-duration-active';
      this.json.dual_selected_activity = this.selected_activity;

      let params = {
        'kid_id': this.kid_account.id,
        'Activities[daily_log_id]': this.kid_account.daily_log.id,
        'Activities[user_kid_id]': this.kid_account.id,
        'Activities[complete]': 0,
        'Activities[json]': JSON.stringify(this.json)
      };
      if (this.selected_activity === 'Nothing') {
        next_page = '/page-activity-diary-duration-active';
      } else {
        params['activity_id'] = this.kid_account.last_incomplete_activity.id;
        params['dual'] = 'true';
        params['dual_activity'] = this.selected_activity;
        params['daily_log_id'] = this.kid_account.daily_log.id;
        params['user_kid_id'] = this.kid_account.id;

        if (this.selected_activity === 'Eat and Drink') {
          next_page = '/page-food-record-main';
        } else if (this.selected_activity === 'Sitting Activities') {
          next_page = '/page-activity-diary-sitting-options'
        } else if (this.selected_activity === 'Travelling') {
          next_page = '/page-activity-diary-travelling-options';
        }
      }

      this._apiService.updateActivity(params, () => {
        this.router.navigate([next_page], { queryParams: { param: '' } });
      });
    }
  }

  isSoundOn() {
    return this._commonService.soundActive;
  }

  playVoiceOver() {
    if (this._commonService.soundActive) {
      if (typeof this._commonService.audio !== 'undefined') {
        this._commonService.audio.pause();
      }
      this.image_type = 'gif';
      if (this.kid_account.daily_log.day_status === '1') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_33);
        this._commonService.audio.play();
      } else if (this.kid_account.day_status === '2') {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_34);
        this._commonService.audio.play();
      } else {
        this._commonService.audio = new Audio(this._commonService.audioList.audio_35);
        this._commonService.audio.play();
      }
      this._commonService.audio.onended = () => {
        this.image_type = 'png';
      };
    }


    // this._commonService.audio.onended = () => {
    //   this._commonService.audio = new Audio(this._commonService.audioList.audio_22);
    //   this._commonService.audio.play();
    // };
  }

  toggleSound() {
    this._commonService.soundActive = !this._commonService.soundActive;
    if (!this._commonService.soundActive) {
      this._commonService.audio.pause();
      // this._commonService.audio = null;
      this.image_type = 'png';
    } else {
      this.playVoiceOver();
    }
  }

}
