--Database--
1. Makesure to install mysql and phpmyadmin
2. Makesure php5.6 is installed
3. Create database named "nus_coc"
4. Import nus_coc.sql via phpmyadmin


--Backend Source Code--
1. Copy and extract backend.zip entirely to workable web server directory. E.g: /var/www/html
2. Copy and extract yii.zip to the same directory with #1
3. Open protected/config/back.php
   - Find section 'db'
   - Replace db_name with the db_name of your creation
   - Replace username and password with the username and password of your mysql user
4. Make sure assets folder and all its subfolders are in 755 (read write by webserver)
5. Make sure protected/runtime and all its subfolders are in 755 (read write by webserver)
