$(document).ready(function(){
	if($('.filterTable').length > 0 && $('td.empty').length < 1){
		$('.filterTable').DataTable({
	          "responsive":true
	        });	
	}
	$('select:not(.select2)').selectpicker({
		liveSearch: true
	});
	$('.datepicker').datepicker({format: 'yyyy-mm-dd'});
});