//ko.applyBindings({
//        email: ko.observable(""),        // Initially blank
//        password: ko.observable("")  // Prepopulate
//    });
    
var viewModel = {
		login: function(){
			var form = {
				'LoginForm[username]':viewModel.loginUsername(),
				'LoginForm[password]': viewModel.loginPassword()
			};
			
			var formPass = true;
	    	if(form['LoginForm[username]'] === '')
	    		formPass = false;
	    	if(form['LoginForm[password]'] === '')
	    		formPass = false;
	    	
			if(formPass){
	    		$.ajax({
	                type: "POST",
	                url: site_url+'/site/login',
	                dataType: "json",
	                data: form,
	                cache : false,
	                success: function(response) {
	                       if(response.status == 'OK'){
	                    	  location.reload();
	                    	  viewModel.loginErrorMessage('');
	                       }else{
	                    	  viewModel.loginErrorMessage(response.message);
	                       }
	                },

	                error: function(jqXHR, textStatus, errorThrown){
	                       // alert('HTTP error occured!' + errorThrown);
	                       //alert ("Something went wrong, please try again later."+errorThrown);
//	                       $('#loading_image').css('display','none');
	                }
	    		});
	    	}else{
	    		viewModel.loginErrorMessage('All entries are mandatory');
	    	}
		},
		register : function(formElement) {
	    	
			var form = {
	    		'Customer[firstname]': viewModel.firstname(),
	    		'Customer[lastname]' : viewModel.lastname(),
	    		'Customer[contact]': viewModel.contact(),
	    		'Customer[address]': viewModel.address(),
	    		'Customer[postal_code]': viewModel.postalCode(),
	    		'Customer[username]': viewModel.username(),
	    		'Customer[email]': viewModel.email(),
	    		'Customer[password]': viewModel.password(),
	    		'Customer[verifyPassword]': viewModel.verifyPassword()
	    	};
	    	
	    	var formPass = true;
	    	if(form['Customer[firstname]'] === '')
	    		formPass = false;
	    	if(form['Customer[lastname]'] === '')
	    		formPass = false;
	    	if(form['Customer[contact]'] === '')
	    		formPass = false;
	    	if(form['Customer[address]'] === '')
	    		formPass = false;
	    	if(form['Customer[postal]'] === '')
	    		formPass = false;
	    	if(form['Customer[username]'] === '')
	    		formPass = false;
	    	if(form['Customer[email]'] === '')
	    		formPass = false;
	    	if(form['Customer[password]'] === '')
	    		formPass = false;
	    	if(form['Customer[verifyPassword]'] === '')
	    		formPass = false;
	    	
	    	
	    	if(formPass){
	    		$.ajax({
	                type: "POST",
	                url: site_url+'/site/register',
	                dataType: "json",
	                data: form,
	                cache : false,
	                success: function(response) {
	                       if(response.status == 'OK'){
	                    	  location.reload();
	                    	  viewModel.errorMessage('');
	                       }else{
	                    	  viewModel.errorMessage(response.message);
	                       }
	                },

	                error: function(jqXHR, textStatus, errorThrown){
	                       // alert('HTTP error occured!' + errorThrown);
	                       //alert ("Something went wrong, please try again later."+errorThrown);
//	                       $('#loading_image').css('display','none');
	                }
	    		});
	    	}else{
	    		viewModel.errorMessage('All entries are mandatory');
	    	}
	    	

	    },
	    firstname: ko.observable(""),
	    lastname: ko.observable(""),
	    contact: ko.observable(""),
	    address: ko.observable(""),
	    postalCode: ko.observable(""),
	    username: ko.observable(""),
        email: ko.observable(""),        // Initially blank
        password: ko.observable(""),  // Prepopulate
        verifyPassword: ko.observable(""),
        errorMessage: ko.observable(""),
        loginErrorMessage: ko.observable(""),
        loginUsername: ko.observable(""),
        loginPassword: ko.observable(""),
        totalCart: ko.observable('0')
};
ko.applyBindings(viewModel);

$(document).ready(function(){
	$('#Purchase_delivery_option').change(function(){
		window.location.href = site_url+'/cart/confirm?delivery_option='+$(this).val();
	});
	$(".search-a").click(function(e) {
		var searchTerm = $('.search_keyword').val();
		if (searchTerm != "" && searchTerm != "SEARCH")
		{
			var $this = $(this);
			location.href=site_url+"/product/search?keyword=" + searchTerm;
		}
		else
		{
			e.preventDefault();
		}
	});
	
	$("#print").on("click", function(e) {
        e.preventDefault();
        var purchase_id = $("#purchase_id").val();
        var myWindow = window.open(site_url+"/cart/printReference/"+purchase_id,"Print Reference","width=600");
	});
//	$("#Purchase_delivery_option").change(function(){
//        var val = $(this).val();
//        if(val != ''){
//            $("#purchase-form").submit();
//        }
//   });
	$('.container').on('blur','.quantity-cart',function(){
		var prod_id = $(this).attr("data-id");
		var qty = $(this).val();
		$.ajax({
            type: "POST",
            url: site_url+'/product/updateCart',
            dataType: "json",
            data: {
            	'quantity': qty,
            	'product_id': prod_id
            },
            cache : false,
            success: function(response) {
               if(response.status == 'OK'){
            	  $('#cart-wrapper').html(response.data);
               }else{
//            	  viewModel.errorMessage(response.message);
               }
            },

            error: function(jqXHR, textStatus, errorThrown){
                   // alert('HTTP error occured!' + errorThrown);
                   //alert ("Something went wrong, please try again later."+errorThrown);
//                   $('#loading_image').css('display','none');
            }
		});
	});
	$('.container').on('click','.quantity-cart',function(){
		var prod_id = $(this).attr("data-id");
		var qty = $(this).val();
		$.ajax({
            type: "POST",
            url: site_url+'/product/updateCart',
            dataType: "json",
            data: {
            	'quantity': qty,
            	'product_id': prod_id
            },
            cache : false,
            success: function(response) {
               if(response.status == 'OK'){
            	  $('#cart-wrapper').html(response.data);
               }else{
//            	  viewModel.errorMessage(response.message);
               }
            },

            error: function(jqXHR, textStatus, errorThrown){
                   // alert('HTTP error occured!' + errorThrown);
                   //alert ("Something went wrong, please try again later."+errorThrown);
//                   $('#loading_image').css('display','none');
            }
		});
	});
	$('.container').on('click','.removecart',function(){
		var prod_id = $(this).attr("data-id");
		var qty = $(this).val();
		var title = $(this).attr("data-title");
		if(confirm('Remove '+title+' from cart?')){
			$.ajax({
	            type: "POST",
	            url: site_url+'/product/removeFromCart',
	            dataType: "json",
	            data: {
	            	'quantity': qty,
	            	'product_id': prod_id
	            },
	            cache : false,
	            success: function(response) {
	               if(response.status == 'OK'){
	            	  $('#cart-wrapper').html(response.data);
	            	  $('.cartrow'+prod_id).remove();
	               }else{
//	            	  viewModel.errorMessage(response.message);
	               }
	            },

	            error: function(jqXHR, textStatus, errorThrown){
	                   // alert('HTTP error occured!' + errorThrown);
	                   //alert ("Something went wrong, please try again later."+errorThrown);
//	                   $('#loading_image').css('display','none');
	            }
			});
		}
	});
	$('.container').on('click','.addtocart',function(){
		var prod_id = $(this).attr("data-id");
		var qty = $('.qty'+prod_id).val();
		var title = $(this).attr("data-title");
		$.ajax({
            type: "POST",
            url: site_url+'/product/addToCart',
            dataType: "json",
            data: {
            	'quantity': qty,
            	'product_id': prod_id
            },
            cache : false,
            success: function(response) {
               if(response.status == 'OK'){
            	   $('#cart-wrapper').html(response.data);
            	   alert(title+' added to your cart');
               }else{
            	   alert(response.message);
            	   //viewModel.errorMessage(response.message);
               }
            },
            error: function(jqXHR, textStatus, errorThrown){
                   // alert('HTTP error occured!' + errorThrown);
                   //alert ("Something went wrong, please try again later."+errorThrown);
//                   $('#loading_image').css('display','none');
            }
		});
	});
	$('.container').on('click','.removewishlist',function(){
		var wishlist_id = $(this).attr("data-id");
		var title = $(this).attr("data-title");
		if(confirm('Remove '+title+' from cart?')){
			$.ajax({
	            type: "POST",
	            url: site_url+'/product/removeFromWishlist',
	            dataType: "json",
	            data: {
	            	'wishlist_id': wishlist_id
	            },
	            cache : false,
	            success: function(response) {
	               if(response.status == 'OK'){
	            	  $('#wishlist-wrapper').html(response.data);
	               }else{
//	            	  viewModel.errorMessage(response.message);
	               }
	            },
	            error: function(jqXHR, textStatus, errorThrown){
	                   // alert('HTTP error occured!' + errorThrown);
	                   //alert ("Something went wrong, please try again later."+errorThrown);
//	                   $('#loading_image').css('display','none');
	            }
			});
		}
	});
	$('.container').on('click','.addtowishlist',function(){
		var prod_id = $(this).attr("data-id");
		var title = $(this).attr("data-title");
		$.ajax({
            type: "POST",
            url: site_url+'/product/addToWishlist',
            dataType: "json",
            data: {
            	'product_id': prod_id
            },
            cache : false,
            success: function(response) {
               if(response.status == 'OK'){
            	   $('#wishlist-wrapper').html(response.data);
            	   alert(title+' added to your wishlist');
               }else{
            	   alert(response.message);
            	   //viewModel.errorMessage(response.message);
               }
            },
            error: function(jqXHR, textStatus, errorThrown){
                   // alert('HTTP error occured!' + errorThrown);
                   //alert ("Something went wrong, please try again later."+errorThrown);
//                   $('#loading_image').css('display','none');
            }
		});
	});
	
//	$('#sort_gd').change(function(){
//		var sort_gd = $("#sort_gd").val();
//		$.ajax({
//            type: "POST",
//            url: site_url+'/site/sortgooddeals',
//            dataType: "html",
//            data: {
//            	'sort_gd' : sort_gd
//            },
//            cache : false,
//            success: function(response) {
//            	$("#gooddeals-wrapper").html(response);
//            },
//            error: function(jqXHR, textStatus, errorThrown){
//                   // alert('HTTP error occured!' + errorThrown);
//                   //alert ("Something went wrong, please try again later."+errorThrown);
////                   $('#loading_image').css('display','none');
//            }
//		});
//	});
	
//    $('#sort_fp, #sort_gd').change(function(){
//		var sort_fp = $("#sort_fp").val();
//		$.ajax({
//            type: "POST",
//            url: site_url+'/site/sortfeaturedproduct',
//            dataType: "html",
//            data: {
//            	'sort_fp' : sort_fp
//            },
//            cache : false,
//            success: function(response) {
//            	$("#featuredproduct-wrapper").html(response);
//            },
//            error: function(jqXHR, textStatus, errorThrown){
//                   // alert('HTTP error occured!' + errorThrown);
//                   //alert ("Something went wrong, please try again later."+errorThrown);
////                   $('#loading_image').css('display','none');
//            }
//		});
//	});
    
    $('#sort_key, #sort_classification').change(function(){
		var sort_key = $("#sort_key").val();
		var sort_classification = $("#sort_classification").val();
		location.href = site_url+'/site/sort?by='+sort_key+"&cat="+sort_classification;
    });
    
    $('.search_keyword').keyup(function(){
    	var keyword = $('.search_keyword').val();
    	
    	document.onkeydown = function(e){
    		if(e.keyCode == 13){
    			$(".search-a").click();
        	}
    	}
    	
    	if(keyword != "" && keyword != "SEARCH"){
    		$('.search-suggestion').css("display", "block");
    		$.ajax({
                type: "POST",
                url: site_url+'/site/search',
                dataType: "html",
                data: {
                	'keyword': keyword
                },
                cache : false,
                success: function(response) {
                	$('.search-suggestion').html(response);
                },
                error: function(jqXHR, textStatus, errorThrown){
                       // alert('HTTP error occured!' + errorThrown);
                       //alert ("Something went wrong, please try again later."+errorThrown);
//                       $('#loading_image').css('display','none');
                }
    		});
    	}else{
    		$('.search-suggestion').css("display", "none");
    	}
	});
    
    $('#wishlist-wrapper').on('click', function(){
    	$('#account-wrapper').removeClass('open');
    	$('#wishlist-wrapper').parent().addClass('open');
    	$('#cart-wrapper').removeClass('open');
    });
    
    $('#cart-wrapper').on('click', function(){
    	$('#account-wrapper').removeClass('open');
    	$('#wishlist-wrapper').removeClass('open');
        $('#cart-wrapper').parent().addClass('open');
    });
    
    $('#account-wrapper').on('click', function(){
    	$('#account-wrapper').parent().addClass('open');
    	$('#wishlist-wrapper').removeClass('open');
        $('#cart-wrapper').removeClass('open');
    });
});

jQuery(document).ready(function($){
	$("#toggleInstruction").click(function(){
		$('.toggleShow').toggleClass('hide');
	});
	$('#myTab').tabCollapse();	

    $('#myCarousel').carousel({
        interval: 5000
    });
 
    $('#carousel-text').html($('#slide-content-0').html());
 
    //Handles the carousel thumbnails
    $('[id^=carousel-selector-]').click( function(){
        var id = this.id.substr(this.id.lastIndexOf("-") + 1);
        var id = parseInt(id);
        $('#myCarousel').carousel(id);
    });
 
    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
             var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
    });
        	
    $('#methods').on('change', function(el) {
        $('#password_register').attr('type', this.checked ? 'text' : 'password');
        //document.getElementById('exampleInputPassword1').type = this.checked ? 'text' : 'password';
    });

    $('.container').on('click','li.dropdown.mega-dropdown a', function (event) {
        $(this).parent().toggleClass("open");
    });
    
    $('body').on('click', function (e) {
        if (!$('li.dropdown.mega-dropdown').is(e.target) && $('li.dropdown.mega-dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
            $('li.dropdown.mega-dropdown').removeClass('open');
        }
    });
    //open popup
    $('.cd-popup-trigger').on('click', function(event){
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
        setTimeout(function(){
            $('.cd-popup').removeClass('is-visible');
        }, 2000)
    });
    //close popup
    $('.cd-popup').on('click', function(event){
        if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.cd-popup').removeClass('is-visible');
        }
    }); 
});