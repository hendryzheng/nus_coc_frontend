String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g, "") ;
}


String.prototype.pad_left = function(len, pad) {
 	var str = this;
 	
	if (typeof(len) == "undefined") {var len = 0;}
	if (typeof(pad) == "undefined") {var pad = ' ';}
	
	if (len + 1 >= str.length) {
 		str = Array(len + 1 - str.length).join(pad) + str;
	}
 
	return str;
 
}


String.prototype.pad_right = function(len, pad) {
 	var str = this;
 	
	if (typeof(len) == "undefined") {var len = 0;}
	if (typeof(pad) == "undefined") {var pad = ' ';}
	
	if (len + 1 >= str.length) {
 		str = str + Array(len + 1 - str.length).join(pad);
	}
 
	return str;
 
}


String.prototype.to_date = function(){
	var d_arr = this.split('/');
	var d = new Date(d_arr[2], d_arr[1] - 1, d_arr[0]);
	
	return d;
}


String.prototype.is_number = function(){
	var n = this.trim();
	return !isNaN(parseFloat(n)) && isFinite(n);
}


Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.getRealMonth = function(){
	return this.getMonth() + 1;
};

Date.prototype.getMonthName = function() {
    return this.monthNames[this.getMonth()];
};

Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};


var api = {
	jq_timer : null,
	
	jq_call : function(jq, p_callback, p_start){
		
		var height = $(document).height();
		var height2 = $(window).height();
		var width = $(window).width();
		var p = $(window).scrollTop();
		
                if(jq.refresh != "true")
		
		$('#loading_image').fadeTo("slow", 0.6);	

		var id = '#loading_image';
		$(id).css({'top':((height2-$(id).height())/2), 'left':((width-$(id).width())/2), 'display':'block'});
		

              var json = null;
		api.jq_destroy_message();
		
		if(typeof p_start === 'function'){
			p_start();
		}
		
		$.ajax({
			 type: jq.method,
			 url: jq.url,
			 dataType: "json",
			 data: jq.postdata,
			 cache : false,
			 success: function(response) {
				if(typeof p_callback === 'function'){
                                        if(jq.refresh != "true")

					json = response
				}
			 },
			 
			 error: function(jqXHR, textStatus, errorThrown){
				// alert('HTTP error occured!' + errorThrown);
                                //alert ("Something went wrong, please try again later."+errorThrown);
				$('#loading_image').css('display','none');
			 }
		}).done(function(){
                    p_callback(json);
                });
	},
	
	
	jq_message : function(msg, wait_secs){
		if(this.jq_timer != null){
			clearTimeout(this.jq_timer);
		}
		
		
		api.jq_destroy_message();
		
		var div = $('<div class="jq-message">' + msg + '</div>');
		div.click(function(event){
			$(this).fadeOut(800);
		});
		
		
		div.hide().appendTo('body').fadeIn(800);
		
		if(wait_secs > 0){
			this.jq_timer = setTimeout(api.jq_destroy_message, (wait_secs * 1000));
		} 
	},
	
	jq_destroy_message : function(){
		$('.jq-message').fadeOut(500);
	},
	
	month_selector : function(class_name, data, cb){
		 function prev_month(selector, data, cb){
			var d = selector.find('.select-date').val();
			var m = selector.find('.select-month').val();
			var y = selector.find('.select-year').val();
			
			m = m - 1;	// javascript month is zero based 
			var m2 = m - 1;
			
			if(m2 < 0){
				m2 = 11;
				y = y - 1;
			}
			
			var dt = new Date(y, m2, d);
			selector.find('.select-year').val(dt.getFullYear());
			selector.find('.select-month').val(dt.getRealMonth());
			
			if(typeof cb === 'function'){
				cb(selector, dt, data);
			}
		}
		
		
		function next_month(selector, data, cb){
			var d = selector.find('.select-date').val();
			var m = selector.find('.select-month').val();
			var y = selector.find('.select-year').val();
			
			m = m - 1;	// javascript month is zero based 
			var m2 = m + 1;
			
			var dt = new Date(y, m2, d);
			selector.find('.select-year').val(dt.getFullYear());
			selector.find('.select-month').val(dt.getRealMonth());
			
			if(typeof cb === 'function'){
				cb(selector, dt, data);
			}
		}
		
		function input_change(selector, data, cb){
			var d = selector.find('.select-date').val();
			var m = selector.find('.select-month').val();
			var y = selector.find('.select-year').val();
			
			m = m - 1;	// javascript month is zero based 
			
			var dt = new Date(y, m, d);
			
			if(typeof cb === 'function'){
				cb(selector, dt, data);
			}
		}
		
		
		$('.' + class_name).each(function(index, elmt){
			var selector = $(this);
			var prev = selector.find('a.prev');
			var next = selector.find('a.next');
			
			prev.bind('click', function(){prev_month(selector, data, cb)});
			next.bind('click', function(){next_month(selector, data, cb)});
			
			var month_sel = selector.find('.select-month');
			month_sel.bind('change', function(){input_change(selector, data, cb);});
			
			var year_input = selector.find('.select-year');
			year_input.bind('blur', function(){input_change(selector, data, cb);});
		});
	
	}
}

var callLoading = {
	showLoading : function(){
           $("#loading_div").fadeIn();
           $(".product_table_wrapper").css("opacity","0.1");
        },
        hideLoading : function(){
//            /$('#loading_icon').hide('fast');    
            $("#loading_div").hide();
            $(".product_table_wrapper").css("opacity","1");
           
        }
};

